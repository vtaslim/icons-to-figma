import utils from './utils';
import * as Food from './food';
import * as Functions from './functions';
import * as Illustrations from './illustrations';
import * as Logos from './logos';

const Svgs = {
	Functions,
	Food,
	Illustrations,
	Logos,
}

const {
	sizes,
	colors,
	themes,
	generateThemeProps,
	getOneFillColor,
	getTwoFillsColor,
	getCustomColor,
	getCommonVariants
} = utils;

export {
	Svgs,
	sizes,
	colors,
	themes,
	generateThemeProps,
	getOneFillColor,
	getTwoFillsColor,
	getCustomColor,
	getCommonVariants
};