// These should be all 24x24 icons from Sketch. With padding.
// Would be helpful to set default overides per icon
// Test transparency
// Some icons are rendered bad due to Illustrator's decimal place - redo them in Figma
// Add rotate prop
// Revist some icons and make white fills an option to align with others?
// Need opacity fill color eqivalent on some products, as can't do opacity. Maybe thi is manual
// Other themes (based on extra colors examples from Audrey)
// Check all white fills
// Add extra icons from differnt sizes
// Icons with animation frames needs work. Lottie?
// Show/hide padding. Instead of doing new designs for 30px...?
// PLACE IN FRAMER CODE componets (merge SvgIcon and SVGIcons)?
// Foundation/Product/Web/Mobile
// Resonsive option prop

// export type Theme = {
// 	primary: string;
// 	secondary: string;
// 	tertiary: string;
// 	svgIconSize: string;
//   };

// Order icon A-Z in this code
// Order icon A-Z in Framer dropdown
// In time, some icons design need cleaning up - probably a illsutrator decimal index issue, so do in Figma

import React from "react";
import {
	themes, 
	colors, 
	generateThemeProps,
	generateOneFillTheme,
	generateTwoFillsTheme,
	generateCustomTheme,
} from "./utils"

const XMLNS = "http://www.w3.org/2000/svg";

// Example
// export const ExampleSvg = ({ primary, tertiary, size, colorList }) => (
// 	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
// 		<path fill={colorList && colorList.color1 || primary || "#4285F4"} d="M23.5,12.3c0-0.8-0.1-1.6-0.2-2.4H12v4.6h6.5c-0.3,1.5-1.1,2.8-2.4,3.6v3h3.9C22.2,19,23.5,15.9,23.5,12.3" />
// 		<path fill={colorList && colorList.color2 || secondary || "#34A853"} d="M12,24c3.2,0,6-1.1,8-2.9l-3.9-3c-1.1,0.7-2.5,1.1-4.1,1.1c-3.1,0-5.8-2.1-6.7-5h-4v3.1C3.3,21.4,7.5,24,12,24" />
// 		<path fill={colorList && colorList.color3 || primary || "#FBBC04"} d="M5.3,14.3c-0.5-1.5-0.5-3.1,0-4.6V6.6h-4c-1.7,3.4-1.7,7.4,0,10.8L5.3,14.3z" />
// 		<path fill={colorList && colorList.color4 || tertiary || "#EA4335"} d="M12,4.7c1.7,0,3.4,0.6,4.6,1.8L20,3.1c-2.2-2-5-3.2-8-3.1C7.5,0,3.3,2.6,1.3,6.6l4,3.1C6.2,6.9,8.9,4.7,12,4.7" />
// 	</svg>
// );

// ExampleSvg.colorCount = 0;
// ExampleSvg.colorVariation = [
// 	"original",
// 	generateCustomTheme("Custom 1", colors.icp11, colors.icp21, colors.icp31, colors.icp33),
// 	generateOneFillTheme("ipc33", colors.icp33),
// ];

export const Download12Svg = ({ primary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 12 12"} width={size} height={size}>
		<title>Download 12</title>
		<path fill={primary} d="M12,12H0V8c0-0.6,0.4-1,1-1s1,0.4,1,1v2h8V8c0-0.6,0.4-1,1-1s1,0.4,1,1V12z M4,0v4H1l5,5l5-5H8V0H4z" />
	</svg>
);

Download12Svg.colorVariation = [
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const Close12Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 12 12"} width={size} height={size}>
		<title>coffee/x/close12</title>
		<path fill={primary} d="M9.7,1.2c0.3-0.3,0.8-0.2,1.1,0.1c0.3,0.3,0.3,0.9,0,1.2l0,0L7.2,6l3.6,3.6c0.3,0.3,0.3,0.9,0,1.2c-0.3,0.3-0.8,0.3-1.1,0.1l-0.1-0.1L6,7.2l-3.6,3.6l-0.1,0.1c-0.3,0.3-0.8,0.2-1.1-0.1c-0.3-0.3-0.3-0.9,0-1.2l0,0L4.8,6L1.2,2.4c-0.3-0.3-0.3-0.9,0-1.2C1.5,0.9,2,0.9,2.3,1.2l0.1,0.1L6,4.8L9.7,1.2L9.7,1.2z" />
	</svg>
);

export const Back12Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 12 12"} width={size} height={size}>
		<title>back12</title>
		<path fill={primary} d="M8.7,2.7c0.4-0.4,0.4-1,0-1.4C8.5,1.1,8.3,1,8,1C7.8,1,7.5,1.1,7.3,1.3c0,0-4,4-4.1,4c-0.4,0.4-0.4,1,0,1.4l4,4c0.4,0.4,1,0.4,1.4,0c0.4-0.4,0.4-1,0-1.4L5.4,6L8.7,2.7" />
	</svg>
);

Back12Svg.colorVariation = [
	generateOneFillTheme("Dark Grey", colors.productDarkGrey),
	generateOneFillTheme("Blue", colors.productBlue),
];

export const Next12Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 12 12"} width={size} height={size}>
		<title>next12</title>
		<path fill={primary} d="M3.3,9.3c-0.4,0.4-0.4,1,0,1.4C3.5,10.9,3.7,11,4,11c0.2,0,0.5-0.1,0.7-0.3c0,0,4-4,4.1-4c0.4-0.4,0.4-1,0-1.4l-4-4c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4L6.6,6L3.3,9.3" />
	</svg>
);

Next12Svg.colorVariation = [
	generateOneFillTheme("Dark Grey", colors.productDarkGrey),
	generateOneFillTheme("Blue", colors.productBlue),
];

export const Down12Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 12 12"} width={size} height={size}>
		<title>down12</title>
		<path fill={primary} d="M2.7,3.3c-0.4-0.4-1-0.4-1.4,0C1.1,3.5,1,3.7,1,4c0,0.2,0.1,0.5,0.3,0.7c0,0,4,4,4,4.1c0.4,0.4,1,0.4,1.4,0l4-4c0.4-0.4,0.4-1,0-1.4c-0.4-0.4-1-0.4-1.4,0L6,6.6L2.7,3.3" />
	</svg>
);

export const Up12Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 12 12"} width={size} height={size}>
		<title>up12</title>
		<path fill={primary} d="M9.3,8.7c0.4,0.4,1,0.4,1.4,0C10.9,8.5,11,8.3,11,8c0-0.2-0.1-0.5-0.3-0.7c0,0-4-4-4-4.1c-0.4-0.4-1-0.4-1.4,0l-4,4c-0.4,0.4-0.4,1,0,1.4c0.4,0.4,1,0.4,1.4,0L6,5.4L9.3,8.7" />
	</svg>
);

//// Desinged for 14px //// Web?
//// Desinged for 16px ////
export const Reset16Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 16 16"} width={size} height={size}>
		<title>convention</title>
		<path fill={primary || colors.productBlue} d="M13.1,6.8l2.4,2.4h-1.7c-0.6,2.7-2.9,4.7-5.8,4.7c-1.9,0-3.6-0.9-4.6-2.2L5,11.2c0.8,0.7,1.8,1.2,3,1.2c2,0,3.6-1.3,4.2-3.2h-1.4L13.1,6.8z M8,2.1c2,0,3.8,1,4.9,2.5l-1.6,0.6c-0.8-0.9-2-1.5-3.3-1.5c-2.3,0-4.1,1.7-4.3,3.9h1.6L2.9,10L0.5,7.6h1.6C2.3,4.5,4.8,2.1,8,2.1z" />
	</svg>
);

Reset16Svg.colorVariation = [
	"original",
];

export const ConventionSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 16 16"} width={size} height={size}>
		<title>convention</title>
		<path fill={primary} d="M8,9C7.4,9,7,8.6,7,8c0-0.5,0.4-1,1-1c0.6,0,1,0.4,1,1C9,8.6,8.6,9,8,9 M13.9,7.2L13.9,7.2c-0.1-0.2-0.2-0.3-0.4-0.3c-0.2,0-0.3,0.1-0.4,0.2l0,0c-0.5,0.8-1.4,1.1-2.3,1c-0.3,0-0.6-0.1-0.9-0.3v0l0,0h0c0.7-0.6,1.2-1.5,1.2-2.5C11.1,3.5,9.6,2,7.9,2C7.6,2,7.4,2,7.2,2.1l0,0C7,2.1,6.9,2.3,6.9,2.5c0,0.2,0.1,0.3,0.2,0.4l0,0c0.8,0.5,1.1,1.4,1,2.3c0,0.3-0.1,0.6-0.3,0.9C7.2,5.4,6.3,4.9,5.3,4.9C3.5,4.9,2,6.4,2,8.1c0,0.3,0,0.4,0.1,0.7l0,0C2.1,9,2.3,9.1,2.5,9.1c0.2,0,0.3-0.1,0.4-0.2l0,0c0.5-0.8,1.4-1.1,2.3-1c0.3,0,0.6,0.1,0.9,0.3h0l0,0l0,0c-0.7,0.6-1.2,1.5-1.2,2.5c0,1.8,1.5,3.2,3.2,3.2c0.2,0,0.4,0,0.7-0.1v0c0.2-0.1,0.3-0.2,0.3-0.4c0-0.2-0.1-0.3-0.2-0.4l0,0c-0.8-0.5-1.1-1.4-1-2.3c0-0.3,0.1-0.6,0.3-0.9l0,0l0,0c0.6,0.7,1.5,1.2,2.5,1.2c1.8,0,3.2-1.5,3.2-3.2C14,7.6,14,7.5,13.9,7.2" />
	</svg>
);

export const IcebathSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 16 16"} width={size} height={size}>
		<title>icebath</title>
		<path fill={primary} d="M3.5,14C1.7,14,1,13.4,1,11.5V6c0-1.2,0-1,0-1.5C1,4,0,4.5,0,4c0-0.5,0.3-1,1-1c1,0,1,1.2,1,4v5c0,0.6,0.5,1,1,1l9.8,0c0.5,0,1.2-0.4,1.2-1V6.5C14,5,14,3,15,3c0.7,0,1,0.5,1,1s-1,0.1-1,0.5C15,5,15,5.4,15,6v5.5c0,1.9-0.7,2.5-2.5,2.5H3.5z" />
		<path fill={secondary === colors.transparent ? primary : secondary} d="M10,8h2c0.6,0,1,0.4,1,1v2c0,0.6-0.4,1-1,1h-2c-0.6,0-1-0.4-1-1V9C9,8.4,9.4,8,10,8z M4,8h2c0.6,0,1,0.4,1,1v2c0,0.6-0.4,1-1,1H4c-0.6,0-1-0.4-1-1V9C3,8.4,3.4,8,4,8z M6.9,4l1.9-0.7c0.5-0.2,1.1,0.1,1.3,0.6l0.7,1.9c0.2,0.5-0.1,1.1-0.6,1.3L8.3,7.8C7.8,8,7.2,7.7,7,7.2L6.3,5.3C6.2,4.8,6.4,4.2,6.9,4z" />
	</svg>
);

IcebathSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const SuperconventionSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 16 16"} width={size} height={size}>
		<title>superconvention</title>
		<path fill={primary} d="M8,9C7.4,9,7,8.6,7,8c0-0.5,0.4-1,1-1c0.6,0,1,0.4,1,1C9,8.6,8.6,9,8,9 M13.9,7.2L13.9,7.2c-0.1-0.2-0.2-0.3-0.4-0.3c-0.2,0-0.3,0.1-0.4,0.2l0,0c-0.5,0.8-1.4,1.1-2.3,1c-0.3,0-0.6-0.1-0.9-0.3v0l0,0h0c0.7-0.6,1.2-1.5,1.2-2.5C11.1,3.5,9.6,2,7.9,2C7.6,2,7.4,2,7.2,2.1l0,0C7,2.1,6.9,2.3,6.9,2.5c0,0.2,0.1,0.3,0.2,0.4l0,0c0.8,0.5,1.1,1.4,1,2.3c0,0.3-0.1,0.6-0.3,0.9C7.2,5.4,6.3,4.9,5.3,4.9C3.5,4.9,2,6.4,2,8.1c0,0.3,0,0.4,0.1,0.7l0,0C2.1,9,2.3,9.1,2.5,9.1c0.2,0,0.3-0.1,0.4-0.2l0,0c0.5-0.8,1.4-1.1,2.3-1c0.3,0,0.6,0.1,0.9,0.3h0l0,0l0,0c-0.7,0.6-1.2,1.5-1.2,2.5c0,1.8,1.5,3.2,3.2,3.2c0.2,0,0.4,0,0.7-0.1v0c0.2-0.1,0.3-0.2,0.3-0.4c0-0.2-0.1-0.3-0.2-0.4l0,0c-0.8-0.5-1.1-1.4-1-2.3c0-0.3,0.1-0.6,0.3-0.9l0,0l0,0c0.6,0.7,1.5,1.2,2.5,1.2c1.8,0,3.2-1.5,3.2-3.2C14,7.6,14,7.5,13.9,7.2 M8,0.3c-1.3,0-2.5,0.3-3.6,0.9L3.7,0L2.1,3.4l3.5-0.3L4.8,1.9c1-0.5,2-0.7,3.2-0.7c3.9,0,7.1,3.1,7.1,7c0,3.8-3.2,6.9-7.1,7c-3.9,0-7.1-3.1-7.1-7c0-1.2,0.3-2.3,0.9-3.3c0.1-0.2,0-0.5-0.2-0.6C1.4,4.1,1.1,4.2,1,4.4C0.4,5.5,0,6.8,0,8.2C0,12.5,3.6,16,8,16c4.4,0,8-3.5,8-7.8C16,3.8,12.4,0.3,8,0.3" />
	</svg>
);

export const UpSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 16 16"} width={size} height={size}>
		<title>up</title>
		<path fill={primary} d="M7.3,3.3c0.4-0.4,0.9-0.4,1.3-0.1l0.1,0.1l7,7c0.4,0.4,0.4,1,0,1.4c-0.4,0.4-0.9,0.4-1.3,0.1l-0.1-0.1L8,5.4l-6.3,6.3c-0.4,0.4-0.9,0.4-1.3,0.1l-0.1-0.1c-0.4-0.4-0.4-0.9-0.1-1.3l0.1-0.1L7.3,3.3z" />
	</svg>
);

export const Email1WebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 16 16"} width={size} height={size}>
		<path fill={primary} d="M15 5.85408V11.8846C15 12.4457 14.7695 12.9837 14.3593 13.3804C13.9491 13.7771 13.3927 14 12.8125 14H3.1875C2.60734 14 2.05094 13.7771 1.6407 13.3804C1.23047 12.9837 1 12.4457 1 11.8846V5.85408L7.77775 9.71085C7.84508 9.74925 7.92183 9.7695 8 9.7695C8.07817 9.7695 8.15492 9.74925 8.22225 9.71085L15 5.85408ZM12.8125 3C13.3508 2.99992 13.8701 3.19174 14.2713 3.53878C14.6725 3.88582 14.9272 4.3637 14.9869 4.881L8 8.85538L1.01313 4.881C1.07276 4.3637 1.32753 3.88582 1.7287 3.53878C2.12986 3.19174 2.64925 2.99992 3.1875 3H12.8125Z" />
	</svg>
);

export const MyBrevilleWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 16 16"} width={size} height={size}>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M3.7,4.7C3.7,2,5.6,0,8,0c2.5,0,4.5,2.1,4.5,4.7c0,1.1-0.4,2.2-1,3c1.8,1.2,2.8,3.4,3.6,6.3c0.1,0.2,0.1,0.4,0.1,0.7c0,0.4-0.1,0.7-0.4,1c-0.3,0.3-0.6,0.4-1,0.4H2.2c-0.8,0-1.4-0.6-1.4-1.3c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0.1,0,0.1c0-0.2-0.1-0.4,0-0.8c1-3.3,2.1-5.3,3.8-6.3C4,6.8,3.7,5.8,3.7,4.7z M11.1,4.7c0-1.9-1.4-3.3-3.1-3.3c-1.7,0-2.9,1.5-2.9,3.3c0,1.1,0.5,2.2,1.3,2.8C6.5,7.6,6.6,7.8,6.6,8c0,0.2-0.2,0.3-0.3,0.4c-1.8,0.7-3.1,2.5-4.1,6c0,0,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2h11.6c0-0.1,0-0.2,0-0.3c-0.9-3-1.9-5.2-3.9-6C9.7,8.3,9.6,8.1,9.5,8c0-0.2,0.1-0.4,0.2-0.5C10.6,6.9,11.1,5.8,11.1,4.7z M9.1,8C8.1,8,7,8,7,8S8.1,9.2,9.1,8z" />
	</svg>
);

//// Desinged for 24px
export const PlaceHolderSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Place Holder</title>
		<path fill={primary} d="M0,0v24h24V0H0z M10.6,12L2,20.6V3.4L10.6,12z M3.4,2h17.2L12,10.6L3.4,2z M12,13.4l8.6,8.6H3.4L12,13.4z M13.4,12L22,3.4v17.2L13.4,12z" />
	</svg>
);

export const TickSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>tick</title>
		<circle fill={tertiary} cx="12" cy="12.2" r="9.2" />
		<path fill={primary} d="M12,0C5.4,0,0,5.4,0,12s5.4,12,12,12s12-5.4,12-12S18.6,0,12,0z M19.2,9.6l-0.1,0.1l-8.5,8.4c-0.2,0.2-0.6,0.4-0.9,0.4c-0.3,0-0.7-0.1-0.9-0.4L8.8,18l-3.9-3.9c-0.5-0.5-0.5-1.2-0.1-1.7l0.1-0.1c0.5-0.5,1.2-0.5,1.7-0.1l0.1,0.1l3.1,3l7.5-7.5l0.1-0.1c0.5-0.4,1.2-0.4,1.7,0.1C19.6,8.3,19.6,9.1,19.2,9.6z" />
	</svg>
);

TickSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const MonitorSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>monitor</title>
		<rect fill={tertiary} x="2.2" y="4.4" width="19.6" height="15.2" />
		<path fill={secondary} d="M6,16.3c-0.4-3.7-0.4-5.5,0-5.5h8c0.7,0,0.7,5.5,0,5.5H6z" />
		<path fill={primary} d="M20.9,3H3.1C1.9,3,1,4,1,5.2v13.5C1,20,1.9,21,3.1,21h17.8c1.2,0,2.1-1,2.1-2.2V5.2C23,4,22.1,3,20.9,3z M21,19H3V5h18V19z M14,7H6C5.4,7,5,7.4,5,8s0.4,1,1,1H14c0.5,0,1-0.4,1-1S14.6,7,14,7 M17.5,13c-0.8,0-1.5,0.7-1.5,1.5c0,0.8,0.7,1.5,1.5,1.5s1.5-0.7,1.5-1.5C19,13.7,18.3,13,17.5,13 M17.5,8C16.7,8,16,8.7,16,9.5s0.7,1.5,1.5,1.5S19,10.3,19,9.5S18.3,8,17.5,8 M13.2,10H6.8c-1,0-1.8,0.9-1.8,2.1v2.9C5,16.1,5.8,17,6.8,17h6.4c1,0,1.8-0.9,1.8-2.1v-2.9C15,10.9,14.2,10,13.2,10zM14,15c0,0.6-0.4,1-0.9,1H6.9C6.4,16,6,15.5,6,15V12c0-0.6,0.4-1,0.9-1h6.2c0.5,0,0.9,0.5,0.9,1V15z" />
	</svg>
);

MonitorSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const MonitorAddSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>monitorAdd</title>
		<path fill={tertiary} style={{ fillRule: "evenodd", clipRule: "evenodd" }} d="M21,1l0,7.9C19.8,8.3,18.4,8,17,8c-5,0-9,4-9,9H1V1H21z" />
		<path fill={secondary} style={{ fillRule: "evenodd", clipRule: "evenodd" }} d="M12,7.5c0.7,0,1.3,0.5,1.5,1.2c-2.1,0.9-3.9,2.6-4.8,4.8l-2.7,0c-0.8,0-1.5-0.7-1.5-1.5V9c0-0.8,0.7-1.5,1.5-1.5H12z" />
		<path fill={primary} style={{ fillRule: "evenodd", clipRule: "evenodd" }} d="M17,10c3.9,0,7,3.1,7,7c0,3.8-3,6.9-6.8,7L17,24c-3.9,0-7-3.1-7-7S13.1,10,17,10z M18,13h-2v3h-3v2h3v3h2v-3h3v-2h-3V13z M20,0c1.1,0,1.9,0.8,2,1.9L22,2l0,7.5c-0.6-0.4-1.3-0.8-2-1L20,2H2v14l6.1,0C8,16.3,8,16.7,8,17c0,0.3,0,0.7,0.1,1L2,18c-1.1,0-1.9-0.8-2-1.9L0,16V2c0-1.1,0.8-1.9,1.9-2L2,0H20z M12,7c0.9,0,1.7,0.7,1.9,1.5c-0.2,0.1-0.4,0.2-0.6,0.3L13,8.9l0-0.1C12.9,8.4,12.5,8,12,8H6C5.5,8,5.1,8.4,5,8.9L5,9v3c0,0.5,0.4,0.9,0.9,1L6,13l2.9,0c-0.2,0.3-0.3,0.7-0.4,1L6,14c-1.1,0-1.9-0.8-2-1.9L4,12V9c0-1.1,0.8-1.9,1.9-2L6,7H12z M16.5,5C17.3,5,18,5.7,18,6.5S17.3,8,16.5,8C15.7,8,15,7.3,15,6.5S15.7,5,16.5,5z M12.7,4c0.6,0,1,0.4,1,1s-0.4,1-1,1l0,0H5.2c-0.6,0-1-0.4-1-1s0.4-1,1-1l0,0H12.7z" />
		<path fill={tertiary} style={{ fillRule: "evenodd", clipRule: "evenodd" }} d="M18,13v3h3v2h-3v3h-2v-3h-3v-2h3v-3H18z" />
	</svg>
);

MonitorAddSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const BhcProcessingLidSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Contents</title>
		<path fill={tertiary} style={{ fillRule: "evenodd", clipRule: "evenodd" }} d="M12,5c1.2,0,2.4,0.2,3.2,0.5c4.1,0.4,6.4,1.7,6.3,4.2c0.9,0.3,1.4,1,1.5,2.1l0,0.2v2.7c0,3-4.2,5.3-11,5.3c-6.7,0-10.9-2.2-11-5.1l0-0.2v-2.6c0-1.2,0.5-1.9,1.5-2.3C2.5,9,3.1,7,5.7,6.3C6.5,6,7.5,5.8,8.6,5.6C9.4,5.2,10.6,5,12,5z" />
		<path fill={primary} style={{ fillRule: "evenodd", clipRule: "evenodd" }} d="M12,4c1.3,0,2.6,0.2,3.5,0.6c4.5,0.5,7,1.9,6.9,4.8c1,0.4,1.5,1.2,1.6,2.4l0,0.2v3c0,3.4-4.6,6-12,6c-7.3,0-11.8-2.5-12-5.8L0,15v-3c0-1.3,0.5-2.2,1.6-2.6c0.1-0.9,0.7-3.1,3.5-4c0.9-0.3,2-0.6,3.2-0.7C9.2,4.3,10.5,4,12,4z M21.9,11.5L21.9,11.5c-0.4,2.1-4.7,3.4-9.9,3.4c-5.1,0-9.3-1.3-9.9-3.2l0-0.1l0,0c0,0.1-0.1,0.2-0.1,0.4L2,12l0,0.2V15c0,1.9,3.7,4,10,4c6.2,0,9.9-2,10-3.9l0-0.1v-2.8l0-0.2C22,11.9,22,11.7,21.9,11.5z M20.8,10.2l-0.1,0.1C19,11.3,15.7,12,12,12c-3.8,0-7.1-0.7-8.8-1.8l-0.1,0.1l0,1.1l0,0.1c0.4,1.2,3.9,2.4,8.2,2.5l0.3,0l0.3,0c4.6,0,8.5-1.2,8.9-2.4l0-0.1l0-1.2L20.8,10.2L20.8,10.2z M16.8,6.4C16.9,6.6,17,6.8,17,7.1c0,0.4-0.7,1.1-2,2.1l-0.2,0.1c-0.4,0.3-1,0.6-2.3,0.7L12,10l-0.2,0c-1.6,0-2.3-0.4-2.8-0.8C7.7,8.2,7,7.5,7,7.1c0-0.2,0.1-0.4,0.2-0.6C6.4,6.6,5.7,6.8,5.1,7C4,7.4,3.3,7.9,3.2,8.4l0,0.1l0,0.2C3.5,9.8,7.3,11,12,11c4.9,0,8.9-1.3,8.9-2.5C20.9,7.7,19.3,6.9,16.8,6.4z M12.2,6L12,6C9.8,6,8,6.4,8,7c0,0.6,1.8,1,4,1c2.2,0,4-0.4,4-1C16,6.5,14.3,6,12.2,6z" />
		<path fill={secondary} style={{ fillRule: "evenodd", clipRule: "evenodd" }} d="M21.9,11.6c-0.2,1-1.4,1.8-3.1,2.4c-1.3,0.5-1.3,3-3.1,4.7c3.9-0.6,6.2-2.2,6.3-3.6l0-0.1v-2.8l0-0.2C22,11.9,22,11.8,21.9,11.6z" />
	</svg>
);

BhcProcessingLidSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const AccountSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>account</title>
		<path fill={tertiary} d="M12.2,1C15.4,1,18,3.6,18,7c0,1.8-0.9,4-2.5,5.3c2.9,1.3,3.8,3.6,4.9,7.3l0.5,1.7 c0,0.2,0.1,0.3,0.1,0.5l0,0.2v0.5c0,0.3-0.2,0.5-0.5,0.5H3.4l0,0C3.1,23,3,22.8,3,22.4c0.1-0.3,0.2-0.8,0.4-1.5c1-4.7,2-7.3,5.1-8.6 C7.1,11.1,6,9,6,7C6,3.6,8.9,1,12.2,1z" />
		<path fill={primary} d="M12.2,1C15.4,1,18,3.6,18,7c0,1.8-0.9,4-2.5,5.3c2.9,1.3,3.8,3.6,4.9,7.3l0.5,1.7 c0,0.2,0.1,0.3,0.1,0.5l0,0.2v0.5c0,0.3-0.2,0.5-0.5,0.5H3.4C3.1,23,3,22.8,3,22.4c0.1-0.3,0.2-0.8,0.4-1.5c1-4.7,2-7.3,5.1-8.6 C7.1,11.1,6,9,6,7C6,3.6,8.9,1,12.2,1z M14.1,13.8l-0.6,0.3c-0.3,0.2-0.7,0.3-1.1,0.5l-0.4,0.2l-0.7,0.3c-0.4,0.2-0.7,0.3-1.1,0.4 C9.5,15.9,9,16.5,9,17.1l0,0.1V21h9.8l-0.4-1.3c-0.9-3.2-1.7-4.8-3.9-5.7C14.3,14,14.2,13.9,14.1,13.8z M9.1,13.6 c-2.2,1-2.9,3.5-3.8,7.4l0,0L8,21l0-3.7c0-1.2,0.8-2.2,1.9-2.6c0.1-0.1,0.3-0.1,0.4-0.2l0,0L9.1,13.6z M13.4,12.8 C12.9,12.9,12.5,13,12,13c-0.5,0-0.9-0.1-1.4-0.2c0,0.1-0.1,0.3-0.2,0.4l-0.1,0.1l1.1,0.7c0.7-0.3,1.4-0.7,2-1 C13.4,13,13.4,12.9,13.4,12.8z M12.1,3C10,3.1,8,4.7,8,7c0,1.5,0.9,3.1,2,3.9l0.4,0.3c0.5,0.3,1.2,0.8,1.6,0.8 c0.5,0,1.8-0.9,2.1-1.1c0.8-0.6,1.5-1.5,1.9-2.6C15.9,8,16,7.8,16,7.5C16,5.1,14.2,3.1,12.1,3z" />
		<path fill={secondary === colors.transparent ? primary : secondary} d="M9.1,13.6l1.3,0.9c-0.1,0.1-0.3,0.1-0.4,0.2C8.8,15.1,8,16.1,8,17.3L8,21l-2.8,0 C6.2,17.2,6.9,14.6,9.1,13.6z" />
		<polygon fill={secondary === colors.transparent ? primary : secondary} points="13,18 13,20 11,20 11,18 " />
	</svg>
);

AccountSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
];

export const ThicknessSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>thickness</title>
		<polygon fill={primary} points="14,10 17,10 12.5,4 8,10 11,10 11,14 8,14 12.5,20 17,14 14,14 " />
		<path fill={secondary} d="M5,0h4v2H5C4.4,2,4,1.6,4,1S4.4,0,5,0z M4,23c0,0.6,0.4,1,1,1h4v-2H5C4.4,22,4,22.4,4,23z M10,2h5V0h-5V2zM21,1c0-0.6-0.4-1-1-1h-4v2h4C20.6,2,21,1.6,21,1z M20,22h-4v2h4c0.6,0,1-0.4,1-1S20.6,22,20,22z M10,24h5v-2h-5V24z" />
	</svg>
);

ThicknessSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const AboutSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>about</title>
		<path fill={primary} d="M11.5,8.2l0.3,0c2,0.1,3.1,1.8,2.6,3.8c-0.2,0.9-1.2,4.7-1.5,5.9c-0.4,2,1.4,2.6,2.6,2.6l0,0h0l-0.3,0.7c-1,0.3-2.2,0.8-3.6,0.7c-2-0.1-3.1-1.8-2.6-3.8c0.3-1.2,1.2-5.1,1.5-5.9c0.4-2-1.4-2.2-2.6-2.2c0,0,0.2-0.8,0.2-0.8c0.9-0.4,2.2-1.1,3.6-1L11.5,8.2z M13.4,2c1.3,0,2.4,1.1,2.4,2.4s-1.1,2.4-2.4,2.4c-1.3,0-2.4-1.1-2.4-2.4S12.1,2,13.4,2z" />
	</svg>
);

export const StartlogSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>starlog</title>
		<path fill={tertiary} fillRule="evenodd" clipRule="evenodd" d="M4.1996 6.5918H16.1839V10.4956C16.1288 10.4942 16.0734 10.4935 16.0179 10.4935C12.4092 10.4935 9.48369 13.419 9.48369 17.0278C9.48369 17.9193 9.66222 18.7691 9.98549 19.5434H4.1996V6.5918ZM16.0044 21.3275C18.3893 21.3275 20.3227 19.3941 20.3227 17.0092C20.3227 14.6243 18.3893 12.6909 16.0044 12.6909C13.6194 12.6909 11.6861 14.6243 11.6861 17.0092C11.6861 19.3941 13.6194 21.3275 16.0044 21.3275Z"/>
		<path fill={secondary} d="M12.8885 4.70386H8.1115V6.29619H12.8885V4.70386Z"/>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M5.00895 7L5 19H9.81805C9.84349 19.0785 9.86539 19.1556 9.88741 19.2331C10.0151 19.6826 10.1471 20.1471 11 21H4C3.4475 21 3.00005 20.605 3.00005 20.1175V5.8825C3.00005 5.395 3.4475 5 4 5H6C6.0005 4.6945 6.1375 4.392 6.402 4.1965C6.488 4.1325 6.5835 4.0835 6.6835 4.0505C6.784 4.017 6.89 4 6.997 4H8.0915C8.036 3.843 8 3.6765 8 3.5C8 2.672 8.6715 2 9.5 2H11.5C12.3285 2 13 2.672 13 3.5C13 3.6765 12.964 3.843 12.908 4H14C14.1385 4 14.27 4.028 14.3895 4.0785C14.6885 4.205 14.9135 4.4725 14.98 4.7985C14.9935 4.8635 15 4.9305 15 5H17C17.552 5 18 5.395 18 5.8825V10.585C17.673 10.534 16.341 10.5 16 10.5L15.9911 10.5L16 7H14.1435L13.7035 7.5945C13.656 7.6585 13.602 7.716 13.5425 7.7655C13.424 7.865 13.2835 7.9365 13.132 7.9725C13.057 7.9905 12.9785 8 12.8995 8H8.116C8.0365 8 7.9585 7.9905 7.883 7.9725C7.732 7.9365 7.5915 7.865 7.4725 7.7655C7.413 7.716 7.359 7.6585 7.312 7.5945L6.8715 7H5.00895ZM15.9911 10.5075L15.9798 10.5062C15.9483 10.5026 15.9278 10.5002 15.9911 10.5L15.9911 10.5075ZM15.991 10.5255C16.076 10.5188 16.0316 10.5123 15.9911 10.5075L15.991 10.5255ZM14 10.8184V9.99988H13V10.9999H13.504C13.666 10.9329 13.832 10.8724 14 10.8184ZM7.5 10C7.224 10 7 10.2235 7 10.5C7 10.7765 7.224 11 7.5 11H11.5C11.776 11 12 10.7765 12 10.5C12 10.2235 11.776 10 11.5 10H7.5ZM7.0002 13.5C7.0002 13.2235 7.2237 13 7.5002 13H10.8872C10.6422 13.3125 10.4247 13.646 10.2397 14H7.5002C7.2237 14 7.0002 13.7765 7.0002 13.5ZM7.5 16C7.224 16 7 16.2235 7 16.5C7 16.7765 7.224 17 7.5 17H9.5C9.5 16.659 9.534 16.327 9.585 16H7.5ZM12 5H9V6H12V5ZM17 18H19V16H17V14H15V16H13V18H15V20H17V18ZM16 22C13.2385 22 11 19.7615 11 17C11 14.2385 13.2385 12 16 12C18.7615 12 21 14.2385 21 17C21 19.7615 18.7615 22 16 22Z"/>
	</svg>
);

StartlogSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const SkipSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>skip</title>
		<path fill={primary} d="M11,12L2.4,4c-0.5-0.5-0.5-1.2,0-1.7s1.3-0.5,1.8,0l9.5,8.8c0.5,0.5,0.5,1.2,0,1.7c0,0-0.1,0.1-0.1,0.1l-9.4,8.7C3.9,21.9,3.6,22,3.3,22c-0.3,0-0.6-0.1-0.9-0.3c-0.5-0.5-0.5-1.2,0-1.7L11,12z M19,12l-8.6-8c-0.5-0.5-0.5-1.2,0-1.7s1.3-0.5,1.8,0l9.5,8.8c0.5,0.5,0.5,1.2,0,1.7c0,0-0.1,0.1-0.1,0.1l-9.4,8.7c-0.2,0.2-0.6,0.3-0.9,0.3c-0.3,0-0.6-0.1-0.9-0.3c-0.5-0.5-0.5-1.2,0-1.7L19,12z" />
	</svg>
);

export const SettingsSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>settings</title>
		<path fill={primary} d="M12,6c3.3,0,6,2.7,6,6s-2.7,6-6,6c-3.3,0-6-2.7-6-6S8.7,6,12,6z M20.5,9.9c-0.2-0.9-0.5-1.7-1-2.4 L20.5,6c0.2-0.4,0.2-0.9-0.2-1.2l-1-1c-0.3-0.3-0.8-0.4-1.2-0.2l-1.6,0.9c-0.7-0.4-1.5-0.8-2.4-1l-0.4-1.7c-0.1-0.4-0.5-0.7-1-0.8 l0,0h-1.4h-0.1l0,0c-0.4,0.1-0.7,0.3-0.8,0.7L9.9,3.5C9,3.7,8.2,4,7.5,4.5L6,3.5C5.6,3.3,5.1,3.4,4.7,3.7l-1,1 C3.5,5,3.4,5.4,3.6,5.8l0.9,1.6C4,8.2,3.7,9,3.5,9.9l-1.7,0.4c-0.4,0.1-0.8,0.5-0.8,1v1.4c0,0.4,0.3,0.8,0.7,1l1.8,0.5 c0.2,0.9,0.5,1.7,1,2.4L3.5,18c-0.2,0.4-0.2,0.9,0.2,1.2l1,1c0.3,0.3,0.8,0.4,1.2,0.2l1.6-0.9c0.7,0.4,1.5,0.8,2.4,1l0.4,1.7 c0.1,0.4,0.5,0.8,1,0.8h1.4c0.4,0,0.8-0.3,1-0.7l0.5-1.8c0.9-0.2,1.7-0.5,2.4-1l1.5,0.9c0.4,0.2,0.9,0.2,1.2-0.2l1-1 c0.3-0.3,0.4-0.8,0.2-1.2l-0.9-1.6c0.4-0.7,0.8-1.5,1-2.4l1.7-0.4c0.4-0.1,0.8-0.5,0.8-1v-1.4c0-0.4-0.3-0.8-0.7-1L20.5,9.9z" />
	</svg>
);

SettingsSvg.colorVariation = [
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
];

export const SearchBrevilleWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>search</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M1,10.4C1,5.2,5.2,1,10.4,1s9.4,4.2,9.4,9.4c0,2.3-0.8,4.3-2.1,6l5.1,5.1c0.4,0.4,0.4,0.9,0,1.3c-0.3,0.3-0.9,0.4-1.2,0.1l-0.1-0.1l-5.1-5.1c-1.6,1.3-3.7,2.1-6,2.1C5.2,19.8,1,15.6,1,10.4z M17.9,10.4c0-4.2-3.4-7.5-7.5-7.5c-4.2,0-7.5,3.4-7.5,7.5c0,4.2,3.4,7.5,7.5,7.5C14.5,17.9,17.9,14.5,17.9,10.4z" />
	</svg>
);

export const SearchSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>search</title>
		<path fill={tertiary} d="M10.6,2.5c4.5,0,8.1,3.6,8.1,8.1c0,1.2-0.3,2.3-0.7,3.3l3.7,3.7c1.1,1.1,1.1,3,0,4.1c-1.1,1.1-2.8,1.1-4,0.1l-0.1-0.1L13.9,18c-0.9,0.4-2,0.7-3,0.7l-0.3,0c-4.5,0-8.1-3.6-8.1-8.1c0-4.4,3.5-7.9,7.9-8.1L10.6,2.5z" />
		<path fill={primary} d="M10.5,1.9c4.7,0,8.5,3.8,8.5,8.5c0,1.2-0.3,2.4-0.8,3.5l3.9,3.9c1.2,1.2,1.2,3.1,0,4.3c-1.1,1.1-3,1.2-4.2,0.1l-0.1-0.1l-3.9-3.9c-1,0.4-2.1,0.7-3.2,0.7l-0.3,0c-4.7,0-8.5-3.8-8.5-8.5c0-4.6,3.7-8.3,8.2-8.5L10.5,1.9zM15.7,17.2l3.5,3.5c0.4,0.4,1.1,0.4,1.5,0c0.4-0.4,0.4-1,0.1-1.4l-0.1-0.1l-3.5-3.5c-0.3,0.4-0.7,0.8-1.1,1.2L15.7,17.2z M10.5,3.9L10.5,3.9C6.9,3.9,4,6.8,4,10.4c0,3.5,2.8,6.4,6.2,6.5l0.3,0c3.6,0,6.5-2.9,6.5-6.5C17,6.9,14.1,3.9,10.5,3.9z M6,10.4c0.3,0,0.5,0.2,0.5,0.5c0,1.9,1.6,3.5,3.5,3.5c0.3,0,0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5c-2.5,0-4.5-2-4.5-4.5C5.5,10.7,5.7,10.4,6,10.4z" />
	</svg>
);

SearchSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
];


export const ReturnSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>return</title>
		<path fill={primary} d="M20,2c1.1,0,1.9,0.8,2,1.8L22,4v16c0,1.1-0.8,1.9-1.8,2L20,22h-8c-1.1,0-1.9-0.8-2-1.8l0-0.1v-5h2 l0,5l8,0l0-16h-8l0,5h-2V4c0-1.1,0.8-1.9,1.8-2L12,2H20z M7.8,6.8c0.4,0.4,0.4,1,0,1.4L5,11h11.1c0.6,0,1,0.4,1,1s-0.4,1-1,1H5.3 l2.6,2.8c0.4,0.4,0.3,1-0.1,1.4c-0.2,0.2-0.4,0.3-0.7,0.3c-0.3,0-0.5-0.1-0.7-0.3l-4-4.4c-0.5-0.6-0.5-1.5,0-2l4-4 C6.8,6.4,7.4,6.4,7.8,6.8z" />
	</svg>
);

export const PulseSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>pulse</title>
		<path fill={primary} d="M11.1,19.4l-3.2-7.9l-1,2C6.7,13.8,6.4,14,6,14H3c-0.6,0-1-0.4-1-1s0.4-1,1-1h2.4l1.7-3.4 C7.3,8.2,7.6,8,8,8c0.4,0,0.7,0.3,0.9,0.6l2.9,7.4L15,4.7C15.2,4.3,15.6,4,16,4c0.5,0,0.8,0.3,1,0.7l1.8,7.3H21c0.6,0,1,0.4,1,1 s-0.4,1-1,1h-3c-0.4,0-0.8-0.3-1-0.7L16,8.5l-3,10.8C12.8,19.7,12.5,20,12,20C11.6,20,11.2,19.8,11.1,19.4z" />
	</svg>
);

export const NextSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>next</title>
		<path fill={primary} d="M7.3,20l7.9-8L7.4,4c-0.4-0.5-0.4-1.2,0-1.7c0.4-0.5,1.2-0.5,1.6,0l8.7,8.8c0.4,0.5,0.4,1.2,0,1.7 c0,0-0.1,0.1-0.1,0.1L9,21.7C8.7,21.9,8.4,22,8.1,22s-0.6-0.1-0.8-0.3C6.9,21.2,6.9,20.5,7.3,20" />
	</svg>
);

export const MenuSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>menu</title>
		<path fill={primary} d="M3,17h18c0.6,0,1,0.4,1,1v1c0,0.6-0.4,1-1,1H3c-0.6,0-1-0.4-1-1v-1C2,17.4,2.4,17,3,17 M3,11h18c0.6,0,1,0.4,1,1v1c0,0.6-0.4,1-1,1H3c-0.6,0-1-0.4-1-1v-1C2,11.4,2.4,11,3,11 M3,5h18c0.6,0,1,0.4,1,1v1c0,0.6-0.4,1-1,1H3C2.4,8,2,7.6,2,7V6C2,5.4,2.4,5,3,5" />
	</svg>
);

export const LogSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>log</title>
		<path fill={tertiary} d="M4.6395 22.4378H19.3605V4.56226H4.6395V22.4378Z"/>
		<path fill={secondary} d="M16.3646 2.63086H7.63547V4.36909H16.3646V2.63086Z"/>
		<path fill={primary}  fillRule="evenodd" clipRule="evenodd" d="M7 11H14V9H7V11ZM7 13H14V15H7V13ZM7 17H14V19H7V17ZM17 9H15V11H17V9ZM17 13H15V15H17V13ZM17 17H15V19H17V17ZM21 22V5C21 3.9 20.1 3 19 3H18V1H14V0H10V1H6V3H5C3.9 3 3 3.9 3 5V22C3 23.1 3.9 24 5 24H19C20.1 24 21 23.1 21 22ZM17 5H19V22H5V5H7V6H17V5ZM9 3H15V4H9V3Z"/>
	</svg>
);

LogSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const LinkSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>link</title>
		<path fill={primary} d="M8.9,19.2c-1.1,1.1-3,1.1-4.1,0c-1.1-1.1-1.1-3,0-4.1L8.9,11c1.1-1.1,3-1.1,4.1,0c0.2,0.2,0.4,0.5,0.6,0.8l1.4-1.4c-0.2-0.3-0.4-0.5-0.6-0.8c-1.9-1.9-5-1.9-6.9,0l-4.1,4.1c-1.9,1.9-1.9,5,0,6.9c1.9,1.9,5,1.9,6.9,0l2.1-2.1L11,17.1L8.9,19.2z M20.6,3.4c-1.9-1.9-5-1.9-6.9,0l-2.1,2.1L13,6.9l2.1-2.1c1.1-1.1,3-1.1,4.1,0c1.1,1.1,1.1,3,0,4.1L15.1,13c-1.1,1.1-3,1.1-4.1,0c-0.2-0.2-0.4-0.5-0.6-0.8L9,13.6c0.2,0.3,0.4,0.5,0.6,0.8c1.9,1.9,5,1.9,6.9,0l4.1-4.1C22.5,8.4,22.5,5.3,20.6,3.4" />
	</svg>
);

export const HomeSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>home</title>
		<polygon fill={tertiary} points="19,9.7 18.7,9.7 18.7,12 18.7,21.2 5.3,21.2 5.3,12 5.3,9.7 5,9.7 12,3.2 " />
		<path fill={primary} d="M11.4,2.2l-9,8.1C1.7,11,2.1,12,3,12h1v9c0,0.5,0.5,1,1,1h14c0.5,0,1-0.5,1-1v-9h1c1,0,1.4-1,0.7-1.6L19,8V4c0-0.5-0.5-1-1-1h-1c-0.5,0-1,0.5-1,1v1l-3.3-2.8C12.5,2.1,12.2,2,12,2C11.8,2,11.5,2.1,11.4,2.2zM18.3,10H18v2v8H6v-8v-2H5.7L12,4.3L18.3,10z" />
	</svg>
);

HomeSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const HeartSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>heart</title>
		<path fill={tertiary} d="M12,21c-0.1,0-0.2,0-0.3-0.1C11.3,20.7,2,15.1,2,8.5C2,5.5,4.4,3,7.3,3c2,0,3.8,1.2,4.7,2.9C12.9,4.2,14.7,3,16.7,3C19.6,3,22,5.5,22,8.5c0,6.6-9.3,12.1-9.7,12.4C12.2,21,12.1,21,12,21" />
		<path fill={primary} d="M12,22c-0.3,0-0.6-0.1-0.8-0.2C10.7,21.5,1,15.7,1,8.5C1,4.9,3.8,2,7.3,2c1.8,0,3.5,0.8,4.7,2.1C13.2,2.8,14.9,2,16.7,2C20.2,2,23,4.9,23,8.5c0,7.1-9.7,13-10.2,13.2C12.6,21.9,12.3,22,12,22z M11.8,20C11.8,20,11.8,20,11.8,20C11.8,20.1,11.8,20,11.8,20z M12.2,20C12.2,20,12.2,20,12.2,20C12.2,20,12.2,20,12.2,20z M7.3,4C4.9,4,3,6,3,8.5c0,5.5,7.7,10.6,9,11.4c1.3-0.8,9-5.9,9-11.4C21,6,19.1,4,16.7,4c-1.6,0-3,0.9-3.8,2.3L12,8l-0.9-1.6C10.3,4.9,8.9,4,7.3,4z" />
	</svg>
);

HeartSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const HeartFilledSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>heart</title>
		<path fill={primary} d="M12,21c-0.1,0-0.2,0-0.3-0.1C11.3,20.7,2,15.1,2,8.5C2,5.5,4.4,3,7.3,3c2,0,3.8,1.2,4.7,2.9C12.9,4.2,14.7,3,16.7,3C19.6,3,22,5.5,22,8.5c0,6.6-9.3,12.1-9.7,12.4C12.2,21,12.1,21,12,21" />
		<path fill={primary} d="M12,22c-0.3,0-0.6-0.1-0.8-0.2C10.7,21.5,1,15.7,1,8.5C1,4.9,3.8,2,7.3,2c1.8,0,3.5,0.8,4.7,2.1C13.2,2.8,14.9,2,16.7,2C20.2,2,23,4.9,23,8.5c0,7.1-9.7,13-10.2,13.2C12.6,21.9,12.3,22,12,22z M11.8,20C11.8,20,11.8,20,11.8,20C11.8,20.1,11.8,20,11.8,20z M12.2,20C12.2,20,12.2,20,12.2,20C12.2,20,12.2,20,12.2,20z M7.3,4C4.9,4,3,6,3,8.5c0,5.5,7.7,10.6,9,11.4c1.3-0.8,9-5.9,9-11.4C21,6,19.1,4,16.7,4c-1.6,0-3,0.9-3.8,2.3L12,8l-0.9-1.6C10.3,4.9,8.9,4,7.3,4z" />
	</svg>
);

Download12Svg.colorVariation = [
];

export const FunctionStepsSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>functionSteps</title>
		<path fill={primary} d="M3,3h12c1.1,0,2,0.9,2,2l0,0c0,1.1-0.9,2-2,2H3C1.9,7,1,6.1,1,5l0,0C1,3.9,1.9,3,3,3z M6,10h12c1.1,0,2,0.9,2,2l0,0c0,1.1-0.9,2-2,2H6c-1.1,0-2-0.9-2-2l0,0C4,10.9,4.9,10,6,10z M9,17h12c1.1,0,2,0.9,2,2l0,0c0,1.1-0.9,2-2,2H9c-1.1,0-2-0.9-2-2l0,0C7,17.9,7.9,17,9,17z" />
	</svg>
);

export const FunctionStartSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>functionStart</title>
		<path fill={primary} d="M6.5,3l13.1,8.2c0.5,0.3,0.6,0.9,0.3,1.4c-0.1,0.1-0.2,0.2-0.3,0.3L6.5,21 c-0.5,0.3-1.1,0.2-1.4-0.3C5.1,20.6,5,20.4,5,20.2V3.8c0-0.6,0.4-1,1-1C6.2,2.8,6.4,2.9,6.5,3z" />
	</svg>
);

export const FunctionForwardSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>functionForward</title>
		<path fill={primary} d="M20,12.3l-5.7-5.8c-0.3-0.3-0.3-0.9,0-1.2c0.3-0.3,0.9-0.3,1.2,0l6.3,6.4c0.3,0.3,0.3,0.9,0,1.2 c0,0-0.1,0-0.1,0.1l-6.3,6.4c-0.2,0.2-0.4,0.2-0.6,0.2s-0.4-0.1-0.6-0.2c-0.3-0.3-0.3-0.9,0-1.2L20,12.3z" />
	</svg>
);

export const FilterSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>filter</title>
		<path fill={primary} d="M16,9c1.3,0,2.4,0.8,2.8,2l2.2,0c0.6,0,1,0.4,1,1s-0.4,1-1,1l-2.2,0c-0.4,1.2-1.5,2-2.8,2s-2.4-0.8-2.8-2L3,13c-0.6,0-1-0.4-1-1s0.4-1,1-1l10.2,0C13.6,9.8,14.7,9,16,9z M16,11c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S16.6,11,16,11z M8,2c1.3,0,2.4,0.8,2.8,2L21,4c0.6,0,1,0.4,1,1s-0.4,1-1,1L10.8,6C10.4,7.2,9.3,8,8,8S5.6,7.2,5.2,6L3,6C2.4,6,2,5.6,2,5s0.4-1,1-1l2.2,0C5.6,2.8,6.7,2,8,2z M8,4C7.4,4,7,4.4,7,5s0.4,1,1,1s1-0.4,1-1S8.6,4,8,4z M9,16c1.3,0,2.4,0.8,2.8,2l9.2,0c0.6,0,1,0.4,1,1s-0.4,1-1,1l-9.2,0c-0.4,1.2-1.5,2-2.8,2s-2.4-0.8-2.8-2L3,20c-0.6,0-1-0.4-1-1s0.4-1,1-1l3.2,0C6.6,16.8,7.7,16,9,16z M9,18c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S9.6,18,9,18z" />
	</svg>
);

export const ExitSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>exit</title>
		<path fill={primary} d="M8,11c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1l10.7,0l-2.6,2.8c-0.4,0.4-0.3,1,0.1,1.4c0.2,0.2,0.4,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3l4-4.4c0.5-0.6,0.5-1.5,0-2l-4-4c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4L19,11L8,11z M4,2h8c1.1,0,2,0.9,2,2v5h-2l0-5H4l0,16l8,0l0-5h2v5c0,1.1-0.9,2-2,2H4c-1.1,0-2-0.9-2-2V4C2,2.9,2.9,2,4,2" />
	</svg>
);

export const DownSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>down</title>
		<path fill={primary} d="M4.5,6.8l8,7.9l8-7.9c0.5-0.4,1.2-0.4,1.7,0c0.5,0.4,0.5,1.2,0,1.6l-8.8,8.7 c-0.5,0.4-1.2,0.4-1.7,0c0,0-0.1-0.1-0.1-0.1L2.8,8.5C2.6,8.2,2.5,7.9,2.5,7.6s0.1-0.6,0.3-0.8C3.3,6.4,4,6.4,4.5,6.8" />
	</svg>
);

export const CrossSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>cross</title>
		<path fill={primary} d="M13.4,12l7.3-7.3c0.4-0.4,0.4-1,0-1.4c-0.4-0.4-0.9-0.4-1.3-0.1l-0.1,0.1L12,10.6L4.7,3.3L4.6,3.2c-0.4-0.3-1-0.3-1.3,0.1 c-0.4,0.4-0.4,1,0,1.4l7.3,7.3l-7.3,7.3c-0.4,0.4-0.4,1,0,1.4c0.4,0.4,0.9,0.4,1.3,0.1l0.1-0.1l7.3-7.3l7.3,7.3l0.1,0.1 c0.4,0.3,1,0.3,1.3-0.1c0.4-0.4,0.4-1,0-1.4L13.4,12z" />
	</svg>
);

CrossSvg.colorVariation = [
	generateOneFillTheme("Dark Grey", colors.productDarkGrey),
];

export const CalenderSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>calender</title>
		<rect fill={tertiary} x="3" y="10.4" width="18" height="10.1" />
		<path fill={secondary} d="M9.4,9.3H6.6V3.7h2.8V9.3z M17.4,3.7h-2.8v5.6h2.8V3.7z" />
		<path fill={primary} d="M21.5,6H19V5c0-1.7-1.3-3-3-3s-3,1.3-3,3v1h-2V5c0-1.7-1.3-3-3-3S5,3.3,5,5v1H2.5H2v5v11h20V11V6H21.5z M15,5c0-0.6,0.4-1,1-1s1,0.4,1,1v1v2c0,0.6-0.4,1-1,1s-1-0.4-1-1V6V5z M7,5c0-0.6,0.4-1,1-1s1,0.4,1,1v1v2c0,0.6-0.4,1-1,1S7,8.6,7,8V6V5z M20,20H4v-9h16V20z" />
	</svg>
);

CalenderSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const BinSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>calender</title>
		<rect fill={secondary} x="4.6" y="6.5" width="13.7" height="2.1" />
		<path fill={tertiary} d="M9.7,3.7h3.6v1.6H9.7V3.7z M5.4,9.5h12.2v11.1H5.4V9.5z" />
		<path fill={primary} d="M9,18H8l0.1-6H9V18z M13,12h-1v6h1V12z M11,12h-1v6h1V12z M15,12h-1v6h1V12z M20,7v3.1h-1v10.3c0,1.2-0.4,1.6-1.6,1.6H5.6C4.4,22,4,21.5,4,20.4V10.1H3V7c0-1.2,0.8-2,2-2h2.9V3.6C7.9,2.5,8.3,2,9.5,2l4,0C14.7,2,15,2.8,15,4v1h3C19.2,5,20,5.8,20,7z M10,5h3V4h-3V5z M17,10H6v10h11V10z M17.9,7H5v1h12.9V7z" />
	</svg>
);

BinSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Alertv1Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>alert v1</title>
		<path fill={primary} d="M10,19h4v4h-4V19z M15,7.5L13.4,17h-2.9L9,7.5V1h6V7.5z" />
	</svg>
);

export const Alertv2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>alert v2</title>
		<path fill={primary} d="M11.5,23C5.2,23,0,17.8,0,11.5C0,5.2,5.2,0,11.5,0C17.8,0,23,5.2,23,11.5C23,17.8,17.8,23,11.5,23z M11.5,2C6.3,2,2,6.3,2,11.5S6.3,21,11.5,21s9.5-4.3,9.5-9.5S16.7,2,11.5,2z M11.5,14c-0.8,0-1.5-0.6-1.5-1.3V5.3C10,4.6,10.7,4,11.5,4S13,4.6,13,5.3v7.4C13,13.4,12.3,14,11.5,14 M10,17.5c0-1.9-0.2-1.5,1.5-1.5s1.5-0.2,1.5,1.5s0.3,1.5-1.5,1.5C9.6,19,10,19.4,10,17.5z" />
	</svg>
);

export const AddSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>add</title>
		<path fill={primary} d="M12,2c0.5,0,0.9,0.3,1,0.7l0,0.1l0,8.2h8.2c0.5,0,0.8,0.4,0.8,1c0,0.5-0.3,0.9-0.7,1l-0.1,0H13l0,8.2 c0,0.5-0.4,0.8-1,0.8c-0.5,0-0.9-0.3-1-0.7l0-0.1l0-8.2H2.8C2.4,13,2,12.6,2,12c0-0.5,0.3-0.9,0.7-1l0.1,0H11l0-8.2 C11,2.4,11.4,2,12,2z" />
	</svg>
);

export const StepsSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>steps</title>
		<path fill={tertiary} fillRule="evenodd" clipRule="evenodd" d="M18.7692 21H5.23077C4.55015 21 4 20.4891 4 19.8571V6.14286C4 5.51086 4.55015 5 5.23077 5H18.7692C19.4498 5 20 5.51086 20 6.14286V19.8571C20 20.4891 19.4498 21 18.7692 21Z"/>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M6.07692 20H17.9231C18.5186 20 19 19.553 19 19V7C19 6.447 18.5186 6 17.9231 6H6.07692C5.48138 6 5 6.447 5 7V19C5 19.553 5.48138 20 6.07692 20ZM20.0661 2.342C20.1794 2.413 20.2842 2.495 20.3795 2.586C20.4759 2.676 20.5627 2.775 20.6379 2.882C20.8666 3.201 21 3.586 21 4V19C21 20.657 19.578 22 17.8235 22H6.17647C4.422 22 3 20.657 3 19V4C3 3.586 3.13341 3.201 3.36212 2.882C3.51353 2.669 3.70835 2.485 3.93388 2.342C4.27165 2.126 4.67929 2 5.11765 2H18.8824C19.3207 2 19.7284 2.126 20.0661 2.342ZM8.11111 8H15.8889C16.5033 8 17 8.447 17 9C17 9.553 16.5033 10 15.8889 10H8.11111C7.49667 10 7 9.553 7 9C7 8.447 7.49667 8 8.11111 8ZM15.8889 12H8.11111C7.49667 12 7 12.447 7 13C7 13.553 7.49667 14 8.11111 14H15.8889C16.5033 14 17 13.553 17 13C17 12.447 16.5033 12 15.8889 12ZM8.11111 16H15.8889C16.5033 16 17 16.447 17 17C17 17.553 16.5033 18 15.8889 18H8.11111C7.49667 18 7 17.553 7 17C7 16.447 7.49667 16 8.11111 16Z"/>
	</svg>
);

StepsSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const StepsThinSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>stepsX</title>
		<rect fill={tertiary} x="5.4" y="5.3" width="14.2" height="15.2" />
		<path fill={primary} d="M19,2c0.4,0,0.8,0.1,1.1,0.3c0.1,0.1,0.2,0.2,0.3,0.2c0.1,0.1,0.2,0.2,0.2,0.3C20.8,3.2,21,3.5,21,3.8L21,4v15c0,1.6-1.2,2.9-2.8,3L18,22H7c-1.6,0-2.9-1.2-3-2.8L4,19V4c0-0.4,0.1-0.8,0.3-1.1c0.1-0.2,0.3-0.4,0.5-0.5C5.2,2.2,5.5,2,5.8,2L6,2H19z M18,6H7C6.5,6,6.1,6.4,6,6.9L6,7v12c0,0.5,0.4,0.9,0.9,1L7,20h11c0.5,0,0.9-0.4,1-0.9l0-0.1V7c0-0.5-0.4-0.9-0.9-1L18,6z M16,16c0.6,0,1,0.4,1,1c0,0.5-0.4,0.9-0.9,1L16,18H9c-0.6,0-1-0.4-1-1c0-0.5,0.4-0.9,0.9-1L9,16H16z M16,12c0.6,0,1,0.4,1,1c0,0.5-0.4,0.9-0.9,1L16,14H9c-0.6,0-1-0.4-1-1c0-0.5,0.4-0.9,0.9-1L9,12H16zM16,8c0.6,0,1,0.4,1,1c0,0.5-0.4,0.9-0.9,1L16,10H9c-0.6,0-1-0.4-1-1c0-0.5,0.4-0.9,0.9-1L9,8H16z" />
	</svg>
);

StepsThinSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const BackSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>back</title>
		<path fill={primary} d="M16.7,20l-7.9-8l7.9-8c0.4-0.5,0.4-1.2,0-1.7c-0.4-0.5-1.2-0.5-1.6,0l-8.7,8.8 c-0.4,0.5-0.4,1.2,0,1.7c0,0,0.1,0.1,0.1,0.1l8.6,8.7c0.2,0.2,0.5,0.3,0.8,0.3c0.3,0,0.6-0.1,0.8-0.3C17.1,21.2,17.1,20.5,16.7,20" />
	</svg>
);



export const ActivetimeIIISvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>activetimeIII</title>
		<path fill={tertiary} d="M19.2,14.9c-0.8,3.7-3.7,6.3-6.9,6.3c-3.3,0-6.1-2.6-6.9-6.3L19.2,14.9" />
		<path fill={secondary} d="M18.2,17.8c0,0-0.7-0.8-3-0.8c-2.2,0-3,1.5-6,1.5s-3-0.7-3-0.7v0.7c1.5,2,3.5,3.5,6,3.5 c2.4,0,4.5-1.2,6-3V17.8z" />
		<path fill={primary} d="M20.5,13.3H4c0,3.7,1.8,6.9,4.5,8.4V23H16v-1.3C18.7,20.1,20.5,16.9,20.5,13.3z M12.2,20.7 c-2.9,0-5.3-2.3-6-5.5h12C17.5,18.4,15.1,20.7,12.2,20.7z M8,8.1L8,8.1l0.8-1.8L9,5.8c0.2-0.4,0.3-0.7,0.5-0.9 c0.3-0.4,0.5-0.7,0.9-1C11,3.4,11.8,3,12.8,2.6c0.7-0.2,1.4-0.4,2.1-0.6l0.5-0.1l0.4-0.1c0.1,0,0.1,0,0.2,0h0.2l1.7-0.6 C18.4,1,19,1.3,19.2,1.8l0,0c0.2,0.5-0.1,1.1-0.6,1.3l-1.8,0.6l0,0h-0.2l0,0c-0.3,0-0.7,0.1-1.1,0.2c-0.7,0.2-1.3,0.3-1.9,0.5 c-0.8,0.3-1.4,0.6-1.8,0.9c-0.2,0.1-0.3,0.3-0.4,0.5c-0.1,0.1-0.1,0.3-0.2,0.4L11.5,6c0.2-0.1,0.5-0.2,0.8-0.4 c0.3-0.1,0.5,0,0.6,0.3l0,0l0,0c0.1,0.3,0,0.5-0.3,0.7l0,0l0,0L12,7c-0.2,0.1-0.4,0.2-0.5,0.3l-0.3,0.2H11l-0.4,0.4L10.5,8 c-0.2,0.2-0.3,0.3-0.5,0.5C9.3,9.4,9,10,8.9,10.1C9.3,9.8,9.7,9.5,10,9.3l0.7-0.6L11.6,8l0.1-0.1c0.4-0.3,0.7-0.5,1-0.6 c0.5-0.2,0.9-0.3,1.6-0.2h0.2c0.5,0.1,1,0.5,0.9,1.1c0,0.2-0.1,0.4-0.3,0.6L15,8.9l0,0l0,0c0.3-0.3,0.7-0.5,1-0.6l0.2-0.1 c0.8-0.3,1.3-1.4,1.5-2.5V5.5V5l2.1-1.2c0.5-0.3,1.1-0.1,1.4,0.4l0,0c0.3,0.5,0.1,1.1-0.4,1.4l-1.2,0.7l0,0v0.1 c-0.3,1.5-1.1,3-2.5,3.6l0,0c-0.3,0.1-0.6,0.3-1,0.7l-0.7,0.7c-0.2,0.2-0.4,0.4-0.6,0.5c-0.7,0.5-1.5,0.5-2.1-0.2 c-0.4-0.5-0.6-1-0.7-1.4l0,0l-0.6,0.5c-0.9,0.7-1.3,1-1.8,1.3c-0.9,0.4-1.8,0.3-2.5-0.4C6.4,10.6,6.9,9.1,8,8.1z" />
	</svg>
);

ActivetimeIIISvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const AddDrinkSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>addDrink</title>
		<path fill={primary} d="M16.5,2C18.5,2,20,3.6,20,5.5l0,0l0,1.7C22.3,7.8,24,9.7,24,12c0,2.8-2.4,5-5.4,5l-0.1,0 c-0.5,0.9-1.2,1.8-2,2.6C14.2,21.8,11.2,23,8,23c-0.1,0-0.2,0-0.2,0c-2.8-0.1-4.6-0.2-6.7-2.1c-0.3-0.2-0.5-0.5-0.5-0.8 c0-0.6,0.4-1,1-1c0.3,0,0.5,0.1,0.7,0.3l0,0l0,0C4.1,21,5.4,21,7.8,21c0.1,0,0.1,0,0.2,0c2.6,0,5.1-1,7-2.9c1.9-1.9,3-4.4,3-7.1l0,0 V5.5C18,4.7,17.4,4,16.5,4l0,0H1C0.4,4,0,3.6,0,3s0.4-1,1-1l0,0H16.5z M5.3,7.2c0.6,0,1,0.4,1,1l0,0v3.3h3.3c0.6,0,1,0.4,1,1 c0,0.6-0.4,1-1,1l0,0H6.3v3.3c0,0.6-0.4,1-1,1c-0.6,0-1-0.4-1-1l0,0v-3.3H1c-0.6,0-1-0.4-1-1c0-0.6,0.4-1,1-1l0,0h3.3V8.2 C4.3,7.7,4.7,7.2,5.3,7.2z M20,9.2l0,1.7c0,1.4-0.2,2.7-0.7,4c1.6-0.3,2.7-1.5,2.7-3C22,10.8,21.2,9.7,20,9.2z" />
	</svg>
);

export const AlarmSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>alarm</title>
		<path fill={primary} d="M18.6,10.4c0-3.2-1.8-6-5-6.7C13.4,3,13.3,2,12,2s-1.5,1-1.6,1.7c-3.1,0.7-5,3.5-5,6.7c0,3.7-2.3,3.8-3.2,5 C1.7,16,2.2,17,3.1,17h8.7h9.1c0.9,0,1.4-1,0.9-1.6C20.9,14.2,18.6,14.1,18.6,10.4z M15.3,18.7H8.7c0.5,2,1.8,3.3,3.3,3.3 C13.5,22,14.9,20.6,15.3,18.7z" />
	</svg>
);

AlarmSvg.colorVariation = [
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const AutostartSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>autostart</title>
		<path fill={secondary === colors.transparent ? primary : secondary} d="M13.5,7.5c0.8,0,1.4,0.6,1.5,1.4L15,9l0,3.8c1.2,0.6,2,1.8,2,3.2c0,1.9-1.6,3.5-3.5,3.5 c-0.4,0-0.7-0.1-1-0.1c0.9-1.2,1.5-2.7,1.5-4.4c0-1.9-0.8-3.6-2-4.9L12,9C12,8.2,12.7,7.5,13.5,7.5z" />
		<path fill={primary} d="M13.5,1C15.9,1,18,3,18,5.5l0,0V10l0.2,0.2c1.7,1.3,2.7,3.4,2.8,5.6l0,0l0,0.3c0,4.1-3.4,7.5-7.5,7.5 c-1.8,0-3.5-0.6-4.8-1.7c0.8-0.2,1.5-0.5,2.1-1c0.8,0.4,1.7,0.7,2.6,0.7c3,0,5.5-2.5,5.5-5.5c0-1.9-1-3.6-2.5-4.6l0,0L16,11.1V5.5 C16,4.1,14.8,3,13.5,3C12.2,3,11,4.1,11,5.5l0,0l0,3.8c-0.6-0.4-1.3-0.8-2-1l0-2.8c0-2.4,2-4.4,4.3-4.5l0,0L13.5,1z M12,15.1 c0-1.4-0.5-2.7-1.6-3.8c-0.4-0.4-1-0.4-1.4,0c-0.4,0.4-0.4,1,0,1.4c0.7,0.7,1,1.5,1,2.4c0,1.6-1.2,2.9-2.9,2.9C5.6,18,4,16.8,4,15.1 c0-1.4,0.2-1.7,1.1-2.3c0.4-0.3,0.5-1,0.2-1.4c-0.3-0.4-1-0.5-1.4-0.2C2.6,12.2,2,13,2,15.1C2,17.8,4.3,20,7.1,20 C9.9,20,12,17.9,12,15.1z M7,10c-0.6,0-1,0.5-1,0.8v3.5C6,14.5,6.4,15,7,15s1-0.5,1-0.8v-3.5C8,10.5,7.6,10,7,10" />
	</svg>
);

AutostartSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const BhcHighSpeedLidSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>bhcHighSpeedLid</title>
		<path fill={tertiary} d="M8.4,2.7c0.1,0,0.2,0,0.4,0c0.4,0,0.7,0,1.1,0.1C11.4,3,13.3,3.6,14,4.5c0.3,0.4,0.3,0.7,0.3,1 l0,0.1V7l0.4,0c3,0.3,6.8,1.2,6.9,3.4c0.8,0.5,1.3,1.6,1.5,3.4v2.8c0,3.2-4.3,5.6-11.2,5.6c-6.8,0-11-2.3-11.2-5.4l0-0.2 c0-0.9,0-1.9,0-2.8c0-1.4,0.5-2.8,1.5-3.4c0.1-1.2,0.8-2.1,2-2.6l0.2-0.1l0-3.1c0-0.1,0-0.1,0-0.2l0-0.1c0.1-1.1,1.3-1.7,3.3-1.7 C8.1,2.7,8.3,2.7,8.4,2.7z" />
		<path fill={primary} d="M8.2,2c0.1,0,0.3,0,0.4,0c0.4,0,0.8,0,1.2,0.1c1.6,0.2,3.7,0.8,4.5,1.8c0.3,0.4,0.4,0.8,0.4,1l0,0.1 v1.6l0.5,0c3.2,0.4,7.3,1.3,7.4,3.7c0.9,0.5,1.4,1.8,1.6,3.6v3c0,3.4-4.6,6-12,6c-7.3,0-11.8-2.5-12-5.8L0,17c0-1,0-2,0-3 c0-1.5,0.5-3,1.6-3.6C1.8,9,2.5,8.1,3.8,7.5L4,7.5l0-3.3C4,4.1,4,4,4,4l0-0.1C4.2,2.7,5.5,2.1,7.6,2C7.8,2,8,2,8.2,2z M21.5,12.2 l0,1.1l0,0.3c-0.4,2-4.6,3.4-9.7,3.4c-5,0-9.2-1.3-9.7-3.3L2,14l0,0.3V17c0,1.9,3.7,4,10,4c6.2,0,9.8-2,10-3.9l0-0.1v-2.8l0-0.2 C22,13.4,22,12.5,21.5,12.2z M11.7,14c-3.8,0-7.2-0.7-8.7-1.9l0,1.3c0.3,1.3,4.1,2.6,8.8,2.6c4.6,0,8.3-1.2,8.7-2.4l0-0.1l0-1.4 C18.9,13.3,15.6,14,11.7,14z M14,8.1l0,2l0,0.1c-0.2,1.2-1.5,1.9-3.8,1.9c-0.5,0-1,0-1.5-0.1l-0.4-0.1C6.4,11.5,4.4,10.8,4,9.4 c-0.6,0.3-0.9,0.7-1,1l0,0.1l0,0.1c0.3,1.2,3.9,2.3,8.4,2.3l0.3,0c4.8,0,8.7-1.3,8.7-2.5C20.5,9.5,17.7,8.4,14,8.1z M8.2,3 C6.6,3,5.2,3.4,5,3.9L5,4L5,4l0,0.1v5c0,0.8,1.7,1.6,3.8,1.8C9.3,11,9.7,11,10.2,11c1.5,0,2.6-0.3,2.8-0.9l0-0.1V5 c0.1-0.8-1.6-1.6-3.8-1.9C8.8,3,8.5,3,8.2,3L8.2,3z M9.1,3.9c1.7,0.2,2.9,0.8,2.9,1.2c-0.1,0.5-1.5,0.7-3.1,0.5L8.5,5.6 C7,5.3,5.9,4.8,6,4.4C6.1,3.9,7.5,3.7,9.1,3.9z" />
	</svg>
);

BhcHighSpeedLidSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const BhcReductionLidSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>bhcReductionLid</title>
		<path fill={tertiary} d="M12,4.5l0.6,0c2.7,0.1,9,0.5,9.1,3.7c1,0.3,1.5,1.5,1.5,3.4v2.8c0,3.2-4.3,5.6-11.2,5.6 c-6.8,0-11-2.3-11.2-5.4l0-0.2v-2.8c0-1.9,0.5-3,1.5-3.4c0-0.8,0.6-1.9,3-2.7l0.2-0.1C7.3,4.9,9.6,4.5,12,4.5z" />
		<path fill={primary} d="M12,4l0.6,0c2.9,0.1,9.7,0.5,9.8,3.9c1.1,0.4,1.6,1.6,1.6,3.6v3c0,3.4-4.6,6-12,6 c-7.3,0-11.8-2.5-12-5.8l0-0.2v-3c0-2,0.5-3.2,1.6-3.6c0.1-0.9,0.7-2,3.2-2.9L5.1,5C7,4.3,9.4,4,12,4z M2,11.5l0,0.1l0,0.2v2.7 c0,1.9,3.7,4,10,4c6.2,0,9.8-2,10-3.9l0-0.1v-2.8l0-0.2c0-0.4,0-0.6-0.1-0.7l0,0.2c-0.4,2-4.7,3.4-9.9,3.4c-5.1,0-9.3-1.3-9.9-3.2 l0-0.1l0-0.1c0-0.1,0,0-0.1,0.3L2,11.5z M20.9,9.7c-1.6,1.2-5,1.9-8.9,1.9c-3.9,0-7.3-0.7-8.9-1.9l0,1.3l0,0.1 c0.4,1.2,3.9,2.4,8.2,2.5l0.3,0l0.3,0c4.6,0,8.5-1.2,8.9-2.4l0-0.1L20.9,9.7z M20.6,7.5l-3.8,1.1C16.9,8.8,17,8.9,17,9.1 c0,0.2-0.2,0.5-0.5,0.7l1.2,0.2l0.5-0.1c1.7-0.5,2.7-1.1,2.7-1.7C20.9,7.9,20.8,7.7,20.6,7.5z M3.5,7.4C3.3,7.6,3.2,7.8,3.2,8l0,0.1 l0,0.1c0.2,0.6,1.2,1.2,2.7,1.6l1.5-0.2C7.1,9.4,7,9.2,7,9.1c0-0.2,0.1-0.3,0.3-0.5L3.5,7.4z M15.7,5.8l-1.6,1.9 c0.5,0.1,0.9,0.2,1.3,0.3l4-1.2c-0.8-0.4-2-0.7-3.4-0.9L15.7,5.8z M8.9,5.7C7.4,5.9,6.1,6.2,5.1,6.5C4.9,6.6,4.8,6.7,4.7,6.7l4,1.2 c0.4-0.1,1-0.2,1.5-0.3L8.9,5.7z M12,5.6l-0.3,0c-0.6,0-1.1,0-1.7,0.1l1.3,1.9c0.2,0,0.5,0,0.7,0c0.3,0,0.7,0,1,0l1.6-1.9 c-0.6-0.1-1.3-0.1-2-0.1L12,5.6z" />
	</svg>
);

BhcReductionLidSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const BluetoothSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>bluetooth</title>
		<path fill={primary} d="M13,2.9v7.4l4.2-3.9L13,2.9z M13,14v7.1l4.2-3.5L13,14z M12.2,24c-0.2,0-0.4,0-0.5-0.1 c-0.4-0.2-0.7-0.6-0.7-1.1v-7.9l-5.3,4.9c-0.4,0.4-1,0.3-1.4-0.1c-0.4-0.4-0.3-1,0.1-1.4l6.5-6L4.4,6.8c-0.4-0.4-0.5-1-0.1-1.4 c0.4-0.4,1-0.5,1.4-0.1L11,9.7V1.2c0-0.5,0.3-0.9,0.7-1.1C12.1-0.1,12.6,0,13,0.3l5.9,5c0.3,0.3,0.5,0.7,0.5,1.1 c0,0.4-0.2,0.8-0.5,1.1l-5.1,4.7l5.1,4.3c0.4,0.3,0.5,0.7,0.5,1.2c0,0.4-0.2,0.8-0.5,1.1l-5.9,5C12.8,23.9,12.5,24,12.2,24L12.2,24z " />
	</svg>
);

export const BookmarkSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>bookmark</title>
		<path fill={primary} d="M16.2,2C18.3,2,20,3.8,20,6v13.6c0,1.1-0.8,2-1.9,2c-0.3,0-0.6-0.1-0.9-0.3L13,18.9 c-0.6-0.3-1.2-0.3-1.8,0l-4.5,2.5c-0.9,0.5-2.1,0.2-2.5-0.8C4.1,20.3,4,20,4,19.7V6c0-2.2,1.7-4,3.8-4H16.2z M16.2,4H7.8 C6.9,4,6.1,4.8,6,5.9L6,6v13.7l4.4-2.5c1-0.6,2.3-0.6,3.3-0.1l0.2,0.1l4.1,2.5V6c0-1.1-0.8-1.9-1.7-2L16.2,4z" />
	</svg>
);

export const BrightnessSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>brightness</title>
		<path fill={primary} d="M12,18c0.6,0,1,0.4,1,1v2c0,0.6-0.4,1-1,1s-1-0.4-1-1v-2c0-0.5,0.4-0.9,0.9-1L12,18z M7.7,16.2 L7.7,16.2c0.5,0.5,0.5,1.1,0.1,1.5l-1.4,1.4c-0.4,0.4-1,0.4-1.4,0c-0.4-0.4-0.4-1,0-1.4l1.4-1.4C6.7,15.9,7.3,15.9,7.7,16.2z  M17.7,16.2l1.4,1.4c0.4,0.4,0.4,1,0,1.4c-0.4,0.4-1,0.4-1.4,0l-1.4-1.4c-0.4-0.4-0.4-0.9-0.1-1.3l0.1-0.1 C16.6,15.9,17.3,15.9,17.7,16.2z M12,8c2.2,0,4,1.8,4,4s-1.8,4-4,4s-4-1.8-4-4S9.8,8,12,8z M5,11c0.5,0,0.9,0.4,1,0.9L6,12 c0,0.6-0.4,1-1,1H3c-0.6,0-1-0.4-1-1s0.4-1,1-1H5z M21,11c0.6,0,1,0.4,1,1s-0.4,1-1,1h-2c-0.5,0-0.9-0.4-1-0.9l0-0.1 c0-0.6,0.4-1,1-1H21z M6.3,4.9l1.4,1.4c0.4,0.4,0.4,0.9,0.1,1.3L7.8,7.8c-0.4,0.4-1,0.4-1.4,0L4.9,6.3c-0.4-0.4-0.4-1,0-1.4 C5.3,4.5,6,4.5,6.3,4.9z M19.1,4.9c0.4,0.4,0.4,1,0,1.4l-1.4,1.4c-0.4,0.4-0.9,0.4-1.3,0.1l-0.1-0.1c-0.4-0.4-0.4-1,0-1.4l1.4-1.4 C18,4.5,18.7,4.5,19.1,4.9z M12,2c0.6,0,1,0.4,1,1v2c0,0.5-0.4,0.9-0.9,1L12,6c-0.6,0-1-0.4-1-1V3C11,2.4,11.4,2,12,2z" />
	</svg>
);

export const BrightnessContrastSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>brightnessContrast</title>
		<path fill={tertiary} d="M17,12c0-2.8-2.2-5-5-5s-5,2.2-5,5s2.2,5,5,5S17,14.8,17,12" />
		<path fill={primary} d="M12.5,18c0.5,0,0.9,0.4,1,0.9l0,0.1v2c0,0.6-0.4,1-1,1c-0.5,0-0.9-0.4-1-0.9l0-0.1v-2 C11.5,18.4,11.9,18,12.5,18z M17.8,16.8L17.8,16.8l1.3,1.3c0.4,0.4,0.4,1,0,1.4c-0.4,0.4-0.9,0.4-1.3,0.1l-0.1-0.1l-1.2-1.2 c-0.4-0.4-0.4-1,0-1.4C16.8,16.5,17.4,16.5,17.8,16.8z M7.4,16.7c0.4,0.4,0.4,0.9,0.1,1.3l-0.1,0.1l-1.2,1.2c-0.4,0.4-1,0.4-1.4,0 C4.4,19,4.4,18.4,4.7,18l0.1-0.1L6,16.7C6.4,16.3,7,16.3,7.4,16.7z M12,6.5c3,0,5.5,2.5,5.5,5.5c0,3-2.5,5.5-5.5,5.5 c-3,0-5.5-2.5-5.5-5.5C6.5,9,9,6.5,12,6.5z M12,7.5L12,7.5c-2.5,0-4.5,2-4.5,4.5c0,2.5,2,4.5,4.5,4.5h0L12,7.5z M4.7,11.8 c0.6,0,1,0.4,1,1c0,0.5-0.4,0.9-0.9,1l-0.1,0H3c-0.6,0-1-0.4-1-1c0-0.5,0.4-0.9,0.9-1l0.1,0H4.7z M21,11.5c0.6,0,1,0.4,1,1 c0,0.5-0.4,0.9-0.9,1l-0.1,0h-2c-0.6,0-1-0.4-1-1c0-0.5,0.4-0.9,0.9-1l0.1,0H21z M6,5.1L6,5.1l1.3,1.3c0.4,0.4,0.4,1,0,1.4 C6.9,8.1,6.4,8.2,6,7.9L5.9,7.8L4.7,6.6c-0.4-0.4-0.4-1,0-1.4C5.1,4.8,5.6,4.8,6,5.1z M19.2,4.9c0.4,0.4,0.4,0.9,0.1,1.3l-0.1,0.1 L18,7.5c-0.4,0.4-1,0.4-1.4,0c-0.4-0.4-0.4-0.9-0.1-1.3l0.1-0.1l1.2-1.2C18.2,4.5,18.8,4.5,19.2,4.9z M12.5,2c0.5,0,0.9,0.4,1,0.9 l0,0.1v2c0,0.6-0.4,1-1,1c-0.5,0-0.9-0.4-1-0.9l0-0.1V3C11.5,2.4,11.9,2,12.5,2z" />
	</svg>
);

BrightnessContrastSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ChefSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>chef</title>
		<path fill={primary} d="M8,20v-2h9v2H8z M10.5,4.1C10.1,4.1,9.7,4,9.3,4C6.4,4,4,6.2,4,9c0,1.2,0.5,2.3,1.3,3.2L6.1,22 h12.8l1-11.3c0.7-0.9,1.1-2,1.1-3.2c0-3-2.6-5.5-5.8-5.5C13.3,2,11.6,2.8,10.5,4.1z M17.1,17H15v-4h-0.9v4L11,17v-4h-1v4H7.8 l-0.5-4.6l0-0.5l-0.3-0.4C6.5,11.1,6,10.4,6,9.4C6,7.5,7.7,6,9.8,6c0.3,0,0.6,0,0.9,0.1l0.9,0.2l0.6-0.7c0.8-1,1.8-1.2,3.2-1.2 C17.8,4.4,19,5.8,19,8c0,0.8-0.4,1.4-0.9,2.1L17.1,17z" />
		<path fill={tertiary} d="M17.1,17H15v-4h-0.9v4L11,17v-4h-1v4H7.8l-0.5-4.6l0-0.5l-0.3-0.4C6.5,11.1,6,10.4,6,9.4 C6,7.5,7.7,6,9.8,6c0.3,0,0.6,0,0.9,0.1l0.9,0.2l0.6-0.7c0.8-1,1.8-1.2,3.2-1.2C17.8,4.4,19,5.8,19,8c0,0.8-0.4,1.4-0.9,2.1L17.1,17 z" />
		<rect fill={secondary} x="8" y="18" width="9" height="2" />
	</svg>
);

ChefSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const CoffeeExtractionSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>coffeeExtraction</title>
		<path fill={primary} d="M5.2,4C5,4.3,4.8,4.7,4.6,5C4.4,5.3,4.3,5.7,4.2,6L3.3,6C2.6,6,2.1,6.5,2,7.1l0,0.1v4.5 c0,2.2,0.9,4.3,2.5,5.9c1.6,1.5,3.7,2.4,6,2.4s4.4-0.8,6-2.4c1.6-1.5,2.5-3.5,2.5-5.6l0-0.3V7.2c0-0.6-0.5-1.2-1.1-1.2l-0.1,0 l-5.9,0c-0.1-0.3-0.3-0.7-0.5-1c-0.2-0.3-0.4-0.7-0.6-1l7,0C19.5,4,21,5.3,21,7c1.8,0.5,3,2.3,3,4.3c0,2.2-1.7,4-3.9,4.2 c-0.5,1.3-1.3,2.5-2.4,3.6c-2,1.9-4.5,3-7.3,3c-2.7,0-5.3-1-7.3-3c-1.9-1.9-3-4.4-3.1-7l0-0.3V7c0-1.6,1.3-2.9,2.9-3L3,4L5.2,4z  M17,13c-0.6,2.8-3.3,5-6.5,5c-3.2,0-5.9-2.2-6.5-5H17z M21,9v4c0.5,0,1-0.8,1-2C22,9.9,21.5,9,21,9z M8,2l0.7,1.1l0.6,1l0.4,0.6 c0.3,0.4,0.5,0.7,0.7,1.1c0.5,0.7,0.6,1.4,0.4,2.1C10.6,9.1,9.3,10,8,10c-1.3,0-2.6-0.9-2.9-2.1C4.9,7.2,5,6.5,5.5,5.8l0.4-0.6 l0.9-1.3C7,3.5,7.6,2.7,8,2z" />
	</svg>
);

export const CoretempSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>coretemp</title>
		<path fill={secondary === colors.transparent ? primary : secondary} d="M21.6,17.5C21.6,17.5,21.6,17.5,21.6,17.5c0,1.7-0.1,1.6-0.7,2.2c-1.1,0.9-2.7,1.3-4.4,1.5c-0.4,0-1.1,0.1-1.2,0.1c-1.6,0-3.1-0.5-4.1-1.3c-0.3-0.3-0.5-0.7-0.5-1.2l0-0.3c1.2,0.9,2.8,1.4,4.6,1.4c0.4,0,0.8,0,1.2-0.1c1.8-0.2,3.5-0.9,4.7-1.9C21.3,17.8,21.4,17.7,21.6,17.5z M8.5,6.3C7.7,6.3,6.7,7,6.7,7.8v4.9c-1.2,0.6-2.1,1.8-2.1,3.2c0,1.4,1.3,2.6,2.5,3.2v-2.6c0-0.5,0.3-1.4,0.4-1.5c0.4-0.7,1.7-1.8,2.6-2.3l0.3-4.8C10.3,7,9.3,6.3,8.5,6.3z" />
		<path fill={tertiary} d="M15.1,19.7c-1.7,0-3.2-0.4-4.3-1C9.9,18.1,9.6,17,10.1,16c0.4-0.7,1.1-1.1,1.8-1.2l0.1,0l1.7,0l0.1,0c0,0,0.1,0,0.1,0c0.3,0,0.6-0.2,0.9-0.4c0.4-0.4,1.2-1,2.6-1.1c0.2,0,0.4,0,0.6,0c0.4,0,0.8,0,1.2,0.1c1.2,0.2,2.1,1,2.4,2.1c0.3,1.1-0.2,2.2-1.2,2.9c-1.1,0.7-2.6,1.2-4.3,1.3C15.9,19.7,15.5,19.7,15.1,19.7 M12.1,10.6l-0.4-0.5l-0.4-5.4c0-1.6-1.8-2.3-2.8-2.3S5.7,3.3,5.7,4.9l-0.5,5.4l-0.4,0.5c-1.6,1-2.2,3.1-2.2,4.9c0,2.5,2.1,4.4,4.5,5C7,20.6,7,20.3,7,20c0,0,0,0,0,0V19c-1.2-0.6-2-1.8-2-3.2c0-1.4,0.8-2.6,2-3.2V8.9c0-0.8,0.7-1.5,1.5-1.5S10,8.1,10,8.9v3.7c0.2-0.1,0.4-0.2,0.6-0.2c0.1,0,0.1,0,0.2,0l2.2-0.1C12.6,11.8,12.7,11,12.1,10.6z" />
		<path fill={primary} d="M22.9,14.7c-0.4-1.4-1.6-2.4-3.2-2.6C19,12,18.3,12,17.7,12c-1.7,0.2-2.7,0.9-3.2,1.4c-0.1,0.1-0.2,0.2-0.4,0.2l0,0l-0.1,0l-1.8,0l-0.2,0c-1,0.1-2,0.7-2.5,1.6c0,0-0.4,0.9-0.4,1.5l0,2.5c0,1,0.5,2,1.4,2.6c1.2,0.8,3,1.2,4.8,1.2h0l0,0c0,0,0.8-0.1,1.2-0.1c1.9-0.2,3.6-0.7,4.9-1.5c1.4-0.9,1.6-1.9,1.6-3.5v-2.5C23,15.1,23,14.9,22.9,14.7z M20.3,19.7c-1,0.6-2.4,1.1-4,1.2c-0.3,0-1,0.1-1.1,0.1c-1.5,0-2.8-0.3-3.7-0.9c-0.3-0.2-0.5-0.5-0.5-0.9l0-0.2c1.1,0.6,2.5,1,4.2,1c0.4,0,0.7,0,1.1,0c1.6-0.2,3.2-0.6,4.3-1.3c0.2-0.1,0.3-0.2,0.4-0.3c0,0,0,0,0,0C21,19.5,20.9,19.3,20.3,19.7z M20.3,18.2c-1,0.7-2.5,1.1-4,1.3c-0.3,0-0.7,0-1,0c-1.6,0-3-0.3-4-0.9c-0.9-0.5-1.2-1.6-0.7-2.5c0.4-0.6,1-1,1.7-1.1l0.1,0l1.6,0l0.1,0c0,0,0.1,0,0.1,0c0.3,0,0.6-0.2,0.8-0.4c0.4-0.4,1.2-0.9,2.5-1c0.2,0,0.4,0,0.6,0c0.4,0,0.7,0,1.1,0.1c1.1,0.2,2,0.9,2.3,1.9C21.7,16.6,21.2,17.6,20.3,18.2z M14.9,11.6C14.9,11.6,14.9,11.6,14.9,11.6c-0.4,0.4-0.9,0.6-1.4,0.7c-0.1,0-0.2,0-0.2,0c0,0-0.1,0-0.1,0l0,0c0,0,0,0,0,0h0l-0.1,0c-0.4-0.6-0.9-1.1-1.5-1.5L11,10.6V5c0-1.6-1.6-2-2.5-2S6,3.4,6,5v5.6l-0.5,0.3c-1.6,1-2.5,2.8-2.5,4.6c0,2.5,1.7,4.7,4.1,5.3c0.2,0.7,0.6,1.3,1.2,1.7c0.3,0.2,0.6,0.3,0.9,0.5c-0.2,0-0.4,0-0.6,0C4.4,23,1,19.6,1,15.5c0-2.4,1.1-4.6,3-6V5c0-2.6,2.3-4,4.5-4h0C10.7,1,13,2.4,13,5v4.5C13.8,10.1,14.4,10.8,14.9,11.6z" />
	</svg>
);

CoretempSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const DatetimeSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>datetime</title>
		<circle fill={tertiary} cx="12" cy="12" r="9" />
		<path fill={secondary} d="M17.7,3.8c1.1,1.6,1.8,3.6,1.8,5.7c0,5.5-4.5,10-10,10c-2.1,0-4.1-0.7-5.7-1.8 C5.6,20.3,8.6,22,12,22c5.5,0,10-4.5,10-10C22,8.6,20.3,5.6,17.7,3.8" />
		{/* WHAT FILL IS THIS */}
		<path fill={primary} d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2 M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8 S7.6,4,12,4 M12,6c0.5,0,0.9,0.4,1,0.9L13,7v4.1l2.6,2.1c0.4,0.3,0.5,0.9,0.2,1.3l-0.1,0.1c-0.3,0.4-0.9,0.5-1.3,0.2l-0.1-0.1 l-3-2.4c-0.2-0.2-0.3-0.4-0.4-0.6l0-0.1V7C11,6.4,11.4,6,12,6z" />
	</svg>
);

DatetimeSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const DemomodeSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>demomode</title>
		<polygon fill={tertiary} points="4,15 4,4 20,4 20,15 " />
		<path fill={primary} d="M20,2c1.1,0,2,0.9,2,2l0,0v11c0,1.1-0.9,2-2,2l0,0h-5l3,5h-1.8c-0.4,0-0.7-0.2-0.9-0.5l0,0 L13,17.7V21c0,0.6-0.4,1-1,1s-1-0.4-1-1l0,0v-3.3l-2.3,3.8C8.5,21.8,8.2,22,7.8,22l0,0H6l3-5H4c-1.1,0-2-0.9-2-2l0,0V4 c0-1.1,0.9-2,2-2l0,0H20z M20,4H4l0,11l16,0L20,4z" />
	</svg>
);

DemomodeSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const DescaleSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>descale</title>
		<path fill={primary} d="M16.7,11.5c0.5,0.2,1.1,0.4,1.6,0.6c0.3,0.1,0.5,0,0.6-0.3c0.2-0.6,0.3-1.2,0.5-1.8c0.1-0.4,0-0.6-0.3-0.7c-0.5-0.2-1.1-0.4-1.6-0.6c-0.3-0.1-0.5,0-0.6,0.3c-0.2,0.6-0.3,1.2-0.5,1.8C16.2,11.2,16.3,11.4,16.7,11.5 M13.5,8.6c0.6-0.2,1.1-0.5,1.7-0.7c0.3-0.1,0.4-0.3,0.3-0.7c-0.2-0.6-0.5-1.1-0.7-1.7c-0.1-0.3-0.3-0.4-0.7-0.3c-0.6,0.2-1.1,0.5-1.7,0.7C12.1,6,12,6.2,12.1,6.6c0.2,0.6,0.5,1.1,0.7,1.7C12.9,8.7,13.1,8.8,13.5,8.6 M2.5,21h1.9C4.8,21,5,20.8,5,20.5v-1.9C5,18.2,4.8,18,4.5,18h-2C2.2,18,2,18.2,2,18.5v1.9C2,20.8,2.2,21,2.5,21 M2.5,9h1.9C4.8,9,5,8.8,5,8.5v-2C5,6.2,4.8,6,4.5,6h-2C2.2,6,2,6.2,2,6.5v1.9C2,8.8,2.2,9,2.5,9 M2.5,13h1.9C4.8,13,5,12.8,5,12.5v-1.9C5,10.2,4.8,10,4.5,10h-2C2.2,10,2,10.2,2,10.5v1.9C2,12.8,2.2,13,2.5,13 M2.5,17h1.9C4.8,17,5,16.8,5,16.5v-1.9C5,14.2,4.8,14,4.5,14h-2C2.2,14,2,14.2,2,14.5v1.9C2,16.8,2.2,17,2.5,17 M6.5,20.9h1.9c0.4,0,0.5-0.2,0.5-0.5v-1.9C9,18.2,8.8,18,8.5,18h-2C6.2,18,6,18.2,6,18.5v1.9C6,20.8,6.2,20.9,6.5,20.9 M6.5,9h1.9C8.8,9,9,8.8,9,8.5v-2C9,6.2,8.8,6,8.5,6h-2C6.2,6,6,6.2,6,6.5v1.9C6,8.8,6.2,9,6.5,9 M6.5,13h1.9C8.8,13,9,12.8,9,12.5v-1.9C9,10.2,8.8,10,8.5,10h-2C6.2,10,6,10.2,6,10.5v1.9C6,12.8,6.2,13,6.5,13 M6.5,17h1.9C8.8,17,9,16.8,9,16.5v-1.9C9,14.2,8.8,14,8.5,14h-2C6.2,14,6,14.2,6,14.5v1.9C6,16.8,6.2,17,6.5,17 M10.5,20.9h1.9c0.4,0,0.5-0.2,0.5-0.5v-1.9c0-0.4-0.2-0.5-0.5-0.5h-1.9c-0.4,0-0.5,0.2-0.5,0.5v1.9C10,20.8,10.2,20.9,10.5,20.9 M10.5,13h1.9c0.4,0,0.5-0.2,0.5-0.5v-1.9c0-0.4-0.2-0.5-0.5-0.5h-1.9c-0.4,0-0.5,0.2-0.5,0.5v1.9C10,12.8,10.2,13,10.5,13 M10.5,17h1.9c0.4,0,0.5-0.2,0.5-0.5v-1.9c0-0.4-0.2-0.5-0.5-0.5h-1.9c-0.4,0-0.5,0.2-0.5,0.5v1.9C10,16.8,10.2,17,10.5,17 M14.5,20.9h1.9c0.4,0,0.5-0.2,0.5-0.5v-1.9c0-0.4-0.2-0.5-0.5-0.5h-1.9c-0.4,0-0.5,0.2-0.5,0.5v1.9C14,20.8,14.2,20.9,14.5,20.9 M14.5,17h1.9c0.4,0,0.5-0.2,0.5-0.5v-1.9c0-0.4-0.2-0.5-0.5-0.5h-1.9c-0.4,0-0.5,0.2-0.5,0.5v1.9C14,16.8,14.2,17,14.5,17" />
		<path fill={secondary === colors.transparent ? primary : secondary} d="M18.3,5.6c0.5,0.3,1.1,0.5,1.6,0.8c0.3,0.2,0.5,0.1,0.7-0.2c0.3-0.5,0.5-1.1,0.8-1.6c0.2-0.3,0.1-0.5-0.2-0.7c-0.5-0.3-1.1-0.5-1.6-0.8C19.2,2.9,19,3,18.9,3.3c-0.3,0.5-0.5,1.1-0.8,1.6C17.9,5.3,18,5.5,18.3,5.6" />
	</svg>
);

DescaleSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];


export const DisplaySvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>display</title>
		<path fill={primary} d="M17.5,20c0.3,0,0.5,0.2,0.5,0.5v1c0,0.3-0.2,0.5-0.5,0.5S17,21.8,17,21.5v-1 C17,20.3,17.2,20.1,17.5,20L17.5,20z M21,3l1,1l-0.1,0.1L4,22l-1-1v0L21,3L21,3z M15.7,19.1L15.7,19.1c0.3,0.3,0.3,0.7,0.1,0.9 L15,20.8c-0.2,0.2-0.6,0.2-0.8,0s-0.2-0.6,0-0.8l0.8-0.8C15.2,19,15.5,18.9,15.7,19.1z M20,19.2l0.8,0.8c0.2,0.2,0.2,0.6,0,0.8 s-0.6,0.2-0.8,0L19.2,20c-0.2-0.2-0.2-0.5-0.1-0.7l0.1-0.1C19.4,18.9,19.8,18.9,20,19.2z M17.5,16c0.8,0,1.5,0.7,1.5,1.5 S18.3,19,17.5,19S16,18.3,16,17.5S16.7,16,17.5,16z M14.5,17c0.2,0,0.4,0.2,0.5,0.4l0,0.1c0,0.3-0.2,0.5-0.5,0.5h-1 c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5H14.5z M21.5,17c0.3,0,0.5,0.2,0.5,0.5S21.8,18,21.5,18h-1c-0.2,0-0.4-0.2-0.5-0.4l0-0.1 c0-0.3,0.2-0.5,0.5-0.5H21.5z M15,14.2l0.8,0.8c0.2,0.2,0.2,0.5,0.1,0.7l-0.1,0.1c-0.2,0.2-0.6,0.2-0.8,0L14.2,15 c-0.2-0.2-0.2-0.6,0-0.8S14.8,13.9,15,14.2z M20.8,14.2c0.2,0.2,0.2,0.6,0,0.8L20,15.8c-0.2,0.2-0.5,0.2-0.7,0.1l-0.1-0.1 c-0.2-0.2-0.2-0.6,0-0.8l0.8-0.8C20.2,13.9,20.6,13.9,20.8,14.2z M17.5,13c0.3,0,0.5,0.2,0.5,0.5v1c0,0.2-0.2,0.4-0.4,0.5l-0.1,0 c-0.3,0-0.5-0.2-0.5-0.5v-1C17,13.2,17.2,13,17.5,13z M6.9,2v9L4.3,8.6H2V4.4h2.3L6.9,2z M9.8,3.1C10.4,3.9,11,5.4,11,6.6 c0,1.2-0.5,2.6-1.1,3.4C9.7,9.9,9.3,9.6,9.2,9.4C9.7,8.7,10,7.7,10,6.6c0-1-0.4-2.1-0.9-2.8L9,3.6l0.5-0.4L9.8,3.1z M8.6,4.4 C8.8,4.8,9,5.9,9,6.6c0,0.7-0.2,1.6-0.4,2C8.5,8.5,7.9,8.1,7.9,8C8,7.7,8.2,7.1,8.2,6.6c0-0.5-0.2-1-0.4-1.4L7.8,5.1L8.6,4.4z" />
	</svg>
);


export const DotsSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>dots</title>
		<path fill={primary} d="M5,10c1.1,0,2,0.9,2,2s-0.9,2-2,2s-2-0.9-2-2S3.9,10,5,10z M12,10c1.1,0,2,0.9,2,2s-0.9,2-2,2s-2-0.9-2-2 S10.9,10,12,10z M19,10c1.1,0,2,0.9,2,2s-0.9,2-2,2s-2-0.9-2-2S17.9,10,19,10z" />
	</svg>
);

export const DrinkReadySvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>drinkReady</title>
		<path fill={primary} d="M13.8,4l-1.4,2L3.3,6C2.6,6,2.1,6.5,2,7.1l0,0.1v4.5c0,2.2,0.9,4.3,2.5,5.9 c1.6,1.5,3.7,2.4,6,2.4s4.4-0.8,6-2.4c1.6-1.5,2.5-3.5,2.5-5.6l0-0.3V7.8l1.6-2.3C20.8,6,21,6.5,21,7c1.8,0.5,3,2.3,3,4.3 c0,2.2-1.7,4-3.9,4.2c-0.5,1.3-1.3,2.5-2.4,3.6c-2,1.9-4.5,3-7.3,3c-2.7,0-5.3-1-7.3-3c-1.9-1.9-3-4.4-3.1-7l0-0.3V7 c0-1.6,1.3-2.9,2.9-3L3,4L13.8,4z M18,2.4c0.2,0,0.5,0.1,0.7,0.2l0,0l0.1,0.1l0,0l0.1,0.1C19,2.9,19,3,19.1,3.2l0,0.1c0,0,0,0,0,0 c0.1,0.2,0.1,0.5,0,0.7l0,0.1c0,0.1-0.1,0.2-0.1,0.2L17.8,6h0l-5.5,7.8c-0.2,0.3-0.5,0.5-0.8,0.5l-0.1,0l0,0l-0.1,0l0,0 c-0.3,0-0.5-0.1-0.8-0.3l-3.7-2.8c-0.5-0.4-0.6-1.1-0.3-1.7l0,0l0,0c0.2-0.3,0.5-0.4,0.8-0.5l0.1,0l0,0c0.3,0,0.5,0.1,0.8,0.3l2.6,2 L16.2,4L17,3c0.2-0.3,0.5-0.5,0.9-0.5L18,2.4L18,2.4z M21,9v4c0.5,0,1-0.8,1-2C22,9.9,21.5,9,21,9z" />
	</svg>
);

export const EasySvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>easy</title>
		<path fill={secondary} d="M7.5,6.9l4.7,8.1c1.6,0.1,2.8,1.4,2.8,3l9,0c0-6.6-5.3-12-12-12C10.4,6,8.9,6.3,7.5,6.9z" />
		<path fill={tertiary} d="M9.8,13.5L6.4,7.4C2.6,9.4,0,13.4,0,18h7C7,16,8.2,14.3,9.8,13.5" />
		<path fill={primary} d="M7.5,6.9l1,1.8C9.6,8.2,10.8,8,12,8c4.8,0,8.9,3.5,9.8,8l-5.2,0c-0.8-1.8-2.5-3-4.6-3c-0.3,0-0.6,0-0.9,0.1 l1.1,1.9c1.6,0.1,2.8,1.4,2.8,3l9,0c0-6.6-5.3-12-12-12C10.4,6,8.9,6.3,7.5,6.9z M12.5,17.5L6.3,6.8C6,6.4,5.4,6.2,4.9,6.5 C4.5,6.7,4.3,7.4,4.6,7.8l0.3,0.5C1.9,10.6,0,14,0,18l9,0c0-0.6,0.2-1.2,0.5-1.7l1.3,2.2c0.2,0.3,0.5,0.5,0.9,0.5 c0.2,0,0.3,0,0.5-0.1C12.7,18.6,12.8,18,12.5,17.5z M7.4,16l-5.2,0c0.5-2.4,1.8-4.5,3.7-5.9l2.6,4.4C8,14.9,7.7,15.4,7.4,16z" />
	</svg>
);

EasySvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Med_medSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>med_med</title>
		<path fill={secondary} d="M1,18C1,11.9,5.9,7,12,7c2.1,0,4,0.6,5.6,1.5l-3.6,6c-0.6-0.4-1.3-0.6-2-0.6c-2.2,0-4,1.8-4,4H1z" />
		<path fill={tertiary} d="M14.8,16.9c0.1,0.3,0.2,0.7,0.2,1.1h9c0-3.7-1.7-7-4.3-9.2L14.8,16.9z" />
		<path fill={primary} d="M19.7,8.8l-1,1.8c1.6,1.4,2.7,3.3,3.2,5.5h-5.2c-0.2-0.4-0.4-0.7-0.6-1l-1.2,1.9c0.1,0.3,0.2,0.7,0.2,1.1h9 C24,14.3,22.3,11,19.7,8.8 M12,13c-2,0-3.8,1.2-4.6,3H2.2c0.9-4.6,5-8,9.8-8c1.5,0,3,0.4,4.3,1l-2.6,4.3C13.1,13.1,12.6,13,12,13  M19,6.5c-0.5-0.3-1.1-0.1-1.4,0.3l-0.3,0.5C15.7,6.5,13.9,6,12,6C5.4,6,0,11.4,0,18h9c0-1.7,1.3-3,3-3c0.2,0,0.4,0,0.6,0.1 l-1.4,2.3c-0.3,0.5-0.1,1.1,0.3,1.4c0.2,0.1,0.3,0.1,0.5,0.1c0.3,0,0.7-0.2,0.9-0.5l6.5-10.6C19.6,7.4,19.5,6.7,19,6.5" />
	</svg>
);

Med_medSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const DifficultSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>difficult</title>
		<path fill={secondary} d="M7.8,18c0-2.2,1.8-4,4-4c1.7,0,3.1,1.1,3.7,2.5l6.5-2.6C20.4,9.9,16.4,7,11.8,7 c-6.1,0-11,4.9-11,11H7.8z" />
		<path fill={tertiary} d="M20.2,16l-3.9,2H24c0-1.3-0.2-2.5-0.6-3.6L20.2,16z" />
		<path fill={primary} d="M23.4,14.4l-1.8,0.9c0.1,0.2,0.1,0.5,0.2,0.7h-1.6l-3.9,2H24C24,16.7,23.8,15.5,23.4,14.4 M12,13 c-2,0-3.8,1.2-4.6,3H2.2c0.9-4.6,5-8,9.8-8c3.5,0,6.6,1.8,8.3,4.5l-4.5,2.3C14.9,13.7,13.6,13,12,13 M23.9,11.8 c-0.3-0.5-0.9-0.7-1.3-0.4l-0.4,0.2C20,8.2,16.3,6,12,6C5.4,6,0,11.4,0,18h9c0-1.7,1.3-3,3-3c0.8,0,1.5,0.3,2,0.8l-2.5,1.3 c-0.5,0.3-0.7,0.9-0.4,1.3c0.2,0.3,0.5,0.5,0.9,0.5c0.2,0,0.3,0,0.5-0.1l11-5.7C23.9,12.9,24.1,12.3,23.9,11.8" />
	</svg>
);

DifficultSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const EditSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>edit</title>
		<polygon fill={secondary} points="9,16.3 15.2,10.5 16,11.2 9.8,17 " />
		<path fill={primary} d="M14.3,4.5l-9.8,9.7v5.3h5.3l9.7-9.8L14.3,4.5z M7,14.2l6.7-6.7l0.8,0.8L7.8,15L7,14.2z M9,16.3 l6.2-5.8l0.8,0.7L9.8,17L9,16.3z M5.5,15.1L5.6,15l3.9,3.4l-0.1,0.1H5.5V15.1z" />
	</svg>
);

EditSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const EmptyCycleSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>emptyCycle</title>
		<path fill={tertiary} d="M11.1,2.5l-0.6,1.1C9.7,5,8.9,6.5,8.4,7.2l-1.7,2.9l-0.6,1c-1.1,1.8-1.4,3.6-0.9,5.4 C5.9,19.7,8.8,22,12,22c3.2,0,6.1-2.3,6.8-5.5c0.4-1.9,0.1-3.6-0.9-5.4l-1.8-3l-0.7-1.2c-0.6-1.1-1.7-3-2.5-4.3 C12.5,1.8,11.5,1.8,11.1,2.5z" />
		<path fill={primary} d="M19.9,7.2c-0.2-0.2-0.5-0.3-0.7-0.1l0,0l-16,12l-0.1,0.1c-0.2,0.2-0.2,0.4,0,0.6 c0.2,0.2,0.5,0.3,0.7,0.1l0,0l2.1-1.6C7.1,20.5,9.5,22,12,22c3.2,0,6.1-2.3,6.8-5.5c0.4-1.8,0.2-3.4-0.8-5.1l-0.2-0.3l-0.6-1 l-0.1-0.2l2.6-2l0.1-0.1C20,7.7,20,7.4,19.9,7.2z M15.5,11.1l0.6,1c0.8,1.3,1,2.5,0.8,3.7l-0.1,0.2C16.3,18.3,14.2,20,12,20 c-1.9,0-3.6-1.2-4.5-2.9L15.5,11.1z M11.1,2.5c-0.7,1.2-1.7,3-2.3,4.1L8.6,6.8L7.9,8.1l-1.8,3c-0.9,1.6-1.3,3.2-1,4.8L7,14.5 c0.1-0.7,0.3-1.4,0.7-2.1l0.2-0.3l0.6-1l1.7-2.9c0.1-0.2,0.3-0.5,0.5-0.9l0.1-0.2L12,5l0.8,1.4c0.3,0.5,0.5,0.9,0.7,1.3l0.1,0.2 L14.3,9l1.6-1.2l-0.4-0.6c-0.3-0.5-0.9-1.5-1.4-2.5l-0.3-0.6l-0.2-0.3l0,0l-0.2-0.3l-0.6-1.1C12.5,1.8,11.5,1.8,11.1,2.5z" />
	</svg>
);

EmptyCycleSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const EnglishSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>english</title>
		<path fill={primary} d="M22,18v1H2v-1H22z M22,16v1H2v-1H22z M22,14v1H2v-1H22z M22,12v1H12v-1H22z M2,13V4h9v9H2z  M4,11H3v1h1V11z M6,11H5v1h1V11z M8,11H7v1h1V11z M10,11H9v1h1V11z M22,10v1H12v-1H22z M10,9H9v1h1V9z M8,9H7v1h1V9z M6,9H5v1h1V9z  M4,9H3v1h1V9z M22,8v1H12V8H22z M10,7H9v1h1V7z M8,7H7v1h1V7z M6,7H5v1h1V7z M4,7H3v1h1V7z M22,6v1H12V6H22z M10,5H9v1h1V5z M8,5H7 v1h1V5z M6,5H5v1h1V5z M4,5H3v1h1V5z M22,4v1H12V4H22z" />
	</svg>
);

export const EspañolSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>español</title>
		<path fill={primary} d="M2,19V4h20v15H2z M21,8H3v7h18V8z" />
		<polygon fill={tertiary} points="21,8 3,8 3,15 21,15 " />
	</svg>
);

EspañolSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ExternalSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>external</title>
		<path fill={secondary} d="M7,18c-1.1,0-1,0.1-1-1V6.5H5.5c-1.1,0-2,0.9-2,2v10c0,1.1,0.9,2,2,2h11c1.1,0,2-0.5,2-2V18H7z" />
		<path fill={primary} d="M16.5,21.5h-11c-1.7,0-3-1.3-3-3v-10c0-1.7,1.3-3,3-3h5c0.6,0,1,0.4,1,1s-0.4,1-1,1h-5c-0.6,0-1,0.4-1,1v10 c0,0.6,0.4,1,1,1h11c0.8,0,1-0.2,1-1v-5c0-0.6,0.4-1,1-1s1,0.4,1,1v5C19.5,20.4,18.4,21.5,16.5,21.5z M18,3h-3c-0.6,0-1,0.4-1,1 s0.4,1,1,1h2.6l-5.3,5.3c-0.4,0.4-0.4,1,0,1.4c0.2,0.2,0.5,0.3,0.7,0.3s0.5-0.1,0.7-0.3L19,6.4V9c0,0.6,0.4,1,1,1s1-0.4,1-1V6 C21,3.4,20.6,3,18,3z" />
	</svg>
);

ExternalSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

// cant get all compinations, need eye V3?
export const EyeV1Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>eye verion 1</title>
		<path fill={tertiary} d="M22.1,12c0,0-3-7-10-7s-10,7-10,7s3,7,10,7S22.1,12,22.1,12z" />
		<path fill={secondary} d="M15.8,13.5c-0.9,2-3.2,3-5.2,2.1c-2-0.9-3-3.2-2.1-5.2C11.2,11.6,12.1,12,15.8,13.5" />
		<path fill={primary} d="M12.1,20c-7.6,0-10.8-7.3-10.9-7.6L1,12l0.2-0.4C1.3,11.3,4.5,4,12.1,4s10.8,7.3,10.9,7.6l0.2,0.4L23,12.4 C22.9,12.7,19.7,20,12.1,20z M3.2,12c0.7,1.4,3.5,6,8.9,6c5.4,0,8.2-4.6,8.9-6c-0.7-1.4-3.5-6-8.9-6S3.9,10.6,3.2,12z M12.1,17 c-0.7,0-1.3-0.1-1.9-0.4c-2.5-1.1-3.7-4-2.7-6.5C8,8.8,9,7.9,10.2,7.4c1.2-0.5,2.6-0.5,3.8,0c1.2,0.5,2.2,1.5,2.7,2.7 c0.5,1.2,0.5,2.6,0,3.8c-0.5,1.2-1.5,2.2-2.7,2.7C13.4,16.9,12.7,17,12.1,17z M12.1,9c-0.4,0-0.8,0.1-1.1,0.2 c-0.7,0.3-1.3,0.9-1.6,1.6c-0.6,1.5,0.1,3.3,1.6,3.9c0.7,0.3,1.6,0.3,2.3,0c0.7-0.3,1.3-0.9,1.6-1.6h0c0.3-0.7,0.3-1.6,0-2.3 c-0.3-0.7-0.9-1.3-1.6-1.6C12.9,9.1,12.5,9,12.1,9z" />
	</svg>
);

EyeV1Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const EyeV2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>eye version 2</title>
		<circle fill={tertiary} cx="12.088" cy="12" r="5.97108"/>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M12.088 4C8.21985 4 5.4729 5.9428 3.7302 7.81955C2.85856 8.75824 2.22408 9.69378 1.80683 10.3948C1.59764 10.7462 1.44149 11.0413 1.33615 11.252C1.28344 11.3574 1.24333 11.4419 1.21557 11.5021C1.20168 11.5321 1.19086 11.5561 1.18309 11.5736L1.17371 11.5949L1.17072 11.6018L1.16964 11.6042L1.16921 11.6052C1.16903 11.6057 1.16885 11.6061 2.088 12L1.16885 11.6061L1.00003 12L1.16885 12.3939L2.088 12C1.16885 12.3939 1.16903 12.3943 1.16921 12.3948L1.16964 12.3958L1.17072 12.3982L1.17371 12.4051L1.18309 12.4264C1.19086 12.4439 1.20168 12.4679 1.21557 12.4979C1.24333 12.5581 1.28344 12.6426 1.33615 12.748C1.44149 12.9587 1.59764 13.2538 1.80683 13.6052C2.22408 14.3062 2.85856 15.2418 3.7302 16.1805C5.4729 18.0572 8.21985 20 12.088 20C15.9561 20 18.7031 18.0572 20.4458 16.1805C21.3174 15.2418 21.9519 14.3062 22.3692 13.6052C22.5784 13.2538 22.7345 12.9587 22.8398 12.748C22.8926 12.6426 22.9327 12.5581 22.9604 12.4979C22.9743 12.4679 22.9851 12.4439 22.9929 12.4264L23.0023 12.4051L23.0053 12.3982L23.0064 12.3958L23.0068 12.3948C23.007 12.3943 23.0071 12.3939 22.088 12L23.0071 12.3939L23.176 12L23.0071 11.6061L22.088 12C23.0071 11.6061 23.007 11.6057 23.0068 11.6052L23.0064 11.6042L23.0053 11.6018L23.0023 11.5949L22.9929 11.5736C22.9851 11.5561 22.9743 11.5321 22.9604 11.5021C22.9327 11.4419 22.8926 11.3574 22.8398 11.252C22.7345 11.0413 22.5784 10.7462 22.3692 10.3948C21.9519 9.69378 21.3174 8.75824 20.4458 7.81955C18.7031 5.9428 15.9561 4 12.088 4ZM14.0175 7.3885C16.567 8.45506 17.7672 11.3843 16.6998 13.9303C15.6321 16.4785 12.7034 17.6786 10.1573 16.6124C7.61007 15.545 6.40962 12.6148 7.4759 10.0685C8.54344 7.52092 11.4719 6.32126 14.0175 7.3885ZM14.8552 13.1583C15.4956 11.6306 14.7755 9.87307 13.2458 9.23313C11.7184 8.59278 9.96132 9.31258 9.32079 10.8412C8.68102 12.3689 9.40129 14.1271 10.9297 14.7675C12.4573 15.4072 14.2145 14.6871 14.8552 13.1583Z"/>
		<path fill={secondary === colors.transparent ? primary : secondary} fillRule="evenodd" clipRule="evenodd" d="M14.8548 13.159C13.5359 12.6071 12.6102 12.2193 11.3047 11.6724C10.7365 11.4344 10.0965 11.1663 9.32071 10.8413C9.32074 10.8413 9.32076 10.8412 9.32079 10.8412C9.96132 9.31258 11.7184 8.59278 13.2458 9.23313C14.7755 9.87307 15.4956 11.6306 14.8552 13.1583C14.8551 13.1585 14.8549 13.1588 14.8548 13.159Z"/>
	</svg>
);

EyeV2Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

//
// UP TO HERE
//

export const FactoryresetSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>factoryreset</title>
		<path fill={primary} d="M12,2c5.5,0,10,4.5,10,10s-4.5,10-10,10C6.5,22,2,17.5,2,12l0,0h2.2c0,4.3,3.5,7.8,7.8,7.8s7.8-3.5,7.8-7.8S16.3,4.2,12,4.2c-1.5,0-2.8,0.4-4,1.1l0,0L6.9,5.9L10,9H3V2l2.6,2.6C7.3,3.4,10,2,12,2z M12.5,15c0.3,0,0.5,0.2,0.5,0.5l0,0v1c0,0.3-0.2,0.5-0.5,0.5l0,0h-1c-0.3,0-0.5-0.2-0.5-0.5l0,0v-1c0-0.3,0.2-0.5,0.5-0.5l0,0H12.5z M12,7c0.6,0,1,0.4,1,1l0,0v5c0,0.6-0.4,1-1,1s-1-0.4-1-1l0,0V8C11,7.4,11.4,7,12,7z" />
		<polygon fill={secondary} points="4.5,5.5 6.5,7.5 4.5,7.5 " />
	</svg>
);

FactoryresetSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const FeedbackSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>feedback</title>
		<path fill={tertiary} d="M17.7,5.9C19,5.9,20,7,20,8.3v5.9c0,0.7,0,1.8-0.1,2.4c-0.9,0-1.7,0.4-2.3,1.1 c-0.5,0.6-0.9,1.4-1.1,2.3c-0.5-0.2-1-0.5-1-1c0-1.3-1-2.4-2.3-2.4H6.3C5,16.6,4,15.5,4,14.2V8.3C4,7,5,5.9,6.3,5.9H17.7" />
		<path fill={secondary} d="M19,13c0,1-1,2-2,2H5c0,1,0.8,2,1,2h6h2c0,0,1,3,3,3c-0.5-1.5,0-3,1-3c0.3,0,0.6-0.2,0.9-0.6 c0.6-0.7,1.1-2.1,1.1-3.4H19z" />
		<path fill={primary} d="M17,5H7C4.8,5,3,6.8,3,9v5c0,2.2,1.8,4,4,4h6c0,2,2,2.9,3.8,2.9c0.6,0,1.1-0.4,1.1-1c0-1.1,0.4-1.9,1.2-1.9 c2,0,2-2,2-4V9C21,6.8,19.2,5,17,5 M17,7c1.1,0,2,0.9,2,2v5c0,0.6,0,1.5-0.1,2c-0.8,0-1.5,0.3-2,0.9c-0.5,0.5-0.8,1.2-1,1.9 C15.4,18.7,15,18.4,15,18c0-1.1-0.9-2-2-2H7c-1.1,0-2-0.9-2-2V9c0-1.1,0.9-2,2-2H17 M16,10H8c-0.3,0-0.5-0.2-0.5-0.5S7.7,9,8,9h8 c0.3,0,0.5,0.2,0.5,0.5S16.3,10,16,10z M16,12H8c-0.3,0-0.5-0.2-0.5-0.5S7.7,11,8,11h8c0.3,0,0.5,0.2,0.5,0.5S16.3,12,16,12z M16,14 H8c-0.3,0-0.5-0.2-0.5-0.5S7.7,13,8,13h8c0.3,0,0.5,0.2,0.5,0.5S16.3,14,16,14z" />
	</svg>
);

FeedbackSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const FilterBasket1Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>filterBasket1</title>
		<path fill={primary} d="M22.1,6c0.3,0,0.5,0.1,0.7,0.3C22.9,6.5,23,6.7,23,7l0,0.1l-0.2,2.1c-0.2,2.2-0.6,4.9-0.6,5.2 c-0.2,1-0.5,1.7-1,2.3c-0.2,0.2-0.4,0.4-0.7,0.6c-0.6,0.4-1.3,0.7-2.1,0.7l-0.3,0H5.8c-0.7,0-1.4-0.2-2-0.5c-0.4-0.2-0.7-0.5-1-0.8 c-0.5-0.5-0.8-1.2-1-1.9l0-0.3c0-0.3-0.3-2.4-0.5-4.3l-0.1-1L1,7.1c0-0.3,0.1-0.6,0.2-0.8C1.4,6.1,1.6,6,1.8,6l0.1,0H22.1z M20.9,8 H3.1l0.1,1.1c0.1,0.8,0.2,1.7,0.3,2.7L3.6,13c0.1,0.5,0.1,0.9,0.2,1.2c0.1,0.5,0.3,0.9,0.5,1.2c0.1,0.2,0.3,0.3,0.5,0.4 C5,15.9,5.3,16,5.6,16l0.2,0h12.4c0.4,0,0.9-0.1,1.2-0.4c0.1-0.1,0.3-0.2,0.4-0.3c0.3-0.3,0.4-0.7,0.5-1.2l0.2-1.2l0.1-0.7 c0.1-1,0.2-2,0.3-2.8L20.9,8z" />
	</svg>
);

export const FilterBasket2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>filterBasket2</title>
		<path fill={primary} d="M16.3,10c0.2,0,0.4,0.1,0.5,0.2c0.1,0.1,0.2,0.3,0.2,0.5v0.1l-0.2,2 c-0.1,1.6-0.4,3.3-0.4,3.5c-0.1,0.7-0.4,1.3-0.7,1.7c-0.2,0.2-0.3,0.3-0.5,0.4c-0.4,0.3-0.9,0.5-1.5,0.5l-0.2,0h-9 c-0.5,0-1-0.1-1.4-0.4c-0.3-0.2-0.5-0.4-0.7-0.6c-0.4-0.4-0.6-1-0.7-1.6l-0.1-1c-0.1-0.7-0.2-1.6-0.3-2.5l0-0.4L1,10.8 c0-0.2,0-0.4,0.2-0.6C1.3,10.1,1.4,10,1.6,10l0.1,0H16.3z M14.9,12H3.1l0,0.3c0,0.5,0.1,1,0.1,1.5l0.1,0.9c0.1,0.6,0.1,1.1,0.2,1.4 c0,0.3,0.1,0.5,0.3,0.6c0.1,0.1,0.1,0.1,0.2,0.2C4.1,16.9,4.2,17,4.4,17l0.1,0h9c0.2,0,0.4-0.1,0.5-0.2c0.1,0,0.1-0.1,0.2-0.1 c0.1-0.1,0.2-0.3,0.3-0.6l0.2-1.2l0.1-1.1c0-0.4,0.1-0.8,0.1-1.2L14.9,12z M22.3,5c0.2,0,0.4,0.1,0.5,0.2C22.9,5.4,23,5.5,23,5.7 v0.1l-0.2,2c-0.1,1.6-0.4,3.3-0.4,3.5c-0.1,0.7-0.4,1.3-0.7,1.7c-0.2,0.2-0.3,0.3-0.5,0.4c-0.4,0.3-0.9,0.5-1.5,0.5l-0.2,0l-1.8,0 c0-0.1,0-0.3,0-0.4l0-0.5c0-0.4,0.1-0.7,0.1-1.1l1.6,0c0.2,0,0.4-0.1,0.5-0.2c0.1,0,0.1-0.1,0.2-0.1c0.1-0.1,0.2-0.3,0.3-0.6 l0.2-1.2l0.1-1.1c0-0.4,0.1-0.8,0.1-1.2l0-0.6H9.1l0,0.3c0,0.5,0.1,1,0.1,1.5l0,0.2l-2,0c0-0.4-0.1-0.8-0.1-1.2l0-0.4L7,5.8 c0-0.2,0-0.4,0.2-0.6C7.3,5.1,7.4,5,7.6,5l0.1,0H22.3z" />
	</svg>
);

export const FlameSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>flame</title>
		<path fill={primary} d="M11.6,2.3C10.1,8.5,4.1,10.2,4,16c0,3.4,2.6,5.3,6,6c-0.9-0.6-2-1.8-2-3c0-3,3.7-3.8,4-7 c0.3,3.2,4,4,4,7c0,1.2-1.1,2.4-2,3c3.4-0.7,6-2.6,6-6c-0.1-5.8-6.5-7.5-8-13.7C12,2.2,11.9,2,11.8,2C11.7,2,11.6,2.2,11.6,2.3" />
	</svg>
);

FlameSvg.colorVariation = [
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),	
];

export const FlameLeftSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>flameLeft</title>
		<path fill={primary} d="M12.5,2c-0.6-0.4-9.9,5.8-8.3,14.5c0.3,1.6,2.2,5.4,7,5.5c-1.6-0.5-2.2-1.9-2.3-2.5 c-0.8-4.3,4.1-7.1,4.1-7.1c-1,3.2,3.1,2.9,3.7,5.8c0.1,0.7,0,2.2-1.3,3.2c4.5-1.7,5-5.9,4.7-7.5c-1-5.5-9.5-6-7.3-11.3 C12.7,2.2,12.6,2.1,12.5,2" />
	</svg>
);

FlameLeftSvg.colorVariation = [
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),	
];

export const FlameRightSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>flameRight</title>
		<path fill={primary} d="M11.5,2c0.6-0.4,9.9,5.8,8.3,14.5c-0.3,1.6-2.2,5.4-7,5.5c1.6-0.5,2.2-1.9,2.3-2.5c0.8-4.3-4.1-7.1-4.1-7.1c1,3.2-3.1,2.9-3.7,5.8c-0.1,0.7,0,2.2,1.3,3.2c-4.5-1.7-5-5.9-4.7-7.5c1-5.5,9.5-6,7.3-11.3C11.3,2.2,11.4,2.1,11.5,2" />
	</svg>
);

FlameRightSvg.colorVariation = [
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),	
];

export const FloWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>flow</title>
		<path fill={secondary} d="M11.9991 12C11.0284 12 10.2006 11.3048 10.0311 10.3465C9.93796 9.81935 10.0555 9.28867 10.3625 8.8513C10.6682 8.41482 11.126 8.12294 11.6522 8.03051C11.768 8.01061 11.8851 8 12.0013 8C12.972 8 13.7998 8.69519 13.9688 9.65351C14.062 10.1807 13.9449 10.7113 13.6379 11.1487C13.3322 11.5856 12.8744 11.8771 12.3482 11.9695C12.2324 11.9898 12.1153 12 11.9991 12Z"/>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M12.9642 2.00012C14.8964 2.00012 16.647 3.66924 15.6558 5.48396C15.6112 5.56558 15.5555 5.64059 15.4902 5.7068L13.971 7.24734C14.3543 7.52396 14.6782 7.88377 14.9131 8.30605L17.8801 8.82828C17.9374 8.83837 17.9938 8.85345 18.0485 8.87334C19.3681 9.35305 20.0483 10.8122 19.5686 12.1326L18.4989 15.0721C17.7919 17.0123 15.1537 17.2743 14.0797 15.5102C14.0432 15.4502 14.0131 15.3864 13.9899 15.3201L13.168 12.9711L10.1736 15.6628C9.09786 16.5654 7.49342 16.4255 6.5913 15.3495L4.58089 12.9536C3.25277 11.3713 4.34508 8.95581 6.41021 8.907C6.51005 8.90464 6.60968 8.91725 6.70579 8.94441L8.651 9.49412L7.92337 5.36524C7.67952 3.982 8.603 2.66328 10.0171 2.41434L12.8216 2.01034C12.8688 2.00354 12.9165 2.00012 12.9642 2.00012ZM15.2858 10.4026C15.2161 10.8459 15.0559 11.2649 14.8217 11.6361L15.8334 14.5328C16.0561 14.7935 16.4959 14.7275 16.6197 14.3877L17.689 11.4492C17.784 11.1877 17.6665 10.9011 17.4253 10.779L15.2858 10.4026ZM6.34791 10.9216C6.03237 11.0038 5.88763 11.3996 6.11288 11.6679L8.12363 14.0642C8.31588 14.2935 8.65837 14.3234 8.86237 14.153L10.4564 12.7225C10.0532 12.4909 9.70057 12.1742 9.42661 11.7919L6.34791 10.9216ZM13.0247 4.00172L10.3334 4.38892C10.0381 4.44102 9.84091 4.72255 9.89299 5.01794L10.2728 7.17267C10.6343 6.93262 11.05 6.76155 11.5044 6.68142C11.5873 6.66663 11.6701 6.65524 11.7526 6.6471L13.9257 4.44493C13.9211 4.41376 13.8929 4.38195 13.83 4.32205C13.6471 4.1476 13.3259 4.01743 13.0247 4.00172ZM11.9993 11.5C11.2713 11.5 10.6504 10.9786 10.5233 10.2599C10.4535 9.86451 10.5416 9.4665 10.7719 9.13847C11.0011 8.81111 11.3445 8.59221 11.7391 8.52289C11.826 8.50796 11.9138 8.5 12.001 8.5C12.729 8.5 13.3498 9.02139 13.4766 9.74013C13.5465 10.1355 13.4586 10.5335 13.2284 10.8615C12.9992 11.1892 12.6558 11.4078 12.2612 11.4771C12.1743 11.4924 12.0865 11.5 11.9993 11.5ZM14.9476 17.5173L14.8981 17.5213L14.8805 17.5231C14.8628 17.5252 14.8451 17.5278 14.8271 17.5312C14.8125 17.5338 14.7979 17.5367 14.7836 17.5397C14.762 17.5445 14.7406 17.55 14.7193 17.5564C14.7033 17.5611 14.6873 17.5662 14.6711 17.5717C14.6473 17.5801 14.6236 17.5893 14.6002 17.5996C14.5878 17.605 14.5755 17.6106 14.5633 17.6165L14.5561 17.6201C14.555 17.6207 14.5539 17.6212 14.5528 17.6218C14.5474 17.6245 14.542 17.6272 14.5368 17.6299C14.5317 17.6326 14.5267 17.6352 14.522 17.6377C14.5055 17.6467 14.4892 17.6562 14.4725 17.6665C14.4501 17.6805 14.4283 17.6952 14.4078 17.7102C14.4001 17.7158 14.3924 17.7216 14.3845 17.7278C14.3651 17.7429 14.3463 17.7587 14.3287 17.7746C14.3186 17.7838 14.3085 17.7933 14.2983 17.8033C14.2845 17.8169 14.2712 17.8308 14.2588 17.8445C14.2425 17.8626 14.2267 17.8813 14.2112 17.9011C14.2054 17.9087 14.1996 17.9163 14.1943 17.9235C14.1784 17.9452 14.1633 17.9676 14.149 17.9909L14.1498 17.9894C14.1399 18.0054 14.1305 18.0217 14.1215 18.0382L14.1195 18.0422C14.1147 18.051 14.1101 18.06 14.1055 18.069C14.092 18.0962 14.0498 18.1699 13.9804 18.274C13.8588 18.4565 13.7119 18.6401 13.5429 18.8091C13.0911 19.2609 12.5804 19.5162 12 19.5162C11.4195 19.5162 10.9089 19.2609 10.4571 18.8091C10.288 18.6401 10.1412 18.4565 10.0195 18.274C9.95013 18.1699 9.90799 18.0962 9.8944 18.069L9.88591 18.0522C9.87307 18.0277 9.85921 18.0036 9.84421 17.9801C9.83574 17.9667 9.82699 17.9536 9.81807 17.9408C9.80638 17.9241 9.79417 17.9078 9.7815 17.892C9.77136 17.8793 9.76095 17.8669 9.75032 17.8548C9.73485 17.8372 9.71871 17.8201 9.70124 17.8029C9.69196 17.7938 9.68253 17.7849 9.67336 17.7766C9.65689 17.7616 9.63985 17.7471 9.62142 17.7324C9.6087 17.7224 9.59576 17.7126 9.58358 17.704C9.56469 17.6905 9.54522 17.6775 9.52518 17.6652L9.52666 17.666C9.51422 17.6583 9.50163 17.6509 9.48889 17.6438L9.48594 17.6422C9.47323 17.6352 9.46031 17.6283 9.44719 17.6218L9.43255 17.6146C9.41451 17.6059 9.39622 17.5978 9.37729 17.5901C9.36486 17.585 9.35239 17.5802 9.33987 17.5756L9.3162 17.5675C9.30639 17.5643 9.29652 17.5611 9.2863 17.5579C9.26309 17.5509 9.23976 17.5449 9.2165 17.5399C9.20058 17.5364 9.18458 17.5332 9.16848 17.5302C9.14983 17.527 9.13114 17.5244 9.11202 17.5224C9.09188 17.5201 9.07165 17.5184 9.05138 17.5173C9.03704 17.5165 9.0227 17.5161 9.00839 17.516H8.99156C8.97691 17.5161 8.96225 17.5166 8.94705 17.5174C8.93068 17.5183 8.91434 17.5196 8.89807 17.5213L8.88049 17.5231C8.8628 17.5252 8.84512 17.5278 8.8271 17.5312C8.81248 17.5338 8.79793 17.5367 8.78359 17.5397C8.76204 17.5445 8.74056 17.55 8.7193 17.5564C8.70325 17.5611 8.68732 17.5662 8.67105 17.5717C8.64725 17.5801 8.62361 17.5893 8.60017 17.5996C8.58778 17.605 8.5755 17.6106 8.56332 17.6165L8.55607 17.6201C8.55496 17.6207 8.55386 17.6212 8.55276 17.6218C8.54818 17.6241 8.54363 17.6264 8.53917 17.6287C8.5332 17.6318 8.52741 17.6348 8.52199 17.6377C8.50547 17.6467 8.48919 17.6562 8.47253 17.6665C8.45006 17.6805 8.42831 17.6952 8.40776 17.7102C8.40378 17.7131 8.39983 17.716 8.39584 17.7191C8.3921 17.7219 8.38834 17.7248 8.38452 17.7278C8.36508 17.7429 8.34632 17.7587 8.32874 17.7746C8.31654 17.7856 8.30458 17.797 8.29247 17.8091C8.28078 17.8208 8.26941 17.8328 8.25884 17.8445C8.24247 17.8626 8.2267 17.8813 8.21124 17.9011C8.20691 17.9067 8.20263 17.9123 8.19858 17.9177C8.19714 17.9197 8.19572 17.9216 8.19434 17.9235C8.17844 17.9452 8.16331 17.9677 8.14901 17.9909L8.14983 17.9894C8.13992 18.0054 8.13048 18.0217 8.12151 18.0382L8.11947 18.0422C8.11471 18.051 8.11007 18.0599 8.10555 18.069C8.09196 18.0962 8.04982 18.1699 7.98042 18.274C7.85879 18.4565 7.71192 18.6401 7.54287 18.8091C7.09109 19.2609 6.58044 19.5162 5.99997 19.5162C5.41951 19.5162 4.90887 19.2609 4.45708 18.8091C4.28803 18.6401 4.14116 18.4565 4.01953 18.274C3.95013 18.1699 3.90799 18.0962 3.8944 18.069C3.64741 17.575 3.04674 17.3748 2.55276 17.6218C2.05878 17.8688 1.85856 18.4694 2.10555 18.9634C2.2747 19.3017 2.58154 19.762 3.04287 20.2233C3.84109 21.0215 4.83044 21.5162 5.99997 21.5162C7.16951 21.5162 8.15886 21.0215 8.95708 20.2233C8.97208 20.2083 8.98692 20.1933 9.00159 20.1783L9.04287 20.2233C9.84109 21.0215 10.8304 21.5162 12 21.5162C13.1695 21.5162 14.1589 21.0215 14.9571 20.2233C14.9721 20.2083 14.9869 20.1933 15.0016 20.1783L15.0429 20.2233C15.8411 21.0215 16.8304 21.5162 18 21.5162C19.1695 21.5162 20.1589 21.0215 20.9571 20.2233C21.4184 19.762 21.7252 19.3017 21.8944 18.9634C22.1414 18.4694 21.9412 17.8688 21.4472 17.6218C20.9532 17.3748 20.3525 17.575 20.1055 18.069C20.092 18.0962 20.0498 18.1699 19.9804 18.274C19.8588 18.4565 19.7119 18.6401 19.5429 18.8091C19.0911 19.2609 18.5804 19.5162 18 19.5162C17.4195 19.5162 16.9089 19.2609 16.4571 18.8091C16.288 18.6401 16.1412 18.4565 16.0195 18.274C15.9501 18.1699 15.908 18.0962 15.8944 18.069L15.8859 18.0522C15.8731 18.0277 15.8592 18.0036 15.8442 17.9801C15.8357 17.9667 15.827 17.9536 15.8181 17.9408C15.8064 17.9241 15.7942 17.9078 15.7815 17.892C15.7714 17.8793 15.761 17.8669 15.7503 17.8548C15.7348 17.8372 15.7187 17.8201 15.7012 17.8029C15.6916 17.7935 15.6819 17.7843 15.6724 17.7757C15.6562 17.761 15.6395 17.7468 15.6214 17.7324C15.6087 17.7224 15.5958 17.7126 15.5836 17.704C15.5647 17.6905 15.5452 17.6775 15.5252 17.6652L15.5267 17.666C15.511 17.6563 15.4951 17.6471 15.479 17.6383L15.4752 17.6363C15.466 17.6313 15.4566 17.6265 15.4472 17.6218L15.4325 17.6146C15.4145 17.6059 15.3962 17.5978 15.3773 17.5901C15.3649 17.585 15.3524 17.5802 15.3399 17.5756L15.3162 17.5675C15.3064 17.5643 15.2965 17.5611 15.2863 17.5579C15.2631 17.5509 15.2398 17.5449 15.2165 17.5399C15.2006 17.5364 15.1846 17.5332 15.1685 17.5302C15.1498 17.527 15.1311 17.5244 15.112 17.5224C15.0919 17.5201 15.0717 17.5184 15.0514 17.5173C15.037 17.5165 15.0227 17.5161 15.0084 17.516H14.9916C14.9769 17.5161 14.9622 17.5166 14.9476 17.5173Z"/>
	</svg>
);

FloWebSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const FrenchSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>french</title>
		<path fill={primary} d="M2,20V5h20v15H2z M8,19h8V6H8V19z" />
		<polygon fill={secondary} points="8,19 16,19 16,6 8,6 " />
	</svg>
);

FrenchSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const FreshSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>fresh</title>
		<path fill={secondary} d="M11.2,20.5c0.2,0,0.5,0,0.7,0c1.5,0,3.5-0.5,5.4-2.4c0.7-0.7,1.3-1.6,1.7-2.5l-0.5-0.1H8.3L6,17.8l0.6,0.6C7.9,19.6,9.5,20.3,11.2,20.5 M12,12.5h7.7l0.5-1.4C21,7.9,21.1,7,21,5.4l-0.8-0.9L12,12.5z M8.5,16l4-3.7V4.8L11.7,4c-1.1,0.2-2.2,0.6-3.2,1v0.7V16z" />
		<path fill={primary} d="M21,4.3l-0.1-0.8l-0.8-0.1c-0.1,0-1.2-0.1-2.7-0.1c-3.3,0-8.7,0.5-11.7,3.6c-3.9,3.9-2.4,8.5-0.3,10.8l0,0l-2.9,2.9L3.9,22l2.9-2.9c1.3,1.1,2.9,1.8,4.6,2c0.2,0,0.5,0,0.7,0c1.5,0,3.6-0.5,5.5-2.4C22,14.3,21,4.7,21,4.3z M18.9,11h-4l4.3-4.3C19.2,8,19.2,9.5,18.9,11z M17.9,5.2l-4.4,4.4V5.5c1.4-0.2,2.8-0.3,3.9-0.3C17.6,5.2,17.7,5.2,17.9,5.2z M9,6.8c0.8-0.4,1.6-0.7,2.5-0.9v5.7L9,14.1V6.8z M7,8.2v7.9l-0.3,0.3C6.2,15.8,3.2,11.9,7,8.2z M11.5,19.2c-1.2-0.1-2.4-0.6-3.4-1.4l0.3-0.3h7.7c-1.2,1.2-2.6,1.8-4,1.8C11.9,19.3,11.7,19.2,11.5,19.2z M17.5,15.6v-0.1h-7.1l2.5-2.5h5.6C18.3,13.9,17.9,14.8,17.5,15.6z" />
	</svg>
);

FreshSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const FrothSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>froth</title>
		<path fill={primary} d="M9.3,2c0.8,0,1.8,0.7,2.7,0.7c0.2,0,0.3,0,0.5-0.1l0.2,0L12,4.7c-0.7,0-1.3-0.2-1.8-0.4L9.7,4.1 c-0.1,0-0.1,0-0.1-0.1L9.4,4.2l0,0L9.3,4.5C8.9,5,8.5,5.6,7.9,6.1c-0.5,0.4-1,0.6-1.5,0.7L5.7,7c-0.1,0-0.1,0-0.2,0l0,0.2l0,0l0,0.8 c0,0.5,0,1.1-0.2,1.7c-0.2,0.6-0.5,1-0.8,1.4l-0.5,0.7C4,11.9,4,12,4,12c0.1,0.1,0.1,0.2,0.2,0.3l0.1,0.1c0.4,0.5,0.8,1.1,1.1,1.8 c0.2,0.6,0.2,1.2,0.2,1.7l0,0.8c0,0.1,0,0.1,0,0.2C5.6,17,5.7,17,5.8,17l0.2,0c0.6,0.2,1.3,0.4,1.9,0.8c0.6,0.5,1,1.1,1.3,1.6 c0.1,0.1,0.2,0.3,0.3,0.4l0.1-0.1l0,0l0.3-0.1c0.5-0.2,1-0.4,1.7-0.5l0.3,0l0.6,2.1c-0.2-0.1-0.4-0.1-0.6-0.1 c-0.9,0-1.9,0.7-2.7,0.7c-0.1,0-0.3,0-0.4-0.1c-1-0.3-1.3-1.8-2.1-2.4C6,18.9,4.5,19,3.9,18.1c-0.6-0.8,0-2.3-0.3-3.3 C3.3,13.9,2,13.1,2,12c0-1.1,1.3-1.9,1.6-2.9c0.3-1-0.2-2.5,0.3-3.3C4.5,5,6,5.1,6.8,4.5C7.6,3.9,8,2.4,8.9,2.1C9.1,2,9.2,2,9.3,2z  M11.5,6.1l-0.4,1.1C9,7.6,7.4,9.5,7.4,11.8c0,2.2,1.4,4.1,3.4,4.6l0.2,0.1l0.3,1.1c-2.8-0.3-4.9-2.7-4.9-5.7c0-2.9,2.1-5.3,4.8-5.7 L11.5,6.1z" />
	</svg>
);

export const FrothCupSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>froth</title>
		<path fill={primary} d="M19,6c0.5,0,0.9,0.4,1,0.9L20,7v0.8l0.4,0.1c1.5,0.3,2.5,1.7,2.6,3.3l0,0.2c0,1.6-1,3.1-2.6,3.5l0,0l-0.6,0.1C18.8,19,15.4,22,11.1,22c-4.9,0-9-4-9.1-9l0-0.3V7c0-0.6,0.4-1,1-1c0.5,0,0.9,0.4,1,0.9L4,7v5.7c0,4,3.2,7.2,7.1,7.2c3.8,0,6.8-3.1,6.9-7l0-0.2V7C18,6.4,18.4,6,19,6z M20,9.8v3c0.6-0.1,1-0.7,1-1.5S20.6,9.9,20,9.8L20,9.8zM11.2,5.5l0.3,0.1c1,0.5,1.9,0.5,2.7,0.2c0.1,0,0.2-0.1,0.3-0.1l0.1,0c0.5-0.3,1.1-0.2,1.4,0.3c0.3,0.5,0.2,1.1-0.3,1.4c-0.2,0.1-0.4,0.2-0.7,0.3c-1.2,0.4-2.6,0.5-4-0.2l-0.3-0.1c-1-0.5-1.9-0.5-2.7-0.2c-0.1,0-0.2,0.1-0.3,0.1l-0.1,0C7.1,7.6,6.5,7.5,6.2,7C5.9,6.6,6,6,6.5,5.7c0.2-0.1,0.4-0.2,0.7-0.3C8.4,4.9,9.8,4.8,11.2,5.5z" />
	</svg>
);

export const FrozenSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>frozen</title>
		<path fill={primary} d="M11.9,1c0.5,0,0.9,0.4,1,0.9l0,0.1l0,1.9l0.9-0.7c0.4-0.3,1.1-0.3,1.4,0.2 c0.3,0.4,0.3,1-0.1,1.3L15,4.8l-2.1,1.7v3.8l3.3-1.9l0.4-2.7c0.1-0.5,0.5-0.8,0.9-0.8l0.1,0l0.1,0c0.5,0.1,0.9,0.5,0.8,1l0,0.1 l-0.2,1.1l1.7-1c0.5-0.3,1.1-0.1,1.4,0.4c0.3,0.4,0.1,1-0.3,1.3l-0.1,0.1l-1.7,1l1,0.4C21,9.4,21.2,10,21,10.5 c-0.2,0.5-0.7,0.7-1.2,0.6l-0.1,0l-2.5-1L13.9,12l3.3,1.9l2.5-1c0.5-0.2,1.1,0.1,1.3,0.6c0.2,0.5,0,1-0.5,1.2l-0.1,0l-1,0.4l1.7,1 c0.5,0.3,0.6,0.9,0.4,1.4c-0.3,0.4-0.8,0.6-1.3,0.4l-0.1-0.1l-1.7-1l0.2,1.1c0.1,0.5-0.3,1.1-0.8,1.1c-0.5,0.1-1-0.2-1.1-0.7l0-0.1 l-0.4-2.7l-3.3-1.9v3.8l2.1,1.7c0.4,0.3,0.5,1,0.2,1.4c-0.3,0.4-0.9,0.5-1.3,0.2l-0.1-0.1l-0.9-0.7l0,1.9c0,0.6-0.4,1-1,1 c-0.5,0-0.9-0.4-1-0.9l0-0.1l0-1.9L10,20.8c-0.4,0.3-1.1,0.3-1.4-0.2c-0.3-0.4-0.3-1,0.1-1.3l0.1-0.1l2.1-1.7v-3.8l-3.3,1.9 l-0.4,2.7c-0.1,0.5-0.6,0.9-1.1,0.8c-0.5-0.1-0.9-0.5-0.8-1l0-0.1l0.2-1.1l-1.7,1c-0.5,0.3-1.1,0.1-1.4-0.4c-0.3-0.4-0.1-1,0.3-1.3 l0.1-0.1l1.6-0.9l-0.7-0.3c-0.5-0.2-0.8-0.8-0.6-1.3c0.2-0.5,0.7-0.7,1.2-0.6l0.1,0l2.2,0.9L9.9,12l-3.3-1.9l-2.5,1 C3.6,11.3,3,11,2.8,10.5c-0.2-0.5,0-1,0.5-1.2l0.1,0l1-0.4l-1.7-1C2.3,7.6,2.1,7,2.4,6.5c0.3-0.4,0.8-0.6,1.3-0.4l0.1,0.1L5.3,7 L5.1,6.3C4.9,5.8,5.2,5.2,5.7,5.1c0.5-0.2,1,0.1,1.2,0.5l0,0.1l0.9,2.8l3,1.7V6.5L8.8,4.8c-0.4-0.3-0.5-1-0.2-1.4 C8.9,3,9.5,2.9,9.9,3.1L10,3.2l0.9,0.7l0-1.9C10.9,1.4,11.4,1,11.9,1z" />
	</svg>
);

export const CircleCrossSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>CircleCross</title>
		<circle fill={tertiary || colors.white} cx="12" cy="12" r="9.6" />
		<path fill={primary || colors.error} d="M12,0C5.4,0,0,5.4,0,12s5.4,12,12,12s12-5.4,12-12S18.6,0,12,0z M17.3,16.4c0.4,0.4,0.4,1,0,1.4c-0.4,0.4-1,0.4-1.4,0L12,13.9l-3.9,3.9c-0.4,0.4-1,0.4-1.4,0c-0.4-0.4-0.4-1,0-1.4l3.9-3.9L6.7,8.6c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l3.9,3.9l3.9-3.9c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4l-3.9,3.9L17.3,16.4z" />
	</svg>
);

CircleCrossSvg.colorVariation = [
	"original",
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Gray", colors.gray),	
];

export const LockScreenSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>lockScreen</title>
		<path fill={primary} d="M16,8.2l0,4.3l0,0H8h0l0-4.3c0-2.3,1.8-4.1,4-4.1C14.2,4.1,16,5.9,16,8.2 M13.8,15.9 c0,0.7-0.3,1.2-0.8,1.5v2.7h-2v-2.7c-0.5-0.3-0.8-0.9-0.8-1.5c0-1,0.8-1.9,1.8-1.9C13,14.1,13.8,14.9,13.8,15.9 M12,2 C8.7,2,6,4.8,6,8.2v4.3c0,0,0,3.5,0,3.5l0,4.2c0,1,0.8,1.8,1.7,1.8h8.5c1,0,1.7-0.8,1.7-1.8v-4.3l0-3.5V8.2C18,4.8,15.3,2,12,2" />
	</svg>
);

export const SelectTickSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>selectTick</title>
		<circle fill={tertiary || colors.white} cx="12" cy="12" r="9.2" />
		<path fill={primary || colors.success} d="M12,0C5.4,0,0,5.4,0,12s5.4,12,12,12s12-5.4,12-12S18.6,0,12,0z M19.2,9.6l-0.1,0.1l-8.5,8.4c-0.2,0.2-0.6,0.4-0.9,0.4c-0.3,0-0.7-0.1-0.9-0.4L8.8,18l-3.9-3.9c-0.5-0.5-0.5-1.2-0.1-1.7l0.1-0.1c0.5-0.5,1.2-0.5,1.7-0.1l0.1,0.1l3.1,3l7.5-7.5l0.1-0.1c0.5-0.4,1.2-0.4,1.7,0.1C19.6,8.3,19.6,9.1,19.2,9.6z" />
	</svg>
);

SelectTickSvg.colorVariation = [
	"original",
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Gray", colors.gray),	
];

export const GiftSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>gift</title>
		<path fill={tertiary} d="M2.5,11L2.5,11c0.6,0,1.1,0.4,1.1,1v9.7h16.9V12c0-0.3,0.1-0.5,0.3-0.7l0.7-0.7v-2h-2.4c-0.6,0-1.3-0.6-0.7-0.6c1.1,0,1.1,0,1.1-1V4.1C19.2,4,19,3.5,18.7,3.6l-4,1.8c-0.3,0.2-0.7,0.2-1.1,0c-0.3-0.2-0.5,0-0.6-0.4c0-0.5-1-0.5-1.1-0.5s-1.1,0-1.1,0.5c0,0.6-0.5,0.5-1.1,0.5c0,0-0.3-0.1-0.4-0.2l-4-1.8C5,3.5,4.8,4,4.6,4v3c0.1,0,0,1,1.1,1c0.6,0-0.2,0.6-0.8,0.6H2.5V11z" />
		<path fill={secondary} d="M5,9h14V4H5V9z M11,21h2V9h-2V21z" />
		<path fill={primary} d="M20.2,2.4C20.7,2.7,21,3.3,21,3.8v2.3c0,0.2-0.1,0.5-0.2,0.8H21c1.1,0,2,0.9,2,2v2c0,0.3-0.1,0.5-0.3,0.7L22,12.4V21c0,0.7-0.4,1.4-1,1.7S20,23,20,23S4.5,23,4,23s-0.4,0.1-1-0.3c-0.6-0.3-1-1-1-1.7v-8c-0.6,0-1-0.4-1-1V9c0-1.1,0.8-1.9,1.8-2H3V3.8c0-0.6,0.3-1.1,0.9-1.4c0.8-0.5,1.8-0.5,2.6-0.1l0.1,0.1l2.9,1.2c1.2-1.4,3.8-1.3,5,0l2.9-1.2c0,0,0.1,0,0.1-0.1C18.3,1.9,19.4,1.9,20.2,2.4z M4,21h6v-9H4V21z M13,21V9h-2v12H13z M20,12h-6v9h6V12z M18.7,9H14v2h6.6l0.4-0.4V9H18.7L18.7,9zM10,11V9H3v2H10z M5.7,4.1C5.3,4.2,5.1,4.4,5,4.7v0.1V7c0,0,0,0.4,0.3,0.7C5.5,7.8,5.8,8,6.1,8h0.2h3.3l0.6-0.4c-0.1-0.3-0.2-0.7-0.2-1V6.4l0,0V6.3L9.4,5.8L5.7,4.1z M12,5c-0.4,0-0.9,0.4-1,1c0,0.1,0,0.3,0,0.5s0,0.3,0,0.5c0.1,0.6,0.6,1,1,1s0.9-0.4,1-1c0-0.2,0-0.3,0-0.5s0-0.3,0-0.5C12.9,5.4,12.4,5,12,5z M18.3,4.1l-3.8,1.8c-0.1,0-0.1,0.1-0.2,0.1L14,6.2c0,0.1,0,0.2,0,0.4c0,0.3-0.1,0.6-0.1,0.9l-0.1,0.2L14.2,8h3.5c0.7,0,1.2-0.3,1.3-0.7V7.2V4.8C19,4.5,18.7,4.2,18.3,4.1z" />
	</svg>
);

GiftSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const GrinderGuideSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>grinderGuide</title>
		<path fill={tertiary} d="M12,2c-4.9,0-8.8,3.7-8.8,8.3v3.3c0,4.6,4,8.3,8.8,8.3c4.9,0,8.8-3.7,8.8-8.3v-3.3 C20.8,5.8,16.9,2,12,2" />
		<path fill={primary} d="M12,3c-4.4,0-8,3.4-8,7.5v3c0,4.1,3.6,7.5,8,7.5c4.4,0,8-3.4,8-7.5v-3C20,6.4,16.4,3,12,3 M12,23 c-5.5,0-10-4.2-10-9.5v-3.1C2,5.2,6.5,1,12,1s10,4.2,10,9.5v3.1C22,18.8,17.5,23,12,23 M14.9,14.4c-0.2-0.8-0.6-1.5-0.9-2.3 l-0.1-0.2c-0.1-0.3-0.3-0.5-0.4-0.8c-1.1-2.1-2.1-4.1-1.3-6.4c0.1-0.3,0-0.6-0.3-0.7c-0.3-0.1-0.6-0.1-0.7,0.2 c-1.2,1.7-1.5,3.6-0.7,5.6c0.3,0.8,0.7,1.5,1.1,2.2c0.2,0.4,0.5,0.9,0.7,1.3c0.9,2,0.9,3.4,0,4.7c-0.1,0.2-0.1,0.5,0.1,0.7 c0.1,0.1,0.3,0.2,0.4,0.2c0.1,0,0.2,0,0.3-0.1C14.6,17.9,15.3,16.2,14.9,14.4" />
	</svg>
);

GrinderGuideSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const HelpSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>help</title>
		<path fill={tertiary} d="M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8S7.6,4,12,4" />
		<path fill={secondary} d="M18.9,5c0.3,2.1,0.9,10.2-6.6,15.9c4.9-0.6,8.7-4.8,8.7-9.8C21,8.8,20.2,6.7,18.9,5" />
		<path fill={primary} d="M12,16c0.6,0,1,0.5,1,1c0,0.6-0.5,1-1,1c-0.6,0-1-0.4-1-1C11,16.5,11.4,16,12,16L12,16z M11,15c0-0.2,0-0.7,0-1 c0-0.9,0.1-1.5,1.1-2.3l0.8-0.6c0.5-0.4,0.7-0.9,0.7-1.4c0-0.8-0.6-1.6-1.7-1.6c-2.2,0-2,1.8-1.9,1.9H8c0-2,1.4-4,3.9-4 C14.6,6,16,7.7,16,9.5c0,1.4-0.7,2.4-1.7,3.1l-0.7,0.5C13,13.6,13,14.3,13,15H11z M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10 S17.5,2,12,2 M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8S7.6,4,12,4" />
	</svg>
);

HelpSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const InfoSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>info</title>
		<circle fill={tertiary} cx="12" cy="12" r="8" />
		<path fill={primary} d="M12,2C6.5,2,2,6.5,2,12c0,5.5,4.5,10,10,10c5.5,0,10-4.5,10-10C22,6.5,17.5,2,12,2z M14.5,18.5c-0.7,0.2-1.5,0.6-2.5,0.5c-1.4-0.1-2.2-1.2-1.8-2.7c0.2-0.9,0.9-3.5,1-4.1c0.3-1.4-1-1.6-1.8-1.6c0,0,0.2-0.6,0.2-0.6c0.6-0.3,1.5-0.8,2.5-0.7c1.4,0.1,2.2,1.2,1.8,2.7c-0.2,0.6-0.8,3.3-1,4.1c-0.3,1.4,1,1.8,1.8,1.8h0L14.5,18.5z M13.1,8.3c-0.9,0-1.7-0.7-1.7-1.7c0-0.9,0.7-1.7,1.7-1.7c0.9,0,1.7,0.7,1.7,1.7C14.7,7.6,14,8.3,13.1,8.3z" />
	</svg>
);

InfoSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const KeepwarmSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>keepwarm</title>
		<path fill={tertiary} d="M12,6.5c-1.8,0-2.5,0.6-2.6,2.3l0,0.2v0.9l-0.9,0.3c-3,1-5.4,4-5.5,6.2l0,0.2v3.8h18v-2.5 c0-2.5-2.5-6.5-5.3-7.6l-0.3-0.1l-0.9-0.3V9C14.6,7.2,13.9,6.5,12,6.5z" />
		<path fill={secondary} d="M2.5,19l19,0.1c0.3,0,0.5,0.7,0.5,1.5c0,0.7-0.2,1.3-0.4,1.4l-0.1,0l-19-0.1 c-0.3,0-0.5-0.7-0.5-1.5C2,19.7,2.2,19.1,2.5,19L2.5,19z" />
		<path fill={primary} d="M12,6c2.2,0,3.5,1,3.9,2.9l0,0.2l0,0.2l0,0c2.6,1.3,4.7,4.6,5,7.2l0,0.2l0,0.2v1l0.2,0 c0.7,0,1.3,0.4,1.6,1l0.1,0.1l0.1,0.1C23,19.5,23,19.7,23,20c0,1.6-1.5,2.9-3.3,3l-0.2,0L5,23c-2.3,0-4-1-4-3c0-0.3,0-0.4,0.1-0.6 C1.2,18.5,1.9,18,2.8,18L3,18v-2c0-2.4,2-5.2,4.7-6.5l0.3-0.1l0.1,0l0-0.2c0.3-1.9,1.5-3,3.5-3.1l0.2,0L12,6z M21,20L3,20l0,0.1 c0.1,0.5,0.6,0.8,1.6,0.9l0.2,0L5,21l14.5,0C20.3,21,20.9,20.5,21,20L21,20z M12,8c-1.4,0-2,0.5-2,1.8l0,0.2v0.7l-0.7,0.2 C7,11.7,5.1,14,5,15.8L5,16v3h14v-2c0-2-2-5.1-4.1-6l-0.2-0.1L14,10.7V10C14,8.5,13.4,8,12,8z M9.7,12.3c0.1,0.3,0,0.5-0.3,0.6 L9.3,13l-0.2,0.1c0,0-0.1,0-0.1,0.1c-0.3,0.2-0.6,0.4-0.9,0.7c-0.7,0.7-1.2,1.6-1.3,2.7c0,0.3-0.3,0.5-0.5,0.4s-0.5-0.3-0.4-0.5 c0.1-1.4,0.7-2.5,1.6-3.3C8,12.5,8.6,12.2,9,12C9.3,11.9,9.6,12,9.7,12.3z M13,9v2h-2V9H13z M3.4,2.1C3.5,2.1,3.5,2.1,3.4,2.1 C4.6,2.8,5,3.4,5,4C5,5.4,2.7,6.4,2.8,7.5c0,0.3,0.2,0.7,0.8,1.2c0.1,0,0.1,0.1,0,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0 C2.4,8.4,2,7.9,2,7.3c0-0.5,0.4-1,0.8-1.4l0.7-0.7c0.5-0.5,0.9-1,0.8-1.4c0-0.4-0.2-0.8-1-1.4l0,0l0,0l0,0l0,0c0,0,0,0,0-0.1l0,0 c0,0,0,0,0,0l0,0C3.3,2.2,3.3,2.2,3.4,2.1L3.4,2.1L3.4,2.1L3.4,2.1z M21.5,2L21.5,2L21.5,2L21.5,2L21.5,2C22.6,2.7,23,3.3,23,3.9 c-0.1,1.4-2.3,2.5-2.2,3.6c0,0.3,0.2,0.7,0.8,1.2c0.1,0,0.1,0.1,0,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0c-1-0.6-1.4-1.1-1.4-1.6 c0-0.5,0.4-1,0.8-1.4l0.7-0.7c0.5-0.5,0.9-1,0.8-1.4c0-0.4-0.2-0.8-0.8-1.3l-0.2-0.1c0,0,0,0,0,0c0,0,0-0.1,0-0.2 C21.3,2,21.4,2,21.5,2L21.5,2z M6.5,2L6.5,2L6.5,2L6.5,2L6.5,2C7.6,2.6,8,3.2,8,3.8C7.9,5.3,5.7,6.3,5.8,7.4c0,0.3,0.2,0.7,0.8,1.2 c0.1,0,0.1,0.1,0,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0C5.4,8.2,5,7.7,5,7.2c0-0.5,0.4-1,0.8-1.4l0.7-0.7c0.5-0.5,0.9-1,0.8-1.4 c0-0.4-0.2-0.8-0.8-1.3L6.4,2.2l0,0v0c0,0,0,0,0,0c0,0,0-0.1,0-0.2C6.3,2,6.4,2,6.5,2L6.5,2z M18.4,2C18.5,2,18.5,2,18.4,2 C19.6,2.6,20,3.2,20,3.8c0,1.4-2.3,2.5-2.2,3.6c0,0.3,0.2,0.7,0.8,1.2c0.1,0,0.1,0.1,0,0.2c0,0-0.1,0.1-0.1,0.1c0,0-0.1,0-0.1,0 c-1-0.6-1.4-1.1-1.4-1.6c0-0.6,0.5-1.1,0.9-1.6l0.5-0.5c0.5-0.5,0.9-1,0.8-1.4c0-0.4-0.2-0.8-0.9-1.4l0,0l0,0l0,0l0,0c0,0,0,0,0-0.1 l0,0c0,0,0,0,0,0l0,0C18.3,2,18.3,2,18.4,2L18.4,2L18.4,2L18.4,2z" />
	</svg>
);

KeepwarmSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),	
];

export const LanguageSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>language</title>
		<path fill={secondary} d="M12,2c5,0.3,9,4.6,9,10c0,5.4-4,9.7-9,10v-0.5c2.3-2.6,3.5-5.7,3.5-9.5c0-3.8-1.2-6.9-3.5-9.5V2z" />
		<path fill={primary} d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2z M16.6,15.7c0.2-0.9,0.3-1.9,0.4-3h3 c-0.1,1.4-0.6,2.7-1.4,3.8C18,16.2,17.3,15.9,16.6,15.7z M17.2,18.1c-0.6,0.5-1.2,0.9-1.8,1.2c0.3-0.5,0.5-1.1,0.7-1.6 C16.4,17.8,16.8,17.9,17.2,18.1z M5.4,16.6c-0.8-1.1-1.3-2.4-1.4-3.8h3c0,1,0.2,2,0.4,3C6.7,15.9,6,16.2,5.4,16.6z M7.9,17.6 c0.2,0.6,0.4,1.1,0.7,1.6C8,19,7.4,18.6,6.8,18.1C7.2,17.9,7.6,17.8,7.9,17.6z M7.4,8.3C7.2,9.1,7.1,9.9,7,10.8H4.1 c0.2-1.2,0.7-2.3,1.3-3.3C6,7.8,6.7,8.1,7.4,8.3z M6.8,5.9C7.4,5.4,8,5,8.7,4.7C8.4,5.3,8.2,5.8,7.9,6.4C7.6,6.2,7.2,6.1,6.8,5.9z  M9.3,8.8C10.2,8.9,11.1,9,12,9c0.9,0,1.8-0.1,2.7-0.2c0.1,0.6,0.2,1.3,0.3,2H9C9.1,10.1,9.2,9.4,9.3,8.8z M9,12.8H15 c0,0.9-0.1,1.7-0.3,2.5C13.8,15.1,12.9,15,12,15s-1.8,0.1-2.7,0.2C9.2,14.4,9.1,13.6,9,12.8z M17,10.8c-0.1-0.8-0.2-1.7-0.3-2.5 c0.7-0.2,1.3-0.5,2-0.9c0.7,1,1.1,2.1,1.3,3.3H17z M16.1,6.4c-0.2-0.6-0.4-1.1-0.7-1.6C16,5,16.6,5.4,17.2,5.9 C16.8,6.1,16.4,6.2,16.1,6.4z M12.5,4c0.7,0.9,1.2,1.8,1.6,2.8C13.4,6.9,12.7,7,12,7c-0.7,0-1.4-0.1-2.1-0.2c0.4-1,0.9-2,1.6-2.8 c0.2,0,0.3,0,0.5,0C12.2,4,12.3,4,12.5,4z M11.5,20c-0.7-0.9-1.2-1.8-1.6-2.8c0.7-0.1,1.4-0.2,2.1-0.2c0.7,0,1.4,0.1,2.1,0.2 c-0.4,1-0.9,2-1.6,2.8c-0.2,0-0.3,0-0.5,0S11.7,20,11.5,20z" />
	</svg>
);

LanguageSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ListSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>list</title>
		<path fill={tertiary} d="M4.1,3.5h15.9v9.1H4.1V3.5z M4.1,13.6h15.9v6.8H4.1V13.6z" />
		<path fill={primary} d="M21,2v20H3V2H21z M19,14H5v6h14V14z M17,16v2H7v-2H17z M19,4H5v8h14V4z" />
	</svg>
);

ListSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

// replace with animation
export const LoadingSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>loading</title>
		<path fill={primary || colors.productBlue} d="M19.1,4.7C20.9,6.5,22,9,22,11.8C22,17.5,17.5,22,12,22l0,0l0-6.1c0,0,0,0,0,0 c2.2,0,4-1.8,4-4.1c0-1.1-0.5-2.2-1.2-2.9L19.1,4.7z" />
	</svg>
);

LoadingSvg.colorVariation = [
	"original",
];

// replace with animation
export const Loadingv2_1Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>loadingv2_1</title>
		<path fill={primary} opacity="1" d="M12.1,8.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,7.7,12.7,8.1,12.1,8.1" />
		<path fill={primary} opacity="0.9" d="M15,9.3c-0.4-0.4-0.4-1,0-1.4l2.8-2.8c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4l-2.8,2.8C16,9.7,15.4,9.7,15,9.3" />
		<path fill={primary} opacity="0.8" d="M21.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S21.7,13.1,21.1,13.1" />
		<path fill={primary} opacity="0.7" d="M17.8,19.2L15,16.4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C18.8,19.6,18.2,19.6,17.8,19.2" />
		<path fill={primary} opacity="0.5" d="M12.1,22.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,21.7,12.7,22.1,12.1,22.1" />
		<path fill={primary} opacity="0.4" d="M5.1,19.2c-0.4-0.4-0.4-1,0-1.4L7.9,15c0.4-0.4,1-0.4,1.4,0s0.4,1,0,1.4l-2.8,2.8C6.1,19.6,5.5,19.6,5.1,19.2" />
		<path fill={primary} opacity="0.3" d="M7.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S7.7,13.1,7.1,13.1" />
		<path fill={primary} opacity="0.1" d="M7.9,9.3L5.1,6.5c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C8.9,9.7,8.3,9.7,7.9,9.3" />
	</svg>
);

export const Loadingv2_2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>loadingv2_2</title>
		<path fill={primary} opacity="0.1" d="M12.1,8.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,7.7,12.7,8.1,12.1,8.1" />
		<path fill={primary} opacity="1" d="M15,9.3c-0.4-0.4-0.4-1,0-1.4l2.8-2.8c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4l-2.8,2.8C16,9.7,15.4,9.7,15,9.3" />
		<path fill={primary} opacity="0.9" d="M21.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S21.7,13.1,21.1,13.1" />
		<path fill={primary} opacity="0.8" d="M17.8,19.2L15,16.4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C18.8,19.6,18.2,19.6,17.8,19.2" />
		<path fill={primary} opacity="0.7" d="M12.1,22.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,21.7,12.7,22.1,12.1,22.1" />
		<path fill={primary} opacity="0.5" d="M5.1,19.2c-0.4-0.4-0.4-1,0-1.4L7.9,15c0.4-0.4,1-0.4,1.4,0s0.4,1,0,1.4l-2.8,2.8C6.1,19.6,5.5,19.6,5.1,19.2" />
		<path fill={primary} opacity="0.4" d="M7.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S7.7,13.1,7.1,13.1" />
		<path fill={primary} opacity="0.3" d="M7.9,9.3L5.1,6.5c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C8.9,9.7,8.3,9.7,7.9,9.3" />
	</svg>
);

export const Loadingv2_3Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>loadingv2_3</title>
		<path fill={primary} opacity="0.3" d="M12.1,8.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,7.7,12.7,8.1,12.1,8.1" />
		<path fill={primary} opacity="0.1" d="M15,9.3c-0.4-0.4-0.4-1,0-1.4l2.8-2.8c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4l-2.8,2.8C16,9.7,15.4,9.7,15,9.3" />
		<path fill={primary} opacity="1" d="M21.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S21.7,13.1,21.1,13.1" />
		<path fill={primary} opacity="0.9" d="M17.8,19.2L15,16.4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C18.8,19.6,18.2,19.6,17.8,19.2" />
		<path fill={primary} opacity="0.8" d="M12.1,22.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,21.7,12.7,22.1,12.1,22.1" />
		<path fill={primary} opacity="0.7" d="M5.1,19.2c-0.4-0.4-0.4-1,0-1.4L7.9,15c0.4-0.4,1-0.4,1.4,0s0.4,1,0,1.4l-2.8,2.8C6.1,19.6,5.5,19.6,5.1,19.2" />
		<path fill={primary} opacity="0.5" d="M7.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S7.7,13.1,7.1,13.1" />
		<path fill={primary} opacity="0.4" d="M7.9,9.3L5.1,6.5c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C8.9,9.7,8.3,9.7,7.9,9.3" />
	</svg>
);

export const Loadingv2_4Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>loadingv2_4</title>
		<path fill={primary} opacity="0.4" d="M12.1,8.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,7.7,12.7,8.1,12.1,8.1" />
		<path fill={primary} opacity="0.3" d="M15,9.3c-0.4-0.4-0.4-1,0-1.4l2.8-2.8c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4l-2.8,2.8C16,9.7,15.4,9.7,15,9.3" />
		<path fill={primary} opacity="0.1" d="M21.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S21.7,13.1,21.1,13.1" />
		<path fill={primary} opacity="1" d="M17.8,19.2L15,16.4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C18.8,19.6,18.2,19.6,17.8,19.2" />
		<path fill={primary} opacity="0.9" d="M12.1,22.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,21.7,12.7,22.1,12.1,22.1" />
		<path fill={primary} opacity="0.8" d="M5.1,19.2c-0.4-0.4-0.4-1,0-1.4L7.9,15c0.4-0.4,1-0.4,1.4,0s0.4,1,0,1.4l-2.8,2.8C6.1,19.6,5.5,19.6,5.1,19.2" />
		<path fill={primary} opacity="0.7" d="M7.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S7.7,13.1,7.1,13.1" />
		<path fill={primary} opacity="0.5" d="M7.9,9.3L5.1,6.5c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C8.9,9.7,8.3,9.7,7.9,9.3" />
	</svg>
);

export const Loadingv2_5Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>loadingv2_5</title>
		<path fill={primary} opacity="0.5" d="M12.1,8.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,7.7,12.7,8.1,12.1,8.1" />
		<path fill={primary} opacity="0.4" d="M15,9.3c-0.4-0.4-0.4-1,0-1.4l2.8-2.8c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4l-2.8,2.8C16,9.7,15.4,9.7,15,9.3" />
		<path fill={primary} opacity="0.3" d="M21.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S21.7,13.1,21.1,13.1" />
		<path fill={primary} opacity="0.1" d="M17.8,19.2L15,16.4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C18.8,19.6,18.2,19.6,17.8,19.2" />
		<path fill={primary} opacity="1" d="M12.1,22.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,21.7,12.7,22.1,12.1,22.1" />
		<path fill={primary} opacity="0.9" d="M5.1,19.2c-0.4-0.4-0.4-1,0-1.4L7.9,15c0.4-0.4,1-0.4,1.4,0s0.4,1,0,1.4l-2.8,2.8C6.1,19.6,5.5,19.6,5.1,19.2" />
		<path fill={primary} opacity="0.8" d="M7.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S7.7,13.1,7.1,13.1" />
		<path fill={primary} opacity="0.7" d="M7.9,9.3L5.1,6.5c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C8.9,9.7,8.3,9.7,7.9,9.3" />
	</svg>
);

export const Loadingv2_6Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>loadingv2_7</title>
		<path fill={primary} opacity="0.7" d="M12.1,8.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,7.7,12.7,8.1,12.1,8.1" />
		<path fill={primary} opacity="0.5" d="M15,9.3c-0.4-0.4-0.4-1,0-1.4l2.8-2.8c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4l-2.8,2.8C16,9.7,15.4,9.7,15,9.3" />
		<path fill={primary} opacity="0.4" d="M21.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S21.7,13.1,21.1,13.1" />
		<path fill={primary} opacity="0.3" d="M17.8,19.2L15,16.4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C18.8,19.6,18.2,19.6,17.8,19.2" />
		<path fill={primary} opacity="0.1" d="M12.1,22.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,21.7,12.7,22.1,12.1,22.1" />
		<path fill={primary} opacity="1" d="M5.1,19.2c-0.4-0.4-0.4-1,0-1.4L7.9,15c0.4-0.4,1-0.4,1.4,0s0.4,1,0,1.4l-2.8,2.8C6.1,19.6,5.5,19.6,5.1,19.2" />
		<path fill={primary} opacity="0.9" d="M7.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S7.7,13.1,7.1,13.1" />
		<path fill={primary} opacity="0.8" d="M7.9,9.3L5.1,6.5c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C8.9,9.7,8.3,9.7,7.9,9.3" />
	</svg>
);

export const Loadingv2_7Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>loadingv2_7</title>
		<path fill={primary} opacity="0.8" d="M12.1,8.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,7.7,12.7,8.1,12.1,8.1" />
		<path fill={primary} opacity="0.7" d="M15,9.3c-0.4-0.4-0.4-1,0-1.4l2.8-2.8c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4l-2.8,2.8C16,9.7,15.4,9.7,15,9.3" />
		<path fill={primary} opacity="0.5" d="M21.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S21.7,13.1,21.1,13.1" />
		<path fill={primary} opacity="0.4" d="M17.8,19.2L15,16.4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C18.8,19.6,18.2,19.6,17.8,19.2" />
		<path fill={primary} opacity="0.3" d="M12.1,22.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,21.7,12.7,22.1,12.1,22.1" />
		<path fill={primary} opacity="0.1" d="M5.1,19.2c-0.4-0.4-0.4-1,0-1.4L7.9,15c0.4-0.4,1-0.4,1.4,0s0.4,1,0,1.4l-2.8,2.8C6.1,19.6,5.5,19.6,5.1,19.2" />
		<path fill={primary} opacity="1" d="M7.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S7.7,13.1,7.1,13.1" />
		<path fill={primary} opacity="0.9" d="M7.9,9.3L5.1,6.5c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C8.9,9.7,8.3,9.7,7.9,9.3" />
	</svg>
);

export const Loadingv2_8Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>loadingv2_7</title>
		<path fill={primary} opacity="0.9" d="M12.1,8.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,7.7,12.7,8.1,12.1,8.1" />
		<path fill={primary} opacity="0.8" d="M15,9.3c-0.4-0.4-0.4-1,0-1.4l2.8-2.8c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4l-2.8,2.8C16,9.7,15.4,9.7,15,9.3" />
		<path fill={primary} opacity="0.7" d="M21.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S21.7,13.1,21.1,13.1" />
		<path fill={primary} opacity="0.5" d="M17.8,19.2L15,16.4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C18.8,19.6,18.2,19.6,17.8,19.2" />
		<path fill={primary} opacity="0.4" d="M12.1,22.1c-0.6,0-1-0.4-1-1v-4c0-0.6,0.4-1,1-1s1,0.4,1,1v4C13.1,21.7,12.7,22.1,12.1,22.1" />
		<path fill={primary} opacity="0.3" d="M5.1,19.2c-0.4-0.4-0.4-1,0-1.4L7.9,15c0.4-0.4,1-0.4,1.4,0s0.4,1,0,1.4l-2.8,2.8C6.1,19.6,5.5,19.6,5.1,19.2" />
		<path fill={primary} opacity="0.1" d="M7.1,13.1h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4c0.6,0,1,0.4,1,1S7.7,13.1,7.1,13.1" />
		<path fill={primary} opacity="1" d="M7.9,9.3L5.1,6.5c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0l2.8,2.8c0.4,0.4,0.4,1,0,1.4C8.9,9.7,8.3,9.7,7.9,9.3" />
	</svg>
);

export const LockSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>lock</title>
		<path fill={tertiary} d="M19.1,23H4.9c-1,0-1.9-0.8-1.9-1.8V10.8C3,9.8,3.9,9,4.9,9H19c1.1,0,1.9,0.8,1.9,1.8v10.5C21,22.2,20.1,23,19.1,23z M12,1c3.2,0,5.8,2.3,6,5.2l0,0V7v2h-3c0-0.5,0-0.8,0-1.1V7.6c0-0.1,0-0.2,0-0.3V6.1l0,0C14.9,4.9,13.6,4,12,4S9.1,4.9,9,6.1v1V9H6V7V6.1C6.2,3.3,8.8,1,12,1z" />
		<path fill={secondary} d="M21,17c0,0-1.3,0-2.7,0.6c-2.7,1.2-7.1,5.1-15.3,3.6V23h18V17z" />
		<path fill={primary} d="M19.1,8L19.1,8L19,6.1C18.8,2.7,15.8,0,12,0S5.2,2.7,5,6.2V8H4.9C3.3,8,2,9.2,2,10.8v10.5C2,22.8,3.3,24,4.9,24H19c1.6,0,2.9-1.2,2.9-2.8V10.8C22,9.2,20.7,8,19.1,8z M7,6.2C7.1,3.9,9.3,2,12,2s4.9,1.9,5,4.2V8h-1V6.1C15.9,4.3,14.1,3,12,3S8.1,4.3,8,6.1V8H7V6.2z M14,8h-4V6.2C10,5.6,10.8,5,12,5s2,0.6,2,1.1V8z M20,21.3c0,0.4-0.4,0.8-0.9,0.8H4.9C4.4,22,4,21.7,4,21.3V10.8C4,10.3,4.4,10,4.9,10H5h5h4h5h0.1c0.5,0,0.9,0.3,0.9,0.8V21.3z M15,15.2c0,0.9-0.8,1.9-1.7,2.3V19h-1.7v-1.5c-0.9-0.3-1.7-1.3-1.7-2.2C9.9,14,11,13,12.4,13S15,14,15,15.2z" />
	</svg>
);

LockSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const MeasurementsSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>measurements</title>
		<path fill={secondary} d="M10.5,4.5c0.6,0,1-0.4,1-0.9V3.4c0-0.5-0.4-0.9-1-0.9s-1,0.3-1,0.9v0.3C9.5,4.1,9.9,4.5,10.5,4.5L10.5,4.5z M20.5,15.5c0.6,0,1-0.4,1-0.9v-0.3c0-0.5-0.4-0.9-1-0.9s-1,0.3-1,0.9v0.3C19.5,15.1,19.9,15.5,20.5,15.5L20.5,15.5z M2.7,18.5l-0.1,0.1l0.9,0.8L19.4,3.5l0,0l-0.8-0.9L2.7,18.5z" />
		<path fill={primary} d="M18,13v2h-3v2h2v2h-2v3h-2v-9H18z M20.5,13c0.9,0,1.5,0.5,1.5,1.3l0,0v0.4c0,0.8-0.6,1.3-1.5,1.3S19,15.5,19,14.7l0,0v-0.4C19,13.5,19.6,13,20.5,13z M20.5,13.9c-0.2,0-0.4,0.2-0.4,0.4l0,0v0.4c0,0.2,0.2,0.4,0.4,0.4c0.2,0,0.4-0.2,0.4-0.4l0,0v-0.4C20.9,14.1,20.7,13.9,20.5,13.9z M6,2l1,1v1H4v5h3v1l-1,1H3l-1-1V3l1-1H6z M10.5,2C11.4,2,12,2.5,12,3.3l0,0v0.4C12,4.5,11.4,5,10.5,5S9,4.5,9,3.7l0,0V3.3C9,2.5,9.6,2,10.5,2z M10.5,2.9c-0.2,0-0.4,0.2-0.4,0.4l0,0v0.4c0,0.2,0.2,0.4,0.4,0.4s0.4-0.2,0.4-0.4l0,0V3.3C10.9,3.1,10.7,2.9,10.5,2.9z" />
	</svg>
);

MeasurementsSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const OverwriteDrinkSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>overwriteDrink</title>
		<path fill={primary} d="M16.5,2C18.5,2,20,3.6,20,5.5l0,0l0,1.7C22.3,7.8,24,9.7,24,12c0,2.8-2.4,5-5.4,5l-0.1,0 c-0.5,0.9-1.2,1.8-2,2.6C14.2,21.8,11.2,23,8,23c-0.1,0-0.2,0-0.2,0c-2.8-0.1-4.6-0.2-6.7-2.1c-0.3-0.2-0.5-0.5-0.5-0.8 c0-0.6,0.4-1,1-1c0.3,0,0.5,0.1,0.7,0.3l0,0l0,0C4.1,21,5.4,21,7.8,21c0.1,0,0.1,0,0.2,0c2.6,0,5.1-1,7-2.9c1.9-1.9,3-4.4,3-7.1l0,0 V5.5C18,4.7,17.4,4,16.5,4l0,0H1C0.4,4,0,3.6,0,3s0.4-1,1-1l0,0H16.5z M5,7c0.5,0,0.9,0.4,0.9,0.9l0,0v6.7L8.5,12 c0.3-0.4,0.9-0.4,1.3,0s0.3,0.9,0,1.3l0,0l-4.1,4.2c-0.2,0.2-0.4,0.3-0.6,0.3c0,0,0,0,0,0l0,0c-0.2,0-0.5-0.1-0.6-0.3l0,0l-4.1-4.2 c-0.3-0.4-0.3-0.9,0-1.3c0.4-0.4,0.9-0.4,1.3,0l0,0l2.6,2.6V7.9C4.1,7.4,4.5,7,5,7z M20,9.2l0,1.7c0,1.4-0.2,2.7-0.7,4 c1.6-0.3,2.7-1.5,2.7-3C22,10.8,21.2,9.7,20,9.2z" />
	</svg>
);

export const PauseSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>pause</title>
		<path fill={primary} d="M5,4h6v16H5V4z M13,4h6v16h-6V4z" />
	</svg>
);

export const PlaySvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>play</title>
		<polygon fill={primary} points="6,4 19,12 6,20 " />
	</svg>
);

export const PlusminusSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>plusminus</title>
		<path fill={primary} d="M17,19c0.6,0,1,0.4,1,1s-0.4,1-1,1H7c-0.6,0-1-0.4-1-1s0.4-1,1-1H17z M12,1c0.6,0,1,0.4,1,1v4h4 c0.6,0,1,0.4,1,1s-0.4,1-1,1h-4v4c0,0.6-0.4,1-1,1s-1-0.4-1-1V8H7C6.4,8,6,7.6,6,7s0.4-1,1-1h4V2C11,1.4,11.4,1,12,1z" />
	</svg>
);

export const PressbuttonSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>pressbutton</title>
		<path fill={secondary} d="M16.6,2.5c-3,0-5.5,2.5-5.5,5.6c0,0.3,0,0.5,0.1,0.7L11.2,9l0.4,0.1l1,0.4l0.4-0.3 c0.5-0.4,1-0.8,1.4-1.3c0.4-0.4,0.7-0.7,1-1.1c0.7-0.9,1.9-1,2.8-0.3c0.9,0.7,1.1,1.9,0.3,2.8l-0.3,0.3L18,10 c-0.2,0.3-0.5,0.5-0.8,0.9l-0.6,0.6c-0.2,0.2-0.5,0.5-0.7,0.7l-1,1l0.8,0.3c0.2,0.1,0.5,0.1,0.8,0.1c3,0,5.5-2.5,5.5-5.5 S19.6,2.5,16.6,2.5z" />
		<path fill={primary} d="M16.6,1c3.9,0,7,3.1,7,7s-3.1,7-7,7c-0.4,0-0.8-0.1-1.2-0.2l0,0.1c0.1,0.8,0.1,1.5-0.4,2 c-0.3,0.4-1.2,0.9-2.6,1.7l-1,0.5c-0.7,0.4-1.5,0.8-2.3,1.2l-0.8,0.4L6.5,23c-0.3,0.4-0.9,0.5-1.3,0.2l-0.1-0.1 c-0.4-0.3-0.5-0.9-0.2-1.3l0.1-0.1l2-2.5l1.3-0.6c0.6-0.3,1.2-0.6,1.7-0.9l1-0.5l0.8-0.4c0.9-0.5,1.6-0.9,1.7-1l0-0.1l0-0.1 c0-0.1,0-0.1,0-0.2c0-0.2-0.1-0.5-0.2-0.7l0-0.1l-0.7,0.8c-0.2,0.2-0.5,0.3-0.7,0.1c-0.2-0.2-0.2-0.4-0.1-0.6l0-0.1l1-1.3l1.2-1.3 c0.6-0.5,1.1-1.1,1.6-1.6l0.3-0.3l0.4-0.4l0.4-0.4c0.3-0.3,0.5-0.6,0.7-0.8c0.2-0.3,0.2-0.5-0.1-0.7c-0.2-0.2-0.5-0.2-0.7,0.1 c-0.2,0.3-1.7,1.8-4.5,4.7c-0.2,0.2-0.5,0.3-0.7,0.1c-0.2-0.2-0.2-0.4-0.1-0.6l0-0.1l0.7-0.9l-0.3-0.1l-0.7-0.2L10,12 c-0.2,0.2-0.5,0.3-0.7,0.1c-0.2-0.2-0.2-0.4-0.1-0.6l0-0.1l0.9-1.1l-0.5-0.2c-0.2,0-0.3-0.1-0.5-0.1L8,11.4 c-0.2,0.2-0.5,0.3-0.7,0.1c-0.2-0.2-0.2-0.4-0.1-0.6l0-0.1l0.8-1c-0.8-0.1-1.3,0-1.6,0.4c-0.1,0.2-0.3,0.4-0.5,0.8 c-0.3,0.5-0.6,1.3-0.9,2.1c-0.3,0.8-0.6,1.6-0.8,2.4l-0.3,1.1l-0.1,0.2l-2,2.5c-0.3,0.4-1,0.5-1.4,0.2c-0.4-0.3-0.5-0.9-0.2-1.3 L0.2,18l1.7-2.2L2.2,15l0-0.1c0.3-0.9,0.6-1.7,0.9-2.5c0.3-0.9,0.7-1.7,1-2.3c0.2-0.4,0.5-0.8,0.7-1.1c1-1.3,2.3-1.6,4.8-0.9l0,0 C9.6,4.1,12.7,1,16.6,1z M16.6,3c-2.8,0-5,2.2-5,5.1c0,0.3,0,0.4,0,0.6l0.2,0.1L12.6,9l0.2-0.2c0.5-0.4,0.9-0.8,1.4-1.2 c0.3-0.4,0.7-0.7,0.9-1c0.9-1.2,2.4-1.3,3.5-0.4C19.7,7,19.9,8.5,19,9.6L18.7,10c-0.3,0.4-0.7,0.8-1.1,1.3L17,11.8 c-0.3,0.4-0.7,0.7-1.1,1.1c0.1,0,0.3,0.1,0.6,0.1c2.8,0,5-2.2,5-5S19.3,3,16.6,3z" />
	</svg>
);

PressbuttonSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ProbeSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>probe</title>
		<path fill={primary} d="M17.5,9C16.1,9,15,7.9,15,6.5S16.1,4,17.5,4S20,5.1,20,6.5S18.9,9,17.5,9 M13,6.5 c0,0.3,0,0.6,0.1,0.9l-2,2c-0.6,0.6-0.6,1.6,0,2.2l0,0l0,0l-9,9L2,22h1.5l9-9c0.6,0.6,1.5,0.6,2.1,0l2-2l0,0 c0.3,0.1,0.6,0.1,0.9,0.1C20,11,22,9,22,6.5S20,2,17.5,2S13,4,13,6.5z" />
		<path fill={secondary} d="M13.5,9.2l-0.7,0.6c-0.4,0.4-0.4,1-0.1,1.4c0,0,0,0,0,0l0,0c0.4,0.4,1,0.4,1.4,0c0,0,0,0,0,0 l0.6-0.7l0,0L13.5,9.2z" />
	</svg>
);

ProbeSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),	
];

export const ProbeCalibrationSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>probeCalibration</title>
		<path fill={primary} d="M17.5,9C16.1,9,15,7.9,15,6.5S16.1,4,17.5,4S20,5.1,20,6.5S18.9,9,17.5,9 M13,6.5c0,0.3,0,0.6,0.1,0.9l-2,2 c-0.6,0.6-0.6,1.6,0,2.2l0,0l0,0l-9,9L2,22h1.5l9-9c0.6,0.6,1.5,0.6,2.1,0l2-2l0,0c0.3,0.1,0.6,0.1,0.9,0.1C20,11,22,9,22,6.5 S20,2,17.5,2S13,4,13,6.5z M14.5,20l-2-2l-1,1l2.8,3.5L22,15l-1-1L14.5,20z" />
		<path fill={secondary} d="M13.5,9.2l-0.7,0.6c-0.4,0.4-0.4,1-0.1,1.4c0,0,0,0,0,0l0,0c0.4,0.4,1,0.4,1.4,0c0,0,0,0,0,0 l0.6-0.7l0,0L13.5,9.2z" />
	</svg>
);

ProbeCalibrationSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),	
];

export const ProbeOilSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>probeOil</title>
		<rect fill={tertiary} x="4" y="11" width="16" height="10" />
		<path fill={primary} d="M20.9,8c0.6,0,1,0.4,1,1c0,0.5-0.3,0.9-0.8,1L21,10l0,0.1l0,0.3l0,8.7c0,1.6-1.3,2.9-2.8,3L18,22H6c-1.6,0-2.9-1.3-3-2.8L3,19v-8c0-0.5,0-0.8-0.1-1l0,0l0,0C2.4,9.9,2.1,9.6,2,9.1L2,9c0-0.6,0.4-1,1-1c1.5,0,2,1,2,2.8L5,11v8c0,0.5,0.4,0.9,0.9,1L6,20h12c0.5,0,0.9-0.4,1-0.9l0-0.1l0-8.4c0-0.7,0-1,0.2-1.4C19.4,8.5,20,8,20.9,8z M15.5,16c0.8,0,1.5,0.7,1.5,1.5c0,0.8-0.7,1.5-1.5,1.5c-0.8,0-1.5-0.7-1.5-1.5C14,16.7,14.7,16,15.5,16z M5.7,2.8c1-1,2.5-1,3.5,0s1,2.5,0,3.5C9.1,6.4,9.1,6.4,9,6.5l0,0V9H8l0,2l9.5,0c0.3,0,0.5,0.2,0.5,0.5c0,0.2-0.2,0.4-0.4,0.5l-0.1,0L8,12l0,6.5L7.5,19L7,18.5L7,12l-0.5,0C6.2,12,6,11.8,6,11.5c0-0.2,0.2-0.4,0.4-0.5l0.1,0L7,11l0-2H6V6.5C5.9,6.4,5.9,6.4,5.7,6.3C4.8,5.3,4.8,3.7,5.7,2.8z M15.5,17c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5c0.3,0,0.5-0.2,0.5-0.5C16,17.2,15.8,17,15.5,17zM10.5,15.5c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1c-0.6,0-1-0.4-1-1C9.5,15.9,9.9,15.5,10.5,15.5z M13,13.1c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9c-0.5,0-0.9-0.4-0.9-0.9C12.1,13.5,12.5,13.1,13,13.1z M8.6,3.5C8,2.9,7,2.9,6.4,3.5S5.8,5,6.4,5.6s1.5,0.6,2.1,0S9.1,4.1,8.6,3.5z" />
	</svg>
);

ProbeOilSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ProbeWaterSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>probeWater</title>
		<rect fill={tertiary} x="4" y="11" width="16" height="10" />
		<path fill={primary} d="M20.9,8c0.6,0,1,0.4,1,1c0,0.5-0.3,0.9-0.8,1L21,10l0,0.1l0,0.3l0,8.7c0,1.6-1.3,2.9-2.8,3 L18,22H6c-1.6,0-2.9-1.3-3-2.8L3,19v-8c0-0.5,0-0.8-0.1-1l0,0l0,0C2.4,9.9,2.1,9.6,2,9.1L2,9c0-0.6,0.4-1,1-1c1.5,0,2,1,2,2.8L5,11 v8c0,0.5,0.4,0.9,0.9,1L6,20h12c0.5,0,0.9-0.4,1-0.9l0-0.1l0-8.4c0-0.7,0-1,0.2-1.4C19.4,8.5,20,8,20.9,8z M5.7,2.8c1-1,2.5-1,3.5,0 s1,2.5,0,3.5C9.1,6.4,9.1,6.4,9,6.5l0,0V9H8l0,2l9.5,0c0.3,0,0.5,0.2,0.5,0.5c0,0.2-0.2,0.4-0.4,0.5l-0.1,0L8,12l0,6.5L7.5,19 L7,18.5L7,12l-0.5,0C6.2,12,6,11.8,6,11.5c0-0.2,0.2-0.4,0.4-0.5l0.1,0L7,11l0-2H6V6.5C5.9,6.4,5.9,6.4,5.7,6.3 C4.8,5.3,4.8,3.7,5.7,2.8z M8.6,3.5C8,2.9,7,2.9,6.4,3.5S5.8,5,6.4,5.6s1.5,0.6,2.1,0S9.1,4.1,8.6,3.5z" />
	</svg>
);
//Dupe?
export const PulseV2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>pulseV2</title>
		<path fill={primary} d="M11.1,19.4l-3.2-7.9l-1,2C6.7,13.8,6.4,14,6,14H3c-0.6,0-1-0.4-1-1s0.4-1,1-1h2.4l1.7-3.4 C7.3,8.2,7.6,8,8,8c0.4,0,0.7,0.3,0.9,0.6l2.9,7.4L15,4.7C15.2,4.3,15.6,4,16,4c0.5,0,0.8,0.3,1,0.7l1.8,7.3H21c0.6,0,1,0.4,1,1 s-0.4,1-1,1h-3c-0.4,0-0.8-0.3-1-0.7L16,8.5l-3,10.8C12.8,19.7,12.5,20,12,20C11.6,20,11.2,19.8,11.1,19.4z" />
	</svg>
);

export const PurgeCycleSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>purgeCycle</title>
		<path fill={tertiary} d="M9.3,7.7C8.6,8.9,7.8,10.3,7,11.6c-0.9,1.6-1.2,3.1-0.8,4.7C6.8,19,9.3,21,12,21 c2.7,0,5.2-2,5.8-4.7c0.4-1.6,0.1-3.1-0.8-4.7c-0.8-1.3-1.6-2.7-2.3-3.9c-0.5-0.8-1.8-3-2.7-4.7C11.1,4.7,9.8,6.9,9.3,7.7z" />
		<path fill={primary} d="M12.9,2.5l0.6,1.1c0.8,1.4,1.6,2.9,2.1,3.6l1.7,2.9l0.6,1c1.1,1.8,1.4,3.6,0.9,5.4 C18.1,19.7,15.2,22,12,22c-3.2,0-6.1-2.3-6.8-5.5c-0.4-1.9-0.1-3.6,0.9-5.4l1.8-3l0.7-1.2c0.6-1.1,1.7-3,2.5-4.3 C11.5,1.8,12.5,1.8,12.9,2.5z M12.8,6.5L12,5l-1.2,2.1c-0.3,0.4-0.5,0.8-0.6,1.1l-1.7,2.9l-0.6,1c-0.8,1.4-1,2.6-0.7,4 C7.7,18.3,9.8,20,12,20c2.2,0,4.3-1.7,4.9-3.9c0.3-1.3,0.1-2.6-0.7-4l-1.8-3l-0.7-1.2C13.4,7.5,13.1,7,12.8,6.5z" />
	</svg>
);

PurgeCycleSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const QuickboilSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>quickboil</title>
		<path fill={tertiary} d="M11.3,6C7,6,5.1,8,4.7,8.6l-2.5,0C2.1,8.6,1.9,8.8,2,9l2.6,3.2v5.7C4.7,19.6,6.1,21,8,21h5.9 c1.8,0,4.2-1.4,4.2-3.1V8.8C18,8.8,16.5,6,11.3,6z" />
		<path fill={primary} d="M11.5,5c3.4,0,5.4,1.1,6.5,2l0,0l0,0h1.9l0.2,0C21.2,7.1,22,8.2,22,9.4l0,0v3.8l0,0.2 c-0.1,2.1-1.3,3.9-3,4.4l0,0l0,0.6c0,1.9-2.7,3.5-4.7,3.5l0,0H7.7c-2,0-3.7-1.6-3.7-3.5l0,0V12L1,8.4C0.9,8.2,1.1,8,1.3,8l0,0l2.8,0 C4.5,7.3,6.6,5,11.5,5z M17,9H6v9.5l0,0.1C6.1,19.4,6.8,20,7.7,20l0,0h6.6l0.2,0c1.2-0.1,2.5-1,2.5-1.5l0,0V9z M11.5,10.5 c0.3,0,0.5,0.3,0.5,0.6l0,0v6.7c0,0.4-0.2,0.6-0.5,0.6l0,0l-0.1,0c-0.2,0-0.4-0.3-0.4-0.6l0,0v-6.7C11,10.8,11.2,10.5,11.5,10.5z  M19.9,9H19l0,6.6c0.6-0.5,1-1.4,1-2.4l0,0V9.4l0-0.1C20,9.1,19.9,9,19.9,9L19.9,9z M11.5,7C9.6,7,8.2,7.4,7.1,8l0,0l0,0l8.9,0 C14.8,7.4,13.4,7,11.5,7z M4.2,2C4.3,2,4.3,2,4.2,2C4.3,2,4.3,2,4.2,2c0.9,0.5,1.3,1,1.3,1.5c0,1.2-1.9,2-1.9,2.9 c0,0.3,0.2,0.6,0.6,1c0.1,0,0.1,0.1,0,0.2c0,0-0.1,0.1-0.1,0.1c0,0,0,0-0.1,0C3.3,7.2,3,6.7,3,6.3c0-0.6,0.5-1,1-1.5 C4.5,4.3,5,3.8,4.9,3.4c0-0.3-0.2-0.7-0.8-1.1c0,0,0,0,0,0v0c0,0,0,0,0,0c0,0,0-0.1,0-0.1C4.1,2,4.2,2,4.2,2L4.2,2z M2.2,2 C2.2,2,2.3,2,2.2,2c1,0.5,1.3,1,1.3,1.5c0,1.2-1.9,2-1.9,2.9c0,0.3,0.2,0.6,0.6,1c0.1,0,0.1,0.1,0,0.2c0,0-0.1,0.1-0.1,0.1 c0,0,0,0-0.1,0C1.3,7.2,1,6.7,1,6.3c0-0.6,0.5-1,1-1.5C2.5,4.3,3,3.8,2.9,3.4c0-0.3-0.2-0.7-0.8-1.1c0,0,0,0,0-0.1l0,0c0,0,0,0,0,0 c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0C2.1,2,2.1,2,2.2,2C2.2,2,2.2,2,2.2,2L2.2,2z" />
	</svg>
);

QuickboilSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
];

export const QuicktempSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>quicktemp</title>
		<path fill={tertiary} d="M8,3c1.7,0,2.9,1.3,3,3v5c1.6,1,3,3.2,3,5.2c0,3.2-2.7,5.8-6,5.8s-6-2.6-6-5.8c0-2,1.4-4.2,3-5.2V6 C5.1,4.3,6.3,3,8,3z" />
		<path fill={primary} d="M15,2h7c0.6,0,1,0.4,1,1l0,0c0,0.6-0.4,1-1,1h-7c-0.6,0-1-0.4-1-1l0,0C14,2.4,14.4,2,15,2z M15,5h7 c0.6,0,1,0.4,1,1l0,0c0,0.6-0.4,1-1,1h-7c-0.6,0-1-0.4-1-1l0,0C14,5.4,14.4,5,15,5z M15,8h7c0.6,0,1,0.4,1,1l0,0c0,0.6-0.4,1-1,1h-7 c-0.6,0-1-0.4-1-1l0,0C14,8.4,14.4,8,15,8z M8,23c-3.9,0-7-3.1-7-6.8c0-2.1,1.2-4.4,3-5.7V6c0.1-2.3,1.8-4,4-4s3.9,1.7,4,3.9l0,4.5 c1.8,1.4,3,3.6,3,5.7C15,19.9,11.9,23,8,23z M8,4C6.7,4,6.1,5,6,6.1l0,5.5l-0.5,0.3C4.1,12.8,3,14.6,3,16.2C3,18.8,5.2,21,8,21 s5-2.2,5-4.8c0-1.6-1.1-3.4-2.5-4.3L10,11.6V6C9.9,5,9.3,4,8,4z" />
		<path fill={secondary === colors.transparent ? primary : secondary} d="M8,9c0.6,0,1,0.4,1,1l0,0l0,3.2c1.2,0.4,2,1.5,2,2.8c0,1.7-1.3,3-3,3s-3-1.3-3-3c0-1.3,0.8-2.4,2-2.8L7,10 l0-0.1C7.1,9.4,7.5,9,8,9z" />
	</svg>
);

QuicktempSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
];

export const ReadyatSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>readyat</title>
		<path fill={primary} d="M12,3c2.2,0,4.2,0.8,5.7,2l0,0l-1.4,1.4C15.1,5.6,13.6,5,12,5c-3.9,0-7,3.1-7,7s3.1,7,7,7 c3.8,0,6.9-3,7-6.7l0,0l1.9-1.9c0.1,0.5,0.2,1.1,0.2,1.6c0,5-4,9-9,9c-5,0-9-4-9-9S7,3,12,3z M20.7,6.3c0.4,0.4,0.4,0.9,0.1,1.3 l-0.1,0.1l-8,8c-0.4,0.4-0.9,0.4-1.3,0.1l-0.1-0.1l-4-4c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,0.9-0.4,1.3-0.1l0.1,0.1l3.3,3.3l7.3-7.3 C19.7,5.9,20.3,5.9,20.7,6.3z" />
	</svg>
);

export const RepeatSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>repeat</title>
		<path fill={primary} d="M19.2,20.5c-0.8-1-1.7-1.9-2.6-2.6c-2.9,2.9-7.5,2.9-10.3,0s-2.9-7.5,0-10.3 c2.3-2.3,5.6-2.7,8.3-1.4l-3.2,3.2L23,11.6L20.8,0l-3.4,3.4C13.1,0.8,7.4,1.3,3.7,5c-4.3,4.3-4.3,11.2,0,15.5S15,24.8,19.2,20.5z" />
	</svg>
);

export const ResetSvg = ({ primary, secondary, tertiary, size }) => (
	// Reset artboard to 0,0 and removed fill={primary} on path as only 1 path
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>reset</title>
		<path fill={primary} d="M18,11l3,3h-2.2c-0.7,3.4-3.7,6-7.3,6c-2.4,0-4.5-1.1-5.8-2.8l2-0.7c1,0.9,2.3,1.5,3.8,1.5c2.5,0,4.6-1.7,5.3-4H15L18,11z M11.5,5c2.5,0,4.8,1.3,6.2,3.2l-2,0.7c-1-1.2-2.5-1.9-4.2-1.9C8.6,7,6.3,9.2,6,12h2l-3,3l-3-3h2C4.3,8.1,7.5,5,11.5,5z" />
	</svg>
);

ResetSvg.colorVariation = [
	generateOneFillTheme("Blue", colors.productBlue),
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const ReverseSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>reverse</title>
		<path fill={primary} d="M18.2,5.7c-3-3-7.6-3.5-11.2-1.4L6,3.2C5.6,2.8,5.2,3,5.1,3.5L4,8.7C3.9,9.2,4.3,9.6,4.8,9.4l5-1.1 c0.5-0.1,0.7-0.5,0.3-0.9L8.5,5.8c0,0-0.1-0.1-0.1-0.1c2.7-1.4,6.2-1,8.5,1.3c2.8,2.8,2.8,7.4,0,10.3c-2.8,2.8-7.4,2.8-10.3,0 c-0.4-0.4-1-0.4-1.3,0c-0.4,0.4-0.4,1,0,1.3c3.6,3.6,9.4,3.6,12.9,0C21.8,15,21.8,9.2,18.2,5.7z" />
	</svg>
);

ReverseSvg.colorVariation = [
	generateOneFillTheme("Blue", colors.productBlue),
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const RotatetraySvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>rotatetray</title>
		<path fill={primary} d="M1.7,13.2c0.6,0,1,0.4,1,1c0,3.6,1.9,5.7,5.3,6L8,18l5,2.8l-5,2.8l0-1.4c-4.5-0.3-7.3-3.3-7.3-8 C0.7,13.7,1.2,13.2,1.7,13.2z M19.9,13.6c0.2,0.5,0.1,1-0.3,1.3l-0.1,0.1l-2,1c-0.1,0.1-0.2,0.1-0.3,0.1L17,16H9 c-0.1,0-0.2,0-0.3-0.1l-0.1,0l-2-1c-0.5-0.2-0.7-0.8-0.4-1.3c0.2-0.5,0.8-0.7,1.2-0.5l0.1,0L9.2,14h7.5l1.8-0.9 C19,12.9,19.6,13.1,19.9,13.6z M15,0.4V2c5,0,8,3,8,8c0,0.6-0.4,1-1,1s-1-0.4-1-1c0-3.9-2.1-6-6-6v2l-5-2.8L15,0.4z M16.9,8.6 c0.2,0.5,0.1,1-0.3,1.3l-0.1,0.1l-2,1c-0.1,0.1-0.2,0.1-0.3,0.1L14,11H6c-0.1,0-0.2,0-0.3-0.1l-0.1,0l-2-1C3.1,9.6,2.9,9,3.1,8.6 c0.2-0.5,0.8-0.7,1.2-0.5l0.1,0L6.2,9h7.5l1.8-0.9C16,7.9,16.6,8.1,16.9,8.6z" />
	</svg>
);

export const ScaleSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>scale</title>
		<polygon fill={tertiary} points="17.2,10 6.7,10 3.9,20 20,20 " />
		<path fill={secondary} d="M13,16.5c0.3,0.1,0.4-0.1,0.6,0.2l0.2,0.3h3.2c-0.1-0.6-0.2-1.8-0.9-2.5c-0.3-0.3-0.4-0.5-0.5-0.5 L13,16.5z" />
		<path fill={primary} d="M21.1,2c0.5,0,0.8,0.4,0.8,0.9l0,0.1v2c0,1.1-0.9,1.9-1.9,2l-0.2,0H14v1h3.2c0.9,0,1.7,0.6,2,1.4 l0,0.2l2.6,10c0.2,0.6,0,1.2-0.4,1.7c-0.4,0.4-0.9,0.7-1.5,0.8l-0.2,0H4.1c-0.7,0-1.3-0.3-1.7-0.8c-0.4-0.4-0.5-1-0.4-1.6l0-0.2 l2.6-10c0.2-0.8,1-1.4,1.9-1.5l0.2,0H10V7H3.9C2.8,7,2.1,6.2,2,5.1L2,5V3c0-0.6,0.3-1,0.9-1c0.5,0,1,0.4,1,0.9l0,0.1v2H20V3 C20,2.4,20.5,2,21.1,2z M17.2,10H6.7L3.9,20H20L17.2,10z M12,12c3.2,0,5.9,2.6,6,5.8l0,0.2h-5c0-0.5-0.5-1-1-1c-0.5,0-0.9,0.4-1,0.9 l0,0.1H6C6,14.7,8.7,12,12,12z M12,13c-2.3,0-4.3,1.6-4.9,3.8l0,0.2h3.2c0.3-0.5,0.6-0.8,1.1-0.9l0.2,0l1.4-1.6 c0.2-0.2,0.5-0.2,0.7-0.1c0.2,0.2,0.2,0.4,0.1,0.6l-0.1,0.1L12.9,16c0.3,0.1,0.5,0.4,0.6,0.7l0.2,0.3h3.2C16.4,14.7,14.4,13,12,13z" />
	</svg>
);

ScaleSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Send_logSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>send_log</title>
		<path fill={secondary} d="M2.8,16.5h4c-0.5,1.6-1.2,2.7-2.1,3.4C3.7,19.2,3.1,18.1,2.8,16.5" />
		<path fill={primary} d="M16.5,9C16.8,9,17,8.8,17,8.5S16.8,8,16.5,8h-4.1C12.2,8,12,8.2,12,8.5S12.2,9,12.5,9H16.5z M15.5,13c0.3,0,0.5-0.2,0.5-0.5S15.8,12,15.5,12h-4.1c-0.3,0-0.5,0.2-0.5,0.5s0.2,0.5,0.5,0.5H15.5z M14.5,17c0.3,0,0.5-0.2,0.5-0.5S14.8,16,14.5,16h-4.1c-0.3,0-0.5,0.2-0.5,0.5s0.2,0.5,0.5,0.5H14.5z M3.8,6h3.5c0.3,0,0.5-0.2,0.5-0.5S7.5,5,7.2,5H3.8C3.5,5,3.2,5.2,3.2,5.5S3.5,6,3.8,6 M5.5,9h-4C1.2,9,1,9.2,1,9.5S1.2,10,1.5,10h4C5.8,10,6,9.8,6,9.5S5.8,9,5.5,9 M3.5,13C3.2,13,3,13.2,3,13.5S3.2,14,3.5,14h2.1c0.3,0,0.5-0.2,0.5-0.5S5.8,13,5.6,13H3.5z M18,9h1V8h-1V9z M17,13h1v-1h-1V13z M16,17h1v-1h-1V17z M19.8,3h-8.5c-1.6,0-2,0.3-2.1,1.9C9,7.5,7.6,12.6,6.7,15.5H2.2c0,0.5,0,1.3,0.2,2.2c0.3,1.4,1.2,3.7,2.2,3.7h8c3.9,0,5.7-1.4,6.7-5c1.7-5.2,2.5-9,2.5-11.4C21.8,3.3,21.5,3,19.8,3z M5.6,17c-0.3,0.6-0.7,1.3-1.1,1.9c-0.2-0.4-0.4-1-0.5-1.5c0-0.1-0.1-0.3-0.1-0.4H5.6z M8.1,16c0.9-2.8,2.3-8.2,2.5-11c0-0.2,0-0.4,0.1-0.5c0.1,0,0.3,0,0.6,0h8.5c0.2,0,0.4,0,0.5,0c0,0.1,0,0.3,0,0.5c0,2.2-0.7,5.9-2.2,11c-0.9,3.1-2.1,4-5.3,4H6.2c0.8-1.1,1.3-2.3,1.6-3l0,0" />
	</svg>
);

Send_logSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ShareSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>share</title>
		<path fill={primary} d="M11.3,2.3L7.5,6.5c-0.3,0.3-0.3,0.7,0,1c0.3,0.3,0.7,0.3,1,0c0,0,0,0,0,0L11,5l0,0l0,9 c0,0.6,0.5,1,1,1c0,0,0,0,0,0c0.6,0,1-0.5,1-1l0-9l0,0l2.5,2.6c0.3,0.3,0.7,0.3,1,0c0,0,0,0,0,0c0.3-0.3,0.3-0.7,0-1l-3.8-4.3 C12.4,1.9,11.7,1.9,11.3,2.3C11.3,2.3,11.3,2.3,11.3,2.3z M15,9V11h3v9H5.9v-9H9V9H4v13h16V9H15z" />
	</svg>
);

export const SoundsdisplaySvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>soundsdisplay</title>
		<path fill={primary} d="M13.8,5H4C3.5,5,3,5.4,3,6s0.5,1,1,1h9.8V5z M20.2,5H16v2h4.2C20.6,7,21,6.6,21,6s-0.5-1-1-1 M6,11H4c-0.5,0-1,0.4-1,1s0.5,1,1,1h2V11z M9,11v2h11c0.5,0,1-0.4,1-1c0-0.5-0.5-1-1-1H9z M12,17H4c-0.5,0-1,0.5-1,1s0.5,1,1,1h8V17z M15,17v2h5c0.4,0,1-0.5,1-1s-0.6-1-1-1H15z" />
		<path fill={secondary === colors.transparent ? primary : secondary} d="M13.5,21c-0.3,0-0.5-0.2-0.5-0.5v-5c0-0.3,0.2-0.5,0.5-0.5h1c0.3,0,0.5,0.2,0.5,0.5v5c0,0.3-0.2,0.5-0.5,0.5H13.5z M7.5,15C7.2,15,7,14.8,7,14.5V9.6c0-0.3,0.2-0.5,0.5-0.5h1C8.8,9.1,9,9.3,9,9.6v4.9C9,14.8,8.8,15,8.5,15H7.5z M15.5,9C15.2,9,15,8.8,15,8.5v-5C15,3.2,15.2,3,15.5,3h1C16.8,3,17,3.2,17,3.5v5C17,8.8,16.8,9,16.5,9H15.5z" />
	</svg>
);

SoundsdisplaySvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const SpeedSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>speed</title>
		<path fill={tertiary} d="M14,2h-2l-1.6,6.3c-0.8,0.4-1.6,1-2.1,1.7H2v2l6.3,1.6c0.3,0.8,0.9,1.4,1.7,1.9V22h2l1.6-6.3c0.8-0.3,1.4-0.9,1.9-1.7H22v-2l-6.3-1.6c-0.3-0.8-0.9-1.7-1.7-2.1V2z" />
		<path fill={secondary} d="M15.4,14H22v-2l-6.3-1.6C15.5,10,15.3,9.6,15,9.2l-5.8,5.6c0.2,0.2,0.5,0.5,0.8,0.6V22h2l1.6-6.3C14.4,15.3,15,14.7,15.4,14" />
		<path fill={primary} d="M12.8,23H9v-7c-0.6-0.4-1-0.9-1.4-1.5L1,12.8V9h6.8C8.3,8.4,9,7.9,9.6,7.6L11.2,1H15v6.8c0.6,0.5,1.1,1.2,1.4,1.8l6.6,1.7V15h-7c-0.4,0.6-0.9,1-1.5,1.4L12.8,23z M11,21h0.2l1.5-6.1l0.5-0.2c0.6-0.2,1.1-0.7,1.4-1.3l0.3-0.5H21v-0.2l-6.1-1.5l-0.2-0.5c-0.3-0.7-0.8-1.4-1.3-1.6L13,8.9V3h-0.2l-1.5,6.1l-0.5,0.2c-0.7,0.3-1.4,0.8-1.6,1.3L8.9,11H3v0.2l6.1,1.5l0.2,0.5c0.2,0.6,0.7,1.1,1.3,1.4l0.5,0.3V21H11z M13,12c0,0.6-0.4,1-1,1s-1-0.4-1-1s0.4-1,1-1S13,11.4,13,12" />
	</svg>
);

SpeedSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
];

export const SteamWandv2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>steamWandv2</title>
		<path fill={tertiary} d="M20.7,8.2v9.1c0,2.1-1.7,3.8-3.7,3.8H7.8c-2,0-3.7-1.7-3.7-3.8V6.9c0-2.1,1.7-3.8,3.7-3.8h7v1.3c0,2.1,1.7,3.8,3.7,3.8H20.7z" />
		<path fill={primary} d="M21.8,16l0,0.3l0,0.1l0,0.1c0,0.3-0.1,0.7-0.2,1c0,0.1-0.1,0.3-0.1,0.4l-0.1,0.2l-0.1,0.1l-0.1,0.1c-0.1,0.1-0.1,0.2-0.2,0.3c-0.2,0.3-0.4,0.6-0.6,0.9l-0.2,0.2c-1,1-2.2,1.5-3.5,1.6l-0.3,0l-0.3,0l-0.1,0l-0.1,0l-0.1,0l-0.3,0l-0.2,0l-0.3-0.1l-0.1,0l-0.1,0c-0.8-0.3-1.5-0.7-2.1-1.3l-4.2-4.4c-0.3-0.4-0.3-0.9,0-1.3l0,0C8.8,14,9,13.9,9.2,13.9l0.1,0c0.2,0,0.5,0.1,0.6,0.3l1.9,2c0,0,0.1,0.1,0.1,0.1c0,0,0.1,0,0.1,0c0.1-0.1,0.1-0.2,0-0.2l-4.3-4.4c-0.3-0.4-0.3-0.9,0-1.3l0,0C8,10,8.2,9.9,8.4,9.9l0.1,0c0.2,0,0.5,0.1,0.6,0.3l3.8,4c0,0,0.1,0.1,0.1,0.1c0,0,0.1,0,0.1,0c0.1-0.1,0.1-0.2,0-0.2L8.5,9c-0.3-0.4-0.3-0.9,0-1.3l0,0C8.7,7.5,8.9,7.5,9,7.4l0.1,0c0.2,0,0.5,0.1,0.6,0.3l4.7,4.9c0,0,0.1,0.1,0.1,0.1c0,0,0.1,0,0.1,0c0.1-0.1,0.1-0.2,0-0.2l-3.9-4c-0.3-0.4-0.3-0.9,0-1.3l0,0c0.1-0.1,0.3-0.2,0.5-0.3l0.1,0c0.2,0,0.5,0.1,0.6,0.3l5.9,6.1c0,0,0.1,0,0.1,0s0.1,0,0.1,0c0.1-0.1,0.1-0.2,0-0.2l-2.6-2.7c-0.4-0.4-0.4-1,0-1.3l0,0l0.1-0.1l0.1-0.1l0,0c0.1-0.1,0.3-0.1,0.4-0.1c0.2,0,0.5,0.1,0.6,0.3l0.1,0.1l3.3,3.4c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.2,0.3,0.3,0.4l0.1,0.2l0.1,0.1l0.1,0.2l0.1,0.2l0.1,0.2l0.1,0.2l0,0.1l0.1,0.2l0.1,0.2l0,0.1l0.1,0.3l0,0.2l0,0.2l0,0.3L21.8,16z M11.5,19.2l-4,0l-0.2,0C6.1,19.1,5,17.9,5,16.5V6.7l0-0.2c0.1-1.4,1.2-2.4,2.5-2.4l5.5,0l0,0.2l0,0.2l0,0.2c0.1,1.6,1.1,2.9,2.4,3.6c0.2-0.2,0.6-0.3,0.9-0.3c0.4,0,0.7,0.1,0.9,0.4l0.5,0.5h1l0,1l2,2.1l0-4.1c0-0.3-0.1-0.6-0.4-0.8l-5.7-4.8l-0.1,0l-0.1-0.1c-0.2-0.1-0.4-0.2-0.5-0.1l-6.5,0l-0.2,0C4.9,2.2,3,4.3,3,6.7v9.8l0,0.2c0.1,2.4,2.1,4.4,4.5,4.4h6.2c-0.4-0.3-0.9-0.6-1.2-1L11.5,19.2z" />
	</svg>
);

SteamWandv2Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

// Wrong icon?
export const SteamWandSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>steamWand</title>
		<path fill={primary} d="M17.8,4.9c0.3,0.4,0.2,1-0.2,1.3l-0.1,0.1L8,13c-0.5,0.3-1.1,0.2-1.4-0.2 c-0.2-0.3-0.2-0.8,0-1.1c-0.9-0.5-1.9-0.4-2.5,0.2C3.4,12.6,3.5,14,4.5,15c0.4,0.4,0.4,1,0,1.4c-0.2,0.2-0.5,0.3-0.8,0.3 c-0.4,1-0.1,2.3,0.8,3.2c0.9,0.9,2.2,1.2,3.2,0.8c0-0.3,0.1-0.6,0.3-0.8c0.4-0.4,1-0.4,1.4,0c1,1,2.4,1.1,3.2,0.4 c0.6-0.6,0.6-1.6,0.2-2.5c-0.3,0.1-0.7,0.1-0.9-0.1c-0.4-0.3-0.5-0.8-0.3-1.3l0.1-0.1l6.3-9.5c0.3-0.5,1-0.6,1.4-0.3 c0.4,0.3,0.5,0.8,0.3,1.3l-0.1,0.1l-5.5,8.3c1.3,1.7,1.4,4.1-0.1,5.5c-1.3,1.3-3.2,1.4-4.8,0.5l0.1-0.1c-1.9,1.3-4.5,0.9-6.2-0.8 c-1.7-1.7-2.1-4.3-0.9-6.1c-0.9-1.6-0.7-3.6,0.5-4.8C4.2,9,6.6,9,8.4,10.4l8.1-5.7C16.9,4.4,17.5,4.5,17.8,4.9z M12.6,11.7 c0.2,0.2,0.2,0.4,0.1,0.6l-0.1,0.1l-3.7,3.7c-0.2,0.2-0.5,0.2-0.7,0C8,15.9,8,15.7,8.1,15.5l0.1-0.1l3.7-3.7 C12.1,11.5,12.4,11.5,12.6,11.7z M20.4,1.9c0.2-0.2,0.5-0.2,0.7,0l0,0l1.4,1.4c0.2,0.2,0.2,0.5,0,0.7l0,0l-1.4,1.4 C20.6,6,19.6,6,19,5.4c-0.6-0.6-0.6-1.5,0-2.1l0,0L20.4,1.9z" />
	</svg>
);

export const StopSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>stop</title>
		<rect fill={primary} x="6" y="6" width="12" height="12" />
	</svg>
);

export const StopwatchSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>stopwatch</title>
		<circle fill={tertiary} cx="12" cy="14" r="7.1" />
		<path fill={primary} d="M12,9c1.8,0,3.4,1,4.3,2.4L12,14V9z M14,2c0.6,0,1,0.4,1,1c0,0.5-0.4,0.9-0.9,1L14,4h-1l0,2.1c3.9,0.5,7,3.9,7,7.9c0,4.4-3.6,8-8,8s-8-3.6-8-8c0-4.1,3.1-7.4,7-7.9L11,4h-1C9.4,4,9,3.6,9,3c0-0.5,0.4-0.9,0.9-1L10,2H14z M12,8c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6-6S15.3,8,12,8z" />
	</svg>
);

StopwatchSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const TareSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>tare</title>
		<path fill={primary} d="M13,16v7h-2v-7H13z M18,16v4h-2v-4H18z M8,16v4H6v-4H8z M3,16v4H1v-4H3z M23,16v4h-2v-4H23z  M12.4,1.7c1.1,0,2,0.3,2.6,0.9c0.6,0.6,0.9,1.3,1,2.1l0,0.2v4.9l0,0.2c0,0.8-0.4,1.5-1,2.1c-0.6,0.6-1.5,0.9-2.6,0.9s-2-0.3-2.6-1 c-0.6-0.6-0.9-1.3-1-2.1l0-0.2V5l0-0.2c0-0.8,0.4-1.5,1-2.1C10.5,2,11.3,1.7,12.4,1.7z M12.4,3.1c-0.7,0-1.2,0.2-1.4,0.6 c-0.2,0.3-0.3,0.7-0.4,1.1l0,0.2v4.6l0,0.2c0,0.2,0,0.4,0.1,0.6c0.1,0.3,0.3,0.5,0.5,0.7c0.3,0.2,0.7,0.4,1.2,0.4 c0.4,0,0.7-0.1,0.9-0.2c0.3-0.1,0.4-0.3,0.6-0.5s0.2-0.4,0.2-0.6c0-0.1,0-0.2,0.1-0.4l0-0.2V4.9l0-0.2c0-0.1,0-0.3-0.1-0.4 c0-0.2-0.1-0.4-0.2-0.6c-0.1-0.2-0.3-0.4-0.6-0.5C13.1,3.1,12.8,3.1,12.4,3.1z" />
	</svg>
);

export const TempSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>temp</title>
		<path fill={tertiary} d="M12.3,0.5C10,0.6,8,2.6,8,5v4.5L7.8,9.7C6.1,11.1,5,13.2,5,15.5c0,4.1,3.4,7.5,7.5,7.5 s7.5-3.4,7.5-7.5l0-0.3c-0.1-2.2-1.1-4.2-2.8-5.6L17,9.5V5c0-2.5-2.1-4.5-4.5-4.5L12.3,0.5z" />
		<path fill={primary} d="M12.3,0.5C10,0.6,8,2.6,8,5v4.5L7.8,9.7C6.1,11.1,5,13.2,5,15.5c0,4.1,3.4,7.5,7.5,7.5 s7.5-3.4,7.5-7.5l0-0.3c-0.1-2.2-1.1-4.2-2.8-5.6L17,9.5V5c0-2.5-2.1-4.5-4.5-4.5L12.3,0.5z M12.5,2.5C13.8,2.5,15,3.6,15,5v5.6 l0.5,0.3c1.6,1,2.5,2.7,2.5,4.6c0,3-2.5,5.5-5.5,5.5S7,18.5,7,15.5c0-1.9,1-3.6,2.5-4.6l0.5-0.3V5C10,3.6,11.2,2.5,12.5,2.5z" />
		<path fill={secondary === colors.transparent ? primary : secondary} d="M12.5,7c0.8,0,1.4,0.6,1.5,1.4l0,0.1l0,3.8c1.2,0.6,2,1.8,2,3.2c0,1.9-1.6,3.5-3.5,3.5 S9,17.4,9,15.5c0-1.4,0.8-2.6,2-3.2l0-3.8C11,7.7,11.7,7,12.5,7z" />
	</svg>
);

TempSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
];

export const TempOffSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>tempOff</title>
		<path fill={primary} d="M20.8,6.4c0.3,0.4,0.3,1,0,1.3l-0.1,0.1l-3.6,3l0.2,0.2c1,1.3,1.8,2.7,1.9,4.7 c0,3.1-2.3,4.7-5.2,5.4c0.8-0.6,1.8-1.6,1.7-2.7c0-1.3-0.8-2.2-1.6-3.1l-0.3-0.3c-0.2-0.3-0.5-0.5-0.7-0.8l-4.3,3.5 c0,0.2-0.1,0.4-0.1,0.7c0,1.1,1,2.1,1.7,2.7c-1.5-0.3-2.7-0.9-3.7-1.7l-2.4,2c-0.4,0.3-1.1,0.3-1.4-0.1c-0.3-0.4-0.3-1,0-1.3 l0.1-0.1L19.4,6.2C19.8,5.9,20.4,5.9,20.8,6.4z M11.8,3C11.9,3,12,3.2,12,3.3c0.5,2,1.5,3.5,2.7,4.9l-9.7,8c0-0.2,0-0.4,0-0.6 c0.1-5.2,5.4-6.7,6.6-12.3C11.6,3.2,11.7,3,11.8,3z" />
	</svg>
);

TempOffSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
];

//Dupe?
export const Tickv2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>tick</title>
		<path fill={primary} d="M20.1,5.3l-11.3,11l-4.9-4.8c-0.4-0.4-1.2-0.4-1.6,0c-0.4,0.4-0.4,1.1,0,1.6l5.6,5.5 c0,0,0,0.1,0.1,0.1C8.2,18.9,8.5,19,8.8,19c0.3,0,0.6-0.1,0.8-0.3L21.7,6.9c0.4-0.4,0.4-1.1,0-1.6C21.2,4.9,20.5,4.9,20.1,5.3" />
	</svg>
);

export const Tick12Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Tick 12</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M20.4 2L8.4 14L3.6 9.2L0 12.8L8 21.2L24 5.6L20.4 2Z"/>
	</svg>
);

Tick12Svg.colorVariation = [
	generateOneFillTheme("Green", colors.success),
];

export const TimeSvg = ({ primary, secondary, tertiary, size, colorList }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>time</title>
		{/* <circle fill={tertiary} cx="12" cy="12" r="9" />
		<path fill={secondary} d="M14.7,13.8l4.4,3.8C20.3,16,21,14.1,21,12c0-5-4-9-9-9v8.6L14.7,13.8z" />
		<path fill={primary} d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2 M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8S7.6,4,12,4M12,6c0.5,0,0.9,0.4,1,0.9V7v4.1l2.6,2.1c0.4,0.3,0.5,0.9,0.2,1.3l-0.1,0.1c-0.3,0.4-0.9,0.5-1.3,0.2l-0.1-0.1l-3-2.4c-0.2-0.2-0.3-0.4-0.4-0.6v-0.1V7C11,6.4,11.4,6,12,6z" /> */}
		<circle fill={(colorList && colorList.color1) || tertiary} cx="12" cy="12" r="9" />
		<path fill={(colorList && colorList.color2) || secondary} d="M14.7,13.8l4.4,3.8C20.3,16,21,14.1,21,12c0-5-4-9-9-9v8.6L14.7,13.8z" />
		<path fill={(colorList && colorList.color3) || primary} d="M12,2C6.5,2,2,6.5,2,12s4.5,10,10,10s10-4.5,10-10S17.5,2,12,2 M12,4c4.4,0,8,3.6,8,8s-3.6,8-8,8s-8-3.6-8-8S7.6,4,12,4M12,6c0.5,0,0.9,0.4,1,0.9V7v4.1l2.6,2.1c0.4,0.3,0.5,0.9,0.2,1.3l-0.1,0.1c-0.3,0.4-0.9,0.5-1.3,0.2l-0.1-0.1l-3-2.4c-0.2-0.2-0.3-0.4-0.4-0.6v-0.1V7C11,6.4,11.4,6,12,6z" />
	</svg>
);

TimeSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateCustomTheme("Green", colors.functionCommercial, colors.success, colors.white),
	generateCustomTheme("Purple", colors.functionCommercial, colors.productPurple2, colors.white),
	generateCustomTheme("Blue", colors.functionCommercial, colors.productBlue2, colors.white),
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),	
];

export const TraySvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>tray</title>
		<path fill={primary} d="M7,15c-2.5,0-3.5-1.7-4-2.6c-0.1-0.1-0.2-0.3-0.3-0.4C2.3,11.9,2,11.5,2,11c0-0.6,0.4-1,1-1 c0.9,0,1.4,0.8,1.8,1.4C5.2,12.2,5.7,13,7,13h10c1.3,0,1.8-0.8,2.2-1.6c0.4-0.6,0.8-1.4,1.8-1.4c0.6,0,1,0.4,1,1 c0,0.5-0.3,0.9-0.8,1c-0.1,0.1-0.2,0.3-0.3,0.4c-0.5,0.9-1.5,2.6-4,2.6H7z" />
	</svg>
);

export const UserSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>user</title>
		<path fill={tertiary} d="M8.5,13c-1.7,0-3,1-3.5,2.5s-0.5,1.5-0.7,3l-0.1,0.3v0.3h8.7l-0.1-0.4c-0.2-1.3-0.6-2.5-1-3.3C11,14,10,13,8.5,13z M8.5,5C7,5,6.2,5.9,6.2,7.7c0,1.2,0.4,1.9,1.2,2.4s1.5,0.5,2.3,0c0.7-0.4,1.1-1,1.2-2V7.9V7.7C10.8,5.9,10,5,8.5,5z" />
		<path fill={secondary} d="M9,19.5h4.5c0-2.5-1-5-2-6c-0.7-0.7-1.5-1-2.5-1V19.5z" />
		<path fill={primary} d="M13.9,4.1c0.4-0.1,1,0,2,0C17.5,4.3,19,6,19,8c0,1.3-0.3,2.7-1.2,3.7l-0.2,0.2h0.2c3.6,0.6,4.2,5.1,4.2,7.8v0.2h-5c-0.2-3.4-1.1-6.5-3-8.3l-0.1-0.1l0.1-0.1c0.6-1,0.9-2.3,0.9-3.6C14.8,6.4,14.5,5.1,13.9,4.1z M8.5,3c2.7,0,4.3,1.9,4.3,4.7c0,1.8-0.6,3.1-1.9,4l-0.2,0.1L11,12c2.9,1,3.9,5.3,4,8.7V21H2c0-3.5,1.4-8.1,4.4-9.1C4.9,11,4.2,9.6,4.2,7.7C4.2,4.9,5.8,3,8.5,3zM8.5,13c-1.7,0-3,1-3.5,2.5s-0.5,1.5-0.7,3l-0.1,0.3v0.3h8.7l-0.1-0.4c-0.2-1.3-0.6-2.5-1-3.3C11,14,10,13,8.5,13z M8.5,5C7,5,6.2,5.9,6.2,7.7c0,1.2,0.4,1.9,1.2,2.4s1.5,0.5,2.3,0c0.7-0.4,1.1-1,1.2-2V7.9V7.7C10.8,5.9,10,5,8.5,5z" />
	</svg>
);

UserSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const VolSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>vol</title>
		<path fill={primary} d="M13,2v20l-6-5.3H2V7.3h5L13,2z M19.7,5C21,6.9,22,10.2,22,12.9c0,2.7-0.7,5.8-2.1,7.6 c-0.3-0.3-1.2-1.1-1.5-1.4c1.1-1.6,1.6-3.9,1.6-6.2c0-2.4-0.9-5-2-6.6l0,0L19.7,5z M16.7,8c0.6,1,1.3,3,1.3,5s-0.7,3.5-1.3,4.4 c-0.2-0.3-1.4-1.1-1.6-1.4c0.4-0.7,0.9-1.5,0.9-3.1s-0.6-2.5-1-3.3l0,0L16.7,8z" />
	</svg>
);

export const WarmSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>warm</title>
		<path fill={primary} d="M22,17v4H2v-4H22z M6.4,3.5C7.5,5.2,7.2,6.5,5.7,9l-0.6,1c-1.1,1.8-1.3,2.6-0.8,3.4l0.1,0.1 c0.3,0.5,0.2,1.1-0.3,1.4S2.9,15,2.6,14.5C1.5,12.8,1.8,11.5,3.3,9l0.6-1l0.3-0.4c0.9-1.5,1-2.3,0.5-3C4.4,4.1,4.6,3.4,5,3.2 C5.5,2.9,6.1,3,6.4,3.5z M13.4,3.5c1.1,1.7,0.8,3.1-0.7,5.5l-0.6,1l-0.3,0.4c-0.9,1.5-1,2.2-0.6,2.9l0.1,0.1 c0.3,0.5,0.2,1.1-0.3,1.4c-0.5,0.3-1.1,0.2-1.4-0.3c-1.1-1.7-0.8-3.1,0.7-5.5l0.6-1l0.3-0.4c0.9-1.5,1-2.2,0.6-2.9l-0.1-0.1 c-0.3-0.5-0.2-1.1,0.3-1.4C12.5,2.9,13.1,3,13.4,3.5z M20.4,3.5c1.1,1.7,0.8,3.1-0.7,5.5l-0.6,1c-1.1,1.8-1.3,2.6-0.8,3.4l0.1,0.1 c0.3,0.5,0.2,1.1-0.3,1.4c-0.5,0.3-1.1,0.2-1.4-0.3c-1.1-1.7-0.8-3.1,0.7-5.5l0.6-1l0.3-0.4c0.9-1.5,1-2.3,0.5-3 c-0.3-0.5-0.2-1.1,0.3-1.4C19.5,2.9,20.1,3,20.4,3.5z" />
	</svg>
);

WarmSvg.colorVariation = [
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
	generateOneFillTheme("Error", colors.error),
];

// combine
export const WaterSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>water</title>
		<path fill={tertiary} d="M11,20c-2.2,0-4.4-1.7-4.9-3.9c-0.3-1.3-0.1-2.6,0.7-3.9c0.6-1.1,1.3-2.2,2-3.3C9.3,8.1,10.2,6.4,11,5c0.8,1.4,1.8,3.1,2.2,3.9c0.1,0.1,0.1,0.2,0.2,0.3c0.6,0.9,1.2,1.9,1.7,2.9c0.8,1.3,1,2.6,0.7,3.9c-0.1,0.3-0.2,0.7-0.4,1C14.7,18.8,12.9,20,11,20" />
		<path fill={primary} d="M14.3,12.6c0.7,1.2,0.8,2.3,0.6,3.4c-0.1,0.3-0.2,0.6-0.3,0.9c-1.2-0.6-2.1-1.8-2.4-3.2c-0.3-1.2-0.1-2.4,0.6-3.6c0-0.1,0.1-0.1,0.1-0.2C13.3,10.9,13.8,11.8,14.3,12.6 M20.9,13.9c-0.3,1.3-1.2,2.5-2.3,3.1c0.5-2.1,0.1-4.2-1.1-6.3c-0.6-1.1-1.3-2.2-1.9-3.3V7.3c-0.1-0.2-0.3-0.6-0.5-0.9c0.4-0.8,0.9-1.6,1.2-2.2c0.1-0.3,0.5-0.3,0.6,0c0.5,1,1.6,2.9,2,3.6c0.5,0.8,1,1.7,1.5,2.5C21,11.5,21.2,12.7,20.9,13.9 M11,20c-2.2,0-4.4-1.7-4.9-3.9c-0.3-1.3-0.1-2.6,0.7-3.9c0.6-1.1,1.3-2.2,2-3.3C9.3,8.1,10.2,6.4,11,5c0.8,1.4,1.8,3.1,2.2,3.9c0.1,0.1,0.1,0.2,0.2,0.3c0.6,0.9,1.2,1.9,1.7,2.9c0.8,1.3,1,2.6,0.7,3.9c-0.1,0.3-0.2,0.7-0.4,1C14.7,18.8,12.9,20,11,20 M16.9,10.9c-0.6-1.1-1.3-2.2-1.9-3.3c-0.1-0.2-0.2-0.3-0.3-0.6c-0.7-1.2-1.8-3.2-2.4-4.3C12,2.3,11.5,2,11,2s-1,0.3-1.2,0.8C9.1,4,7.6,6.6,7,7.6c-0.6,1.1-1.3,2.2-1.9,3.3c-1.1,1.8-1.4,3.6-0.9,5.5C4.9,19.6,7.8,22,11,22c3,0,5.8-2.2,6.7-5.1c0-0.1,0.1-0.3,0.1-0.4C18.2,14.5,17.9,12.7,16.9,10.9" />
	</svg>
);

WaterSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),
];


export const WaterHardnessSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>waterHardness</title>
		<rect fill={tertiary} x="9.8" y="3.3" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -4.9199 11.7999)" width="4" height="17" />
		<path fill={primary} opacity="25%" d="M14.9,13.5l-1.1-1.1l-1.4,1.4l1.1,1.1c0.2,0.2,0.4,0.2,0.6,0l0.8-0.8C15.1,13.9,15.1,13.7,14.9,13.5" />
		<polygon fill={primary} opacity="50%" points="13.1,11.7 11.7,10.3 10.3,11.6 11.6,13.1 " />
		<polygon fill={primary} opacity="75%" points="8.1,9.5 9.5,10.9 10.9,9.5 9.5,8.1 " />
		<path fill={primary} d="M16.4,21.3c-0.5,0-1-0.2-1.4-0.6L3,8.7c-0.8-0.8-0.8-2,0-2.8L5.8,3c0.8-0.8,2-0.8,2.8,0l12,12c0.8,0.8,0.8,2,0,2.8l-2.8,2.8C17.5,21.1,16.9,21.3,16.4,21.3z M7.2,4.4L4.4,7.2l12,12l2.8-2.8L7.2,4.4z M6.3,7.7l1.1,1.1l1.4-1.4L7.7,6.3c-0.2-0.2-0.4-0.2-0.6,0L6.3,7.1C6.1,7.3,6.1,7.5,6.3,7.7" />
	</svg>
);

WaterHardnessSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

// Maybe we can combine these 3 and provide opacity prop for other icons
export const Wifi1Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>wifi1</title>
		<path fill={primary} d="M12,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S13.1,16,12,16" />
		<path fill={primary} opacity="50%" d="M21.6,7.9C19,5.4,15.6,4,12,4C8.4,4,5,5.4,2.4,7.9c-0.6,0.6-0.6,1.5,0,2.1 c0.6,0.6,1.5,0.6,2.1,0C6.5,8.1,9.2,7,12,7c2.8,0,5.5,1.1,7.4,3.1c0.3,0.3,0.7,0.4,1.1,0.4c0.4,0,0.8-0.1,1.1-0.4 C22.1,9.5,22.1,8.5,21.6,7.9" />
		<path fill={primary} opacity="50%" d="M12,10c-2.1,0-4.1,0.8-5.6,2.4c-0.6,0.6-0.6,1.6,0,2.2c0.6,0.6,1.5,0.6,2.1,0 c0.9-1,2.2-1.5,3.5-1.5c1.3,0,2.5,0.5,3.5,1.5c0.3,0.3,0.7,0.4,1,0.4c0.4,0,0.8-0.1,1-0.4c0.6-0.6,0.6-1.6,0-2.2 C16.1,10.8,14.1,10,12,10" />
	</svg>
);

Wifi1Svg.colorVariation = [
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const Wifi2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>wifi2</title>
		<path fill={primary} d="M12,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S13.1,16,12,16" />
		<path fill={primary} opacity="50%" d="M21.6,7.9C19,5.4,15.6,4,12,4C8.4,4,5,5.4,2.4,7.9c-0.6,0.6-0.6,1.5,0,2.1c0.6,0.6,1.5,0.6,2.1,0 C6.5,8.1,9.2,7,12,7c2.8,0,5.5,1.1,7.4,3.1c0.3,0.3,0.7,0.4,1.1,0.4c0.4,0,0.8-0.1,1.1-0.4C22.1,9.5,22.1,8.5,21.6,7.9z" />
		<path fill={primary} d="M12,10c-2.1,0-4.1,0.8-5.6,2.4c-0.6,0.6-0.6,1.6,0,2.2c0.6,0.6,1.5,0.6,2.1,0 c0.9-1,2.2-1.5,3.5-1.5c1.3,0,2.5,0.5,3.5,1.5c0.3,0.3,0.7,0.4,1,0.4c0.4,0,0.8-0.1,1-0.4c0.6-0.6,0.6-1.6,0-2.2 C16.1,10.8,14.1,10,12,10" />
	</svg>
);

Wifi2Svg.colorVariation = [
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const Wifi3Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>wifi3</title>
		<path fill={primary} d="M12,16c-1.1,0-2,0.9-2,2s0.9,2,2,2s2-0.9,2-2S13.1,16,12,16" />
		<path fill={primary} d="M21.6,7.9C19,5.4,15.6,4,12,4C8.4,4,5,5.4,2.4,7.9c-0.6,0.6-0.6,1.5,0,2.1c0.6,0.6,1.5,0.6,2.1,0 C6.5,8.1,9.2,7,12,7c2.8,0,5.5,1.1,7.4,3.1c0.3,0.3,0.7,0.4,1.1,0.4c0.4,0,0.8-0.1,1.1-0.4C22.1,9.5,22.1,8.5,21.6,7.9" />
		<path fill={primary} d="M12,10c-2.1,0-4.1,0.8-5.6,2.4c-0.6,0.6-0.6,1.6,0,2.2c0.6,0.6,1.5,0.6,2.1,0 c0.9-1,2.2-1.5,3.5-1.5c1.3,0,2.5,0.5,3.5,1.5c0.3,0.3,0.7,0.4,1,0.4c0.4,0,0.8-0.1,1-0.4c0.6-0.6,0.6-1.6,0-2.2 C16.1,10.8,14.1,10,12,10" />
	</svg>
);

Wifi3Svg.colorVariation = [
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const WifiAddSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>wifiAdd</title>
		<polygon fill={tertiary} points="17.2,10 14.8,10 14.8,13.8 11,13.8 11,16.2 14.8,16.2 14.8,20 17.2,20 17.2,16.2 21,16.2 21,13.8 17.2,13.8 " />
		<path fill={primary} d="M11.1,20L11.1,20L11.1,20C9.9,20,9,19.1,9,18c0-0.4,0.1-0.7,0.3-1C9.6,18.1,10.3,19.2,11.1,20z M9.1,14.1L9,14.2c-0.1,0.1-0.3,0.2-0.4,0.4c-0.6,0.6-1.5,0.6-2.1,0s-0.6-1.6,0-2.2c1.3-1.3,2.9-2.1,4.6-2.3C10,11.1,9.3,12.5,9.1,14.1z M12,4c3.6,0,7,1.4,9.6,3.9c0.6,0.6,0.6,1.5,0,2.1c-0.1,0.1-0.2,0.2-0.4,0.3C20,9,18.4,8.2,16.6,8c-1.4-0.7-3-1-4.6-1c-2.8,0-5.4,1.1-7.4,3.1c-0.6,0.6-1.5,0.6-2.1,0S1.9,8.6,2.5,8C5,5.4,8.4,4,12,4z M16,9c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6-6S19.3,9,16,9z M20,16h-3v3h-2v-3h-3v-2h3v-3h2v3h3V16z" />
	</svg>
);

WifiAddSvg.colorVariation = [
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const WifiErrorSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>wifiError</title>
		<circle fill={tertiary} cx="16" cy="15" r="4.8" />
		<path fill={primary} d="M11.1,20L11.1,20L11.1,20C9.9,20,9,19.1,9,18c0-0.4,0.1-0.7,0.3-1C9.6,18.1,10.3,19.2,11.1,20z M9.1,14.1L9,14.2c-0.1,0.1-0.3,0.2-0.4,0.4c-0.6,0.6-1.5,0.6-2.1,0s-0.6-1.6,0-2.2c1.3-1.3,2.9-2.1,4.6-2.3C10,11.1,9.3,12.5,9.1,14.1z M12,4c3.6,0,7,1.4,9.6,3.9c0.6,0.6,0.6,1.5,0,2.1c-0.1,0.1-0.2,0.2-0.4,0.3C20,9,18.4,8.2,16.6,8c-1.4-0.7-3-1-4.6-1c-2.8,0-5.4,1.1-7.4,3.1c-0.6,0.6-1.5,0.6-2.1,0S1.9,8.6,2.5,8C5,5.4,8.4,4,12,4z M16,9c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6-6S19.3,9,16,9z M17,18.5c0,0.3-0.2,0.5-0.5,0.5h-1c-0.3,0-0.5-0.2-0.5-0.5v-1c0-0.3,0.2-0.5,0.5-0.5h1c0.3,0,0.5,0.2,0.5,0.5V18.5zM17,15.3c0,0.4-0.5,0.7-1,0.7c-0.6,0-1-0.3-1-0.7v-3.6c0-0.4,0.4-0.7,1-0.7c0.5,0,1,0.3,1,0.7V15.3z" />
	</svg>
);

WifiErrorSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const WifiOffSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>wifiOff</title>
		<path fill={primary} d="M12,16c1.1,0,2,0.9,2,2s-0.9,2-2,2s-2-0.9-2-2S10.9,16,12,16z M22.8,4.4 c0.3,0.4,0.3,1,0,1.3l-0.1,0.1l-1.8,1.5c0.3,0.2,0.5,0.4,0.7,0.7c0.6,0.6,0.6,1.5,0,2.1c-0.3,0.3-0.7,0.4-1.1,0.4 c-0.4,0-0.8-0.1-1.1-0.4c-0.3-0.3-0.6-0.6-1-0.9l-2.4,2c0.5,0.3,1.1,0.8,1.5,1.2c0.6,0.6,0.6,1.6,0,2.2c-0.3,0.3-0.7,0.4-1,0.4 c-0.4,0-0.8-0.1-1-0.4c-0.6-0.6-1.3-1-2-1.3l-7.3,6c-0.4,0.3-1.1,0.3-1.4-0.1c-0.3-0.4-0.3-1,0-1.3l0.1-0.1L21.4,4.2 C21.8,3.9,22.4,3.9,22.8,4.4z M11.8,10l-5.5,4.4c-0.5-0.6-0.4-1.5,0.1-2C7.9,10.9,9.8,10.1,11.8,10z M12,4c2,0,3.9,0.4,5.7,1.3 L15,7.4C14,7.1,13,7,12,7c-2.8,0-5.4,1.1-7.4,3.1c-0.6,0.6-1.5,0.6-2.1,0c-0.6-0.6-0.6-1.5,0-2.1C5,5.4,8.4,4,12,4z" />
	</svg>
);

WifiOffSvg.colorVariation = [
	generateOneFillTheme("Light Grey", colors.productLightGrey),
];

export const InStoreWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>inStoreWeb</title>
		<path fill={secondary} fillRule="evenodd" clipRule="evenodd" d="M11.4131 3.58832L14.0601 8.39224H21.119L18.3739 3.58832H11.4131Z" />
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M21.902 8.4902C21.902 8.58824 22 8.68627 22 8.68627C22 9.86275 21.3137 10.8431 20.2353 11.3333C20.2353 11.3333 20.2353 11.3333 20.2353 11.4314V19.9608C20.2353 20.2549 20.0392 20.451 19.7451 20.451H10.7255H6.90196H4.15686C3.86275 20.451 3.66667 20.2549 3.66667 19.9608V11.3333C2.68627 10.8431 2 9.86275 2 8.68627C2 8.58824 2 8.4902 2.09804 8.4902L5.03922 3.29412C5.13725 3.09804 5.23529 3 5.43137 3H18.4706C18.6667 3 18.7647 3.09804 18.8627 3.29412L21.902 8.4902ZM17.8824 4.37255H6.11765L4.15686 7.80392H19.7451L17.8824 4.37255ZM14.3529 10.451C15.2353 10.451 15.9216 9.86274 16.0196 9.07843H12.6863C12.8824 9.86274 13.5686 10.451 14.3529 10.451ZM9.54902 10.451C10.4314 10.451 11.1176 9.86274 11.2157 9.07843H7.88235C8.07843 9.86274 8.76471 10.451 9.54902 10.451ZM6.60784 9.07843H3.07843C3.27451 9.86274 3.96078 10.451 4.84314 10.451C5.72549 10.451 6.41176 9.86274 6.60784 9.07843ZM7.78431 18.9804H9.84314V14.5686C9.84314 14.2745 9.64706 13.9804 9.35294 13.9804H8.37255C8.07843 13.9804 7.88235 14.2745 7.88235 14.5686V18.9804H7.78431ZM11.2157 18.9804H18.9608V11.5294C18.9608 11.5294 19.2549 11.5294 19.1569 11.5294H19.0588C18.0784 11.5294 17.1961 11.0392 16.7059 10.2549C16.2157 11.0392 15.3333 11.5294 14.3529 11.5294C13.3725 11.5294 12.4902 11.0392 12 10.2549C11.5098 11.0392 10.6275 11.5294 9.64706 11.5294C8.66667 11.5294 7.78431 11.0392 7.29412 10.2549C6.70588 11.0392 5.82353 11.5294 4.84314 11.5294C4.7451 11.5294 4.94118 11.5294 4.94118 11.5294V18.9804H6.41176V14.1765C6.41176 13.2941 7.19608 12.5098 8.07843 12.5098H9.54902C10.4314 12.5098 11.2157 13.2941 11.2157 14.1765V18.9804ZM17.4902 8.98039C17.5882 9.76471 18.2745 10.3529 19.1569 10.3529C19.9412 10.3529 20.6275 9.76471 20.8235 8.98039H17.4902ZM13.4706 12.5098H16.2157C17 12.5098 17.5882 13.098 17.5882 13.8824V15.9412C17.5882 16.7255 17 17.3137 16.2157 17.3137H13.4706C12.6863 17.3137 12.098 16.7255 12.098 15.9412V13.7843C12.098 13.098 12.6863 12.5098 13.4706 12.5098ZM15.9216 16.0392C16.1176 16.0392 16.2157 15.9412 16.2157 15.7451V13.9804C16.2157 13.7843 16.1176 13.6863 15.9216 13.6863H13.6667C13.4706 13.6863 13.3725 13.7843 13.3725 13.9804V15.7451C13.3725 15.9412 13.4706 16.0392 13.6667 16.0392H15.9216Z" />
	</svg>
);

InStoreWebSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const StarRatingWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>starRatingWeb</title>
		<path fill={primary}  d="M12,0.5l2.7,8.8h8.8l-7.1,5.4l2.7,8.8L12,18.1l-7.1,5.4l2.7-8.8L0.5,9.3h8.8L12,0.5z" />
	</svg>
);

export const StarHalfRatingWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>starHalfRatingWeb</title>
		<path fill={secondary} d="M12,0.5l2.7,8.8h8.8l-7.1,5.4l2.7,8.8L12,18.1l-2.8-5.6L12,0.5z" />
		<path fill={primary} d="M12,0.5v17.6l-7.1,5.4l2.7-8.8L0.5,9.3h8.8L12,0.5z" />
	</svg>
);

StarHalfRatingWebSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ExperienceLevelWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>experienceLevelWeb</title>
		<path fill={secondary} d="M11.5,16.5V4.9l2.1,5.5h6.3l-5.3,3.9l2.1,6.6L11.5,16.5z" />
		<path fill={tertiary} d="M6.2,20.9l2.1-6.6l-5.3-3.9h6.3l2.7-6.2v12.6L6.2,20.9z" />
		<path fill={primary} d="M18.3,23l-6.8-5.2L4.7,23l2.6-8.4L0.5,9.4h8.4L11.5,1l2.6,8.4h8.4l-6.8,5.2L18.3,23z M11.5,15.8l3.8,2.9L13.8,14l3.8-2.9H13l-1.5-4.7L10,11.1H5.4L9.2,14l-1.5,4.7L11.5,15.8z" />
	</svg>
);

ExperienceLevelWebSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ActiveTimeWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>activeTimeWeb</title>
		<path fill={tertiary} d="M18.9597 2.23492L2.71289 18.8667L5.07876 21.2886C6.40082 22.642 8.65975 22.5317 10.1143 21.0426L17.4341 13.5494C18.703 12.2504 18.9468 10.3248 18.1105 8.95586L21.8178 5.16074C22.6848 4.27325 22.749 2.89499 21.961 2.0883C21.1731 1.28166 19.8267 1.34739 18.9597 2.23492Z" />
		<path fill={secondary} d="M11.7129 19.4484L14.1482 7.9806L15.1223 6.9834C15.6094 7.3158 16.8757 8.37948 18.0447 9.975C19.2136 11.5705 13.3364 17.9526 11.7129 19.4484Z" />
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M2.08263 19.0533L19.0394 1.69472C19.9443 0.768407 21.4166 0.768445 22.3214 1.69472C23.2263 2.62104 23.2263 4.12817 22.3214 5.05445L18.4521 9.01542C19.4386 10.5606 19.2698 12.658 17.9455 14.0137L10.3058 21.8344C8.78765 23.3885 6.3175 23.3885 4.79937 21.8344L2.08263 19.0533ZM16.8515 12.8938C17.7664 11.9572 17.7664 10.4334 16.8515 9.49676L15.2287 7.83556L4.27061 19.0533L5.89336 20.7145C6.80829 21.6511 8.29688 21.6511 9.21179 20.7145L16.8515 12.8938ZM17.4167 7.83556L21.2274 3.93454C21.5291 3.62574 21.529 3.12339 21.2274 2.81463C20.9258 2.50587 20.435 2.50587 20.1334 2.81463L16.3227 6.71565L17.4167 7.83556ZM3.72635 15.1327C3.17524 14.5685 3.25532 13.9124 3.30316 13.5205C3.31494 13.4244 3.33233 13.2819 3.32413 13.2278L3.32117 13.2247L3.31815 13.2216C3.26523 13.2132 3.1259 13.2311 3.03211 13.2431L3.03185 13.2431C2.64896 13.2921 2.00834 13.3739 1.45727 12.8098C0.90619 12.2457 0.986245 11.5898 1.03409 11.1978L1.03412 11.1976L1.03416 11.1973C1.04586 11.1013 1.06321 10.9591 1.05505 10.9049L1.05217 10.9019L1.05418 10.8999L1.05341 10.8967L1.05439 10.8996L2.14616 9.78199C2.69723 10.3461 2.61718 11.002 2.56933 11.394L2.56931 11.3941C2.55757 11.4902 2.54018 11.6326 2.54834 11.6868L2.55126 11.6899L2.55429 11.6929C2.60673 11.7012 2.74411 11.6837 2.83793 11.6718L2.83796 11.6718L2.84026 11.6715C3.22312 11.6225 3.86397 11.5405 4.41513 12.1047C4.96625 12.6689 4.88617 13.3249 4.83832 13.7168L4.83832 13.7168L4.83796 13.7198C4.8263 13.8158 4.80926 13.956 4.81738 14.0097L4.82034 14.0128L4.8183 14.0148L4.81903 14.0179L4.81808 14.0151L3.72635 15.1327ZM5.61168 9.16478L5.61165 9.16504C5.56384 9.557 5.48384 10.2128 6.03491 10.7769L7.12664 9.65933L7.12751 9.66209L7.12685 9.65911L7.1289 9.65702L7.12594 9.65392C7.1178 9.60019 7.13489 9.45954 7.14655 9.36349L7.14655 9.36348L7.14684 9.36114C7.19472 8.96921 7.27483 8.3132 6.72365 7.74895C6.17253 7.18478 5.53164 7.26672 5.14878 7.31573C5.05499 7.32775 4.91576 7.3456 4.86285 7.3372L4.85982 7.33417L4.85687 7.33108C4.84873 7.27691 4.86609 7.13438 4.87783 7.03837L4.87785 7.03821C4.9257 6.64622 5.00575 5.99036 4.45468 5.42622L3.36292 6.54385L3.362 6.54098L3.3627 6.54408L3.36069 6.54613L3.36364 6.54923C3.37176 6.60308 3.35457 6.74399 3.34287 6.83994L3.34264 6.84182L3.34261 6.84205C3.29477 7.23404 3.21471 7.88991 3.76579 8.45405C4.31682 9.01813 4.95753 8.93623 5.34041 8.88729L5.34059 8.88726L5.34347 8.8869L5.34348 8.8869C5.43729 8.87496 5.57426 8.85752 5.62663 8.86584L5.62966 8.86886L5.63261 8.87196C5.64082 8.92613 5.62346 9.06873 5.61168 9.16478ZM9.91971 8.79056C9.3686 8.22638 9.44868 7.57034 9.49652 7.17837C9.5083 7.08232 9.52569 6.93983 9.51749 6.88566L9.51453 6.88257L9.51151 6.87954C9.45859 6.87114 9.31926 6.88895 9.22547 6.90097L9.22521 6.901C8.84232 6.94995 8.2017 7.03184 7.65063 6.46771C7.09955 5.90357 7.1796 5.24771 7.22745 4.85571L7.22748 4.85549L7.22752 4.85518C7.23922 4.7592 7.25657 4.61697 7.24841 4.56282L7.24553 4.5598L7.24753 4.55775L7.24677 4.55457L7.24775 4.55752L8.33952 3.43989C8.89059 4.00402 8.81054 4.65989 8.76269 5.05188L8.76267 5.05204C8.75093 5.14805 8.73354 5.29054 8.7417 5.34474L8.74462 5.34781L8.74765 5.35083C8.80009 5.35912 8.93749 5.34163 9.03132 5.32969L9.03362 5.3294C9.41648 5.28039 10.0573 5.19841 10.6085 5.76262C11.1596 6.3268 11.0795 6.98277 11.0317 7.37467L11.0317 7.37473L11.0313 7.37763C11.0197 7.47366 11.0026 7.61392 11.0107 7.66755L11.0137 7.67065L11.0117 7.67274L11.0124 7.6758L11.0114 7.67296L9.91971 8.79056Z" />
	</svg>
);

ActiveTimeWebSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const FlagUSAWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>flagUSAWeb</title>
		<rect fill={tertiary} x="1.3" y="1.3" width="16.2" height="11.4" />
		<path fill={primary} d="M23,2.7V1h-5.3H1v1.7v1.7v1.7v1.7v1.7v1.7v1.7v0h16.7v0H23v-1.7h-5.3V9.5H23V7.8h-5.3V6.1H23V4.4h-5.3V2.7H23zM2.8,12.2l-0.4-0.3L2,12.2l0.2-0.5l-0.4-0.3h0.5L2.4,11l0.2,0.5H3l-0.4,0.3L2.8,12.2z M2.6,9.4l0.2,0.5L2.4,9.6L2,9.8l0.2-0.5L1.7,9.1h0.5l0.2-0.5l0.2,0.5H3L2.6,9.4z M2.6,7l0.2,0.5L2.4,7.2L2,7.5L2.1,7L1.7,6.7h0.5l0.2-0.5l0.2,0.5H3L2.6,7z M2.6,4.6l0.2,0.5L2.4,4.8L2,5.1l0.2-0.5L1.7,4.3h0.5l0.2-0.5l0.2,0.5H3L2.6,4.6z M2.6,2.3l0.2,0.5L2.4,2.4L2,2.7l0.2-0.5L1.7,2h0.5l0.2-0.5L2.5,2H3L2.6,2.3z M4.2,11l-0.4-0.3L3.4,11l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L4,10.6L4.2,11z M4,8.2l0.2,0.5L3.8,8.4L3.4,8.7l0.2-0.5L3.1,7.9h0.5l0.2-0.5l0.2,0.5h0.5L4,8.2z M4,5.8l0.2,0.5L3.8,6L3.4,6.3l0.2-0.5L3.1,5.5h0.5l0.2-0.5l0.2,0.5h0.5L4,5.8z M4,3.4l0.2,0.5L3.8,3.6L3.4,3.9l0.2-0.5L3.1,3.2h0.5l0.2-0.5l0.2,0.5h0.5L4,3.4z M5.6,12.2l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3H5L5.2,11l0.2,0.5h0.5l-0.4,0.3L5.6,12.2z M5.4,9.4l0.2,0.5L5.2,9.6L4.8,9.8l0.2-0.5L4.5,9.1H5l0.2-0.5l0.2,0.5h0.5L5.4,9.4z M5.4,7l0.2,0.5L5.2,7.2L4.8,7.5L4.9,7L4.5,6.7H5l0.2-0.5l0.2,0.5h0.5L5.4,7z M5.4,4.6l0.2,0.5L5.2,4.8L4.8,5.1l0.2-0.5L4.5,4.3H5l0.2-0.5l0.2,0.5h0.5L5.4,4.6z M5.4,2.3l0.2,0.5L5.2,2.4L4.8,2.7l0.2-0.5L4.5,2H5l0.2-0.5L5.3,2h0.5L5.4,2.3z M7,11l-0.4-0.3L6.2,11l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5l-0.4,0.3L7,11z M6.8,8.2L7,8.7L6.6,8.4L6.2,8.7l0.2-0.5L5.9,7.9h0.5l0.2-0.5l0.2,0.5h0.5L6.8,8.2z M6.8,5.8L7,6.3L6.6,6L6.2,6.3l0.2-0.5L5.9,5.5h0.5l0.2-0.5l0.2,0.5h0.5L6.8,5.8z M6.8,3.4L7,3.9L6.6,3.6L6.2,3.9l0.2-0.5L5.9,3.2h0.5l0.2-0.5l0.2,0.5h0.5L6.8,3.4z M8.4,12.2L8,11.9l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5L8,11l0.2,0.5h0.5l-0.4,0.3L8.4,12.2z M8.2,9.4l0.2,0.5L8,9.6L7.6,9.8l0.2-0.5L7.3,9.1h0.5L8,8.6l0.2,0.5h0.5L8.2,9.4z M8.2,7l0.2,0.5L8,7.2L7.6,7.5L7.7,7L7.3,6.7h0.5L8,6.2l0.2,0.5h0.5L8.2,7z M8.2,4.6l0.2,0.5L8,4.8L7.6,5.1l0.2-0.5L7.3,4.3h0.5L8,3.9l0.2,0.5h0.5L8.2,4.6z M8.2,2.3l0.2,0.5L8,2.4L7.6,2.7l0.2-0.5L7.3,2h0.5L8,1.5L8.1,2h0.5L8.2,2.3z M9.8,11l-0.4-0.3L9,11l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5H10l-0.4,0.3L9.8,11z M9.6,8.2l0.2,0.5L9.4,8.4L9,8.7l0.2-0.5L8.7,7.9h0.5l0.2-0.5l0.2,0.5H10L9.6,8.2z M9.6,5.8l0.2,0.5L9.4,6L9,6.3l0.2-0.5L8.7,5.5h0.5l0.2-0.5l0.2,0.5H10L9.6,5.8z M9.6,3.4l0.2,0.5L9.4,3.6L9,3.9l0.2-0.5L8.7,3.2h0.5l0.2-0.5l0.2,0.5H10L9.6,3.4z M11.2,12.2l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L11,11.7L11.2,12.2z M11,9.4l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L11,9.4z M11,7l0.2,0.5l-0.4-0.3l-0.4,0.3L10.5,7l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L11,7z M11,4.6l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L11,4.6z M11,2.3l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5L10.1,2h0.5l0.2-0.5L10.9,2h0.5L11,2.3z M12.5,11l-0.4-0.3L11.7,11l0.2-0.5l-0.4-0.3H12l0.2-0.5l0.2,0.5h0.5l-0.4,0.3L12.5,11z M12.4,8.2l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3H12l0.2-0.5l0.2,0.5h0.5L12.4,8.2z M12.4,5.8l0.2,0.5L12.1,6l-0.4,0.3l0.2-0.5l-0.4-0.3H12l0.2-0.5l0.2,0.5h0.5L12.4,5.8z M12.4,3.4l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3H12l0.2-0.5l0.2,0.5h0.5L12.4,3.4z M13.9,12.2l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5l-0.4,0.3L13.9,12.2z M13.8,9.4l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L13.8,9.4z M13.8,7l0.2,0.5l-0.4-0.3l-0.4,0.3L13.3,7l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L13.8,7z M13.8,4.6l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L13.8,4.6z M13.8,2.3l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5L12.9,2h0.5l0.2-0.5L13.7,2h0.5L13.8,2.3z M15.3,11l-0.4-0.3L14.5,11l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5l-0.4,0.3L15.3,11z M15.2,8.2l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L15.2,8.2z M15.2,5.8l0.2,0.5L14.9,6l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L15.2,5.8z M15.2,3.4l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5h0.5L15.2,3.4z M16.7,12.2l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5H17l-0.4,0.3L16.7,12.2z M16.6,9.4l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5H17L16.6,9.4z M16.6,7l0.2,0.5l-0.4-0.3l-0.4,0.3L16.1,7l-0.4-0.3h0.5l0.2-0.5l0.2,0.5H17L16.6,7z M16.6,4.6l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5l-0.4-0.3h0.5l0.2-0.5l0.2,0.5H17L16.6,4.6z M16.6,2.3l0.2,0.5l-0.4-0.3l-0.4,0.3l0.2-0.5L15.7,2h0.5l0.2-0.5L16.5,2H17L16.6,2.3z M1,14.5h22v1.7H1V14.5z M1,19.6h22v-1.7H1V19.6z M1,23h22v-1.7H1V23z" />
	</svg>
);

FlagUSAWebSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const CartWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>cartWeb</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M23.1,5.8c0.3,0,0.5,0.1,0.7,0.3C24,6.3,24,6.6,24,6.8l-1.8,9.1c-0.1,0.4-0.4,0.7-0.8,0.7H6.7c-0.4,0-0.8-0.3-0.8-0.7L3.5,3.8L0.7,3.3C0.2,3.2-0.1,2.7,0,2.3c0.1-0.5,0.5-0.8,1-0.7l3.4,0.6C4.7,2.3,5,2.5,5.1,2.9l0.5,2.9H23.1zM7.4,14.9h13.2l1.4-7.4H5.9L7.4,14.9z M10.3,16.7c-1.6,0-2.8,1.3-2.8,2.8c0,1.6,1.3,2.8,2.8,2.8c1.6,0,2.8-1.3,2.8-2.8C13.1,18,11.9,16.7,10.3,16.7z M10.3,20.7c-0.6,0-1.1-0.5-1.1-1.1s0.5-1.1,1.1-1.1c0.6,0,1.1,0.5,1.1,1.1S10.9,20.7,10.3,20.7zM17.6,16.7c-1.6,0-2.8,1.3-2.8,2.8c0,1.6,1.3,2.8,2.8,2.8c1.6,0,2.8-1.3,2.8-2.8C20.4,18,19.1,16.7,17.6,16.7z M17.6,20.7c-0.6,0-1.1-0.5-1.1-1.1s0.5-1.1,1.1-1.1s1.1,0.5,1.1,1.1S18.2,20.7,17.6,20.7z" />
	</svg>
);

//// Desinged for 30px
export const AlarmBellSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>AlarmBell</title>
		<path fill={tertiary} d="M24.2,13.2c0-4.4-2.4-8.7-6.8-9.6c-0.3-1-0.6-2.2-2.4-2.2s-2.2,1.1-2.5,2.2c-4.4,1-6.9,5.2-6.9,9.6c0,5.2-3.2,5.4-4.4,7.1c-0.7,0.9,0,2.3,1.3,2.3h12.2h12.7c1.2,0,2-1.4,1.3-2.3C27.5,18.6,24.2,18.4,24.2,13.2z" />
		<path fill={secondary} d="M24.9,12.8c0-4.8-2.6-9.4-7.3-10.5C17.3,1.2,17,0,15,0c0,0,1,21-6.5,23c2.3,0,8.9,0,19.8,0c1.3,0,2.1-1.5,1.4-2.5C28.4,18.7,24.9,18.5,24.9,12.8z" />
		<path fill={primary} d="M20,25H10c0.7,2.9,2.7,5,5,5C17.3,30,19.3,27.9,20,25z M29.7,20.5c-0.1-0.1-0.2-0.3-0.4-0.4L29.2,20l-0.2-0.2c0,0-0.1-0.1-0.1-0.1l-0.2-0.2L28,19.1l-0.4-0.3l-0.3-0.2c-0.1-0.1-0.2-0.1-0.3-0.2l-0.2-0.2c-1.1-1-2-2.5-2-5.4l0-0.3c-0.1-4.7-2.7-9.2-7.3-10.2L17.5,2l-0.1-0.2C17.2,0.8,16.7,0,15,0c-1.7,0-2.2,0.9-2.5,1.9l-0.1,0.2l-0.1,0.2C7.7,3.4,5,8,5,12.8l0,0.3C5,16.1,4,17.5,2.9,18.4l-0.2,0.2l-0.2,0.2L2.1,19l-0.7,0.5l-0.2,0.2l-0.2,0.2c0,0-0.1,0.1-0.1,0.1l-0.2,0.2c-0.1,0.1-0.2,0.3-0.4,0.4c-0.7,1,0,2.5,1.4,2.5h26.7l0.1,0C29.7,22.9,30.4,21.5,29.7,20.5z M2.8,21l0.7-0.5l0.3-0.2C5.9,18.6,7,16.4,7,12.8l0-0.3c0.1-4.1,2.4-7.5,5.8-8.3L14,4l0,0h1.9l0,0l1.2,0.3l0.2,0.1c3.3,0.9,5.5,4.3,5.5,8.5l0,0.4c0.1,3.5,1.3,5.5,3.5,7.2l0.2,0.2l0.6,0.4H2.8z" />
	</svg>
);

AlarmBellSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
	generateOneFillTheme("Light Grey 2", colors.productLightGrey2),	
];

export const AppSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>app</title>
		<path fill={primary} d="M21,0H9C7.3,0,6,1.5,6,3.3v23.5C6,28.5,7.3,30,9,30h12c1.7,0,3-1.5,3-3.3V3.3C24,1.5,22.7,0,21,0z M8,5.5h14v16H8V5.5z M9,2h12c0.5,0,1,0.6,1,1.3v1.2H8V3.3C8,2.6,8.5,2,9,2z M21,28H9c-0.5,0-1-0.6-1-1.3v-4.2h14v4.2C22,27.4,21.5,28,21,28zM14,25c0-0.6,0.4-1,1-1c0.6,0,1,0.4,1,1c0,0.6-0.4,1-1,1C14.4,26,14,25.6,14,25z M18.7,14.7l-3,3c-0.1,0.1-0.2,0.2-0.3,0.2C15.3,18,15.1,18,15,18c-0.1,0-0.3,0-0.4-0.1c-0.1-0.1-0.2-0.1-0.3-0.2l-3-3c-0.4-0.4-0.4-1,0-1.4s1-0.4,1.4,0l1.3,1.3V9c0-0.6,0.4-1,1-1c0.6,0,1,0.4,1,1v5.6l1.3-1.3c0.4-0.4,1-0.4,1.4,0S19.1,14.3,18.7,14.7z" />
	</svg>
);

export const CrossV2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>crossV2</title>
		<path fill={primary} d="M21.4,7.3c0.4-0.4,1-0.4,1.3,0c0.4,0.4,0.4,1,0,1.3l0,0L16.3,15l6.4,6.4c0.4,0.4,0.4,1,0,1.3c-0.2,0.2-0.4,0.3-0.7,0.3s-0.5-0.1-0.7-0.3l0,0l-6.5-6.5l-6.3,6.5c-0.4,0.4-0.9,0.4-1.3,0c-0.4-0.4-0.4-1,0-1.3l6.2-6.4L7.3,8.6c-0.4-0.4-0.4-1,0-1.3C7.4,7.1,7.7,7,7.9,7c0.2,0,0.5,0.1,0.6,0.3l6.3,6.5L21.4,7.3z M15,0C6.6,0,0,6.6,0,15c0,8.4,6.6,15,15,15s15-6.6,15-15C30,6.6,23.4,0,15,0 M15,2c7.3,0,13,5.7,13,13c0,7.3-5.7,13-13,13S2,22.3,2,15C2,7.7,7.5,2,14.8,2" />
	</svg>
);

export const GrateCoarseSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>grateCoarse</title>
		<path
		 fill={primary}
			d="M19.5,1C17,1,15,3,15,5.5s2,4.5,4.5,4.5S24,8,24,5.5S22,1,19.5,1 M19.5,3C20.9,3,22,4.1,22,5.5S20.9,8,19.5,8
S17,6.9,17,5.5S18.1,3,19.5,3 M19.5,21C17,21,15,23,15,25.5s2,4.5,4.5,4.5s4.5-2,4.5-4.5S22,21,19.5,21 M19.5,23
c1.4,0,2.5,1.1,2.5,2.5S20.9,28,19.5,28S17,26.9,17,25.5S18.1,23,19.5,23 M21.5,11C19,11,17,13,17,15.5s2,4.5,4.5,4.5s4.5-2,4.5-4.5
S24,11,21.5,11 M21.5,13c1.4,0,2.5,1.1,2.5,2.5S22.9,18,21.5,18S19,16.9,19,15.5S20.1,13,21.5,13"
		/>
		<path
		 fill={primary}
			opacity="0.5"
			d="M7,2C5.3,2,4,3.3,4,5s1.3,3,3,3s3-1.3,3-3S8.7,2,7,2z M8,6H6V4h2V6z M10,9c-1.7,0-3,1.3-3,3s1.3,3,3,3
s3-1.3,3-3S11.7,9,10,9z M11,13H9v-2h2V13z M10,16c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S11.7,16,10,16z M11,20H9v-2h2V20z M7,23
c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S8.7,23,7,23z M8,27H6v-2h2V27z"
		/>
	</svg>
);

//same
export const GrateFineSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>grateFine</title>
		<path
		 fill={primary}
			opacity="0.5"
			d="M19.5,1C17,1,15,3,15,5.5s2,4.5,4.5,4.5S24,8,24,5.5S22,1,19.5,1 M19.5,3C20.9,3,22,4.1,22,5.5
S20.9,8,19.5,8S17,6.9,17,5.5S18.1,3,19.5,3 M19.5,21C17,21,15,23,15,25.5s2,4.5,4.5,4.5s4.5-2,4.5-4.5S22,21,19.5,21 M19.5,23
c1.4,0,2.5,1.1,2.5,2.5S20.9,28,19.5,28S17,26.9,17,25.5S18.1,23,19.5,23 M21.5,11C19,11,17,13,17,15.5s2,4.5,4.5,4.5s4.5-2,4.5-4.5
S24,11,21.5,11 M21.5,13c1.4,0,2.5,1.1,2.5,2.5S22.9,18,21.5,18S19,16.9,19,15.5S20.1,13,21.5,13"
		/>
		<path
		 fill={primary}
			d="M7,2C5.3,2,4,3.3,4,5s1.3,3,3,3s3-1.3,3-3S8.7,2,7,2z M8,6H6V4h2V6z M10,9c-1.7,0-3,1.3-3,3s1.3,3,3,3
s3-1.3,3-3S11.7,9,10,9z M11,13H9v-2h2V13z M10,16c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S11.7,16,10,16z M11,20H9v-2h2V20z M7,23
c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S8.7,23,7,23z M8,27H6v-2h2V27z"
		/>
	</svg>
);

export const ManualSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>manual</title>
		<path fill={primary} d="M24,30H6.5C4.1,30,2,28.1,2,26v-9.5C2,15,1.4,15,1,15c-0.6,0-1-0.4-1-1s0.4-1,1-1c0.9,0,3,0.3,3,3.5V26c0,1,1.2,2,2.5,2H24c1.3,0,2-1,2-2v-9c0-2.5,1.1-4,3-4c0.6,0,1,0.4,1,1s-0.4,1-1,1c-0.8,0-1,1.1-1,2v9C28,28.2,26.2,30,24,30zM24,17.5c0,0.3-0.2,0.5-0.5,0.5H19v6.5c0,0.8-0.4,2.6-3.6,2.6h-0.2c-3.5,0-3.7-2-3.7-2.6V18h-5C6.2,18,6,17.8,6,17.5S6.2,17,6.5,17h5v-3c-0.9-0.7-1.5-1.8-1.5-3V4.1c0-2.2,1.9-4,4.3-4h2.4c2.4,0,4.3,1.8,4.3,4V11c0,1.4-0.8,2.7-2,3.4V17h4.5C23.8,17,24,17.2,24,17.5z M12,11c0,1.1,1,2,2.3,2h2.4c1.3,0,2.3-0.9,2.3-2V4.1c0-1.1-1-2-2.3-2h-2.4C13,2,12,2.9,12,4.1V11zM13.5,17H17v-2c-0.1,0-0.2,0-0.3,0h-2.4c-0.3,0-0.5,0-0.8-0.1V17z M17,18h-3.5v6.5c0,0.6,1.4,0.6,1.7,0.6h0.2c1.1,0,1.6-0.2,1.6-0.6V18z" />
	</svg>
);

export const MypresetSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>mypreset</title>
		<path fill={primary} d="M21,8.8l4,0.7V10H12.5l1-3l4.8,1.2L21,8.8z M4,10c0.7,0,4,0,5.9,0c0.5,0,0.9,0,1.2,0l0.1,0.3c0.2,0.5,0.6,0.7,1.2,0.7H26l0,14.7L4,26V10z M11.5,9.2C11.4,9.1,11.2,9,11,9c-0.4,0-0.1,0-1.5,0C8.6,9,7.1,9,5,9l7.6-2.8L11.5,9.2zM17,4.6l0.9,2.5L13.5,6L17,4.6z M28,11c0-1.5-1-1.5-1.3-1.5L27,8l-7.2-1.5L18.2,2L2.3,7.9L2.7,9c-0.4,0.3-0.6,0.8-0.6,1.4v16.2c0,0.8,0.6,1.4,1.4,1.4H26c0.9,0,2-1,2-1.9L28,11z M10,13l1.2,2.3l2.8,0.4l-2,1.8l0.5,2.5L10,18.8L7.5,20L8,17.5l-2-1.8l2.8-0.4L10,13z" />
	</svg>
);

export const NameSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>name</title>
		<path fill={primary} d="M5.8,18L9,9l3.2,9H5.8z M10.4,4C10.2,3.4,9.6,3,9,3C8.4,3,7.8,3.4,7.6,4L0.1,25c-0.3,0.8,0.1,1.6,0.9,1.9c0.8,0.3,1.6-0.1,1.9-0.9l1.8-5h8.6l1.8,5c0.2,0.6,0.8,1,1.4,1c0.2,0,0.3,0,0.5-0.1c0.8-0.3,1.2-1.1,0.9-1.9L10.4,4z M27.6,27c-0.9,0-1.8-0.7-1.8-1.5v-21c0-0.8,1-1.5,1.8-1.5h0.8C29.3,3,30,2.3,30,1.5S29.3,0,28.4,0h-0.8c-1.2,0-2.3,0.4-3.1,1.2C23.7,0.4,22.6,0,21.4,0h-0.8C19.7,0,19,0.7,19,1.5S19.7,3,20.6,3h0.8C22.2,3,23,3.7,23,4.5v21c0,0.8-0.8,1.5-1.6,1.5h-0.8c-0.9,0-1.6,0.7-1.6,1.5s0.7,1.5,1.6,1.5h0.8c1.2,0,2.3-0.4,3.1-1.2c0.8,0.7,1.9,1.2,3.1,1.2h0.8c0.9,0,1.6-0.7,1.6-1.5S29.3,27,28.4,27H27.6z" />
	</svg>
);

export const QuestionSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>question</title>
		<path fill={primary} d="M16.4,19.8c0.3,0,0.4-0.1,0.4-0.4l0,0v-3.1c1.8-0.1,3.2-0.7,4.3-2c1.1-1.2,1.7-2.9,1.7-5.1c0-1.2-0.2-2.3-0.6-3.4c-0.4-1.1-1.2-2-2.3-2.8s-2.5-1.2-4.2-1.2c-2.3,0-4.2,0.6-5.5,1.8S8.2,6.2,8.2,7.8l0,0V8c0,0.2,0.1,0.3,0.3,0.3l0,0h4.1c0.1,0,0.2-0.1,0.2-0.2c0-0.7,0.3-1.3,0.8-1.7c0.5-0.4,1.2-0.6,2-0.6c0.9,0,1.6,0.3,2,1c0.5,0.7,0.7,1.4,0.7,2.3c0,1.1-0.2,2-0.7,2.5s-1.3,0.8-2.5,0.8l0,0h-2.4c-0.2,0-0.2,0.1-0.2,0.3l0,0l0,6.6c0,0.3,0.1,0.4,0.4,0.4l0,0H16.4z M16.5,27c0.3,0,0.5-0.2,0.5-0.5l0,0v-4c0-0.4-0.2-0.5-0.5-0.5l0,0h-4c-0.3,0-0.5,0.2-0.5,0.5l0,0v4c0,0.3,0.2,0.5,0.5,0.5l0,0H16.5z" />
	</svg>
);

export const SliceSizeSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>sliceSize</title>
		<path fill={primary} d="M11.4,24.9c0.4-0.9,1.3-1.7,2.5-2.4l1.9-1.1c0.8-0.5,1.4-0.9,1.8-1.3c0.5-0.5,0.8-1.2,0.8-1.9c0-0.8-0.2-1.5-0.7-1.9c-0.5-0.5-1.1-0.7-2-0.7c-1.2,0-2,0.5-2.5,1.4c-0.2,0.5-0.4,1.2-0.4,2H11c0-1.2,0.2-2.2,0.7-3c0.8-1.4,2.1-2,4-2c1.6,0,2.8,0.4,3.5,1.3c0.7,0.9,1.1,1.8,1.1,2.9c0,1.1-0.4,2.1-1.2,2.9c-0.5,0.5-1.3,1-2.5,1.7l-1.3,0.7c-0.6,0.4-1.1,0.7-1.5,1c-0.7,0.6-1.1,1.2-1.2,1.9h7.6V28h-9.6C10.7,26.8,10.9,25.7,11.4,24.9z M16,8v5h-2V8H16z M22,8v3h-2V8H22zM4,8v3H2V8H4z M28,8v3h-2V8H28z M10,8v3H8V8H10z M19,0l-4,5l-4-5H19z" />
	</svg>
);

export const Tickv3Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>tickv3</title>
		<path fill={primary} d="M15,0C6.6,0,0,6.6,0,15c0,8.4,6.6,15,15,15s15-6.6,15-15C30,6.6,23.4,0,15,0 M15,2c7.3,0,13,5.7,13,13c0,7.3-5.7,13-13,13S2,22.3,2,15C2,7.7,7.5,2,14.8,2 M23.2,9.3L12.5,19.5l-4.6-4.4c-0.4-0.4-1.1-0.4-1.5,0c-0.4,0.4-0.4,1,0,1.4l5.3,5.1c0,0,0,0.1,0.1,0.1c0.2,0.2,0.5,0.3,0.8,0.3s0.5-0.1,0.8-0.3l11.5-11c0.4-0.4,0.4-1,0-1.4C24.3,8.9,23.6,8.9,23.2,9.3z" />
	</svg>
);

export const TimealarmSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>timealarm</title>
		<path fill={tertiary} d="M15,28c7.2,0,13-5.8,13-13S22.2,2,15,2S2,7.8,2,15S7.8,28,15,28" />
		<path fill={secondary} d="M16.8,26c0.6-0.5,1.2-1.3,1.2-3c0-1.3,0.7-2.7,2.2-3l0.6-0.1l0.2-0.6l0.2,0.6l0.6,0.1c1.5,0.3,2.2,1.8,2.2,3c0,1.7,0.7,2.5,1.3,3H16.8z" />
		<path fill={primary} d="M12.2,29C5.7,27.7,1,21.8,1,15.1C1,7.3,7.3,1,15,1s14,6.3,14,14.1c0,1.8-0.3,3.5-1,5.1c-0.2,0.5-0.7,0.7-1.2,0.5c-0.5-0.2-0.7-0.7-0.5-1.2c0.6-1.4,0.7-3,0.7-4.6c0-6.8-5.3-12-12-12C8.3,3,3,8.2,3,15c0,5.8,3.9,10.9,9.5,12c0.5,0.1,0.9,0.7,0.8,1.2c-0.1,0.4-0.5,0.8-0.9,0.8C12.3,29,12.3,29,12.2,29z M17,18.2c-0.2,0-0.4-0.1-0.6-0.2L14,16.4V8c0-0.6,0.4-1,1-1c0.6,0,1,0.4,1,1v7.3l1.5,1c0.5,0.3,0.6,0.9,0.3,1.4C17.6,18,17.3,18.2,17,18.2M23,28h-4c0.3,1.2,1.1,2,2,2S22.7,29.2,23,28 M21,18c-0.8,0-0.9,0.6-1,1c-1.9,0.4-3,2.1-3,4c0,2.2-1.4,2.3-1.9,3c-0.3,0.4,0,1,0.5,1h5.2h5.4c0.5,0,0.9-0.6,0.6-1c-0.5-0.7-1.9-0.8-1.9-3c0-1.9-1.1-3.6-3-4C21.9,18.6,21.8,18,21,18 M21,20.8l0.5,0.1c1,0.2,1.4,1.3,1.4,2.1c0,0.8,0.1,1.4,0.3,2h-2.4h-2.2c0.2-0.5,0.3-1.2,0.3-2c0-0.9,0.4-1.8,1.4-2.1L21,20.8" />
	</svg>
);

TimealarmSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ToolboxSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>toolbox</title>
		<path fill={primary} d="M25.5,8h-2.7c-0.3-1.4-1.1-2.2-1.7-3c-1.7-2-4.3-2-6.5-2h-0.3c-2.2,0-4.8,0-6.5,2C7.1,5.9,6.5,7,6.2,8H5.5H0v17h30V8H25.5z M9.3,6.3c1.1-1.3,3-1.3,5-1.3h0.3c2,0,3.8,0,5,1.3c0.5,0.6,0.9,1.1,1.1,1.7H20H9H8.3C8.6,7.4,8.9,6.8,9.3,6.3z M28,23H2v-7h11v0.3c0,0.3,0.1,0.5,0.4,0.6l0.2,0.5c0.1,0.3,0.3,0.6,0.7,0.6h1.4c0.2,0,0.8-0.1,0.8-0.5l0.2-0.6c0,0,0,0,0,0c0.2-0.1,0.3-0.4,0.3-0.6V16h11V23z M14.4,17l-0.1-0.4c0,0,0-0.1,0-0.1h1.4c0,0,0,0,0,0L15.6,17H14.4z M16,15h-2v-0.5c0,0,0,0,0,0l2,0V15z M28,15H17v-0.6c0-0.5-0.5-0.9-1-0.9h-2c-0.6,0-1,0.4-1,0.9V15H2v-5h3.5H7h2h11h2h3.5H28V15z" />
	</svg>
);

export const UnlockScreenSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>unlockScreen</title>
		<path fill={primary} d="M13.9,27.3l0-4.1c-0.8-0.5-1.3-1.3-1.3-2.3c0-1.5,1.3-2.8,2.9-2.8c1.6,0,2.9,1.3,2.9,2.8c0,1-0.5,1.8-1.3,2.3v4.1H13.9z M25,27.3v-6.4l0-5.3H9.2h0V9.2c0-3.4,2.8-6.1,6.3-6.1c3.5,0,6.3,2.7,6.3,6.1l3.2,0C25,4.1,20.7,0,15.5,0C10.2,0,6,4.1,6,9.2v6.4c0,0,0,5.3,0,5.3l0,6.4C6,28.8,7.2,30,8.8,30h13.5C23.8,30,25,28.8,25,27.3L25,27.3z" />
	</svg>
);

export const WeightChangeSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>unlockScreen</title>
		<path fill={primary} d="M16.6,18.9c1,0,1.8,0.3,2.3,0.8c0.5,0.5,0.8,1.2,0.8,1.9l0,0.2v2.3l0,0.2c0,0.8-0.3,1.4-0.8,1.9c-0.6,0.5-1.3,0.8-2.3,0.8s-1.8-0.3-2.3-0.9c-0.5-0.5-0.8-1.2-0.8-2l0-0.2V22l0-0.2c0-0.8,0.3-1.5,0.8-2C14.8,19.2,15.6,18.9,16.6,18.9z M25.6,0.9L27,2.3l-0.1,0.1L2.3,27l-1.4-1.4L1,25.5L25.6,0.9z M27.1,19.1L27.1,19.1C27.2,19.1,27.2,19.1,27.1,19.1c0.1,0.1,0.1,0.1,0.1,0.1l0,0l-4,6.4H27l0,0c0,0,0,0,0.1,0.1l0,0v1.2l0,0c0,0,0,0-0.1,0.1l0,0h-6.2l0,0c0,0,0,0-0.1-0.1c0,0,0-0.1,0-0.1l4.1-6.4l-3.7,0l0,0c0,0,0,0-0.1-0.1l0,0v-1.1l0,0c0,0,0,0,0.1-0.1l0,0H27.1z M16.6,20.3c-0.5,0-0.8,0.1-1.1,0.4c-0.3,0.2-0.4,0.6-0.4,1l0,0.2v2.4l0,0.2c0,0.4,0.2,0.7,0.4,0.9c0.3,0.3,0.7,0.4,1.1,0.4s0.8-0.1,1.1-0.4c0.3-0.2,0.4-0.5,0.4-0.9l0-0.2v-2.5l0-0.2c0-0.4-0.2-0.7-0.4-1C17.4,20.4,17,20.3,16.6,20.3z M6.3,2.9c0.3,0,0.7,0.1,1,0.2c0.3,0.1,0.5,0.3,0.7,0.5L8,3.7V3.3l0-0.1c0-0.1,0.1-0.1,0.1-0.1l0.1,0h1.2l0.1,0c0.1,0,0.1,0.1,0.1,0.1l0,0.1v7.8l0,0.3c0,0.9-0.3,1.6-0.9,2.2c-0.6,0.6-1.4,0.9-2.3,0.9c-1.3,0-2.3-0.4-3-1.2c0-0.1-0.1-0.1,0-0.2l0,0l0.8-0.9l0,0c0,0,0.1,0,0.2,0c0.5,0.5,1.2,0.8,1.9,0.8c0.5,0,0.9-0.2,1.2-0.5c0.3-0.3,0.4-0.6,0.4-1.1l0-0.2v-0.9l-0.1,0.1c-0.4,0.5-1,0.8-1.7,0.8c-0.9,0-1.7-0.3-2.2-0.9c-0.5-0.5-0.7-1.2-0.8-2l0-0.2v-2l0-0.3c0-0.8,0.3-1.5,0.8-2C4.5,3.2,5.3,2.9,6.3,2.9z M6.4,4.3c-1,0-1.5,0.5-1.6,1.6l0,0.2v2.1l0,0.2c0,1,0.5,1.5,1.4,1.5c0.6,0,1-0.2,1.3-0.6c0.2-0.4,0.4-0.8,0.4-1.3l0-0.2l0-1.5l0-0.2C7.9,4.8,7.4,4.3,6.4,4.3z" />
	</svg>
);

export const FaqWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>faqWeb</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M30 20.3027C30 15.9299 27.0536 12.1095 22.8987 10.9582C22.6376 4.87152 17.6049 0 11.4551 0C5.13863 0 0 5.13863 0 11.4551C0 13.5136 0.547943 15.518 1.58844 17.2776L0.0421143 22.8678L5.63255 21.3217C7.25006 22.2782 9.07471 22.817 10.9579 22.8983C12.109 27.0534 15.9297 30 20.3027 30C22.0482 30 23.7456 29.5351 25.237 28.6519L29.9577 29.9577L28.6519 25.237C29.5351 23.7456 30 22.0482 30 20.3027ZM5.90996 19.4211L2.56325 20.3469L3.48907 17.0002L3.27805 16.6702C2.28355 15.1142 1.75781 13.3109 1.75781 11.4551C1.75781 6.10794 6.10794 1.75781 11.4551 1.75781C16.8022 1.75781 21.1523 6.10794 21.1523 11.4551C21.1523 16.8022 16.8022 21.1523 11.4551 21.1523C9.5993 21.1523 7.79617 20.6266 6.24001 19.6321L5.90996 19.4211ZM27.4368 27.4368L24.9534 26.7496L24.6217 26.9655C23.3361 27.8007 21.8424 28.2422 20.3027 28.2422C16.8658 28.2422 13.8457 26.02 12.7757 22.8335C18.0386 22.2267 22.2267 18.0386 22.8337 12.7755C26.02 13.8457 28.2422 16.8658 28.2422 20.3027C28.2422 21.8424 27.8007 23.3361 26.9655 24.6217L26.7496 24.9534L27.4368 27.4368ZM12.334 15.8789V17.6367H10.5762V15.8789H12.334ZM12.6407 10.0866C13.0096 9.74899 13.2129 9.28825 13.2129 8.78906C13.2129 7.81975 12.4244 7.03125 11.4551 7.03125C10.4858 7.03125 9.69727 7.81975 9.69727 8.78906H7.93945C7.93945 6.85043 9.51645 5.27344 11.4551 5.27344C13.3937 5.27344 14.9707 6.85043 14.9707 8.78906C14.9707 9.77303 14.5539 10.7185 13.8274 11.3834L12.334 12.7503V14.1211H10.5762V11.9762L12.6407 10.0866Z" />
	</svg>
);

export const MyMasterClassesWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>myMasterClasses</title>
		<path fill={primary} d="M26.7,2.9h-1.4v1.9h1.4c0.2,0,0.5,0.4,0.5,0.9v2.8H2.9V5.7c0-0.6,0.3-0.9,0.5-0.9h1.4V2.9H3.3C2,2.9,1,4.1,1,5.7v20.5C1,27.8,2.2,29,3.8,29h22.4c1.6,0,2.8-1.2,2.8-2.8V5.7C29,4.1,28,2.9,26.7,2.9z M27.1,26.2c0,0.6-0.4,0.9-0.9,0.9H3.8c-0.6,0-0.9-0.4-0.9-0.9V10.3h24.3V26.2z M7.5,6.6C7,6.6,6.6,6.2,6.6,5.7V1.9C6.6,1.4,7,1,7.5,1s0.9,0.4,0.9,0.9v3.7C8.5,6.2,8.1,6.6,7.5,6.6z M22.5,6.6c-0.6,0-0.9-0.4-0.9-0.9V1.9c0-0.6,0.4-0.9,0.9-0.9c0.6,0,0.9,0.4,0.9,0.9v3.7C23.4,6.2,23,6.6,22.5,6.6z M10.3,2.9h9.3v1.9h-9.3V2.9z M23.4,12.2h1.9v1.9h-1.9V12.2z M19.7,12.2h1.9v1.9h-1.9V12.2z M15.9,12.2h1.9v1.9h-1.9V12.2z M12.2,12.2h1.9v1.9h-1.9V12.2z M8.5,12.2h1.9v1.9H8.5V12.2z M23.4,15.9h1.9v1.9h-1.9V15.9z M19.7,15.9h1.9v1.9h-1.9V15.9z M15.9,15.9h1.9v1.9h-1.9V15.9z M12.2,15.9h1.9v1.9h-1.9V15.9z M8.5,15.9h1.9v1.9H8.5V15.9z M4.7,15.9h1.9v1.9H4.7V15.9z M23.4,19.7h1.9v1.9h-1.9V19.7z M19.7,19.7h1.9v1.9h-1.9V19.7z M15.9,19.7h1.9v1.9h-1.9V19.7z M12.2,19.7h1.9v1.9h-1.9V19.7z M8.5,19.7h1.9v1.9H8.5V19.7z M4.7,19.7h1.9v1.9H4.7V19.7z M19.7,23.4h1.9v1.9h-1.9V23.4z M15.9,23.4h1.9v1.9h-1.9V23.4z M12.2,23.4h1.9v1.9h-1.9V23.4z M8.5,23.4h1.9v1.9H8.5V23.4z M4.7,23.4h1.9v1.9H4.7V23.4z" />
	</svg>
);

export const MySubscriptionsWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>mySubscriptions</title>
		<path fill={primary} d="M28.2,8H1.8C1.4,8,1,8.4,1,8.8v19.3C1,28.6,1.4,29,1.8,29h26.4c0.5,0,0.8-0.4,0.8-0.8V8.8C29,8.4,28.6,8,28.2,8z M27.4,27.4H2.6V9.7h24.7V27.4z M15.5,12.2c-3.3,0-6,2.7-6,6.1h-2l2.6,2.6l0,0.1l2.7-2.7h-2c0-2.6,2.1-4.7,4.7-4.7s4.7,2.1,4.7,4.7S18.1,23,15.5,23c-1.3,0-2.5-0.5-3.3-1.4l-0.9,1c1.1,1.1,2.6,1.8,4.2,1.8c3.3,0,6-2.7,6-6.1S18.8,12.2,15.5,12.2z M25.5,4.5H4.5C4,4.5,3.6,4.9,3.6,5.3S4,6.2,4.5,6.2h21.1c0.5,0,0.8-0.4,0.8-0.8S26,4.5,25.5,4.5z M22.9,1H7.1C6.6,1,6.3,1.4,6.3,1.8c0,0.5,0.4,0.8,0.8,0.8h15.8c0.5,0,0.8-0.4,0.8-0.8C23.7,1.4,23.4,1,22.9,1z" />
	</svg>
);

//// Desinged for 40px
export const Cup1FilterSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>cup1Filter</title>
		<path fill={primary} 
			d="M37,21c0.8,0,1.3,1,0.8,1.6l0,0l-9,11.1l-0.1,0.1C27.1,35.2,25.1,36,23,36l0,0h-6
c-2.1,0-4.1-0.8-5.7-2.2l0,0l-0.1-0.1l-9-11C1.7,22,2.2,21,3,21l0,0H37z M34.9,23H5.1l7.6,9.4l0.2,0.2c1.1,0.9,2.4,1.4,3.8,1.5l0,0
l0.3,0h6c1.6,0,3.1-0.6,4.3-1.7l0,0l-0.1,0L34.9,23z M30.7,25l-4.9,6C25,31.6,24,32,23,32l0,0h-6c-1,0-2-0.4-2.8-1l0,0l-4.9-6H30.7z
M14.3,13c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C12.2,13.6,13.2,12.9,14.3,13z M20.3,13
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C18.2,13.6,19.2,12.9,20.3,13z M26.3,13
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C24.2,13.6,25.2,12.9,26.3,13z M17.3,9
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C15.2,9.6,16.2,8.9,17.3,9z M23.3,9
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C21.2,9.6,22.2,8.9,23.3,9z M20.3,5
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C18.2,5.6,19.2,4.9,20.3,5z"
		/>
	</svg>
);

export const Cup2FilterSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>cup2Filter</title>
		<path fill={primary} 
			d="M37,21c0.6,0,1,0.5,1,1.1l0,0l-0.3,3.4c-0.1,1.6-0.3,3.4-0.6,5.4l0,0l0,0.4c-0.1,0.6-0.2,1.1-0.2,1.6
l0,0l-0.2,1.1c-0.2,1.3-0.7,2.3-1.5,3.2c-0.3,0.3-0.7,0.6-1.1,0.9c-1.1,0.7-2.3,1-3.6,1l0,0H9.5c-1.1,0-2.2-0.3-3.1-0.8
c-0.6-0.3-1.1-0.7-1.6-1.2c-0.8-0.8-1.3-1.9-1.5-3.2l0,0l0-0.1c-0.1-0.3-0.1-0.8-0.2-1.2l0,0l-0.2-1.6c-0.3-2-0.5-3.9-0.6-5.4l0,0
L2,22.1C2,21.5,2.4,21,3,21l0,0H37z M35.9,23H4.1l0.2,2.8c0.1,1,0.2,2,0.3,3.1l0,0l0.1,1.1L4.9,31C5,31.5,5,31.9,5.1,32.3l0,0
l0.2,1.2l0.1,0.3c0.2,0.8,0.5,1.4,1,1.9c0.3,0.3,0.7,0.6,1.1,0.8C8,36.8,8.7,37,9.5,37l0,0h20.9c0.9,0,1.8-0.3,2.5-0.7
c0.3-0.2,0.5-0.4,0.7-0.6c0.5-0.5,0.8-1.2,1-2.1l0,0l0-0.1c0.1-0.7,0.2-1.6,0.4-2.9l0,0l0.1-1.2c0.2-1.5,0.3-2.9,0.4-4.2l0,0
L35.9,23z M33.8,25l0,0.2c-0.1,0.7-0.1,1.4-0.2,2.1l-0.1,0.5c-0.3,2.8-0.7,5.4-0.7,5.5c-0.1,0.5-0.2,0.9-0.5,1.1
c-0.1,0.1-0.2,0.2-0.4,0.3C31.5,34.9,31,35,30.5,35l0,0H9.5c-0.5,0-0.9-0.1-1.2-0.3c-0.2-0.1-0.4-0.3-0.6-0.4
c-0.2-0.2-0.4-0.6-0.5-1.1c0,0,0,0,0,0l0-0.1c-0.1-0.5-0.4-3-0.7-5.6L6.4,27c-0.1-0.6-0.1-1.2-0.2-1.8l0,0l0-0.2H33.8z M11.3,13
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C9.2,13.6,10.2,12.9,11.3,13z M17.3,13
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C15.2,13.6,16.2,12.9,17.3,13z M23.3,13
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C21.2,13.6,22.2,12.9,23.3,13z M29.3,13
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C27.2,13.6,28.2,12.9,29.3,13z M14.3,9
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C12.2,9.6,13.2,8.9,14.3,9z M20.3,9
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C18.2,9.6,19.2,8.9,20.3,9z M26.3,9
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C24.2,9.6,25.2,8.9,26.3,9z M17.3,5
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C15.2,5.6,16.2,4.9,17.3,5z M23.3,5
c1.1,0.2,1.8,1.2,1.7,2.3c-0.2,1.1-1.2,1.8-2.3,1.7c-1.1-0.2-1.8-1.2-1.7-2.3C21.2,5.6,22.2,4.9,23.3,5z M20.3,1"
		/>
	</svg>
);

export const AddMilkSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>addMilk</title>
		<path fill={primary} 
			d="M28.6,1c1.1,0,2,0.8,2.1,1.9l0,0.2L31,7l2.9,0c2.8,0,5,2.2,5.1,4.9l0,0.2v6.6c0,2.8-2.1,5.1-4.8,5.2L34,24
l-1.6,0l0.6,7.4c0.2,1.9-0.3,4-1.6,5.4c-1.2,1.3-2.8,2.1-4.5,2.1l-0.3,0H10.4c-1.9,0-3.6-0.7-4.8-2.2c-1.2-1.3-1.7-3-1.6-4.8l0-0.3
L5.9,7.2C6,6.5,5.6,5.7,4.7,4.7C4.1,4.1,4,3.2,4.3,2.3c0.3-0.8,1-1.3,1.8-1.3l0.2,0H28.6z M28.6,3H6.3c0,0-0.1,0-0.1,0.1
c0,0.1,0,0.2,0,0.3C7.4,4.7,8,5.8,7.9,7.1l0,0.2L6,31.9c-0.1,1.5,0.2,2.7,1,3.6c0.8,0.9,1.9,1.4,3.1,1.5l0.3,0h16.2
c1.3,0,2.5-0.5,3.3-1.5c0.7-0.8,1.1-2.2,1.1-3.7l0-0.3L28.7,3.2C28.7,3.1,28.6,3,28.6,3L28.6,3z M18,20c0.6,0,1,0.4,1,1v4h4
c0.6,0,1,0.4,1,1s-0.4,1-1,1h-4v4c0,0.6-0.4,1-1,1s-1-0.4-1-1v-4h-4c-0.6,0-1-0.4-1-1s0.4-1,1-1h4v-4C17,20.4,17.4,20,18,20z
M31.2,9l1,13H34c1.6,0,2.8-1.2,3-2.9l0-0.2l0-0.2v-6.6c0-1.7-1.3-3.1-2.9-3.1l-0.2,0L31.2,9z"
		/>
	</svg>
);

export const ContainerSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>container</title>
		<path
		fill={primary} 
			d="M39.7,10c0-0.6-0.4-1-1-1H1.4c-0.6,0-1,0.4-1,1c0,0.1,0,0.2,0.1,0.3c-0.1,0.1-0.1,0.3-0.1,0.4c0,0.6,0.4,1,1,1l0.1,0
c0.8,0.1,1.1,0.8,1.1,2.9v12.7l0,0.2c0.1,2.6,2.5,4.7,5.4,4.7h24.6l0.2,0c2.8-0.1,5.2-2.2,5.2-4.9l0-13.7c0-0.7,0-1.1,0.1-1.3l0-0.1
c0.1-0.3,0.2-0.4,0.7-0.4c0.6,0,1-0.4,1-1c0-0.1,0-0.2-0.1-0.3C39.6,10.2,39.7,10.1,39.7,10z M35.8,27.3l0,0.2
c-0.1,1.5-1.6,2.7-3.4,2.7H7.9l-0.2,0c-1.8-0.1-3.2-1.4-3.2-2.9V14.6l0-0.3c0-1.4-0.2-2.5-0.6-3.3h32.4c-0.1,0.1-0.1,0.2-0.2,0.3
c-0.2,0.6-0.3,1.1-0.3,2.3L35.8,27.3z M11.3,14.2c0.3,0,0.5,0.2,0.5,0.5c0,0.2-0.2,0.4-0.4,0.5l-0.1,0h-4c-0.3,0-0.5-0.2-0.5-0.5
c0-0.2,0.2-0.4,0.4-0.5l0.1,0H11.3z M11.3,19.5c0.3,0,0.5,0.2,0.5,0.5c0,0.2-0.2,0.4-0.4,0.5l-0.1,0h-4c-0.3,0-0.5-0.2-0.5-0.5
c0-0.2,0.2-0.4,0.4-0.5l0.1,0H11.3z M11.3,24.2c0.3,0,0.5,0.2,0.5,0.5c0,0.2-0.2,0.4-0.4,0.5l-0.1,0h-4c-0.3,0-0.5-0.2-0.5-0.5
c0-0.2,0.2-0.4,0.4-0.5l0.1,0H11.3z"
		/>
	</svg>
);

export const DescaleSolutionSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>descaleSolution</title>
		<path
		fill={primary} 
			d="M33.4,5.1L35.3,7c0.8,0.8,0.8,2,0,2.8l-0.5,0.5c-0.3,0.3-0.7,0.5-1.2,0.6c-0.1,0.3-0.2,0.6-0.3,0.9l0,0
l0,0l0,0.1c1.4,2.7-0.3,7.7-2.7,10.3l0,0l-0.2,0.2L16,36.8c-1,1-2.7,1-3.7,0l0,0l-9-9c-1-1-1.1-2.7,0-3.7l0,0L17.7,9.6
c2.5-2.5,7.7-4.2,10.4-2.9l0,0l0.1,0l0.1,0c0.4-0.2,0.9-0.4,1.3-0.4c0.1-0.3,0.3-0.5,0.5-0.8l0.5-0.5C31.3,4.3,32.6,4.3,33.4,5.1z
M27.6,8.7c-1.5-1.3-6.4,0.2-8.5,2.3l0,0L4.6,25.5c-0.2,0.2-0.2,0.6,0,0.9l0,0l9,9c0.3,0.3,0.7,0.3,0.9,0l0,0l14.5-14.5
c2.1-2.1,3.5-6.9,2.3-8.3l0,0l-0.7-0.8l0.7-0.7c0.4-0.4,0.4-0.9,0.2-1.1l0,0l-1.4-1.4c-0.2-0.2-0.8-0.2-1.1,0.2l0,0l-0.7,0.7
L27.6,8.7z M16.5,26.4l0.9,0.9l-0.9,0.9l-0.9-0.9L16.5,26.4z M17.9,25l0.9,0.9l-0.9,0.9L17,26L17.9,25z M15.1,25L16,26l-0.9,0.9
L14.2,26L15.1,25z M20.1,23.4l0.8,1.4l-1.4,0.8l-0.8-1.4L20.1,23.4z M16.5,23.6l0.9,0.9l-0.9,0.9l-0.9-0.9L16.5,23.6z M13.7,23.6
l0.9,0.9l-0.9,0.9l-0.9-0.9L13.7,23.6z M12.3,22.2l0.9,0.9l-0.9,0.9l-0.9-0.9L12.3,22.2z M17.9,22.2l0.9,0.9l-0.9,0.9L17,23.1
L17.9,22.2z M15.1,22.2l0.9,0.9l-0.9,0.9l-0.9-0.9L15.1,22.2z M22.7,22.2l0.4,1.4L21.7,24l-0.4-1.4L22.7,22.2z M20,21.1l1.4,0.8
l-0.8,1.4l-1.4-0.8L20,21.1z M13.7,20.8l0.9,0.9l-0.9,0.9l-0.9-0.9L13.7,20.8z M16.5,20.8l0.9,0.9l-0.9,0.9l-0.9-0.9L16.5,20.8z
M15.1,19.4l0.9,0.9l-0.9,0.9l-0.9-0.9L15.1,19.4z M17.9,19.4l0.9,0.9l-0.9,0.9L17,20.3L17.9,19.4z M16.5,17.9l0.9,0.9l-0.9,0.9
l-0.9-0.9L16.5,17.9z M32,6.5L31.5,7l1.9,1.9l0.5-0.5L32,6.5z"
		/>
	</svg>
);

//Dupe?
export const Espresso1v2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>espresso1</title>
		<path
		fill={primary} 
			d="M32.5,4C33.8,4,34.9,5,35,6.4l0,0.2V9l0.3,0c2.6,0.4,4.6,2.6,4.7,5.2l0,0.2
c0,2.7-2,5-4.7,5.5l0,0L35,20c-0.5,9.4-7.8,17-17.3,17C8.1,37,0.2,29.1,0,19.4L0,19V6.6C0,5.2,1,4.1,2.4,4l0.2,0L32.5,4z M32.5,6
L2.5,6C2.3,6,2,6.2,2,6.5l0,0.1V19c0,8.8,7.1,16,15.7,16c8.4,0,15.1-6.9,15.3-15.7l0-0.3V6.5C33,6.2,32.8,6,32.5,6L32.5,6z M31.2,22
c-1.3,6.3-7,11-13.8,11C10.7,33,5,28.3,3.8,22H31.2z M35,11v7c1.7-0.3,3-1.7,3-3.5S36.7,11.3,35,11L35,11z"
		/>
	</svg>
);

//Dupe?
export const Espresso2v2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>espresso2</title>
		<path
		fill={primary} 
			d="M5.7,13l19.7,0l0.1,0c0.8,0.1,1.4,0.7,1.5,1.5l0,0.1v7.9l0,0.3C26.8,29,21.7,34,15.4,34
C9.4,34,4.8,29.6,4.1,24l-0.4,0C1.7,23.7,0,22.1,0,20c0-2.1,1.7-3.7,3.7-4L4,16v-1.3l0-0.1c0.1-0.8,0.7-1.4,1.5-1.5L5.7,13z M6,15
v7.6l0,0.3c0.1,5,4.1,9,9.1,9.2l0.3,0l0.3,0c5.1-0.1,9.2-4.2,9.4-9.1l0-0.3V15L6,15z M24,24c-0.8,4-4.3,7-8.5,7
c-4.1,0-7.6-2.9-8.4-6.7L7,24H24z M34.3,7c0.9,0,1.6,0.7,1.7,1.5l0,0.1V10l0.3,0c2,0.3,3.6,1.8,3.7,3.8l0,0.2c0,2-1.5,3.6-3.5,3.9
l-0.2,0l-0.4,0c-0.6,4.9-4.2,8.8-9.1,9.8c0.3-0.7,0.6-1.5,0.8-2.3c3.7-1.2,6.2-4.6,6.4-8.7l0-0.3V9L15,9v3l-2,0V8.7
c0-0.9,0.7-1.6,1.5-1.6l0.1,0L34.3,7z M33,18c-0.6,2.9-2.5,5.2-5.1,6.3c0.1-0.6,0.1-1.2,0.1-1.8l0-4.5H33z M4,18c-1.1,0.2-2,1-2,2
s0.9,1.8,2,2V18z M36,12v4c1.1-0.2,2-1,2-2S37.1,12.2,36,12z"
		/>
	</svg>
);

export const LongblackSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>longblack</title>
		<path
		fill={primary} 
			d="M33.7,2c0.6,0,1.2,0.2,1.6,0.7l0.1,0.1c0.4,0.5,0.6,1,0.6,1.6l0,0.2l-0.8,6c0,0,0,0,0.1,0
c0.2,0,0.4,0,0.5,0c0.6,0.1,1.2,0.3,1.7,0.6c0.6,0.3,1.1,0.8,1.5,1.3c0.8,1,1.1,2.3,1,3.6c-0.2,1.3-0.8,2.4-1.8,3.2
c-0.6,0.4-1.2,0.7-1.8,0.9c-0.4,0.1-0.7,0.1-1.1,0.1c-0.2,0-0.4,0-0.6,0c-0.2,0-0.4-0.1-0.6-0.1l-0.1,0l0,0.1l-0.6,4.3
C32.3,32.2,25.7,38,18,38c-7.6,0-14.1-5.6-15.3-13.2l0-0.3L0,4.6C-0.1,4,0.1,3.3,0.6,2.8C1,2.3,1.5,2.1,2.1,2l0.2,0H33.7z M34.7,3.5
l-0.7,0.6c0,0-0.1-0.1-0.1-0.1l-0.1,0H2.3C2.2,4,2.1,4,2.1,4.1C2,4.2,2,4.2,2,4.3l0,0.1l2.6,19.9C5.5,30.9,11.3,36,18,36
c6.6,0,12.3-4.9,13.3-11.5l0-0.3l0.8-6l0.7-5.5l0.2-1.7L34,4.3c0-0.1,0-0.1,0-0.2l0,0L34.7,3.5z M31.2,10l-1.9,14.2
c-0.7,5.6-5.6,9.8-11.2,9.8c-5.6,0-10.4-4.2-11.2-9.8L5,10H31.2z M35,12.4L35,12.4L34.9,13l-0.7,5.3c0,0,0,0,0.1,0
c0.2,0.1,0.4,0.1,0.6,0.1c0.6,0.1,1.2,0,1.8-0.3c0.2-0.1,0.3-0.2,0.5-0.3c0.6-0.5,1-1.2,1.1-2c0.1-1-0.3-2-0.9-2.6
c-0.4-0.4-1-0.7-1.6-0.8C35.4,12.4,35.2,12.4,35,12.4z"
		/>
	</svg>
);

export const GlassSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>glass</title>
		<path
		fill={primary} 
			d="M20,1c6.8,0,12,1.1,12,3.3c0,0,0,0.1,0,0.1l0,0l0,0.1l-0.5,6l-0.4,0.3c-0.1,0-0.2,0.2-0.4,0.4
c-0.3,0.4-0.5,0.8-0.5,1.4c-0.1,0.7,0,1.1,0.1,1.3l0,0l0.5,0.3l-1.2,9.9l-0.2,0.2c-0.1,0.1-0.2,0.3-0.4,0.6
c-0.2,0.4-0.4,0.8-0.4,1.2c-0.1,0.5,0,0.9,0.1,1.2c0,0.1,0.1,0.2,0.1,0.2l0,0l0.3,0.3l-0.8,8l0,0.1c-0.2,2-3.5,3.1-7.8,3.2l0,0
l-0.3,0c-4.4,0-8.1-1.2-8.1-3.3l0,0.1L10,21.4l0.3-0.3l0.1-0.2c0.1-0.4,0.2-0.9,0.2-1.6c0-0.6-0.2-1-0.4-1.3
c-0.1-0.1-0.1-0.1-0.1-0.2l0,0l-0.1,0l-0.5-0.2L8,4.6l0-0.1l0-0.1l0-0.1C8,2.2,12.8,1.1,19.3,1l0,0L20,1z M20,3
c-2.9,0-5.6,0.2-7.6,0.7c-0.9,0.2-1.7,0.4-2.1,0.7c-0.1,0.1-0.2,0.1-0.3,0.2l0,0l0,0l1.4,11.8l0.1,0.1c0.1,0.1,0.2,0.2,0.3,0.3l0,0
l0.1,0.2c0.5,0.6,0.7,1.4,0.8,2.4c0,1-0.1,1.8-0.3,2.4c-0.1,0.1-0.1,0.2-0.2,0.3l0,0l2,13.6l0,0.1c0,0.1,0.4,0.4,1.4,0.7
c1.2,0.4,2.9,0.6,4.7,0.6s3.5-0.2,4.7-0.6c0.9-0.3,1.4-0.7,1.4-0.7l0,0l0-0.1l0.7-7.3l0-0.1L26.8,28c-0.3-0.6-0.4-1.4-0.3-2.2
c0.1-0.7,0.3-1.4,0.7-2c0.1-0.3,0.3-0.5,0.4-0.6l0,0l0,0l1-8.1l0,0l-0.1-0.1c-0.3-0.6-0.5-1.4-0.4-2.4c0.1-0.9,0.4-1.7,0.9-2.4
c0.2-0.2,0.3-0.4,0.5-0.6l0,0l0,0L30,4.5l0,0c0,0-0.1-0.1-0.2-0.1l0,0l-0.1-0.1c-0.5-0.2-1.2-0.5-2.1-0.7C25.6,3.2,22.9,3,20,3z
M29.4,5.9c0.1,0.3-0.1,0.5-0.3,0.6c-2,0.6-5.5,0.9-9.4,0.9c-3.5,0-6.7-0.3-8.8-0.8c-0.3-0.1-0.4-0.3-0.4-0.6s0.3-0.4,0.6-0.4
c1.9,0.4,4.8,0.7,8.1,0.7l0.5,0c3.8,0,7.2-0.3,9.1-0.9C29.1,5.5,29.4,5.6,29.4,5.9z"
		/>
	</svg>
);

export const HopperSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>hopper</title>
		<path
		fill={primary} 
			d="M20,9c9.7,0,17.2,4.3,17.9,9.3c0,0.1,0.1,0.2,0.1,0.2c0,0.1,0,0.2,0,0.3c0,0.1,0,0.2,0,0.2
c0,0.3,0,0.6-0.1,0.9c-0.2,2.3-0.8,5.4-2,7.1c-1.4,2.1-3.2,3.8-5.2,4.9c-0.1,0-0.2,0.1-0.2,0.1c-0.2,0-0.3-0.1-0.4-0.3
c-0.1-0.2-0.1-0.5,0.2-0.7c1.9-1.1,3.5-2.6,4.9-4.6c0.6-0.9,1.1-2.2,1.4-3.6c-2.2,2.6-6.8,4.4-12.5,5l0,0v-0.9c0,0,0,0,0.1,0
c0.5-0.1,1-0.2,1.6-0.4c0.4-0.1,0.9-0.2,1.3-0.3l0-0.3c0,0,0,0,0,0c0.7-0.4,1.4-1,2-2l0,0l0,1.8c2.5-0.8,4.4-1.9,5.7-3.2
c-0.4-4.2-6.4-8.5-15-8.5s-14.6,4.3-15,8.5c1.3,1.3,3.2,2.4,5.6,3.2c0.2,0.1,0.4,0.1,0.6,0.2l0-2.1c0-1.4,0.5-3.1,3-4.1l0,0v-0.4
c0-1.8,1.2-3.2,2.7-3.2l0,0h6.6c1.5,0,2.7,1.4,2.7,3.2l0,0v0.6c0.4,0.2,0.8,0.4,1.1,0.6c-0.1,0.1-0.2,0.1-0.3,0.2
c-0.1,0.1-0.1,0.1-0.2,0.2c-0.1,0.1-0.2,0.1-0.3,0.2c-0.1,0.1-0.2,0.1-0.3,0.2c-0.1,0.1-0.2,0.1-0.4,0.2c-0.1,0.1-0.3,0.1-0.4,0.2
c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.4,0-0.5,0.1c-0.2,0-0.4,0.1-0.6,0.1l0,0V21c0-0.1,0-0.1,0-0.2l0,0V19c-0.1-0.5-0.3-0.9-0.6-1
c0,0-0.1,0-0.1,0l0,0h-6.6c-0.3,0-0.7,0.5-0.7,1.2l0,0V21c0.2,0.2,1.1,0.7,2.7,0.9l0,0l-1.2,0.8c-1.3-0.3-2.2-0.8-2.4-1.3
C13.3,22,13,23,13,23.7l0,0l0,2.6c1.7,0.3,3.5,0.6,5.5,0.6l0,0l0.6,0.4l1,0.6l0,0l0,0L20,28c-0.1,0-0.1,0-0.2,0l-0.1,0
c-3.4,0-6.6-0.4-9.2-1.2c-3.3-1-5.9-2.5-7.3-4.3c0.2,1.5,0.5,3,1.2,4c1.2,1.7,3.4,3.6,5.3,4.6c0.2,0.1,0.3,0.4,0.2,0.7
C9.9,31.9,9.7,32,9.5,32c-0.1,0-0.2,0-0.2-0.1c-2.1-1.1-4.4-3-5.7-4.9c-1.2-1.8-1.5-5-1.6-7.6c0-0.1,0-0.2,0-0.4c0-0.1,0-0.1,0-0.2
c0-0.1,0-0.3,0-0.4c0-0.1,0-0.2,0.1-0.2C2.8,13.3,10.3,9,20,9z M29,20c0,6-5.5,5.9-6,6l0,0v2c0,0.5-0.6,0.3-1,0l0,0l-4.8-3
c-0.2-0.2-0.2-0.6,0-0.8l0,0L22,21c0.4-0.3,1-0.5,1,0l0,0v2C26.9,23,29,20,29,20z M20,10C9.8,10,3,14.7,3,19.1l0-0.1
c0,0.1,0,0.3,0,0.4c0.1,0.8,0.4,1.6,0.9,2.4c1-4.7,7.5-8.7,15.9-8.7c8.6,0,15.3,4.3,15.9,9.2c0.7-0.8,1.1-1.6,1.2-2.5
c0-0.3,0-0.6,0.1-0.9C36.8,14.5,30,10,20,10z M17.8,19.1c0,0,0.5,0.4,1.2,0.4h2c0.8,0,1.1-0.3,1.2-0.4c0.2-0.2,0.5-0.2,0.7,0
c0.2,0.2,0.2,0.5,0,0.7c-0.1,0.1-0.7,0.6-1.9,0.6h-2c-1,0-1.8-0.6-1.8-0.6c-0.2-0.2-0.3-0.5-0.1-0.7C17.3,19,17.6,18.9,17.8,19.1z"
		/>
	</svg>
);

export const TestStripSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>testStrip</title>
		<path
		fill={primary} 
			d="M37.8,29.4c0.4,0.4,0.4,1,0,1.4l-6.4,6.4c-0.4,0.4-1,0.4-1.4,0L3.8,11.1c-0.4-0.4-0.4-1,0-1.4l6.4-6.4
c0.4-0.4,1-0.4,1.4,0L37.8,29.4z M35.6,30.2L10.9,5.4l-4.9,4.9l24.7,24.7L35.6,30.2z"
		/>
		<path fill={primary} d="M21.7,19l-2.1,2.1l1.7,1.7c0.2,0.2,0.6,0.3,0.9,0l1.3-1.2c0.2-0.2,0.2-0.6,0-0.9L21.7,19z" />
		<polygon fill={primary} points="20.6,17.9 18.4,15.8 16.3,17.8 18.4,20 " />
		<polygon fill={primary} points="13,14.6 15.2,16.8 17.3,14.7 15.1,12.5 " />
		<path fill={primary} 
			d="M10.2,11.7l1.7,1.7l2.1-2.1l-1.7-1.7c-0.2-0.2-0.6-0.3-0.9,0l-1.3,1.2
C9.9,11.1,9.9,11.5,10.2,11.7"
		/>
	</svg>
);

export const Espresso1Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>espresso1</title>
		<path fill={tertiary} d="M30,3l-5.7,30.6c-0.4,2-2.2,3.4-4.1,3.4h-3.9H11c-2,0-3.7-1.4-4.1-3.4L1,3H30z" />
		<path
			fill={primary}
			d="M34.4,4h-3.5L31,3.2c0.1-0.3,0-0.6-0.2-0.8C30.6,2.2,30.3,2,30,2H1C0.7,2,0.4,2.1,0.2,2.4C0,2.6,0,2.9,0,3.2
l5.8,30.6C6.4,36.3,8.5,38,11,38h9.2c2.5,0,4.6-1.7,5.1-4.2L27.8,20c0.1,0,0.1,0,0.2,0c0.2,0,0.3,0,0.5-0.1l6.3-3.3
c3.1-1.4,5.2-2.7,5.2-6.4C40,6.6,37.6,4,34.4,4z M23.3,33.4C23,34.9,21.7,36,20.2,36H11c-1.5,0-2.8-1.1-3.1-2.6L2.2,4h26.6
L23.3,33.4z M33.9,14.8l-5.6,3L30.5,6h0.1h3.8c2.1,0,3.6,1.7,3.6,4.1C38,12.5,37.1,13.3,33.9,14.8z"
		/>
		<path
			fill={primary}
			d="M8.4,26h14.2l-1.3,7c-0.1,0.3-0.2,0.6-0.5,0.8c-0.2,0.2-0.4,0.2-0.7,0.3c-0.1,0-0.3,0-0.4,0H16h-4
c-0.9,0-1.4-0.1-1.8-0.3C10,33.5,9.8,33.2,9.8,33L8.4,26z"
		/>
	</svg>
);

Espresso1Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Espresso2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>espresso2</title>
		<path fill={tertiary} d="M30,3l-5.7,30.6c-0.4,2-2.2,3.4-4.1,3.4h-3.9H11c-2,0-3.7-1.4-4.1-3.4L1,3H30z" />
		<path fill={primary}
			d="M34.4,4h-3.5L31,3.2c0.1-0.3,0-0.6-0.2-0.8C30.6,2.2,30.3,2,30,2H1C0.7,2,0.4,2.1,0.2,2.4C0,2.6,0,2.9,0,3.2
l5.8,30.6C6.4,36.3,8.5,38,11,38h9.2c2.5,0,4.6-1.7,5.1-4.2L27.8,20c0.1,0,0.1,0,0.2,0c0.2,0,0.3,0,0.5-0.1l6.3-3.3
c3.1-1.4,5.2-2.7,5.2-6.4C40,6.6,37.6,4,34.4,4z M23.3,33.4C23,34.9,21.7,36,20.2,36H11c-1.5,0-2.8-1.1-3.1-2.6L2.2,4h26.6
L23.3,33.4z M33.9,14.8l-5.6,3L30.5,6h0.1h3.8c2.1,0,3.6,1.7,3.6,4.1C38,12.5,37.1,13.3,33.9,14.8z"
		/>
		<path
			fill={primary}
			d="M20.1,34H11c-0.3,0-0.5-0.1-0.7-0.3C10,33.5,9.8,33.3,9.8,33L7.9,23h15.3l-1.9,10
c-0.1,0.3-0.2,0.6-0.5,0.8C20.7,33.9,20.4,34,20.1,34z"
		/>
	</svg>
);

Espresso2Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Espresso3Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>espresso3</title>
		<path fill={tertiary} d="M30,3l-5.7,30.6c-0.4,2-2.2,3.4-4.1,3.4h-3.9H11c-2,0-3.7-1.4-4.1-3.4L1,3H30z" />
		<path fill={primary}
			d="M34.4,4h-3.5L31,3.2c0.1-0.3,0-0.6-0.2-0.8C30.6,2.2,30.3,2,30,2H1C0.7,2,0.4,2.1,0.2,2.4C0,2.6,0,2.9,0,3.2
l5.8,30.6C6.4,36.3,8.5,38,11,38h9.2c2.5,0,4.6-1.7,5.1-4.2L27.8,20c0.1,0,0.1,0,0.2,0c0.2,0,0.3,0,0.5-0.1l6.3-3.3
c3.1-1.4,5.2-2.7,5.2-6.4C40,6.6,37.6,4,34.4,4z M23.3,33.4C23,34.9,21.7,36,20.2,36H11c-1.5,0-2.8-1.1-3.1-2.6L2.2,4h26.6
L23.3,33.4z M33.9,14.8l-5.6,3L30.5,6h0.1h3.8c2.1,0,3.6,1.7,3.6,4.1C38,12.5,37.1,13.3,33.9,14.8z"
		/>
		<path
			fill={primary}
			d="M23.8,20l-2.4,13c-0.1,0.3-0.2,0.6-0.5,0.8c-0.2,0.2-0.5,0.3-0.7,0.3H11c-0.3,0-0.5-0.1-0.7-0.3
C10,33.5,9.8,33.3,9.8,33L7.3,20H23.8z"
		/>
	</svg>
);

Espresso3Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Espresso4Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>espresso4</title>
		<path fill={tertiary} d="M30,3l-5.7,30.6c-0.4,2-2.2,3.4-4.1,3.4h-3.9H11c-2,0-3.7-1.4-4.1-3.4L1,3H30z" />
		<path
		fill={primary}
			d="M34.4,4h-3.5L31,3.2c0.1-0.3,0-0.6-0.2-0.8C30.6,2.2,30.3,2,30,2H1C0.7,2,0.4,2.1,0.2,2.4C0,2.6,0,2.9,0,3.2
l5.8,30.6C6.4,36.3,8.5,38,11,38h9.2c2.5,0,4.6-1.7,5.1-4.2L27.8,20c0.1,0,0.1,0,0.2,0c0.2,0,0.3,0,0.5-0.1l6.3-3.3
c3.1-1.4,5.2-2.7,5.2-6.4C40,6.6,37.6,4,34.4,4z M23.3,33.4C23,34.9,21.7,36,20.2,36H11c-1.5,0-2.8-1.1-3.1-2.6L2.2,4h26.6
L23.3,33.4z M33.9,14.8l-5.6,3L30.5,6h0.1h3.8c2.1,0,3.6,1.7,3.6,4.1C38,12.5,37.1,13.3,33.9,14.8z"
		/>
		<path
			fill={primary}
			d="M20.1,34H11c-0.3,0-0.5-0.1-0.7-0.3C10,33.5,9.8,33.3,9.8,33L6.5,16h18l-3.2,17
c-0.1,0.3-0.2,0.6-0.5,0.8C20.7,33.9,20.4,34,20.1,34z"
		/>
	</svg>
);

Espresso4Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Espresso5Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>espresso5</title>
		<path fill={tertiary} d="M30,3l-5.7,30.6c-0.4,2-2.2,3.4-4.1,3.4h-3.9H11c-2,0-3.7-1.4-4.1-3.4L1,3H30z" />
		<path
		fill={primary}
			d="M34.4,4h-3.5L31,3.2c0.1-0.3,0-0.6-0.2-0.8C30.6,2.2,30.3,2,30,2H1C0.7,2,0.4,2.1,0.2,2.4C0,2.6,0,2.9,0,3.2
l5.8,30.6C6.4,36.3,8.5,38,11,38h9.2c2.5,0,4.6-1.7,5.1-4.2L27.8,20c0.1,0,0.1,0,0.2,0c0.2,0,0.3,0,0.5-0.1l6.3-3.3
c3.1-1.4,5.2-2.7,5.2-6.4C40,6.6,37.6,4,34.4,4z M23.3,33.4C23,34.9,21.7,36,20.2,36H11c-1.5,0-2.8-1.1-3.1-2.6L2.2,4h26.6
L23.3,33.4z M33.9,14.8l-5.6,3L30.5,6h0.1h3.8c2.1,0,3.6,1.7,3.6,4.1C38,12.5,37.1,13.3,33.9,14.8z"
		/>
		<path
			fill={primary}
			d="M20.1,34H11c-0.3,0-0.5-0.1-0.7-0.3C10,33.5,9.8,33.3,9.8,33l-4-21h19.5l-3.9,21
c-0.1,0.3-0.2,0.6-0.5,0.8C20.7,33.9,20.4,34,20.1,34z"
		/>
	</svg>
);

Espresso5Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Espresso6Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>espresso6</title>
		<path fill={tertiary} d="M30,3l-5.7,30.6c-0.4,2-2.2,3.4-4.1,3.4h-3.9H11c-2,0-3.7-1.4-4.1-3.4L1,3H30z" />
		<path
		fill={primary}
			d="M34.4,4h-3.5L31,3.2c0.1-0.3,0-0.6-0.2-0.8C30.6,2.2,30.3,2,30,2H1C0.7,2,0.4,2.1,0.2,2.4C0,2.6,0,2.9,0,3.2
l5.8,30.6C6.4,36.3,8.5,38,11,38h9.2c2.5,0,4.6-1.7,5.1-4.2L27.8,20c0.1,0,0.1,0,0.2,0c0.2,0,0.3,0,0.5-0.1l6.3-3.3
c3.1-1.4,5.2-2.7,5.2-6.4C40,6.6,37.6,4,34.4,4z M23.3,33.4C23,34.9,21.7,36,20.2,36H11c-1.5,0-2.8-1.1-3.1-2.6L2.2,4h26.6
L23.3,33.4z M33.9,14.8l-5.6,3L30.5,6h0.1h3.8c2.1,0,3.6,1.7,3.6,4.1S37.1,13.3,33.9,14.8z"
		/>
		<path
			fill={primary}
			d="M20.1,34h-4.6H11c-0.3,0-0.5-0.1-0.7-0.3C10,33.5,9.8,33.3,9.8,33L5.2,9h20.6l-4.5,24
c-0.1,0.3-0.2,0.6-0.5,0.8C20.7,33.9,20.4,34,20.1,34z"
		/>
	</svg>
);

Espresso6Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Espresso7Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>espresso7</title>
		<path fill={tertiary} d="M30,3l-5.7,30.6c-0.4,2-2.2,3.4-4.1,3.4h-3.9H11c-2,0-3.7-1.4-4.1-3.4L1,3H30z" />
		<path
		fill={primary}
			d="M34.4,4h-3.5L31,3.2c0.1-0.3,0-0.6-0.2-0.8C30.6,2.2,30.3,2,30,2H1C0.7,2,0.4,2.1,0.2,2.4C0,2.6,0,2.9,0,3.2
l5.8,30.6C6.4,36.3,8.5,38,11,38h9.2c2.5,0,4.6-1.7,5.1-4.2L27.8,20c0.1,0,0.1,0,0.2,0c0.2,0,0.3,0,0.5-0.1l6.3-3.3
c3.1-1.4,5.2-2.7,5.2-6.4C40,6.6,37.6,4,34.4,4z M23.3,33.4C23,34.9,21.7,36,20.2,36H11c-1.5,0-2.8-1.1-3.1-2.6L2.2,4h26.6
L23.3,33.4z M33.9,14.8l-5.6,3L30.5,6h0.1h3.8c2.1,0,3.6,1.7,3.6,4.1C38,12.5,37.1,13.3,33.9,14.8z"
		/>
		<path
			fill={primary}
			d="M20.6,33.9C20.3,34,20,34,19.6,34h-5.1H11c-0.3,0-0.5-0.1-0.7-0.3C10,33.5,9.8,33.3,9.8,33L4.6,6h21.8
l-5.1,27c-0.1,0.3-0.2,0.6-0.5,0.8C20.8,33.8,20.7,33.8,20.6,33.9z"
		/>
	</svg>
);

Espresso7Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Foam1Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>foam1</title>
		<path
			fill={tertiary}
			d="M6.8,1.4C6,1.4,5.3,2,5,2.7C4.6,3.5,4.8,4.4,5.3,5c0.9,1,1.2,1.7,1.2,2.3L4.7,31
c-0.2,1.8,0.3,3.6,1.5,4.9C7.4,37.3,9,38,10.8,38h15.4c1.8,0,3.4-0.7,4.6-2.1c1.2-1.4,1.7-3.4,1.5-5.2L30.1,3.4c-0.1-1.1-1-2-2-2
C28.1,1.4,6.8,1.4,6.8,1.4z"
		/>
		<path
			fill={primary}
			d="M33.9,7H31l-0.3-4c0-0.6-0.3-1.1-0.7-1.4C29.6,1.3,29.1,1,28.6,1H6.3C5.9,1,5.4,1.1,5.1,1.4
C4.7,1.6,4.5,2,4.3,2.4C4.1,2.7,4.1,3.2,4.2,3.6c0,0.4,0.2,0.8,0.5,1.1c0.9,1,1.3,1.8,1.2,2.4L4,31.7c-0.2,2,0.4,3.8,1.5,5.1
c1.2,1.4,2.9,2.2,4.8,2.2h16.2c1.9,0,3.6-0.8,4.8-2.1c1.2-1.3,1.7-3.3,1.6-5.4L32.4,24H34c2.8,0,5-2.3,5-5.2v-6.6
C39,9.3,36.8,7,33.9,7z M29.9,35.5c-0.9,1-2,1.5-3.3,1.5H10.4c-1.3,0-2.5-0.5-3.3-1.5c-0.8-0.9-1.2-2.1-1-3.6L7.9,7.3
C8,6.1,7.5,4.8,6.2,3.4c0,0,0-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0.1-0.1h22.4c0,0,0.1,0.1,0.1,0.2L29.1,8
c-0.3,2.2,0.6,10.5,1.1,14l0.8,9.7C31.1,33.1,30.7,34.6,29.9,35.5z M37,18.8c0,1.8-1.3,3.2-3,3.2h-1.8l-1-13h2.7
c1.7,0,3.1,1.4,3.1,3.1V18.8z M28.2,22.3l0.8,9.5c0.1,1-0.1,1.9-0.6,2.4S27.3,35,26.6,35H10.4c-0.7,0-1.4-0.3-1.8-0.8
C8.1,33.7,7.9,33,8,32.1l0.8-10.7c3.1-0.5,6.7-0.5,9.4,0.3C21.1,22.4,25.2,22.7,28.2,22.3z"
		/>
		<path
			fill={primary}
			d="M27.7,19.5c0.4-0.4,0.5-1,0.1-1.4s-1-0.5-1.4-0.1c-1.6,1.4-4.4,1.4-7.2,0c-3-1.5-6.9-1.3-9.7,0.4
c-0.5,0.3-0.6,0.9-0.3,1.4s0.9,0.6,1.4,0.3c2.2-1.3,5.4-1.5,7.8-0.3c1.6,0.8,3.3,1.2,4.9,1.2C24.9,21,26.5,20.5,27.7,19.5z"
		/>
	</svg>
);

Foam1Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Foam2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>foam2</title>
		<path
			fill={tertiary}
			d="M6.8,1.4C6,1.4,5.3,2,5,2.7C4.6,3.5,4.8,4.4,5.3,5c0.9,1,1.2,1.7,1.2,2.3L4.7,31
c-0.2,1.8,0.3,3.6,1.5,4.9C7.4,37.3,9,38,10.8,38h15.4c1.8,0,3.4-0.7,4.6-2.1c1.2-1.4,1.7-3.4,1.5-5.2L30.1,3.4c-0.1-1.1-1-2-2-2
C28.1,1.4,6.8,1.4,6.8,1.4z"
		/>
		<path
			fill={primary}
			d="M33.9,7H31l-0.3-4c0-0.6-0.3-1.1-0.7-1.4C29.6,1.3,29.1,1,28.6,1H6.3C5.9,1,5.4,1.1,5.1,1.4
C4.7,1.6,4.5,2,4.3,2.4C4.1,2.7,4.1,3.2,4.2,3.6c0,0.4,0.2,0.8,0.5,1.1c0.9,1,1.3,1.8,1.2,2.4L4,31.7c-0.2,2,0.4,3.8,1.5,5.1
c1.2,1.4,2.9,2.2,4.8,2.2h16.2c1.9,0,3.6-0.8,4.8-2.1c1.2-1.3,1.7-3.3,1.6-5.4L32.4,24H34c2.8,0,5-2.3,5-5.2v-6.6
C39,9.3,36.8,7,33.9,7z M29.9,35.5c-0.9,1-2,1.5-3.3,1.5H10.4c-1.3,0-2.5-0.5-3.3-1.5c-0.8-0.9-1.2-2.1-1-3.6L7.9,7.3
C8,6.1,7.5,4.8,6.2,3.4c0,0,0-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0.1-0.1h22.4c0,0,0.1,0.1,0.1,0.2L29.1,8
c-0.3,2.2,0.6,10.5,1.1,14l0.8,9.7C31.1,33.1,30.7,34.6,29.9,35.5z M37,18.8c0,1.8-1.3,3.2-3,3.2h-1.8l-1-13h2.7
c1.7,0,3.1,1.4,3.1,3.1V18.8z M28.2,22.3l0.8,9.5c0.1,1-0.1,1.9-0.6,2.4S27.3,35,26.6,35H10.4c-0.7,0-1.4-0.3-1.8-0.8
C8.1,33.7,7.9,33,8,32.1l0.8-10.7c3.1-0.5,6.7-0.5,9.4,0.3C21.1,22.4,25.2,22.7,28.2,22.3z"
		/>
		<path
			fill={primary}
			d="M27.7,17.8c0.4-0.4,0.5-1,0.1-1.4s-1-0.5-1.4-0.1c-1.6,1.4-4.4,1.4-7.2,0c-3-1.5-6.9-1.3-9.7,0.4
C9,16.9,8.9,17.5,9.1,18c0.3,0.5,0.9,0.6,1.4,0.3c2.2-1.3,5.4-1.5,7.8-0.3c1.6,0.8,3.3,1.2,4.9,1.2C24.9,19.3,26.5,18.8,27.7,17.8z"
		/>
	</svg>
);

Foam2Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Foam3Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>foam3</title>
		<path
			fill={tertiary}
			d="M6.8,1.4C6,1.4,5.3,2,5,2.7C4.6,3.5,4.8,4.4,5.3,5c0.9,1,1.2,1.7,1.2,2.3L4.7,31
c-0.2,1.8,0.3,3.6,1.5,4.9C7.4,37.3,9,38,10.8,38h15.4c1.8,0,3.4-0.7,4.6-2.1c1.2-1.4,1.7-3.4,1.5-5.2L30.1,3.4c-0.1-1.1-1-2-2-2
C28.1,1.4,6.8,1.4,6.8,1.4z"
		/>
		<path
			fill={primary}
			d="M33.9,7H31l-0.3-4c0-0.6-0.3-1.1-0.7-1.4C29.6,1.3,29.1,1,28.6,1H6.3C5.9,1,5.4,1.1,5.1,1.4
C4.7,1.6,4.5,2,4.3,2.4C4.1,2.7,4.1,3.2,4.2,3.6c0,0.4,0.2,0.8,0.5,1.1c0.9,1,1.3,1.8,1.2,2.4L4,31.7c-0.2,2,0.4,3.8,1.5,5.1
c1.2,1.4,2.9,2.2,4.8,2.2h16.2c1.9,0,3.6-0.8,4.8-2.1c1.2-1.3,1.7-3.3,1.6-5.4L32.4,24H34c2.8,0,5-2.3,5-5.2v-6.6
C39,9.3,36.8,7,33.9,7z M29.9,35.5c-0.9,1-2,1.5-3.3,1.5H10.4c-1.3,0-2.5-0.5-3.3-1.5c-0.8-0.9-1.2-2.1-1-3.6L7.9,7.3
C8,6.1,7.5,4.8,6.2,3.4c0,0,0-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0.1-0.1h22.4c0,0,0.1,0.1,0.1,0.2L29.1,8
c-0.3,2.2,0.6,10.5,1.1,14l0.8,9.7C31.1,33.1,30.7,34.6,29.9,35.5z M37,18.8c0,1.8-1.3,3.2-3,3.2h-1.8l-1-13h2.7
c1.7,0,3.1,1.4,3.1,3.1V18.8z M28.2,22.3l0.8,9.5c0.1,1-0.1,1.9-0.6,2.4S27.3,35,26.6,35H10.4c-0.7,0-1.4-0.3-1.8-0.8
C8.1,33.7,7.9,33,8,32.1l0.8-10.7c3.1-0.5,6.7-0.5,9.4,0.3C21.1,22.4,25.2,22.7,28.2,22.3z"
		/>
		<path
			fill={primary}
			d="M23.1,17.6c-1.5,0-3.2-0.4-4.9-1.2c-2.4-1.2-5.5-1.1-7.8,0.3C9.9,17,9.3,16.8,9,16.4
c-0.3-0.5-0.1-1.1,0.3-1.4c2.8-1.7,6.7-1.8,9.7-0.4c2.8,1.4,5.6,1.3,7.2,0c0.4-0.4,1-0.3,1.4,0.1c0.4,0.4,0.3,1-0.1,1.4
C26.5,17,24.9,17.6,23.1,17.6z"
		/>
	</svg>
);

Foam3Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Foam4Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>foam4</title>
		<path
			fill={tertiary}
			d="M6.8,1.4C6,1.4,5.3,2,5,2.7C4.6,3.5,4.8,4.4,5.3,5c0.9,1,1.2,1.7,1.2,2.3L4.7,31
c-0.2,1.8,0.3,3.6,1.5,4.9C7.4,37.3,9,38,10.8,38h15.4c1.8,0,3.4-0.7,4.6-2.1c1.2-1.4,1.7-3.4,1.5-5.2L30.1,3.4c-0.1-1.1-1-2-2-2
C28.1,1.4,6.8,1.4,6.8,1.4z"
		/>
		<path
			fill={primary}
			d="M33.9,7H31l-0.3-4c0-0.6-0.3-1.1-0.7-1.4C29.6,1.3,29.1,1,28.6,1H6.3C5.9,1,5.4,1.1,5.1,1.4
C4.7,1.6,4.5,2,4.3,2.4C4.1,2.7,4.1,3.2,4.2,3.6c0,0.4,0.2,0.8,0.5,1.1c0.9,1,1.3,1.8,1.2,2.4L4,31.7c-0.2,2,0.4,3.8,1.5,5.1
c1.2,1.4,2.9,2.2,4.8,2.2h16.2c1.9,0,3.6-0.8,4.8-2.1c1.2-1.3,1.7-3.3,1.6-5.4L32.4,24H34c2.8,0,5-2.3,5-5.2v-6.6
C39,9.3,36.8,7,33.9,7z M29.9,35.5c-0.9,1-2,1.5-3.3,1.5H10.4c-1.3,0-2.5-0.5-3.3-1.5c-0.8-0.9-1.2-2.1-1-3.6L7.9,7.3
C8,6.1,7.5,4.8,6.2,3.4c0,0,0-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0.1-0.1h22.4c0,0,0.1,0.1,0.1,0.2L29.1,8
c-0.3,2.2,0.6,10.5,1.1,14l0.8,9.7C31.1,33.1,30.7,34.6,29.9,35.5z M37,18.8c0,1.8-1.3,3.2-3,3.2h-1.8l-1-13h2.7
c1.7,0,3.1,1.4,3.1,3.1V18.8z M28.2,22.3l0.8,9.5c0.1,1-0.1,1.9-0.6,2.4S27.3,35,26.6,35H10.4c-0.7,0-1.4-0.3-1.8-0.8
C8.1,33.7,7.9,33,8,32.1l0.8-10.7c3.1-0.5,6.7-0.5,9.4,0.3C21.1,22.4,25.2,22.7,28.2,22.3z"
		/>
		<path
			fill={primary}
			d="M27.7,14.3c0.4-0.4,0.5-1,0.1-1.4s-1-0.5-1.4-0.1c-1.6,1.4-4.4,1.4-7.2,0c-3-1.5-6.9-1.3-9.7,0.4
c-0.5,0.3-0.6,0.9-0.3,1.4s0.9,0.6,1.4,0.3c2.2-1.3,5.4-1.5,7.8-0.3c1.6,0.8,3.3,1.2,4.9,1.2C24.9,15.8,26.5,15.3,27.7,14.3z"
		/>
	</svg>
);

Foam4Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Foam5Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>foam5</title>
		<path
			fill={tertiary}
			d="M6.8,1.4C6,1.4,5.3,2,5,2.7C4.6,3.5,4.8,4.4,5.3,5c0.9,1,1.2,1.7,1.2,2.3L4.7,31
c-0.2,1.8,0.3,3.6,1.5,4.9C7.4,37.3,9,38,10.8,38h15.4c1.8,0,3.4-0.7,4.6-2.1c1.2-1.4,1.7-3.4,1.5-5.2L30.1,3.4c-0.1-1.1-1-2-2-2
C28.1,1.4,6.8,1.4,6.8,1.4z"
		/>
		<path
			fill={primary}
			d="M33.9,7H31l-0.3-4c0-0.6-0.3-1.1-0.7-1.4C29.6,1.3,29.1,1,28.6,1H6.3C5.9,1,5.4,1.1,5.1,1.4
C4.7,1.6,4.5,2,4.3,2.4C4.1,2.7,4.1,3.2,4.2,3.6c0,0.4,0.2,0.8,0.5,1.1c0.9,1,1.3,1.8,1.2,2.4L4,31.7c-0.2,2,0.4,3.8,1.5,5.1
c1.2,1.4,2.9,2.2,4.8,2.2h16.2c1.9,0,3.6-0.8,4.8-2.1c1.2-1.3,1.7-3.3,1.6-5.4L32.4,24H34c2.8,0,5-2.3,5-5.2v-6.6
C39,9.3,36.8,7,33.9,7z M29.9,35.5c-0.9,1-2,1.5-3.3,1.5H10.4c-1.3,0-2.5-0.5-3.3-1.5c-0.8-0.9-1.2-2.1-1-3.6L7.9,7.3
C8,6.1,7.5,4.8,6.2,3.4c0,0,0-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0.1-0.1h22.4c0,0,0.1,0.1,0.1,0.2L29.1,8
c-0.3,2.2,0.6,10.5,1.1,14l0.8,9.7C31.1,33.1,30.7,34.6,29.9,35.5z M37,18.8c0,1.8-1.3,3.2-3,3.2h-1.8l-1-13h2.7
c1.7,0,3.1,1.4,3.1,3.1V18.8z M28.2,22.3l0.8,9.5c0.1,1-0.1,1.9-0.6,2.4S27.3,35,26.6,35H10.4c-0.7,0-1.4-0.3-1.8-0.8
C8.1,33.7,7.9,33,8,32.1l0.8-10.7c3.1-0.5,6.7-0.5,9.4,0.3C21.1,22.4,25.2,22.7,28.2,22.3z"
		/>
		<path
			fill={primary}
			d="M27.7,12.6c0.4-0.4,0.5-1,0.1-1.4s-1-0.5-1.4-0.1c-1.6,1.4-4.4,1.4-7.2,0c-3-1.5-6.9-1.3-9.7,0.4
c-0.5,0.3-0.6,0.9-0.3,1.4s0.9,0.6,1.4,0.3c2.2-1.3,5.4-1.5,7.8-0.3c1.6,0.8,3.3,1.2,4.9,1.2C24.9,14.1,26.5,13.6,27.7,12.6z"
		/>
	</svg>
);

Foam5Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Foam6Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>foam6</title>
		<path
			fill={tertiary}
			d="M6.8,1.4C6,1.4,5.3,2,5,2.7C4.6,3.5,4.8,4.4,5.3,5c0.9,1,1.2,1.7,1.2,2.3L4.7,31
c-0.2,1.8,0.3,3.6,1.5,4.9C7.4,37.3,9,38,10.8,38h15.4c1.8,0,3.4-0.7,4.6-2.1c1.2-1.4,1.7-3.4,1.5-5.2L30.1,3.4c-0.1-1.1-1-2-2-2
C28.1,1.4,6.8,1.4,6.8,1.4z"
		/>
		<path
			fill={primary}
			d="M33.9,7H31l-0.3-4c0-0.6-0.3-1.1-0.7-1.4C29.6,1.3,29.1,1,28.6,1H6.3C5.9,1,5.4,1.1,5.1,1.4
C4.7,1.6,4.5,2,4.3,2.4C4.1,2.7,4.1,3.2,4.2,3.6c0,0.4,0.2,0.8,0.5,1.1c0.9,1,1.3,1.8,1.2,2.4L4,31.7c-0.2,2,0.4,3.8,1.5,5.1
c1.2,1.4,2.9,2.2,4.8,2.2h16.2c1.9,0,3.6-0.8,4.8-2.1c1.2-1.3,1.7-3.3,1.6-5.4L32.4,24H34c2.8,0,5-2.3,5-5.2v-6.6
C39,9.3,36.8,7,33.9,7z M29.9,35.5c-0.9,1-2,1.5-3.3,1.5H10.4c-1.3,0-2.5-0.5-3.3-1.5c-0.8-0.9-1.2-2.1-1-3.6L7.9,7.3
C8,6.1,7.5,4.8,6.2,3.4c0,0,0-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0.1-0.1h22.4c0,0,0.1,0.1,0.1,0.2L29.1,8
c-0.3,2.2,0.6,10.5,1.1,14l0.8,9.7C31.1,33.1,30.7,34.6,29.9,35.5z M37,18.8c0,1.8-1.3,3.2-3,3.2h-1.8l-1-13h2.7
c1.7,0,3.1,1.4,3.1,3.1V18.8z M28.2,22.3l0.8,9.5c0.1,1-0.1,1.9-0.6,2.4S27.3,35,26.6,35H10.4c-0.7,0-1.4-0.3-1.8-0.8
C8.1,33.7,7.9,33,8,32.1l0.8-10.7c3.1-0.5,6.7-0.5,9.4,0.3C21.1,22.4,25.2,22.7,28.2,22.3z"
		/>
		<path
			fill={primary}
			d="M23.1,12.4c-1.5,0-3.2-0.4-4.9-1.2c-2.4-1.2-5.5-1.1-7.8,0.3c-0.5,0.3-1.1,0.1-1.4-0.3
c-0.1-0.5,0-1.1,0.5-1.4c2.8-1.7,6.7-1.8,9.7-0.4c2.8,1.4,5.6,1.3,7.2,0c0.4-0.4,1-0.3,1.4,0.1c0.4,0.4,0.3,1-0.1,1.4
C26.5,11.9,24.9,12.4,23.1,12.4z"
		/>
	</svg>
);

Foam6Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Foam7Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>foam7</title>
		<path
			fill={tertiary}
			d="M6.8,1.4C6,1.4,5.3,2,5,2.7C4.6,3.5,4.8,4.4,5.3,5c0.9,1,1.2,1.7,1.2,2.3L4.7,31
c-0.2,1.8,0.3,3.6,1.5,4.9C7.4,37.3,9,38,10.8,38h15.4c1.8,0,3.4-0.7,4.6-2.1c1.2-1.4,1.7-3.4,1.5-5.2L30.1,3.4c-0.1-1.1-1-2-2-2
C28.1,1.4,6.8,1.4,6.8,1.4z"
		/>
		<path
			fill={primary}
			d="M33.9,7H31l-0.3-4c0-0.6-0.3-1.1-0.7-1.4C29.6,1.3,29.1,1,28.6,1H6.3C5.9,1,5.4,1.1,5.1,1.4
C4.7,1.6,4.5,2,4.3,2.4C4.1,2.7,4.1,3.2,4.2,3.6c0,0.4,0.2,0.8,0.5,1.1c0.9,1,1.3,1.8,1.2,2.4L4,31.7c-0.2,2,0.4,3.8,1.5,5.1
c1.2,1.4,2.9,2.2,4.8,2.2h16.2c1.9,0,3.6-0.8,4.8-2.1c1.2-1.3,1.7-3.3,1.6-5.4L32.4,24H34c2.8,0,5-2.3,5-5.2v-6.6
C39,9.3,36.8,7,33.9,7z M29.9,35.5c-0.9,1-2,1.5-3.3,1.5H10.4c-1.3,0-2.5-0.5-3.3-1.5c-0.8-0.9-1.2-2.1-1-3.6L7.9,7.3
C8,6.1,7.5,4.8,6.2,3.4c0,0,0-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0.1-0.1h22.4c0,0,0.1,0.1,0.1,0.2L29.1,8
c-0.3,2.2,0.6,10.5,1.1,14l0.8,9.7C31.1,33.1,30.7,34.6,29.9,35.5z M37,18.8c0,1.8-1.3,3.2-3,3.2h-1.8l-1-13h2.7
c1.7,0,3.1,1.4,3.1,3.1V18.8z M28.2,22.3l0.8,9.5c0.1,1-0.1,1.9-0.6,2.4S27.3,35,26.6,35H10.4c-0.7,0-1.4-0.3-1.8-0.8
C8.1,33.7,7.9,33,8,32.1l0.8-10.7c3.1-0.5,6.7-0.5,9.4,0.3C21.1,22.4,25.2,22.7,28.2,22.3z"
		/>
		<path
			fill={primary}
			d="M23.1,12.4c-1.5,0-3.2-0.4-4.9-1.2c-2.4-1.2-5.5-1.1-7.8,0.3c-0.5,0.3-1.1,0.1-1.4-0.3
c-0.1-0.5,0-1.1,0.5-1.4c2.8-1.7,6.7-1.8,9.7-0.4c2.8,1.4,5.6,1.3,7.2,0c0.4-0.4,1-0.3,1.4,0.1c0.4,0.4,0.3,1-0.1,1.4
C26.5,11.9,24.9,12.4,23.1,12.4z"
		/>
	</svg>
);

Foam7Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Foam8Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>foam8</title>
		<path
			fill={tertiary}
			d="M6.8,1.4C6,1.4,5.3,2,5,2.7C4.6,3.5,4.8,4.4,5.3,5c0.9,1,1.2,1.7,1.2,2.3L4.7,31
c-0.2,1.8,0.3,3.6,1.5,4.9C7.4,37.3,9,38,10.8,38h15.4c1.8,0,3.4-0.7,4.6-2.1c1.2-1.4,1.7-3.4,1.5-5.2L30.1,3.4c-0.1-1.1-1-2-2-2
C28.1,1.4,6.8,1.4,6.8,1.4z"
		/>
		<path
			fill={primary}
			d="M33.9,7H31l-0.3-4c0-0.6-0.3-1.1-0.7-1.4C29.6,1.3,29.1,1,28.6,1H6.3C5.9,1,5.4,1.1,5.1,1.4
C4.7,1.6,4.5,2,4.3,2.4C4.1,2.7,4.1,3.2,4.2,3.6c0,0.4,0.2,0.8,0.5,1.1c0.9,1,1.3,1.8,1.2,2.4L4,31.7c-0.2,2,0.4,3.8,1.5,5.1
c1.2,1.4,2.9,2.2,4.8,2.2h16.2c1.9,0,3.6-0.8,4.8-2.1c1.2-1.3,1.7-3.3,1.6-5.4L32.4,24H34c2.8,0,5-2.3,5-5.2v-6.6
C39,9.3,36.8,7,33.9,7z M29.9,35.5c-0.9,1-2,1.5-3.3,1.5H10.4c-1.3,0-2.5-0.5-3.3-1.5c-0.8-0.9-1.2-2.1-1-3.6L7.9,7.3
C8,6.1,7.5,4.8,6.2,3.4c0,0,0-0.1-0.1-0.1c0,0,0-0.1,0-0.2c0,0,0-0.1,0.1-0.1h22.4c0,0,0.1,0.1,0.1,0.2L29.1,8
c-0.3,2.2,0.6,10.5,1.1,14l0.8,9.7C31.1,33.1,30.7,34.6,29.9,35.5z M37,18.8c0,1.8-1.3,3.2-3,3.2h-1.8l-1-13h2.7
c1.7,0,3.1,1.4,3.1,3.1V18.8z M28.2,22.3l0.8,9.5c0.1,1-0.1,1.9-0.6,2.4S27.3,35,26.6,35H10.4c-0.7,0-1.4-0.3-1.8-0.8
C8.1,33.7,7.9,33,8,32.1l0.8-10.7c3.1-0.5,6.7-0.5,9.4,0.3C21.1,22.4,25.2,22.7,28.2,22.3z"
		/>
		<path
			fill={primary}
			d="M23.1,9c-1.5,0-3.2-0.4-4.9-1.2c-2.4-1.2-5.5-1.1-7.8,0.3c-0.4,0.3-1,0.1-1.3-0.4C8.9,7.3,9,6.6,9.5,6.4
c2.8-1.7,6.7-1.8,9.7-0.4c2.8,1.4,5.6,1.3,7.2,0c0.4-0.4,1-0.3,1.4,0.1s0.3,1-0.1,1.4C26.5,8.5,24.9,9,23.1,9z"
		/>
	</svg>
);

Foam8Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Temp1Svg = ({ primary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp1</title>
		<path
			// fill="red"
			// This is not comign through as transparent.
			// So how do I add the theme?
			fill={tertiary || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#FCDF01"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24v-4.1C23.3,18.7,22.2,18,21,18h-1c-1.2,0-2.3,0.7-2.7,1.8v4.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);

Temp1Svg.colorVariation = [
	"original",
];
// FCDF01

export const Temp2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp2</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#FCC401"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24v-5.1C23.3,17.7,22.2,17,21,17h-1c-1.2,0-2.3,0.7-2.7,1.8v5.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);

Temp2Svg.colorVariation = [
	"original",
];
//FCC401

export const Temp3Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp3</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#FCC401"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24v-6.1C23.3,16.7,22.2,16,21,16h-1c-1.2,0-2.3,0.7-2.7,1.8v6.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);

Temp3Svg.colorVariation = [
	"original",
];
//FCC401

export const Temp4Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp4</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#FCAA01"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24v-7.1C23.3,15.7,22.2,15,21,15h-1c-1.2,0-2.3,0.7-2.7,1.8v7.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);
Temp4Svg.figmaName = "function/Temp4"
Temp4Svg.colorVariation = [
	"original",
];
//FCAA01

export const Temp5Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp5</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#FCAA01"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24v-8.1C23.3,14.7,22.2,14,21,14h-1c-1.2,0-2.3,0.7-2.7,1.8v8.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);
Temp5Svg.figmaName = "function/Temp5"
Temp5Svg.colorVariation = [
	"original",
];
//FCAA01

export const Temp6Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp6</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#EB661C"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24v-8.1C23.3,14.7,22.2,14,21,14h-1c-1.2,0-2.3,0.7-2.7,1.8v8.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);
Temp6Svg.figmaName = "function/Temp6"
Temp6Svg.colorVariation = [
	"original",
];
//EB661C

export const Temp7Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp7</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#EB661C"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24v-9.1C23.3,13.7,22.2,13,21,13h-1c-1.2,0-2.3,0.7-2.7,1.8v9.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);
Temp7Svg.figmaName = "function/Temp7"
Temp7Svg.colorVariation = [
	"original",
];
//EB661C

export const Temp8Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp8</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#E42520"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24V13.8C23.3,12.7,22.2,12,21,12h-1c-1.2,0-2.3,0.7-2.7,1.8v10.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);
Temp8Svg.figmaName = "function/Temp8"
Temp8Svg.colorVariation = [
	"original",
];
//E42520

export const Temp9Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp9</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#E42520"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24V12.8C23.3,11.7,22.2,11,21,11h-1c-1.2,0-2.3,0.7-2.7,1.8v11.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);
Temp9Svg.figmaName = "function/Temp9"
Temp9Svg.colorVariation = [
	"original",
];
//E42520

export const Temp10Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp10</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#981737"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24V11.8C23.3,10.7,22.2,10,21,10h-1c-1.2,0-2.3,0.7-2.7,1.8v12.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);

Temp10Svg.colorVariation = [
	"original",
];
//981737

export const Temp11Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp11</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#981737"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24V10.8C23.3,9.7,22.2,9,21,9h-1c-1.2,0-2.3,0.7-2.7,1.8v13.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);

Temp11Svg.colorVariation = [
	"original",
];
//981737

// WIP - need more colors as was originally 11
export const Temp12Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp12</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#981737"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24V9.8C23.3,8.7,22.2,8,21,8h-1c-1.2,0-2.3,0.7-2.7,1.8v14.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);

Temp12Svg.colorVariation = [
	"original",
];

export const Temp13Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp13</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#981737"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24V8.8C23.3,7.7,22.2,7,21,7h-1c-1.2,0-2.3,0.7-2.7,1.8v15.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);

Temp13Svg.colorVariation = [
	"original",
];

export const Temp14Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp14</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#981737"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24V7.8C23.3,6.7,22.2,6,21,6h-1c-1.2,0-2.3,0.7-2.7,1.8v16.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);

Temp14Svg.colorVariation = [
	"original",
];

export const Temp15Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>temp15</title>
		<path
			fill={tertiary  || colors.transparent}
			d="M27,21.9V7.2C27,4.6,24,2,21.2,2h-1c-2.9,0-5.8,2.6-5.8,5.2V22c-2.3,1.6-3.3,4-3.3,6.8
c0,4.8,4.4,8.9,9.6,8.9s9.6-4.1,9.6-8.9C30.3,25.9,29.3,23.5,27,21.9z"
		/>
		<path
			fill={primary || "#981737"}
			d="M27,29.1c0-2.2-0.8-3.8-2.5-4.7L23.7,24V6.8C23.3,5.7,22.2,5,21,5h-1c-1.2,0-2.3,0.7-2.7,1.8v17.1
l-0.8,0.4C14.8,25.2,14,26.8,14,29c0,1.5,0.7,3,1.9,4.1c1.2,1.2,2.9,1.8,4.6,1.8s3.4-0.7,4.6-1.8C26.3,32,27,30.6,27,29.1z"
		/>
		<path
			fill={primary || primary}
			d="M27.8,21.7V7.3C27.8,4.1,24.4,1,21,1h-1c-3.4,0-6.7,3.1-6.7,6.3v14.4C11.1,23.4,10,26,10,29.1
c0,5.3,4.8,9.9,10.5,9.9s10.5-4.6,10.6-9.9C31.1,26,29.9,23.4,27.8,21.7z M20.5,37c-4.6,0-8.5-3.6-8.5-7.9c0-2.7,1-4.8,2.8-6.1
c0.2-0.2,0.4-0.5,0.4-0.8V7.3C15.2,5.2,17.7,3,20,3h1c2.3,0,4.7,2.2,4.8,4.3v14.9c0,0.3,0.1,0.6,0.4,0.8c1.8,1.4,2.8,3.4,2.8,6.1
C29,33.4,25.1,37,20.5,37z"
		/>
	</svg>
);

Temp15Svg.colorVariation = [
	"original",
];

export const Water1Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>water1</title>
		<path
			fill={tertiary}
			d="M13.8,12.9c-1.3,2.1-2.6,4.3-3.9,6.5c-1.8,3.1-2.3,6.1-1.6,9.3C9.6,34,14.6,38,20,38s10.4-4,11.7-9.3
c0.7-3.1,0.2-6.2-1.6-9.3c-1.3-2.2-2.6-4.4-3.9-6.5C24.7,10.5,20.5,3.2,20,2C19.5,3.2,15.3,10.5,13.8,12.9z"
		/>
		<path fill={primary} d="M11.3,28h17.5c-0.5,1.9-1.6,3.7-3.2,5s-3.6,2-5.5,2s-3.9-0.7-5.5-2C12.9,31.7,11.7,29.9,11.3,28z" />
		<path
			fill={primary}
			d="M31,18.9c-1.2-2.1-2.5-4.2-3.7-6.2L27,12.4c-1.5-2.5-5.6-9.7-6.1-10.8C20.8,1.2,20.4,1,20,1s-0.8,0.2-0.9,0.6
c-0.5,1.1-4.6,8.3-6.1,10.8l-0.1,0.1c-1.3,2.1-2.6,4.2-3.8,6.4c-2,3.3-2.5,6.6-1.7,10C8.7,34.7,14.1,39,20,39s11.3-4.3,12.7-10.1
C33.5,25.5,32.9,22.2,31,18.9z M30.7,28.4C29.6,33.3,25,37,20,37s-9.6-3.7-10.7-8.6c-0.7-2.9-0.2-5.7,1.5-8.5
c1.2-2.1,2.5-4.3,3.8-6.3l0.1-0.2c1.1-1.8,3.8-6.6,5.3-9.2c1.5,2.7,4.2,7.4,5.3,9.2l0.2,0.3c1.2,2,2.5,4.1,3.7,6.2
C30.9,22.8,31.4,25.6,30.7,28.4z"
		/>
	</svg>
);

Water1Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Water2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>water2</title>
		<path
			fill={tertiary}
			d="M13.8,12.9c-1.3,2.1-2.6,4.3-3.9,6.5c-1.8,3.1-2.3,6.1-1.6,9.3C9.6,34,14.6,38,20,38s10.4-4,11.7-9.3
c0.7-3.1,0.2-6.2-1.6-9.3c-1.3-2.2-2.6-4.4-3.9-6.5C24.7,10.5,20.5,3.2,20,2C19.5,3.2,15.3,10.5,13.8,12.9z"
		/>
		<path
			fill={primary}
			d="M11.2,24h17.5c0.3,1.3,0.3,2.6,0,4c-0.5,1.9-1.6,3.7-3.2,5s-3.6,2-5.5,2s-3.9-0.7-5.5-2s-2.8-3.1-3.2-5
C10.9,26.6,10.9,25.3,11.2,24z"
		/>
		<path
			fill={primary}
			d="M31,18.9c-1.2-2.1-2.5-4.2-3.7-6.2L27,12.4c-1.5-2.5-5.6-9.7-6.1-10.8C20.8,1.2,20.4,1,20,1s-0.8,0.2-0.9,0.6
c-0.5,1.1-4.6,8.3-6.1,10.8l-0.1,0.1c-1.3,2.1-2.6,4.2-3.8,6.4c-2,3.3-2.5,6.6-1.7,10C8.7,34.7,14.1,39,20,39s11.3-4.3,12.7-10.1
C33.5,25.5,32.9,22.2,31,18.9z M30.7,28.4C29.6,33.3,25,37,20,37s-9.6-3.7-10.7-8.6c-0.7-2.9-0.2-5.7,1.5-8.5
c1.2-2.1,2.5-4.3,3.8-6.3l0.1-0.2c1.1-1.8,3.8-6.6,5.3-9.2c1.5,2.7,4.2,7.4,5.3,9.2l0.2,0.3c1.2,2,2.5,4.1,3.7,6.2
C30.9,22.8,31.4,25.6,30.7,28.4z"
		/>
	</svg>
);

Water2Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Water3Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>water3</title>
		<path
			fill={tertiary}
			d="M13.8,12.9c-1.3,2.1-2.6,4.3-3.9,6.5c-1.8,3.1-2.3,6.1-1.6,9.3C9.6,34,14.6,38,20,38
s10.4-4,11.7-9.3c0.7-3.1,0.2-6.2-1.6-9.3c-1.3-2.2-2.6-4.4-3.9-6.5C24.7,10.5,20.5,3.2,20,2C19.5,3.2,15.3,10.5,13.8,12.9z"
		/>
		<path
			fill={primary}
			d="M13.1,20H27c0.2,0.3,0.4,0.6,0.5,0.9c1.4,2.4,1.8,4.7,1.3,7.1c-0.5,1.9-1.6,3.7-3.2,5s-3.6,2-5.5,2
s-3.9-0.7-5.5-2s-2.8-3.1-3.2-5c-0.6-2.4-0.2-4.6,1.3-7.1C12.7,20.6,12.9,20.3,13.1,20z"
		/>
		<path
			fill={primary}
			d="M31,18.9c-1.2-2.1-2.5-4.2-3.7-6.2L27,12.4c-1.5-2.5-5.6-9.7-6.1-10.8C20.8,1.2,20.4,1,20,1s-0.8,0.2-0.9,0.6
c-0.5,1.1-4.6,8.3-6.1,10.8l-0.1,0.1c-1.3,2.1-2.6,4.2-3.8,6.4c-2,3.3-2.5,6.6-1.7,10C8.7,34.7,14.1,39,20,39s11.3-4.3,12.7-10.1
C33.5,25.5,32.9,22.2,31,18.9z M30.7,28.4C29.6,33.3,25,37,20,37s-9.6-3.7-10.7-8.6c-0.7-2.9-0.2-5.7,1.5-8.5
c1.2-2.1,2.5-4.3,3.8-6.3l0.1-0.2c1.1-1.8,3.8-6.6,5.3-9.2c1.5,2.7,4.2,7.4,5.3,9.2l0.2,0.3c1.2,2,2.5,4.1,3.7,6.2
C30.9,22.8,31.4,25.6,30.7,28.4z"
		/>
	</svg>
);

Water3Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Water4Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>water4</title>
		<path
			fill={tertiary}
			d="M13.8,12.9c-1.3,2.1-2.6,4.3-3.9,6.5c-1.8,3.1-2.3,6.1-1.6,9.3C9.6,34,14.6,38,20,38
s10.4-4,11.7-9.3c0.7-3.1,0.2-6.2-1.6-9.3c-1.3-2.2-2.6-4.4-3.9-6.5C24.7,10.5,20.5,3.2,20,2C19.5,3.2,15.3,10.5,13.8,12.9z"
		/>
		<path
			fill={primary}
			d="M14.8,17h10.3c0.8,1.3,1.6,2.6,2.3,3.9c1.4,2.4,1.8,4.7,1.3,7.1c-0.5,1.9-1.6,3.7-3.2,5s-3.6,2-5.5,2
s-3.9-0.7-5.5-2s-2.8-3.1-3.2-5c-0.6-2.4-0.2-4.6,1.3-7.1C13.3,19.6,14.1,18.3,14.8,17z"
		/>
		<path
			fill={primary}
			d="M31,18.9c-1.2-2.1-2.5-4.2-3.7-6.2L27,12.4c-1.5-2.5-5.6-9.7-6.1-10.8C20.8,1.2,20.4,1,20,1s-0.8,0.2-0.9,0.6
c-0.5,1.1-4.6,8.3-6.1,10.8l-0.1,0.1c-1.3,2.1-2.6,4.2-3.8,6.4c-2,3.3-2.5,6.6-1.7,10C8.7,34.7,14.1,39,20,39s11.3-4.3,12.7-10.1
C33.5,25.5,32.9,22.2,31,18.9z M30.7,28.4C29.6,33.3,25,37,20,37s-9.6-3.7-10.7-8.6c-0.7-2.9-0.2-5.7,1.5-8.5
c1.2-2.1,2.5-4.3,3.8-6.3l0.1-0.2c1.1-1.8,3.8-6.6,5.3-9.2c1.5,2.7,4.2,7.4,5.3,9.2l0.2,0.3c1.2,2,2.5,4.1,3.7,6.2
C30.9,22.8,31.4,25.6,30.7,28.4z"
		/>
	</svg>
);

Water4Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Water5Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>water5</title>
		<path
			fill={tertiary}
			d="M13.8,12.9c-1.3,2.1-2.6,4.3-3.9,6.5c-1.8,3.1-2.3,6.1-1.6,9.3C9.6,34,14.6,38,20,38
s10.4-4,11.7-9.3c0.7-3.1,0.2-6.2-1.6-9.3c-1.3-2.2-2.6-4.4-3.9-6.5C24.7,10.5,20.5,3.2,20,2C19.5,3.2,15.3,10.5,13.8,12.9z"
		/>
		<path
			fill={primary}
			d="M23.7,14.5c0,0,2.6,4.3,3.9,6.4c1.4,2.4,1.8,4.7,1.3,7.1c-0.5,1.9-1.6,3.7-3.2,5s-3.6,2-5.5,2
s-3.9-0.7-5.5-2s-2.8-3.1-3.2-5c-0.6-2.4-0.2-4.6,1.3-7.1c1.3-2.2,3.9-6.4,3.9-6.4H23.7z"
		/>
		<path
			fill={primary}
			d="M31,18.9c-1.2-2.1-2.5-4.2-3.7-6.2L27,12.4c-1.5-2.5-5.6-9.7-6.1-10.8C20.8,1.2,20.4,1,20,1s-0.8,0.2-0.9,0.6
c-0.5,1.1-4.6,8.3-6.1,10.8l-0.1,0.1c-1.3,2.1-2.6,4.2-3.8,6.4c-2,3.3-2.5,6.6-1.7,10C8.7,34.7,14.1,39,20,39s11.3-4.3,12.7-10.1
C33.5,25.5,32.9,22.2,31,18.9z M30.7,28.4C29.6,33.3,25,37,20,37s-9.6-3.7-10.7-8.6c-0.7-2.9-0.2-5.7,1.5-8.5
c1.2-2.1,2.5-4.3,3.8-6.3l0.1-0.2c1.1-1.8,3.8-6.6,5.3-9.2c1.5,2.7,4.2,7.4,5.3,9.2l0.2,0.3c1.2,2,2.5,4.1,3.7,6.2
C30.9,22.8,31.4,25.6,30.7,28.4z"
		/>
	</svg>
);

Water5Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Water6Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>water6</title>
		<path
			fill={tertiary}
			d="M13.8,12.9c-1.3,2.1-2.6,4.3-3.9,6.5c-1.8,3.1-2.3,6.1-1.6,9.3C9.6,34,14.6,38,20,38
s10.4-4,11.7-9.3c0.7-3.1,0.2-6.2-1.6-9.3c-1.3-2.2-2.6-4.4-3.9-6.5C24.7,10.5,20.5,3.2,20,2C19.5,3.2,15.3,10.5,13.8,12.9z"
		/>
		<path
			fill={primary}
			d="M18.4,11h3.2c0.8,1.4,1.6,2.7,2,3.5c1.3,2.1,2.6,4.3,3.9,6.4c1.4,2.4,1.8,4.7,1.3,7.1
c-0.5,1.9-1.6,3.7-3.2,5s-3.6,2-5.5,2s-3.9-0.7-5.5-2s-2.8-3.1-3.2-5c-0.6-2.4-0.2-4.6,1.3-7.1c1.3-2.2,2.6-4.3,3.9-6.4
C16.9,13.7,17.6,12.4,18.4,11z"
		/>
		<path
			fill={primary}
			d="M31,18.9c-1.2-2.1-2.5-4.2-3.7-6.2L27,12.4c-1.5-2.5-5.6-9.7-6.1-10.8C20.8,1.2,20.4,1,20,1s-0.8,0.2-0.9,0.6
c-0.5,1.1-4.6,8.3-6.1,10.8l-0.1,0.1c-1.3,2.1-2.6,4.2-3.8,6.4c-2,3.3-2.5,6.6-1.7,10C8.7,34.7,14.1,39,20,39s11.3-4.3,12.7-10.1
C33.5,25.5,32.9,22.2,31,18.9z M30.7,28.4C29.6,33.3,25,37,20,37s-9.6-3.7-10.7-8.6c-0.7-2.9-0.2-5.7,1.5-8.5
c1.2-2.1,2.5-4.3,3.8-6.3l0.1-0.2c1.1-1.8,3.8-6.6,5.3-9.2c1.5,2.7,4.2,7.4,5.3,9.2l0.2,0.3c1.2,2,2.5,4.1,3.7,6.2
C30.9,22.8,31.4,25.6,30.7,28.4z"
		/>
	</svg>
);

Water6Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const Water7Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>water7</title>
		<path
			fill={tertiary}
			d="M13.8,12.9c-1.3,2.1-2.6,4.3-3.9,6.5c-1.8,3.1-2.3,6.1-1.6,9.3C9.6,34,14.6,38,20,38
s10.4-4,11.7-9.3c0.7-3.1,0.2-6.2-1.6-9.3c-1.3-2.2-2.6-4.4-3.9-6.5C24.7,10.5,20.5,3.2,20,2C19.5,3.2,15.3,10.5,13.8,12.9z"
		/>
		<path
			fill={primary}
			d="M20,8.3c0.1,0.2,0.3,0.4,0.4,0.7l0.4,0.6c1.1,2,2.2,3.9,2.9,4.9c1.3,2.1,2.6,4.3,3.9,6.4
c1.4,2.4,1.8,4.7,1.3,7.1c-0.5,1.9-1.6,3.7-3.2,5s-3.6,2-5.5,2s-3.9-0.7-5.5-2s-2.8-3.1-3.2-5c-0.6-2.4-0.2-4.6,1.3-7.1
c1.3-2.2,2.6-4.3,3.9-6.4c0.6-1,1.8-3,2.9-4.9L20,9C19.7,8.7,19.9,8.5,20,8.3z"
		/>
		<path
			fill={primary}
			d="M31,18.9c-1.2-2.1-2.5-4.2-3.7-6.2L27,12.4c-1.5-2.5-5.6-9.7-6.1-10.8C20.8,1.2,20.4,1,20,1s-0.8,0.2-0.9,0.6
c-0.5,1.1-4.6,8.3-6.1,10.8l-0.1,0.1c-1.3,2.1-2.6,4.2-3.8,6.4c-2,3.3-2.5,6.6-1.7,10C8.7,34.7,14.1,39,20,39s11.3-4.3,12.7-10.1
C33.5,25.5,32.9,22.2,31,18.9z M30.7,28.4C29.6,33.3,25,37,20,37s-9.6-3.7-10.7-8.6c-0.7-2.9-0.2-5.7,1.5-8.5
c1.2-2.1,2.5-4.3,3.8-6.3l0.1-0.2c1.1-1.8,3.8-6.6,5.3-9.2c1.5,2.7,4.2,7.4,5.3,9.2l0.2,0.3c1.2,2,2.5,4.1,3.7,6.2
C30.9,22.8,31.4,25.6,30.7,28.4z"
		/>
	</svg>
);

Water7Svg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const PlaylistWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 40 40"} width={size} height={size}>
		<title>Playlist</title>
		<path fill={primary} d="M32.9,15.1H4.4v4.7h28.4V15.1z M32.9,5.6H4.4v4.7h28.4V5.6z M23.4,24.6h-19v4.7h19V24.6zM28.1,24.6v14.2L40,31.7L28.1,24.6z" />
	</svg>
);

//// Desinged for 60px

export const TutorialsWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>washrice</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M53.2388 44.208H54.9997C55.5611 44.208 56.0163 44.6631 56.0163 45.2246V47.8609C56.0163 49.5918 54.6079 51.0002 52.8769 51.0002H7.10669C5.37566 51.0002 3.96729 49.5919 3.96729 47.8609V45.2246C3.96729 44.6631 4.42241 44.208 4.98387 44.208H6.74489C6.54534 43.7706 6.43311 43.2852 6.43311 42.7737V16.672C6.43311 14.76 7.98868 13.2044 9.90066 13.2044H16.2439C16.3809 10.4956 18.6276 8.33398 21.3697 8.33398H34.5124H35.0747H38.6359C41.378 8.33398 43.6248 10.4956 43.7617 13.2044H50.0831C51.9951 13.2044 53.5506 14.7599 53.5506 16.672V42.7737C53.5506 43.2851 53.4385 43.7706 53.2388 44.208ZM34.5124 10.3671C34.512 10.3671 34.5116 10.3671 34.5112 10.3671L21.3697 10.3671C19.6609 10.3671 18.2705 11.7574 18.2705 13.4664V26.176C18.2705 27.885 19.6608 29.2754 21.3697 29.2754H24.1009H24.1012C24.6626 29.2754 25.1178 29.7305 25.1178 30.2919V32.6902L29.0162 29.5048C29.1977 29.3565 29.425 29.2755 29.6595 29.2755H29.66H38.6354C40.3444 29.2755 41.7349 27.885 41.7349 26.1759L41.7351 13.4664C41.7351 11.7575 40.3448 10.3671 38.6359 10.3671H34.5124ZM23.0844 34.8339V31.3086H21.3697C18.5396 31.3086 16.237 29.0061 16.237 26.1759V19.5386H13.0033V39.9069H46.9795V19.5386H43.7683V26.1761C43.7683 29.0062 41.4659 31.3086 38.6359 31.3086H38.6354H30.0225L24.7446 35.6213C24.093 36.1529 23.0844 35.6748 23.0844 34.8339ZM8.46627 16.6719V42.7736C8.46627 43.5642 9.10915 44.2074 9.89965 44.2079H50.084C50.8745 44.2074 51.5175 43.5642 51.5175 42.7736V16.6719C51.5175 15.881 50.874 15.2375 50.0831 15.2375H43.7683V17.5055H47.996C48.5575 17.5055 49.0126 17.9606 49.0126 18.5221V40.9235C49.0126 41.4849 48.5575 41.94 47.996 41.94H11.9867C11.4252 41.94 10.9701 41.4849 10.9701 40.9235V18.5221C10.9701 17.9606 11.4252 17.5055 11.9867 17.5055H16.237V15.2375H9.90066C9.10976 15.2375 8.46627 15.881 8.46627 16.6719ZM6.00045 46.2411V47.8608C6.00045 48.4708 6.49664 48.967 7.10669 48.9669H23.2905C23.0953 48.5349 22.9856 48.0562 22.9856 47.5521V46.2411H9.90056H9.89863H6.00045ZM25.0189 47.5522C25.0189 48.3323 25.6535 48.967 26.4337 48.967H33.5499C34.33 48.967 34.9648 48.3323 34.9648 47.5522V46.2412H25.0189V47.5522ZM52.8769 48.9669C53.4869 48.9669 53.9831 48.4708 53.9831 47.8608V46.2411H50.0849H50.083H36.9978V47.5521C36.9978 48.0562 36.8883 48.5349 36.693 48.9669H52.8769ZM25.3567 24.5293V15.476C25.3567 14.8875 25.6732 14.3392 26.1828 14.0451C26.6925 13.7508 27.3258 13.7508 27.8353 14.045L35.6757 18.5716C36.1853 18.8658 36.5018 19.4142 36.5018 20.0026C36.5018 20.591 36.1853 21.1394 35.6757 21.4337L27.8353 25.9603C27.3257 26.2546 26.6924 26.2546 26.1828 25.9603C25.6732 25.6661 25.3567 25.1178 25.3567 24.5293ZM34.088 20.0026L27.3898 16.1355V23.8698L34.088 20.0026Z" />
	</svg>
);

export const CoffeeMachineWeb2Svg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>washrice</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M51.8428 22.9444H46.6483V20.1111H48.5372C48.7877 20.1111 49.0279 20.0116 49.205 19.8345C49.3822 19.6574 49.4817 19.4171 49.4817 19.1667V8.30556C49.4807 7.42916 49.1321 6.58895 48.5124 5.96924C47.8927 5.34954 47.0525 5.00097 46.1761 5H11.2317C10.3553 5.00097 9.51505 5.34954 8.89535 5.96924C8.27565 6.58895 7.92708 7.42916 7.92611 8.30556V19.1667C7.92611 19.4171 8.02561 19.6574 8.20273 19.8345C8.37985 20.0116 8.62007 20.1111 8.87055 20.1111H10.7594V48.4444H6.03722C5.78674 48.4444 5.54651 48.5439 5.36939 48.7211C5.19228 48.8982 5.09277 49.1384 5.09277 49.3889V52.6944C5.09374 53.5708 5.44232 54.411 6.06202 55.0308C6.68172 55.6505 7.52194 55.999 8.39833 56H49.0094C49.8858 55.999 50.726 55.6505 51.3458 55.0308C51.9655 54.411 52.314 53.5708 52.315 52.6944V49.3889C52.315 49.1384 52.2155 48.8982 52.0384 48.7211C51.8613 48.5439 51.621 48.4444 51.3706 48.4444H46.6483V31.4444H51.8428C52.7192 31.4435 53.5594 31.0949 54.1791 30.4752C54.7988 29.8555 55.1474 29.0153 55.1483 28.1389V26.25C55.1474 25.3736 54.7988 24.5334 54.1791 23.9137C53.5594 23.294 52.7192 22.9454 51.8428 22.9444ZM33.1605 6.88889L32.6883 8.77778H24.7195L24.2473 6.88889H33.1605ZM9.815 18.2222V8.30556C9.81531 7.92993 9.96466 7.56978 10.2303 7.30417C10.4959 7.03856 10.856 6.8892 11.2317 6.88889H22.2994L23.0656 9.95125C23.1166 10.1556 23.2345 10.337 23.4005 10.4666C23.5665 10.5962 23.7711 10.6666 23.9817 10.6667H33.4261C33.6367 10.6666 33.8413 10.5962 34.0073 10.4666C34.1732 10.337 34.2911 10.1556 34.3422 9.95125L35.1084 6.88889H46.1761C46.5517 6.8892 46.9119 7.03856 47.1775 7.30417C47.4431 7.56978 47.5925 7.92993 47.5928 8.30556V18.2222H9.815ZM40.5094 44.6667C41.3858 44.6657 42.226 44.3171 42.8458 43.6974C43.4655 43.0777 43.814 42.2375 43.815 41.3611V40.4167C43.814 39.5403 43.4655 38.7001 42.8458 38.0804C42.226 37.4607 41.3858 37.1121 40.5094 37.1111H39.0928V36.1667C39.0928 35.9162 38.9933 35.676 38.8162 35.4988C38.639 35.3217 38.3988 35.2222 38.1483 35.2222H19.2594C19.009 35.2222 18.7687 35.3217 18.5916 35.4988C18.4145 35.676 18.315 35.9162 18.315 36.1667V44.6667C18.314 46.0615 18.8296 47.4073 19.7624 48.4444H12.6483V20.1111H20.2039V22.9444C20.2039 23.1949 20.3034 23.4352 20.4805 23.6123C20.6576 23.7894 20.8978 23.8889 21.1483 23.8889H22.0928V30.5C22.0928 30.7505 22.1923 30.9907 22.3694 31.1678C22.5465 31.3449 22.7867 31.4444 23.0372 31.4444H24.9261V33.3333C24.9261 33.5838 25.0256 33.824 25.2027 34.0012C25.3798 34.1783 25.6201 34.2778 25.8706 34.2778H31.5372C31.7877 34.2778 32.0279 34.1783 32.205 34.0012C32.3822 33.824 32.4817 33.5838 32.4817 33.3333V31.4444H34.3706C34.621 31.4444 34.8613 31.3449 35.0384 31.1678C35.2155 30.9907 35.315 30.7505 35.315 30.5V29.5556H38.1483V30.5C38.1483 30.7505 38.2478 30.9907 38.4249 31.1678C38.6021 31.3449 38.8423 31.4444 39.0928 31.4444H44.7594V48.4444H37.6454C38.5782 47.4073 39.0938 46.0615 39.0928 44.6667H40.5094ZM39.0928 42.7778V39H40.5094C40.8851 39.0003 41.2452 39.1497 41.5108 39.4153C41.7764 39.6809 41.9258 40.041 41.9261 40.4167V41.3611C41.9258 41.7367 41.7764 42.0969 41.5108 42.3625C41.2452 42.6281 40.8851 42.7775 40.5094 42.7778H39.0928ZM37.2039 37.1111V44.6667C37.2028 45.6683 36.8044 46.6285 36.0962 47.3367C35.3879 48.0449 34.4277 48.4433 33.4261 48.4444H23.9817C22.9801 48.4433 22.0198 48.0449 21.3116 47.3367C20.6034 46.6285 20.205 45.6683 20.2039 44.6667V37.1111H37.2039ZM22.0928 22V20.1111H35.315V22H22.0928ZM35.315 26.7222H38.1483V27.6667H35.315V26.7222ZM23.9817 29.5556V23.8889H33.4261V29.5556H23.9817ZM30.5928 31.4444V32.3889H26.815V31.4444H30.5928ZM38.1483 23.8889V24.8333H35.315V23.8889H36.2594C36.5099 23.8889 36.7501 23.7894 36.9273 23.6123C37.1044 23.4352 37.2039 23.1949 37.2039 22.9444V20.1111H44.7594V22.9444H39.0928C38.8423 22.9444 38.6021 23.0439 38.4249 23.2211C38.2478 23.3982 38.1483 23.6384 38.1483 23.8889ZM50.4261 50.3333V52.6944C50.4258 53.0701 50.2764 53.4302 50.0108 53.6958C49.7452 53.9614 49.3851 54.1108 49.0094 54.1111H8.39833C8.0227 54.1108 7.66255 53.9614 7.39694 53.6958C7.13133 53.4302 6.98198 53.0701 6.98166 52.6944V50.3333H50.4261ZM53.2594 28.1389C53.2591 28.5145 53.1098 28.8747 52.8442 29.1403C52.5786 29.4059 52.2184 29.5552 51.8428 29.5556H40.0372V24.8333H51.8428C52.2184 24.8336 52.5786 24.983 52.8442 25.2486C53.1098 25.5142 53.2591 25.8744 53.2594 26.25V28.1389ZM42.8689 16.3334C43.6161 16.3334 44.3465 16.1119 44.9678 15.6968C45.589 15.2816 46.0732 14.6916 46.3591 14.0013C46.6451 13.311 46.7199 12.5515 46.5741 11.8186C46.4284 11.0858 46.0686 10.4127 45.5402 9.88436C45.0119 9.35602 44.3388 8.99622 43.6059 8.85046C42.8731 8.70469 42.1135 8.7795 41.4232 9.06544C40.7329 9.35137 40.1429 9.83557 39.7278 10.4568C39.3127 11.0781 39.0912 11.8085 39.0912 12.5556C39.0923 13.5572 39.4907 14.5175 40.1989 15.2257C40.9071 15.9339 41.8673 16.3323 42.8689 16.3334ZM42.8689 10.6668C43.2425 10.6668 43.6077 10.7775 43.9183 10.9851C44.229 11.1926 44.4711 11.4877 44.614 11.8328C44.757 12.1779 44.7944 12.5577 44.7215 12.9242C44.6486 13.2906 44.4687 13.6271 44.2046 13.8913C43.9404 14.1555 43.6038 14.3354 43.2374 14.4082C42.871 14.4811 42.4912 14.4437 42.1461 14.3008C41.8009 14.1578 41.5059 13.9157 41.2984 13.6051C41.0908 13.2944 40.98 12.9292 40.98 12.5556C40.9806 12.0549 41.1798 11.5748 41.5339 11.2207C41.888 10.8666 42.3681 10.6674 42.8689 10.6668ZM18.0373 16.0568C17.8602 16.2339 17.62 16.3334 17.3695 16.3334H11.7028C11.4523 16.3334 11.2121 16.2339 11.035 16.0568C10.8579 15.8797 10.7584 15.6395 10.7584 15.389V9.72231C10.7584 9.47183 10.8579 9.23161 11.035 9.05449C11.2121 8.87737 11.4523 8.77787 11.7028 8.77787H17.3695C17.62 8.77787 17.8602 8.87737 18.0373 9.05449C18.2144 9.23161 18.3139 9.47183 18.3139 9.72231V15.389C18.3139 15.6395 18.2144 15.8797 18.0373 16.0568ZM16.425 10.6668H12.6472V14.4445H16.425V10.6668Z" />
	</svg>
);

export const CoffeeCategoryWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>coffee</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M34 18.9C31.6 18.9 28.9 17.7 26.8 15.6C25.1 13.9 24 11.9 23.6 9.8C23.2 7.6 23.7 5.8 25 4.5C26 3.5 27.3 3 28.9 3C31.3 3 34 4.2 36.1 6.3C37.8 8 38.9 10 39.3 12.1C39.7 14.3 39.2 16.1 37.9 17.4C36.9 18.4 35.6 18.9 34 18.9ZM37.6 12.4C37.3 10.7 36.3 9 34.9 7.5C33.1 5.7 30.9 4.7 29 4.7C28.2 4.7 27.6 4.9 27 5.2L29.2 7.4C29.5 7.7 29.5 8.3 29.2 8.6C29 8.8 28.8 8.9 28.6 8.9C28.4 8.9 28.2 8.8 28 8.6L25.8 6.4C25.3 7.2 25.2 8.3 25.4 9.5C25.7 11.2 26.7 12.9 28.1 14.4C29.9 16.2 32.1 17.2 34 17.2C34.8 17.2 35.4 17 36 16.7L30.4 11.1C30.1 10.8 30.1 10.2 30.4 9.9C30.7 9.6 31.3 9.6 31.6 9.9L37.2 15.5C37.7 14.7 37.8 13.6 37.6 12.4ZM13.1 44.2H17.8C21.4 47.9 26.3 49.9 31.5 49.9C42.1 49.9 50.7 41.3 50.7 30.7V21.5C50.7 21 50.3 20.6 49.8 20.6H13.1C12.6 20.6 12.2 21 12.2 21.5V24.1C9.8 24.3 7.6 25.3 6 27C4 28.9 3 31.5 3 34.1C3 36.8 4 39.3 5.9 41.2C7.8 43.1 10.4 44.2 13.1 44.2ZM7.2 28.2C8.8 26.6 10.9 25.8 13.1 25.8C13.6 25.8 14 25.4 14 24.9V22.3H49V30.6C49 40.3 41.1 48.1 31.5 48.1C26.6 48.1 22.1 46.2 18.8 42.6C18.6 42.4 18.4 42.3 18.2 42.3H13.2C11 42.3 8.9 41.4 7.3 39.9C5.7 38.3 4.9 36.2 4.9 34C4.7 31.9 5.6 29.8 7.2 28.2ZM13 40.7H15.5C15.8 40.7 16.1 40.5 16.2 40.3C16.4 40 16.4 39.7 16.2 39.4C14.7 36.7 13.9 33.7 13.9 30.6V28.3C13.9 27.8 13.5 27.4 13 27.4C11.2 27.4 9.6 28.1 8.3 29.3C7.1 30.5 6.4 32.2 6.4 34C6.4 35.8 7.1 37.4 8.3 38.7C9.6 40.1 11.3 40.7 13 40.7ZM9.6 30.7C10.3 30 11.2 29.5 12.2 29.3V30.7C12.2 33.6 12.9 36.4 14.1 39H13.1C11.8 39 10.6 38.5 9.6 37.6C8.7 36.7 8.2 35.5 8.2 34.1C8.2 32.8 8.7 31.6 9.6 30.7ZM41.2 43.1C38.4 45.3 35 46.5 31.4 46.5C22.7 46.5 15.6 39.4 15.6 30.7V25C15.6 24.5 16 24.1 16.5 24.1C18.5 24.1 19.6 24.6 20.6 25C21.5 25.4 22.3 25.7 24 25.7C25.7 25.7 26.5 25.4 27.4 25C28.4 24.6 29.4 24.1 31.5 24.1C33.5 24.1 34.6 24.6 35.6 25C36.5 25.4 37.3 25.7 39 25.7C40.7 25.7 41.5 25.4 42.4 25C43.4 24.6 44.4 24.1 46.5 24.1C46.7 24.1 46.9 24.2 47.1 24.4C47.3 24.6 47.4 24.8 47.4 25V30.7C47.4 34.3 46.2 37.7 44 40.5C43.7 40.9 43.2 40.9 42.8 40.6C42.4 40.3 42.4 39.8 42.7 39.4C44.7 36.9 45.7 33.9 45.7 30.7V25.9C44.6 26 43.9 26.3 43.2 26.6C42.2 27 41.2 27.5 39.1 27.5C37.1 27.5 36 27 35 26.6C34.1 26.2 33.3 25.9 31.6 25.9C29.9 25.9 29.1 26.2 28.2 26.6C27.2 27 26.2 27.5 24.1 27.5C22.1 27.5 21 27 20 26.6C19.3 26.3 18.6 26 17.5 25.9V30.7C17.5 38.5 23.8 44.8 31.6 44.8C34.8 44.8 37.8 43.7 40.3 41.8C40.7 41.5 41.2 41.6 41.5 41.9C41.7 42.2 41.6 42.8 41.2 43.1ZM56.1 51.2H6.7C6.2 51.2 5.8 51.6 5.8 52.1C5.8 52.6 6.2 53 6.7 53H7.2L7.7 54.7C8.1 56 9.4 57 10.7 57H52.1C53.4 57 54.8 56 55.1 54.7L55.6 53H56.1C56.6 53 57 52.6 57 52.1C57 51.6 56.6 51.2 56.1 51.2ZM53.5 54.2C53.3 54.7 52.7 55.2 52.1 55.2H10.8C10.2 55.2 9.6 54.7 9.4 54.2L9 53H53.8L53.5 54.2Z" />
	</svg>
);

export const JuicingCategoryWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>juicing</title>
		<path fill={primary} d="M40.4,4.8c0.2,0,0.5-0.1,0.6-0.3c0.2-0.2,0.3-0.4,0.3-0.6c0-0.2-0.1-0.5-0.3-0.6C40.9,3.1,40.7,3,40.4,3H29.5c-0.2,0-0.5,0.1-0.6,0.2c-0.2,0.2-0.3,0.4-0.3,0.6l-0.3,4.6c-6.7,0.5-12,5.2-12.4,10.9c-0.5,0.2-1,0.5-1.3,1c-0.3,0.5-0.5,1-0.5,1.6v1.8c0,0.2,0.1,0.5,0.3,0.6c0.2,0.2,0.4,0.3,0.6,0.3h1l3.6,31.6c0,0.2,0.1,0.4,0.3,0.6s0.4,0.2,0.6,0.2h18.2c0.2,0,0.4-0.1,0.6-0.2c0.2-0.1,0.3-0.4,0.3-0.6l3.6-31.6h1c0.2,0,0.5-0.1,0.6-0.3c0.2-0.2,0.3-0.4,0.3-0.6v-1.8c0-0.6-0.2-1.1-0.5-1.6c-0.3-0.5-0.8-0.8-1.3-1c-0.4-5.9-6.1-10.7-13.1-10.9l0.3-3.6H40.4z M28.1,10.3l-0.7,8.9h-9.7C18.2,14.5,22.6,10.8,28.1,10.3z M15.8,21.9c0-0.2,0.1-0.5,0.3-0.6c0.2-0.2,0.4-0.3,0.6-0.3h10.6l-0.1,1.8H15.8V21.9z M27,24.6l-0.3,4.6c-2.2-0.5-4.7-0.9-8.5-1l-0.4-3.6H27z M37.8,55.2H21.2L18.4,30c3.7,0,6.1,0.5,8.2,1l-1.6,21.4c0,0.1,0,0.2,0,0.3c0,0.1,0.1,0.2,0.2,0.3c0.1,0.1,0.2,0.2,0.3,0.2c0.1,0.1,0.2,0.1,0.3,0.1c0,0,0,0,0.1,0c0.2,0,0.5-0.1,0.6-0.2c0.2-0.2,0.3-0.4,0.3-0.6l1.6-21.1c0.3,0.1,0.6,0.2,0.9,0.3c2.8,0.8,5.7,1.7,11,1.8L37.8,55.2z M40.4,31.8c-5.2-0.1-7.8-0.9-10.7-1.8c-0.4-0.1-0.9-0.3-1.3-0.4l0.4-5.1h12.4L40.4,31.8z M43.2,22.8H29l0.1-1.8h13.1c0.2,0,0.5,0.1,0.6,0.3c0.2,0.2,0.3,0.4,0.3,0.6V22.8z M41.3,19.2h-12l0.7-9C35.9,10.4,40.8,14.3,41.3,19.2z" />
	</svg>
);

export const CocktailCategoryWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>cocktail</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M53.8028 12.8974C53.8019 10.3501 52.8194 7.90121 51.0594 6.0598C49.2993 4.21838 46.8973 3.12617 44.3527 3.01026C41.8081 2.89436 39.3167 3.76367 37.3966 5.43746C35.4764 7.11125 34.2753 9.46071 34.0429 11.9973H7.90005C7.66134 11.9973 7.43241 12.0922 7.26362 12.261C7.09483 12.4298 7 12.6587 7 12.8974C7 23.0178 14.9953 31.3052 25.0011 31.7766V50.1434L14.6979 55.295C14.5166 55.3857 14.3712 55.5349 14.2853 55.7185C14.1994 55.9022 14.178 56.1094 14.2246 56.3068C14.2712 56.5041 14.383 56.6799 14.542 56.8057C14.7009 56.9315 14.8977 57 15.1005 57H36.7018C36.9045 57 37.1013 56.9315 37.2603 56.8057C37.4192 56.6799 37.5311 56.5041 37.5777 56.3068C37.6243 56.1094 37.6029 55.9022 37.5169 55.7185C37.431 55.5349 37.2856 55.3857 37.1043 55.295L26.8012 50.1434V31.7766C29.9057 31.6264 32.9254 30.7134 35.5931 29.1186C38.2609 27.5237 40.4946 25.296 42.0966 22.6325C43.5253 22.897 44.9948 22.8439 46.4007 22.4769C47.8066 22.1099 49.1145 21.4381 50.2317 20.509C51.3489 19.58 52.2479 18.4164 52.8651 17.101C53.4823 15.7855 53.8024 14.3504 53.8028 12.8974ZM45.8591 13.7974H51.9515C51.7582 15.5276 51.0109 17.1489 49.8209 18.4197L45.8591 13.7974ZM44.8022 4.8481C46.3218 5.01747 47.7618 5.61544 48.9544 6.57227L44.8022 10.7244V4.8481ZM43.0021 10.7244L38.85 6.57227C40.0425 5.61544 41.4826 5.01747 43.0021 4.8481V10.7244ZM51.9515 11.9973H46.0751L50.2273 7.84517C51.1842 9.0377 51.7822 10.4778 51.9515 11.9973ZM37.5771 7.84517L41.7292 11.9973H35.8529C36.0222 10.4778 36.6202 9.0377 37.5771 7.84517ZM18.9132 55.1999L25.9011 51.7059L32.889 55.1999H18.9132ZM25.9011 29.9984C16.7738 29.9984 9.29356 22.8092 8.82351 13.7974H42.9787C42.5087 22.8098 35.0284 29.9984 25.9011 29.9984ZM42.9995 20.9477C43.8618 19.1241 44.4241 17.1733 44.6646 15.1705L48.4553 19.5929C47.1141 20.5097 45.5269 20.9995 43.9022 20.9979C43.6006 20.9978 43.2993 20.9811 42.9995 20.9477ZM33.7604 23.8681C31.5152 25.4808 28.8283 26.3638 26.0642 26.3973C23.3 26.4309 20.5924 25.6132 18.3088 24.0554C18.2115 23.9853 18.101 23.9353 17.9841 23.9085C17.8671 23.8817 17.7459 23.8785 17.6277 23.8992C17.5095 23.9199 17.3966 23.964 17.2957 24.0289C17.1948 24.0939 17.1079 24.1784 17.0401 24.2774C16.9723 24.3765 16.925 24.4881 16.901 24.6056C16.877 24.7232 16.8767 24.8444 16.9003 24.9621C16.9238 25.0798 16.9706 25.1916 17.0379 25.2909C17.1053 25.3902 17.1918 25.4751 17.2925 25.5405C19.8816 27.3074 22.9516 28.2348 26.0859 28.1968C29.2203 28.1588 32.267 27.1572 34.8124 25.3281C34.9097 25.2595 34.9924 25.1724 35.0557 25.0717C35.1191 24.971 35.1618 24.8587 35.1815 24.7414C35.2012 24.6241 35.1974 24.504 35.1704 24.3881C35.1434 24.2723 35.0937 24.1629 35.0242 24.0664C34.9546 23.9699 34.8667 23.8881 34.7653 23.8257C34.664 23.7634 34.5513 23.7218 34.4338 23.7033C34.3163 23.6848 34.1962 23.6898 34.0806 23.718C33.9651 23.7462 33.8562 23.797 33.7604 23.8675V23.8681ZM40.2237 15.6891C40.347 15.7495 40.4549 15.8374 40.539 15.9459C40.6231 16.0545 40.6813 16.181 40.7091 16.3155C40.7368 16.4501 40.7334 16.5892 40.6991 16.7222C40.3532 18.0624 39.8267 19.3492 39.134 20.5475C39.0761 20.6522 38.998 20.7444 38.9041 20.8186C38.8103 20.8927 38.7025 20.9475 38.5873 20.9795C38.472 21.0116 38.3515 21.0204 38.2328 21.0053C38.1141 20.9902 37.9996 20.9517 37.8959 20.8918C37.7923 20.832 37.7017 20.7521 37.6293 20.6568C37.5569 20.5615 37.5043 20.4528 37.4745 20.3369C37.4446 20.221 37.4382 20.1004 37.4555 19.982C37.4728 19.8636 37.5136 19.7499 37.5754 19.6474C37.9895 18.9308 38.3368 18.1776 38.6127 17.3973H13.1841C13.6058 18.5905 14.1938 19.7183 14.9308 20.7472C15.0018 20.8432 15.0529 20.9524 15.0813 21.0684C15.1097 21.1844 15.1147 21.3049 15.096 21.4229C15.0774 21.5408 15.0354 21.6539 14.9726 21.7555C14.9098 21.8571 14.8274 21.9451 14.7302 22.0146C14.6331 22.084 14.523 22.1334 14.4066 22.1599C14.2901 22.1864 14.1696 22.1894 14.0519 22.1688C13.9343 22.1482 13.8219 22.1045 13.7214 22.04C13.6208 21.9756 13.5341 21.8918 13.4663 21.7935C12.3706 20.2638 11.5679 18.5444 11.0989 16.7222C11.0646 16.5892 11.0611 16.4501 11.0888 16.3156C11.1165 16.1811 11.1747 16.0546 11.2588 15.9461C11.3429 15.8375 11.4508 15.7496 11.5741 15.6891C11.6975 15.6286 11.833 15.5972 11.9704 15.5972H39.8276C39.9649 15.5972 40.1004 15.6286 40.2237 15.6891Z" />
	</svg>
);

export const DessertCategoryWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>dessert</title>
		<path fill={primary} d="M50.1211 34.4392C52.2034 32.0995 52.6217 28.5444 51.0398 25.8636C49.9671 24.046 48.2091 23.03 46.0713 22.9064C46.1656 22.665 46.239 22.4112 46.2904 22.1467C46.6435 20.3191 45.8672 18.2765 44.3579 17.0648C43.6154 16.4683 42.746 16.1074 41.8094 15.9879C42.0352 15.276 42.0711 14.4767 41.8745 13.6676C41.4692 11.9982 40.0839 10.5274 37.7954 10.3552C37.9373 9.75494 37.9811 9.08752 37.9181 8.38302C37.727 6.24687 36.6393 4.30476 35.0081 3.18786C34.6876 2.96869 34.2714 2.93902 33.9225 3.11082C33.574 3.28303 33.3478 3.62869 33.3328 4.01349C33.2188 6.95302 31.2521 8.85887 29.6226 9.94034C27.5587 11.3102 24.8674 12.1276 22.4236 12.1276C20.2111 12.1276 18.9064 13.0171 18.2014 13.7636C17.3349 14.6807 16.8578 15.9059 16.8578 17.214C16.8578 17.7697 16.9438 18.2921 17.1016 18.7651C15.571 18.6024 14.2304 19.1779 13.4199 20.3484C12.5041 21.67 12.3877 23.5351 13.106 25.0615C9.84122 25.3561 8.36369 27.1816 7.80482 28.1465C6.58272 30.2571 6.77764 32.9816 8.21803 34.8742C8.05692 35.4143 7.96926 35.9849 7.96926 36.5757C7.96926 37.1719 8.05733 37.7602 8.23096 38.325L11.1305 51.2281C11.2583 51.7962 11.8288 52.1542 12.404 52.0281C12.9795 51.9021 13.3427 51.3393 13.2149 50.7712L10.3062 37.8273C10.2995 37.7985 10.292 37.7701 10.2828 37.7421C10.1647 37.3667 10.1042 36.9745 10.1042 36.5757C10.1042 36.0476 10.211 35.5433 10.4034 35.0827C10.4093 35.0699 10.4139 35.0571 10.4193 35.0444C11.0245 33.6378 12.4365 32.6487 14.0785 32.6487C16.1587 32.6487 17.8775 34.2492 18.0349 36.2704C18.0349 36.4064 18.0407 36.5304 18.0507 36.64C18.0524 36.6606 18.0561 36.6804 18.0591 36.7006L19.729 54.8923H15.3987C14.3732 54.8923 13.8615 53.9286 13.735 53.1034C13.7312 53.0804 13.7271 53.0573 13.7216 53.0338L13.1131 50.3163C12.9858 49.7482 12.4165 49.3894 11.8405 49.515C11.2645 49.6407 10.901 50.203 11.0283 50.7712L11.6297 53.4565C11.9657 55.5444 13.5108 57 15.3987 57H20.8952H20.896H20.8973H37.5788H37.58H37.5809H43.0352C44.9226 57 46.4678 55.5449 46.8042 53.4565L50.215 38.2575C50.3928 37.6832 50.483 37.0854 50.483 36.4801V36.4402C50.4784 35.7386 50.3503 35.0654 50.1211 34.4392ZM19.7628 15.2006C20.368 14.5599 21.2633 14.2349 22.4236 14.2349C25.2752 14.2349 28.4114 13.2832 30.8139 11.6892C32.9567 10.2675 34.4196 8.41269 35.0774 6.32968C35.5094 7.07084 35.7302 7.88699 35.7911 8.56842C35.8775 9.53412 35.673 10.3873 35.2577 10.7956C34.9209 11.1256 34.8458 11.6353 35.0728 12.0468C35.2999 12.458 35.7749 12.6722 36.2377 12.5721C39.038 11.969 39.6707 13.6342 39.798 14.1587C39.9942 14.9653 39.7529 15.9104 39.04 16.2957C38.8948 16.3447 38.7495 16.3986 38.6043 16.458C38.6039 16.458 38.603 16.458 38.6026 16.4584C32.7989 18.6699 25.1466 19.7963 20.4077 19.1367C20.4031 19.1359 20.3985 19.1359 20.3939 19.1355C20.3889 19.1347 20.3843 19.1334 20.3793 19.133L19.9932 19.0893C19.3413 18.9031 18.9927 18.0355 18.9927 17.2136C18.9927 16.4526 19.2732 15.7185 19.7628 15.2006ZM15.182 21.5374C15.5523 21.003 16.1274 20.776 16.8916 20.8625L19.6176 21.1686C19.797 21.2057 19.9828 21.2267 20.1727 21.2317C21.1765 21.368 22.2842 21.434 23.4642 21.434C28.3108 21.434 34.3633 20.3253 39.3255 18.4417C39.51 18.3902 39.6878 18.3255 39.8564 18.2481C41.3832 17.7697 42.4054 18.2135 43.0106 18.6996C43.9138 19.4251 44.4001 20.68 44.193 21.7516C44.0854 22.3082 43.7402 23.0386 42.7088 23.3719C42.7034 23.3736 42.698 23.3752 42.693 23.3769C42.6909 23.3777 42.6884 23.3781 42.6863 23.379C42.6817 23.3806 42.6767 23.3822 42.6721 23.3835C42.6717 23.3839 42.6713 23.3839 42.6713 23.3839C42.6708 23.3839 42.6708 23.3839 42.6708 23.3839C39.3485 24.5173 34.324 25.3252 28.5199 25.6597C23.0418 25.9753 18.8329 25.7565 17.2385 25.4195C16.8078 25.3198 16.395 25.2399 15.9993 25.1793C15.5485 24.9593 15.2705 24.6067 15.1023 24.2919C14.6407 23.4292 14.6745 22.2707 15.182 21.5374ZM28.1601 54.8923H21.8727L20.1798 36.4509C20.1794 36.446 20.1785 36.4418 20.1781 36.4369C20.1764 36.364 20.1752 36.291 20.171 36.2189C20.1823 35.6521 20.3584 34.5174 21.1135 33.7017C21.7813 32.9811 22.8047 32.6157 24.1553 32.6157C26.3307 32.6157 28.1126 34.3671 28.1267 36.5201C28.1272 36.6062 28.1393 36.6898 28.1601 36.7702V54.8923ZM36.6004 54.8923H30.295V36.13C30.4098 35.4069 30.8339 34.4392 31.6211 33.6774C32.1361 33.1789 33.0055 32.5902 34.2105 32.6153C35.2364 32.6367 36.3738 33.2283 37.252 34.1977C37.8188 34.8227 38.1923 35.528 38.2662 36.0838L36.6004 54.8923ZM48.3477 36.4801C48.3477 36.8868 48.2859 37.2872 48.164 37.6708C48.1548 37.6996 48.1465 37.7293 48.1398 37.759L44.7123 53.0334C44.7068 53.0565 44.7027 53.0799 44.6989 53.1034C44.562 53.9937 43.9948 54.8923 43.0352 54.8923H38.7437L40.3427 36.8324C40.3723 36.7508 40.3928 36.6651 40.4011 36.5753C40.412 36.4591 40.4161 36.3413 40.4141 36.2222C40.5485 34.1751 42.2743 32.5498 44.3767 32.5498C46.5521 32.5498 48.3335 34.3016 48.3477 36.4501V36.4801ZM48.9516 32.4925C47.8305 31.2372 46.1936 30.4425 44.3767 30.4425C42.2126 30.4425 40.3076 31.5614 39.2229 33.2448C39.1047 33.0924 38.9783 32.9412 38.8435 32.7929C37.5592 31.3752 35.8867 30.5422 34.2556 30.5084C32.7129 30.4746 31.2847 31.0514 30.1264 32.1724C29.7996 32.4888 29.5041 32.8431 29.2466 33.2201C28.1505 31.5878 26.2744 30.5084 24.1558 30.5084C22.1807 30.5084 20.6268 31.1041 19.5383 32.2795C19.3271 32.5074 19.1438 32.7471 18.9848 32.9935C17.8708 31.507 16.084 30.5409 14.0785 30.5409C12.2099 30.5409 10.5353 31.3748 9.41383 32.6849C8.98517 31.5717 9.04778 30.2468 9.65841 29.1925C10.605 27.5577 12.6727 26.8685 15.4296 27.2294C15.8812 27.3962 16.3724 27.4881 16.8912 27.5009C18.2673 27.7749 20.626 27.9186 23.3945 27.9186C25.0398 27.9186 26.8299 27.868 28.6439 27.7633C32.1077 27.5639 38.6101 26.99 43.3311 25.3878C43.3424 25.3845 43.3533 25.3812 43.3641 25.3775C46.0011 24.5482 48.1244 25.1109 49.1945 26.9241C50.0773 28.4196 50.1407 30.7222 48.9516 32.4925Z" />
	</svg>
);

export const BakingCategoryWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>baking</title>
		<path fill={primary} d="M56.059 51.507L51.2577 46.7057C51.7332 45.0289 51.3133 43.1498 49.9958 41.8323C49.8318 41.6683 49.6576 41.5192 49.4755 41.3816C50.0234 40.752 50.3233 39.954 50.3233 39.1066C50.3233 38.1755 49.9624 37.3033 49.3082 36.6511L47.4547 34.7976C47.0431 34.386 46.3757 34.386 45.9641 34.7976C45.5529 35.2092 45.5529 35.8766 45.9641 36.2878L47.8189 38.1425C48.0747 38.3975 48.2156 38.7399 48.2156 39.1066C48.2156 39.4728 48.0747 39.8152 47.818 40.0714L46.9928 40.8962L40.8967 46.9924L39.9998 47.8893C39.4671 48.4224 38.5998 48.4224 38.0671 47.8893L30.173 39.9956C30.1471 39.9668 30.1199 39.9392 30.0906 39.9132L26.4771 36.2997C26.4548 36.275 26.4314 36.2515 26.4062 36.2284L20.6516 30.4738C20.6236 30.4425 20.5943 30.4132 20.5638 30.386L12.17 21.9926C11.6373 21.4599 11.6373 20.5931 12.17 20.06L19.9883 12.2421C20.2433 11.9871 20.5861 11.8466 20.9532 11.8466C21.3203 11.8466 21.663 11.9871 21.918 12.2421L46.4783 36.8023C46.8898 37.2139 47.5573 37.2139 47.9688 36.8023C48.3804 36.3907 48.3804 35.7233 47.9688 35.3118L39.6183 26.9612C39.7324 26.8738 39.8416 26.7791 39.9445 26.6765C40.5592 26.0614 40.8975 25.2424 40.8967 24.3694C40.8962 24.0344 40.846 23.7077 40.7492 23.3975L52.6086 13.7562C53.7391 12.8383 54.4053 11.489 54.4848 9.95682C54.573 8.24666 53.9023 6.50189 52.6898 5.28941C51.4794 4.07899 49.7371 3.40787 48.029 3.49521C46.496 3.57349 45.1447 4.23885 44.2234 5.36975L34.7313 17.0459C34.262 16.7913 33.733 16.6553 33.1822 16.6549H33.1789C32.3075 16.6549 31.4893 16.9927 30.8751 17.607C30.7721 17.71 30.6773 17.8192 30.59 17.9333L23.4086 10.7515C22.7552 10.0985 21.8834 9.73888 20.9532 9.73888C20.1012 9.73888 19.299 10.0413 18.6683 10.5933C18.5167 10.3853 18.3511 10.1875 18.1681 10.0046C17.2346 9.0694 15.9879 8.554 14.6584 8.554C14.19 8.554 13.7327 8.61909 13.2952 8.74269L8.49591 3.94139C7.88905 3.33453 7.08032 3 6.21844 3C5.35657 3 4.54784 3.33412 3.94098 3.94098C3.33412 4.54784 3 5.35657 3 6.21844C3 7.08032 3.33412 7.88905 3.94139 8.49591L8.7431 13.2952C8.61951 13.7327 8.554 14.1904 8.554 14.6588C8.554 15.9883 9.06898 17.2354 10.0067 18.1731C10.1875 18.3527 10.3832 18.5163 10.5884 18.6658C9.32648 20.0266 9.35655 22.1599 10.6799 23.4832L14.5418 27.3452C11.2595 29.0339 8.91243 30.5735 7.55534 31.9306C4.59439 34.8919 3.01978 38.2138 3.00206 41.5365C2.98558 44.6247 4.27675 47.5573 6.73631 50.0168C9.18063 52.4611 12.0913 53.7515 15.1582 53.7515H15.2167C18.5397 53.7338 21.8612 52.1591 24.8226 49.1978C26.171 47.8493 27.7819 45.5727 29.6185 42.4222L36.5766 49.3803C37.2539 50.0576 38.1438 50.3963 39.0332 50.3963C39.8778 50.3963 40.722 50.0901 41.3849 49.48C41.5212 49.6604 41.6695 49.8331 41.8323 49.9958C42.8004 50.964 44.0714 51.4476 45.3428 51.4476C45.8018 51.4476 46.2607 51.3838 46.7057 51.2577L51.507 56.059C52.1138 56.6659 52.9226 57 53.784 57H53.7869C54.648 56.9992 55.4555 56.6646 56.0574 56.0603C56.6646 55.4555 56.9992 54.6484 57 53.7869C57.0008 52.9242 56.6667 52.1147 56.059 51.507ZM45.8582 6.70047C46.3975 6.03841 47.2066 5.64784 48.1365 5.60046C49.26 5.54279 50.4033 5.98402 51.1992 6.77998C51.9964 7.57718 52.4377 8.72415 52.3796 9.84805C52.3318 10.7758 51.9412 11.5829 51.2796 12.1202L39.5387 21.6647L36.3154 18.4388L45.8582 6.70047ZM32.3656 19.0976C32.5815 18.8813 32.8703 18.7626 33.1793 18.7626C33.4871 18.7626 33.775 18.8813 33.9905 19.0976L38.454 23.5611C38.4573 23.5639 38.4597 23.5672 38.4626 23.5701C38.4861 23.594 38.5084 23.6183 38.5294 23.6439C38.5318 23.6463 38.5339 23.6492 38.536 23.6521C38.6995 23.8536 38.7889 24.1049 38.7889 24.3714C38.7893 24.6804 38.6703 24.9696 38.454 25.1859C38.3485 25.2914 38.2237 25.3734 38.0869 25.4298L32.1209 19.4638C32.1774 19.3283 32.2593 19.2035 32.3656 19.0976ZM16.6763 11.4939C16.8621 11.6797 17.0187 11.8849 17.1435 12.1062L12.1057 17.1439C11.8845 17.0195 11.6797 16.8646 11.4944 16.6805C10.9575 16.1432 10.6617 15.4255 10.6617 14.6588C10.6617 13.8925 10.9571 13.1757 11.4944 12.6397L12.6401 11.4939C13.1757 10.9571 13.8925 10.6621 14.6584 10.6621C15.4243 10.6621 16.1412 10.9571 16.6763 11.4939ZM9.79697 11.3683L5.43155 7.00534C5.22267 6.79688 5.10773 6.51714 5.10773 6.21844C5.10773 5.91975 5.22267 5.64001 5.43155 5.43155C5.64001 5.22267 5.91975 5.10773 6.21844 5.10773C6.51714 5.10773 6.79688 5.22267 7.00534 5.43114L11.3683 9.79655C11.3596 9.80438 11.3518 9.81262 11.3436 9.82004C11.2772 9.87978 11.2126 9.94075 11.1491 10.0042L10.005 11.1483C9.94075 11.2126 9.87895 11.2781 9.8188 11.3448C9.8118 11.353 9.80397 11.3605 9.79697 11.3683ZM8.22647 48.5258C7.35553 47.6561 7.69089 46.1347 8.12595 45.0116C8.92685 42.9451 10.7557 40.4476 13.4138 37.789C14.7124 36.4904 16.8465 34.7341 19.7605 32.5638L24.2919 37.0952C22.0647 39.9301 20.2738 42.0296 18.9645 43.3393C16.3064 45.9971 13.8089 47.8255 11.742 48.6259C10.6185 49.061 9.09741 49.3968 8.22647 48.5258ZM9.04591 33.4211C10.3301 32.137 12.7044 30.6217 16.1094 28.9128L18.2547 31.058C15.3831 33.2061 13.2548 34.9673 11.9236 36.2985C9.01872 39.2034 7.07991 41.8788 6.16077 44.2502C6.0417 44.558 5.94324 44.8521 5.86372 45.1335C4.23967 41.3828 5.31784 37.1492 9.04591 33.4211ZM23.332 47.7072C19.6031 51.4361 15.3687 52.5143 11.6175 50.8886C11.8997 50.8091 12.1943 50.711 12.5029 50.5915C14.8751 49.6728 17.5502 47.734 20.4547 44.8295C21.7907 43.4934 23.5849 41.3976 25.7928 38.5961L28.076 40.8798C26.2151 44.1233 24.6207 46.4189 23.332 47.7072ZM43.3228 48.5056C43.1593 48.3417 43.0184 48.1625 42.9014 47.9688L47.9688 42.9014C48.1625 43.0184 48.3417 43.1593 48.5056 43.3228C49.6192 44.4369 49.6192 46.2496 48.5056 47.3636L47.3636 48.5056C46.2492 49.6192 44.4369 49.6192 43.3228 48.5056ZM54.5672 54.5701C54.3604 54.7773 54.0827 54.8919 53.7853 54.8923H53.784C53.4857 54.8923 53.2064 54.7773 52.9975 54.5685L48.6329 50.2039C48.6387 50.1989 48.6441 50.1931 48.6498 50.1878C48.7191 50.126 48.7874 50.0621 48.8538 49.9958L49.9958 48.8538C50.0621 48.7874 50.126 48.7191 50.1878 48.6498C50.1931 48.6441 50.1985 48.6387 50.2039 48.6329L54.5685 52.9975C54.7773 53.2064 54.8923 53.4862 54.8923 53.7853C54.8919 54.0827 54.7777 54.3604 54.5672 54.5701Z" />
	</svg>
);

export const GrillingCategoryWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>grilling</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M23.51 13.8841L23.5104 13.8839C24.3051 13.3462 24.6289 13.0813 24.6289 12.617C24.6289 12.1523 24.3051 11.8874 23.5104 11.3498C22.6468 10.7656 21.4648 9.96589 21.4648 8.33565C21.4648 6.70606 22.6463 5.90638 23.5093 5.32224L23.5104 5.32155C24.3051 4.78391 24.6289 4.519 24.6289 4.05469C24.6289 3.47255 25.101 3 25.6836 3C26.2657 3 26.7383 3.47255 26.7383 4.05469C26.7383 5.68466 25.5563 6.48434 24.6928 7.06851L24.6924 7.06879C23.8976 7.60643 23.5742 7.87134 23.5742 8.33565C23.5742 8.79996 23.8976 9.06487 24.6924 9.60252L24.6928 9.6028C25.5563 10.187 26.7383 10.9866 26.7383 12.6166C26.7383 14.2469 25.5559 15.0469 24.6924 15.6311C23.8976 16.1688 23.5742 16.4337 23.5742 16.898C23.5742 17.4805 23.1021 17.9527 22.5195 17.9527C21.937 17.9527 21.4648 17.4805 21.4648 16.898C21.4648 15.2684 22.6469 14.4683 23.51 13.8841ZM30.7825 13.8846L30.7836 13.8839C31.5787 13.3462 31.9021 13.0813 31.9021 12.6166C31.9021 12.1523 31.5783 11.8874 30.7836 11.3493C29.92 10.7656 28.7376 9.96548 28.7376 8.33565C28.7376 6.70568 29.9197 5.90601 30.7831 5.32183L30.7836 5.32155C31.5783 4.78391 31.9021 4.51859 31.9021 4.05469C31.9021 3.47214 32.3742 3 32.9568 3C33.5389 3 34.0115 3.47214 34.0115 4.05469C34.0115 5.68451 32.8291 6.48459 31.9655 7.06879C31.1708 7.60643 30.847 7.87134 30.847 8.33565C30.847 8.79996 31.1708 9.06487 31.9655 9.60252L31.966 9.6028C32.8295 10.187 34.0115 10.9866 34.0115 12.6166C34.0115 14.2469 32.8291 15.0469 31.9655 15.6311C31.1708 16.1688 30.8474 16.4337 30.8474 16.898C30.8474 17.4805 30.3753 17.9527 29.7927 17.9527C29.2102 17.9527 28.738 17.4805 28.738 16.898C28.738 15.2684 29.9195 14.4687 30.7825 13.8846ZM38.0572 13.8839L38.0569 13.8841C37.1938 14.4683 36.0117 15.2684 36.0117 16.898C36.0117 17.4805 36.4839 17.9527 37.0664 17.9527C37.649 17.9527 38.1211 17.4805 38.1211 16.898C38.1211 16.4337 38.4449 16.1688 39.2396 15.6311L39.24 15.6309C40.1031 15.0467 41.2852 14.2466 41.2852 12.6166C41.2852 10.987 40.1037 10.1873 39.2407 9.60321L39.2396 9.60252C38.4449 9.06487 38.1211 8.79996 38.1211 8.33565C38.1211 7.87134 38.4449 7.60643 39.2396 7.06879L39.2407 7.0681C40.1037 6.48396 41.2852 5.68428 41.2852 4.05469C41.2852 3.47255 40.8126 3 40.2305 3C39.6479 3 39.1758 3.47255 39.1758 4.05469C39.1758 4.519 38.852 4.78391 38.0572 5.32155L38.0562 5.32224C37.1932 5.90638 36.0117 6.70606 36.0117 8.33565C36.0117 9.96524 37.1932 10.7649 38.0562 11.3491L38.0572 11.3498C38.852 11.8874 39.1758 12.1523 39.1758 12.617C39.1758 13.0813 38.852 13.3462 38.0572 13.8839ZM49.9628 27.1992H53.8211C54.4036 27.1992 54.8757 27.6713 54.8757 28.2539C54.8757 28.836 54.4032 29.3086 53.8211 29.3086H49.5014C48.0685 34.4757 44.5864 38.7991 39.9886 41.3452L42.5412 46.1354L42.545 46.1428L47.2066 54.8905H48.56C49.1425 54.8905 49.6147 55.3631 49.6147 55.9452C49.6147 56.5278 49.1425 56.9999 48.56 56.9999H44.0846C43.5024 56.9999 43.0299 56.5278 43.0299 55.9452C43.0299 55.3631 43.5024 54.8905 44.0846 54.8905H44.8162L40.9827 47.696H20.1444L16.3104 54.8905H16.7912C17.3737 54.8905 17.8459 55.3631 17.8459 55.9452C17.8459 56.5278 17.3737 56.9999 16.7912 56.9999H12.3158C11.7332 56.9999 11.2611 56.5278 11.2611 55.9452C11.2611 55.3631 11.7332 54.8905 12.3158 54.8905H13.9205L18.5813 46.1432L18.5858 46.135L21.0812 41.4515C16.3874 38.9207 12.8275 34.5482 11.3744 29.3082H7.05469C6.47214 29.3082 6 28.836 6 28.2535C6 27.6713 6.47214 27.1988 7.05469 27.1988H10.913C10.7918 26.449 10.7127 25.6851 10.6785 24.9094C9.46358 24.485 8.58975 23.3282 8.58975 21.9703C8.58975 20.2539 9.98598 18.8573 11.7027 18.8573H49.173C50.8898 18.8573 52.2864 20.2539 52.2864 21.9703C52.2864 23.3282 51.4122 24.485 50.1972 24.9094C50.1634 25.6851 50.0843 26.449 49.9628 27.1992ZM22.9912 42.3525L21.2683 45.5866H39.8588L38.0905 42.2681C35.7352 43.2601 33.1496 43.8089 30.4379 43.8089C27.8049 43.8089 25.2913 43.2906 22.9912 42.3525ZM12.7982 25.0836C13.3453 34.3385 21.0483 41.6995 30.4379 41.6995C39.8279 41.6995 47.5304 34.3385 48.0775 25.0836H12.7982ZM11.7027 22.9743H49.173C49.7267 22.9743 50.177 22.524 50.177 21.9703C50.177 21.417 49.7267 20.9667 49.173 20.9667H11.7027C11.1494 20.9667 10.6991 21.417 10.6991 21.9703C10.6991 22.524 11.1494 22.9743 11.7027 22.9743ZM30.4384 39.8435C35.9162 39.8435 40.9288 37.063 43.8478 32.4059C44.1572 31.9124 44.008 31.2614 43.514 30.952C43.0205 30.6426 42.3695 30.7922 42.0606 31.2857C39.5297 35.3232 35.1853 37.7342 30.4384 37.7342C29.8558 37.7342 29.3837 38.2063 29.3837 38.7888C29.3837 39.371 29.8558 39.8435 30.4384 39.8435ZM44.7163 29.041C44.2705 29.0694 43.8482 28.8037 43.6764 28.3921C43.4782 27.9158 43.6657 27.3489 44.1082 27.0844C44.4682 26.8694 44.933 26.8916 45.2724 27.138C45.6399 27.4041 45.7953 27.8989 45.6478 28.3278C45.5102 28.7283 45.139 29.0138 44.7163 29.041Z" />
	</svg>
);

export const SteamingCategoryWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>steaming</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M55.2833 19.9214H58.3035C59.4384 19.9214 60.116 20.6661 60.116 21.9136V26.0775C60.116 26.572 60.116 27.4927 58.972 28.157L54.2848 30.8659V37.7234C54.2848 37.7283 54.2848 37.7332 54.2847 37.738V47.1727C54.2847 50.0308 52.1814 52.4064 49.4421 52.8314V54.0691C49.4421 55.6852 48.1273 57 46.5112 57H43.7554C42.1393 57 40.8245 55.6852 40.8245 54.0691V52.8994H19.2911V54.0691C19.2911 55.6852 17.9749 57 16.3571 57H13.6046C11.9886 57 10.6738 55.6852 10.6738 54.0691V52.8316C7.93273 52.4066 5.82784 50.031 5.82784 47.1727V30.8659L1.14232 28.1579C0 27.4913 0 26.5716 0 26.0775V21.9136C0 20.6661 0.677596 19.9214 1.81264 19.9215H4.83264V19.7969C4.83264 18.9245 5.38883 18.1816 6.16447 17.8985C7.20769 13.7157 10.6734 11.3309 15.7713 11.3309H16.3252V8.04434C16.3252 5.2628 18.5896 3 21.3728 3H38.7432C41.5245 3 43.7874 5.2628 43.7874 8.04434V11.3309H44.3447C49.44 11.3309 52.9043 13.7151 53.9479 17.8971C54.7254 18.1792 55.2833 18.9231 55.2833 19.7968V19.9214ZM52.5236 36.366V23.8294H32.01C31.5236 23.8294 31.1294 23.4352 31.1294 22.9488C31.1294 22.4623 31.5236 22.0682 32.01 22.0682H53.2611C53.4026 22.0682 53.5221 21.9485 53.5221 21.807V19.7968C53.5221 19.6553 53.4026 19.5357 53.2611 19.5357H53.2145H6.89819H6.85487C6.71338 19.5357 6.59385 19.6553 6.59385 19.7968V21.807C6.59385 21.9511 6.71092 22.0682 6.85487 22.0682H27.9429C28.4293 22.0682 28.8235 22.4623 28.8235 22.9488C28.8235 23.4352 28.4293 23.8294 27.9429 23.8294H7.58906V47.1727C7.58906 49.3593 9.36788 51.1381 11.5544 51.1381H18.4106H22.4492V36.3587C22.4492 32.267 25.778 28.9379 29.8699 28.9379H30.246C34.336 28.9379 37.6634 32.2669 37.6634 36.3587V51.1381H48.5615C50.7461 51.1381 52.5235 49.3593 52.5235 47.1727V36.3806C52.5235 36.3757 52.5236 36.3709 52.5236 36.366ZM47.681 52.8993H42.5858V54.069C42.5858 54.7139 43.1106 55.2387 43.7555 55.2387H46.5113C47.1563 55.2387 47.681 54.7139 47.681 54.069V52.8993ZM29.8699 30.6991C26.7493 30.6991 24.2104 33.238 24.2104 36.3587V51.1381H35.9022V36.3587C35.9022 33.238 33.3648 30.6991 30.246 30.6991H29.8699ZM17.53 52.8993H12.435L12.4351 54.069C12.4351 54.7139 12.9598 55.2387 13.6048 55.2387H16.3572C17.0039 55.2387 17.53 54.7139 17.53 54.069V52.8993ZM1.76121 26.0775C1.76121 26.4648 1.76121 26.48 2.0268 26.635L5.82784 28.8318V23.5462C5.23326 23.1936 4.83264 22.5471 4.83264 21.8071V21.6826H1.90305C1.82414 21.6819 1.76121 21.8048 1.76121 21.9136V26.0775ZM21.3728 4.76121C19.5607 4.76121 18.0865 6.23405 18.0865 8.04434V11.3309H20.5621V9.20592C20.5621 7.98622 21.5544 6.99396 22.7741 6.99396H37.3385C38.5582 6.99396 39.5504 7.98622 39.5504 9.20592V11.3309H42.0261V8.04434C42.0261 6.23405 40.5534 4.76121 38.7431 4.76121H21.3728ZM37.7893 11.3309V9.20592C37.7893 8.96158 37.5829 8.75528 37.3386 8.75517H22.7741C22.5298 8.75517 22.3233 8.96158 22.3233 9.20592V11.3309H37.7893ZM15.7713 13.0921C13.3745 13.0921 9.35508 13.7294 8.02901 17.7746H52.0836C50.7578 13.7294 46.7402 13.0921 44.3446 13.0921H15.7713ZM58.0891 26.6331C58.3548 26.4788 58.3548 26.4638 58.3548 26.0775V21.9135C58.3548 21.6983 58.2112 21.6824 58.1988 21.6824H55.2833V21.807C55.2833 22.5483 54.8812 23.1959 54.2848 23.548V28.8317L58.0891 26.6331ZM30.0572 36.6106C29.9899 36.6106 29.9216 36.6028 29.8532 36.5865C29.3801 36.4743 29.0874 35.9997 29.1998 35.5264L29.2132 35.4699C29.3254 34.9967 29.8001 34.7041 30.2732 34.8165C30.7464 34.9287 31.039 35.4033 30.9266 35.8765L30.9133 35.933C30.8173 36.338 30.4559 36.6106 30.0572 36.6106ZM29.2401 39.7353C29.3771 39.2686 29.8665 39.0016 30.3331 39.1384C30.7997 39.2753 31.0669 39.7646 30.9299 40.2314L30.9132 40.288C30.8005 40.6719 30.4494 40.9208 30.0687 40.9208C29.9866 40.9208 29.9031 40.9092 29.8203 40.8849C29.3536 40.748 29.0864 40.2586 29.2234 39.7919L29.2401 39.7353ZM32.6518 43.5263H27.4598C26.7411 43.5263 26.1564 44.1109 26.1564 44.8297V47.4291C26.1564 48.1497 26.741 48.7358 27.4598 48.7358H32.6518C33.3705 48.7358 33.9553 48.1497 33.9553 47.4291V44.8297C33.9551 44.111 33.3705 43.5263 32.6518 43.5263ZM32.1939 46.9745H27.9176V45.2875H32.1939V46.9745Z" />
	</svg>
);

export const PizzaCategoryWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 60 60"} width={size} height={size}>
		<title>pizza</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M48.9005 3.0029C49.7722 2.96623 50.5991 3.27811 51.229 3.88209C51.8594 4.48606 52.2063 5.29933 52.2067 6.17234V55.9497C52.2067 56.7786 51.2459 57.2903 50.5579 56.8215L45.6004 53.4432V53.8939C45.6004 55.5909 44.2194 56.9719 42.5225 56.9719C40.8251 56.9719 39.4445 55.5909 39.4445 53.8939V49.2487L39.3267 49.1684L32.1028 44.2459L32.1024 44.2457L32.102 44.2455L31.2286 43.6502V45.7114C31.2286 47.4083 29.8476 48.7889 28.1506 48.7889C26.4532 48.7889 25.0726 47.4083 25.0726 45.7114V39.4553L22.3115 37.5742V49.6759C22.3115 51.3733 20.9305 52.7539 19.2335 52.7539C17.5365 52.7539 16.1555 51.3733 16.1555 49.6759V44.0177C16.1555 43.4351 16.6277 42.963 17.2102 42.963C17.7928 42.963 18.2653 43.4351 18.2653 44.0177V49.6759C18.2653 50.2099 18.6996 50.6445 19.2335 50.6445C19.7675 50.6445 20.2021 50.2103 20.2021 49.6759V35.5789C20.2021 35.1883 20.418 34.8291 20.7636 34.6466C21.1093 34.4636 21.5279 34.4875 21.8505 34.7075L26.7214 38.0261C27.0094 38.2226 27.182 38.5489 27.182 38.8979V45.7114C27.182 46.2449 27.6167 46.6795 28.1506 46.6795C28.6846 46.6795 29.1188 46.2453 29.1188 45.7114V41.6549C29.1188 41.2644 29.3351 40.9055 29.6807 40.7226C30.0264 40.5401 30.4446 40.5636 30.7676 40.7836L32.1951 41.7563C32.8032 40.6216 33.7825 39.7037 34.9835 39.1871C37.888 37.9375 41.2675 39.2839 42.5167 42.188C43.4181 44.283 42.9711 46.6857 41.4859 48.3184C41.53 48.4358 41.5539 48.5619 41.5539 48.6913V53.8939C41.5539 54.4278 41.9885 54.8625 42.5225 54.8625C43.0564 54.8625 43.491 54.4282 43.491 53.8939V51.4487C43.491 51.0578 43.7073 50.6989 44.0526 50.516C44.3982 50.3331 44.8164 50.357 45.1398 50.577L50.0969 53.9549V49.6969C49.1728 49.3805 48.3352 48.8272 47.6839 48.0988C46.3482 46.6054 45.8847 44.4643 46.4903 42.5531C47.0374 40.8268 48.3842 39.4438 50.0969 38.8563V12.3406C48.6607 12.3752 47.2261 12.4803 45.8002 12.6566C45.9004 12.835 45.9922 13.018 46.073 13.2062C46.6782 14.6132 46.6996 16.1713 46.1327 17.5943C45.5658 19.0169 44.479 20.1338 43.072 20.7395C42.3445 21.0522 41.5761 21.2091 40.8069 21.2091C40.0892 21.2091 39.3707 21.0728 38.6839 20.7988C37.2609 20.2323 36.144 19.1451 35.5388 17.7381C35.2158 16.9871 35.0609 16.1932 35.0712 15.3988C34.8796 15.4751 34.6885 15.5525 34.4981 15.6308C32.9787 16.257 31.4898 16.9681 30.0544 17.7682C28.6215 18.5662 27.2306 19.4347 25.8933 20.3843C24.6063 21.2985 23.3736 22.2848 22.19 23.3288C20.9602 24.4136 19.7951 25.5762 18.7012 26.7969C17.6371 27.9843 16.6359 29.2252 15.7057 30.5201L17.8043 31.9501C18.0923 32.1466 18.2649 32.4729 18.2649 32.8219V35.5797C18.2649 36.1623 17.7928 36.6344 17.2102 36.6344C16.6277 36.6344 16.1555 36.1623 16.1555 35.5797V33.3793L13.6342 31.6613L13.6338 31.6609L9.38697 28.7671C8.66517 28.2752 8.1885 27.5295 8.0443 26.6676C7.90052 25.8061 8.10939 24.9463 8.63221 24.2471C13.3141 17.9837 19.4465 12.7432 26.3663 9.0921C33.2873 5.44023 41.0797 3.33496 48.9005 3.0029ZM35.8169 41.1247C34.9732 41.4876 34.3181 42.1357 33.9498 42.9519L35.952 44.3164L39.8544 46.9753C40.8518 45.9437 41.1633 44.3802 40.5791 43.0215C39.7889 41.1857 37.6532 40.3349 35.8169 41.1247ZM10.3218 25.5099C10.1471 25.7443 10.0771 26.0319 10.1253 26.3207C10.1735 26.6095 10.3333 26.8591 10.5747 27.0235L13.9605 29.3307C14.0919 29.1477 14.225 28.9656 14.3593 28.7844C15.4321 27.3354 16.5857 25.9416 17.8249 24.6311C19.0873 23.2967 20.4196 22.0282 21.8319 20.8528C23.2319 19.6881 24.6965 18.6062 26.2213 17.61C27.7716 16.5973 29.3895 15.6749 31.0564 14.8682C32.699 14.0726 34.3857 13.3504 36.1098 12.7502C38.5468 11.9015 41.0755 11.2571 43.6258 10.8348C44.1 10.7565 44.5775 10.6894 45.0541 10.6251C46.7256 10.401 48.4102 10.2675 50.0965 10.2292V6.17192C50.0965 5.87982 49.9803 5.6075 49.7693 5.4048C49.5695 5.21322 49.3104 5.1094 49.036 5.1094C49.0208 5.1094 49.0051 5.10981 48.9895 5.11064C41.4793 5.42911 33.9972 7.45074 27.3505 10.9576C20.706 14.4636 14.8174 19.4957 10.3218 25.5099ZM42.2382 18.8015C43.1277 18.4187 43.8145 17.7126 44.1729 16.8132C44.5309 15.9138 44.5177 14.9292 44.135 14.0397C43.9698 13.6549 43.7452 13.3101 43.4651 13.0093C43.0164 13.0896 42.5694 13.1782 42.1228 13.2709C40.4885 13.6104 38.8735 14.0566 37.291 14.5872C37.0957 15.356 37.1579 16.1635 37.4764 16.9042C37.8591 17.7937 38.5653 18.4809 39.4647 18.8389C40.364 19.1974 41.3487 19.1838 42.2382 18.8015ZM48.5919 42.9358C48.2339 43.8352 48.247 44.8202 48.6298 45.7093C48.9359 46.4208 49.4525 47.0017 50.0969 47.3869V41.1671C49.4163 41.5729 48.8902 42.1872 48.5919 42.9358ZM27.5466 34.5896C26.829 34.5896 26.1104 34.4533 25.4237 34.1797C24.0006 33.6128 22.8837 32.526 22.2785 31.119C21.6733 29.7121 21.6519 28.1535 22.2188 26.7309C22.7857 25.3079 23.8725 24.191 25.2795 23.5858C26.6864 22.9802 28.245 22.9592 29.6676 23.5261C31.0906 24.0926 32.2075 25.1798 32.8127 26.5867C34.0623 29.4909 32.7159 32.8704 29.8118 34.12C29.0842 34.4331 28.3158 34.5896 27.5466 34.5896ZM26.1133 25.5234C25.2238 25.9061 24.5366 26.6123 24.1786 27.5117C23.8202 28.4106 23.8338 29.3957 24.2165 30.2852C24.5989 31.1742 25.305 31.8614 26.2044 32.2199C27.1038 32.5779 28.0888 32.5647 28.9779 32.182C30.8137 31.3922 31.6649 29.256 30.8751 27.4202C30.4924 26.5311 29.7862 25.8439 28.8868 25.4859C28.4526 25.3129 27.9986 25.2264 27.545 25.2264C27.0588 25.2264 26.5731 25.3256 26.1133 25.5234ZM41.0455 36.3403C41.7323 36.6143 42.4508 36.7507 43.1684 36.7507C43.9376 36.7507 44.706 36.5937 45.4336 36.2806C46.8405 35.6754 47.9273 34.5585 48.4942 33.1359C49.0611 31.7129 49.0401 30.1547 48.4345 28.7474C47.8293 27.3408 46.7124 26.2536 45.2898 25.6867C43.8668 25.1198 42.3082 25.1412 40.9017 25.7464C39.4947 26.3517 38.4075 27.4686 37.8406 28.8916C37.2741 30.3146 37.2951 31.8727 37.9003 33.2797C38.5056 34.6866 39.6225 35.7739 41.0455 36.3403ZM39.8004 29.6723C40.1585 28.7729 40.8457 28.0672 41.7351 27.6844C42.1949 27.4867 42.6807 27.3874 43.1668 27.3874C43.6204 27.3874 44.0744 27.4735 44.5087 27.6465C45.408 28.005 46.1142 28.6922 46.4969 29.5812C46.8792 30.4707 46.8928 31.4558 46.5344 32.3552C46.176 33.2541 45.4892 33.9603 44.5997 34.343C43.7106 34.7253 42.7256 34.7389 41.8262 34.3805C40.9268 34.0225 40.2207 33.3353 39.8379 32.4462C39.4556 31.5567 39.4424 30.5717 39.8004 29.6723ZM18.2063 40.1403C18.0617 40.5609 17.6555 40.8538 17.2093 40.8538C16.7149 40.8538 16.2778 40.4962 16.1768 40.0129C16.0866 39.5791 16.2856 39.1206 16.6671 38.8944C17.0358 38.6756 17.5096 38.7016 17.8512 38.9611C18.2071 39.2318 18.3513 39.7188 18.2063 40.1403Z" />
	</svg>
);


//// Desinged for 100px
export const BHCSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 100 100"} width={size} height={size}>
		<title>BHC</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M58 0.000900269C58.758 0.000900269 59.45 0.427712 59.789 1.10541C60.128 1.78311 60.055 2.59276 59.6 3.19949L59 3.99914H73C75.311 3.99914 76.787 6.8049 76.979 8.6141H77.767C83.031 8.6141 87 12.2155 87 16.9934V61.5658C87.516 62.7353 88.077 63.5879 88.578 64.3475C89.279 65.4101 89.941 66.4146 89.943 67.6971L90 77.9556C90 80.1666 88.002 81.409 85.721 81.8149C85.513 82.5075 85.151 83.2652 84.524 83.862C84.351 84.0249 84.167 84.1698 83.973 84.2938C83.99 84.5846 84 84.8335 84 84.9635C84 86.6617 83.263 88.317 82.576 89.4895L82.616 89.5564C83.359 90.8609 82.996 92.2632 82.916 92.5321C82.89 92.6231 82.856 92.711 82.817 92.797L80.614 97.5729C80.286 98.4735 79.913 99.8109 78 99.9569C77.8768 99.9665 77.1479 99.9713 76.1346 99.9731L75.6057 99.9738C75.5141 99.9739 75.4209 99.9739 75.3263 99.974L74.743 99.974C74.6434 99.9739 74.5428 99.9739 74.4413 99.9738L73.51 99.9729C70.9911 99.9696 68.2977 99.9595 67.999 99.9569C67.417 99.9499 66.366 99.69 65.991 99.2442L64.069 96.9582H36.879L34.721 99.3091C34.347 99.716 33.822 99.9509 33.27 99.9569C32.388 99.9649 24.264 100.052 23 99.9569C21.052 99.8079 20.447 98.4245 20.138 97.5229L18.153 92.724C18.019 92.4172 17.602 91.2947 17.944 90.1112C17.059 88.9277 16 87.0725 16 84.9635C16 84.8295 16.006 84.6236 16.035 84.1768C15.195 83.6091 14.624 82.7534 14.307 81.8188C12.014 81.419 10 80.1766 10 77.9665V67.971C10 66.7575 10.62 65.7889 11.338 64.6674C11.879 63.8218 12.487 62.8722 13 61.5998V16.9934C13 11.9346 17.55 8.83901 22.116 8.6251C22.591 5.87631 24.5 3.99914 27 3.99914H42L41.401 3.19949C40.946 2.59276 40.872 1.78311 41.211 1.10541C41.55 0.427712 42.242 0.000900269 43 0.000900269H58ZM22.258 90.9038C22.169 91.0768 22.056 91.2357 21.922 91.3756L23.825 95.9726C24.5851 96.0044 26.8118 96.0046 29.1314 95.9918L29.7955 95.9878C30.6811 95.982 31.5613 95.9746 32.36 95.9666L34.527 93.6067C34.906 93.1948 35.44 92.9599 36 92.9599H65C65.591 92.9599 66.152 93.2198 66.532 93.6716L68.461 95.9676C69.1619 95.9746 69.9235 95.9811 70.6951 95.9863L71.6888 95.9923C74.0017 96.0046 76.21 96.0038 76.947 95.9726L78.954 91.6215C78.658 91.4596 78.412 91.2207 78.23 90.9368C78.151 90.9398 78.08 90.9608 78 90.9608H23C22.749 90.9608 22.502 90.9368 22.258 90.9038ZM75.184 55.9702H24.942C23.136 55.9702 21.101 60.4313 21.101 62.8082L21.096 62.9112C20.671 67.0304 19.754 76.0934 19.287 81.2611C19.84 81.6669 20.195 82.3226 20.136 83.0303C20.0226 84.3887 20.0032 84.8087 20.0004 84.9324L20 84.9635C20 86.4628 21.467 88.1711 21.861 88.5459C21.95 88.6308 22.022 88.7308 22.093 88.8308C22.389 88.9087 22.691 88.9617 23 88.9617H78L78.138 88.9437C78.201 88.8208 78.271 88.6998 78.361 88.5909C78.784 88.0731 80 86.2869 80 84.9635C79.997 84.6156 79.883 82.9973 79.883 82.9933C79.844 82.4236 80.05 81.8628 80.451 81.455C80.536 81.368 80.63 81.2921 80.728 81.2231C80.552 79.1617 80.3102 76.4894 80.0577 73.76L79.9193 72.2689C79.6417 69.2886 79.3634 66.3602 79.156 64.2026L79.074 64.2136V63.35C79.059 63.1971 79.044 63.0471 79.031 62.9052C79.026 60.4313 76.991 55.9702 75.184 55.9702ZM50.0003 74.9674C53.3093 74.9674 56.0003 77.6572 56.0003 80.9647C56.0003 84.2723 53.3093 86.9621 50.0003 86.9621C46.6913 86.9621 44.0003 84.2723 44.0003 80.9647C44.0003 77.6572 46.6913 74.9674 50.0003 74.9674ZM37.5101 77.966C39.4281 77.966 40.9891 79.5354 40.9891 81.4645C40.9891 83.3937 39.4281 84.963 37.5101 84.963C35.5911 84.963 34.0311 83.3937 34.0311 81.4645C34.0311 79.5354 35.5911 77.966 37.5101 77.966ZM62.5003 77.966C64.4303 77.966 66.0003 79.5354 66.0003 81.4645C66.0003 83.3937 64.4303 84.963 62.5003 84.963C60.5703 84.963 59.0003 83.3937 59.0003 81.4645C59.0003 79.5354 60.5703 77.966 62.5003 77.966ZM50.0003 76.9665C47.7943 76.9665 46.0003 78.7597 46.0003 80.9647C46.0003 83.1698 47.7943 84.963 50.0003 84.963C52.2063 84.963 54.0003 83.1698 54.0003 80.9647C54.0003 78.7597 52.2063 76.9665 50.0003 76.9665ZM37.5101 79.9652C36.6951 79.9652 36.0311 80.6379 36.0311 81.4645C36.0311 82.2911 36.6951 82.9638 37.5101 82.9638C38.3251 82.9638 38.9891 82.2911 38.9891 81.4645C38.9891 80.6379 38.3251 79.9652 37.5101 79.9652ZM62.5003 79.9652C61.6733 79.9652 61.0003 80.6379 61.0003 81.4645C61.0003 82.2911 61.6733 82.9638 62.5003 82.9638C63.3273 82.9638 64.0003 82.2911 64.0003 81.4645C64.0003 80.6379 63.3273 79.9652 62.5003 79.9652ZM20.981 12.8063C18.832 13.312 17 14.7974 17 16.9934V61.9736C17 62.2125 16.957 62.4484 16.874 62.6733C16.187 64.5105 15.332 65.8469 14.707 66.8225C14.411 67.2843 14.044 67.859 13.992 68.0669L14 77.9665C14.394 78.1555 15.225 77.9665 16 77.9665C16.611 77.9665 17.151 78.2464 17.518 78.6772C17.6793 76.9642 17.8634 75.0679 18.0478 73.1976L18.1215 72.4514C18.551 68.1122 18.9656 64.0664 19.078 62.9732H19V24.9899C19 23.8174 20.547 21.3255 23.215 19.9921H20V13.459L20.981 12.8063ZM77.767 12.6123H77.729L79 13.459V19.9921H76.979C79.625 21.3485 81 23.9014 81 24.9899V62.2915C81.015 62.4694 81.026 62.6443 81.026 62.8082C81.0602 63.1463 81.4754 67.426 81.9161 72.1461L81.9808 72.8395C81.9916 72.9555 82.0024 73.0716 82.0131 73.1879L82.0779 73.8862C82.0886 74.0027 82.0994 74.1191 82.1102 74.2356L82.1744 74.9333C82.2917 76.21 82.4059 77.4692 82.51 78.6433C82.876 78.2314 83.405 77.9665 84 77.9665C84.775 77.9665 85.606 78.1555 86 77.9665V67.971C85.942 67.759 85.527 66.9844 85.238 66.5466C84.653 65.659 83.853 64.4445 83.15 62.7313C83.051 62.4904 83 62.2325 83 61.9736V16.9934C83 14.1177 80.367 12.6123 77.767 12.6123ZM65.0003 57.9745V72.9678H35.0003V57.9745H65.0003ZM63.0003 59.9736H37.0003V70.9687H63.0003V59.9736ZM75.577 21.5544L75.5778 21.8715C75.5666 25.6925 74.7873 33.7706 70.211 47.8548C69.677 49.5011 69.247 51.5712 68.917 53.9771H75C76.602 53.9771 78.016 55.2286 79.074 56.8738V24.9529C79.019 24.4281 77.821 22.448 75.577 21.5544ZM24.931 21.4325C22.433 22.2671 21.057 24.4441 20.999 25.0059L21 56.8489C22.028 55.2156 23.408 53.9771 25 53.9771H31.589C31.259 51.5712 30.829 49.5021 30.294 47.8548C25.537 33.2143 24.884 25.0629 24.931 21.4325ZM46.027 19.9921H27C26.829 21.8133 26.545 29.8467 32.197 47.2381C32.787 49.0583 33.257 51.3363 33.607 53.9771H66.899C67.249 51.3353 67.719 49.0583 68.309 47.2381C73.914 29.9867 73.681 21.9442 73.498 19.9921H54.974C54.981 20.0801 55 20.165 55 20.255V21.7273C55 21.7823 54.987 21.8323 54.985 21.8873H55V51.0594C55 52.1349 53.955 52.9776 52.621 52.9776H48.379C47.045 52.9776 46 52.1349 46 51.0594V20.255C46 20.165 46.02 20.0801 46.027 19.9921ZM53 24.735C52.612 24.8989 52.185 24.9899 51.737 24.9899H49.264C48.816 24.9899 48.389 24.8989 48 24.735V50.8995C48.075 50.9375 48.207 50.9784 48.379 50.9784H52.621C52.793 50.9784 52.925 50.9375 53 50.8995V24.735ZM51.737 18.9925H49.264C49.17 18.9925 49.088 19.0265 49 19.0455V19.9921H48.054C48.034 20.0791 48 20.161 48 20.255V21.7273C48 22.424 48.567 22.9908 49.264 22.9908H51.737C52.434 22.9908 53 22.424 53 21.7273V20.255C53 20.161 52.966 20.0791 52.948 19.9921H52V19.0455C51.912 19.0265 51.83 18.9925 51.737 18.9925ZM74.698 12.9952H24.303L22 14.5295V17.993H46.922C47.516 17.3782 48.344 16.9934 49.264 16.9934H51.737C52.657 16.9934 53.485 17.3782 54.078 17.993H77V14.5295L74.698 12.9952ZM72.575 7.99738H27C26.311 7.99738 26 9.16586 26 9.9965V10.6132C26 10.7442 25.986 10.8711 25.961 10.9961H73.039C73.015 10.8711 73 10.7442 73 10.6132V8.99694C72.985 8.76204 72.769 8.27226 72.575 7.99738ZM54 3.99914H47L47.6 4.79779C47.865 5.15163 47.984 5.57344 47.984 5.99826H53.017C53.017 5.57344 53.136 5.15163 53.401 4.79779L54 3.99914Z" />
	</svg>
);

export const BMCSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 100 100"} width={size} height={size}>
		<title>BMC</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M26.2049 0H73.7949C81.0579 0 86.9999 5.913 86.9999 13.14V34.814V79.207C86.9999 89.543 78.5009 98 68.1129 98H57.0003V98.67C57.0003 99.4017 56.2823 100 55.4043 100H44.5953C43.7173 100 43.0003 99.4017 43.0003 98.67V98H40.9997V98.261C40.9997 98.667 40.6677 99 40.2607 99H32.7397C32.3317 99 31.9997 98.667 31.9997 98.261V98H31.8869C21.4989 98 12.9999 89.543 12.9999 79.207V34.814V13.14C12.9999 5.913 18.9419 0 26.2049 0ZM44.5953 96H55.4043H68.1129C77.4249 96 84.9999 88.467 84.9999 79.207V34.814V13.14C84.9999 6.998 79.9739 2 73.7949 2H26.2049C20.0259 2 14.9999 6.998 14.9999 13.14V34.814V79.207C14.9999 88.467 22.5749 96 31.8869 96H44.5953ZM26.2049 4H27.7869H72.2129H73.7949C78.8709 4 82.9999 8.1 82.9999 13.14V14.109V58.891V79.207C82.9999 87.364 76.3219 94 68.1129 94H31.8869C23.6779 94 16.9999 87.364 16.9999 79.207V58.891V14.109V13.14C16.9999 8.1 21.1289 4 26.2049 4ZM81.9999 14.109V58.891C81.9999 63.914 77.6089 68 72.2129 68H27.7869C22.3909 68 17.9999 63.914 17.9999 58.891V14.109C17.9999 9.086 22.3909 5 27.7869 5H72.2129C77.6089 5 81.9999 9.086 81.9999 14.109ZM27.7869 69C23.4671 69 19.7185 66.5877 17.9999 63.1292V79.207C17.9999 86.813 24.2299 93 31.8869 93H68.1129C75.7709 93 81.9999 86.813 81.9999 79.207V63.1296C80.2815 66.5878 76.5333 69 72.2129 69H27.7869ZM63.8408 70H36.1588C33.8708 70 31.9998 71.872 31.9998 74.159V85.841C31.9998 88.128 33.8708 90 36.1588 90H63.8408C66.1288 90 67.9998 88.128 67.9998 85.841V74.159C67.9998 71.872 66.1288 70 63.8408 70ZM63.8408 71C65.5828 71 66.9998 72.417 66.9998 74.159V85.841C66.9998 87.583 65.5828 89 63.8408 89H36.1588C34.4168 89 32.9998 87.583 32.9998 85.841V74.159C32.9998 72.417 34.4168 71 36.1588 71H63.8408ZM63 87H37V73H63V87ZM62 86H38V74H62V86ZM55.0002 34.9025C55.0002 32.6205 53.1492 30.7695 50.8662 30.7695C48.5842 30.7695 46.7332 32.6205 46.7332 34.9025C46.7332 37.1855 48.5842 39.0365 50.8662 39.0365C53.1492 39.0365 55.0002 37.1855 55.0002 34.9025ZM49.7692 34.9027C49.7692 34.2967 50.2612 33.8057 50.8662 33.8057C51.4722 33.8057 51.9642 34.2967 51.9642 34.9027C51.9642 35.5087 51.4722 35.9997 50.8662 35.9997C50.2612 35.9997 49.7692 35.5087 49.7692 34.9027Z" />
	</svg>
);

export const CsvSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 100 100"} width={size} height={size}>
		<title>csv</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M38.5013 20.9329L39.1013 8.94427C39.1287 7.71694 39.8873 6.67427 40.956 6.22961C41.312 6.08161 41.702 6.00027 42.1113 6.00027H57.5553C58.1693 6.00027 58.7407 6.18361 59.2173 6.50027C60.012 7.02694 60.5427 7.92094 60.5653 8.94427L61.1653 20.9329L61.7653 32.9223V32.9229C61.7693 33.1149 61.7507 33.3023 61.72 33.4849C61.7127 33.5293 61.7019 33.5723 61.6912 33.6155L61.6912 33.6155C61.6862 33.6353 61.6813 33.655 61.6767 33.6749C61.65 33.7876 61.6187 33.8983 61.58 34.0056C61.552 34.0829 61.5213 34.1583 61.4873 34.2316C61.45 34.3129 61.4073 34.3903 61.364 34.4669L61.3532 34.4859C61.3082 34.565 61.2631 34.6441 61.21 34.7176C61.1947 34.7395 61.1776 34.7599 61.1604 34.7804L61.1603 34.7805C61.1499 34.793 61.1394 34.8054 61.1293 34.8183C60.7647 35.2849 60.2673 35.6463 59.69 35.8363C59.394 35.9336 59.084 36.0003 58.7547 36.0003H40.912C40.71 36.0003 40.5147 35.9776 40.3247 35.9403C40.2654 35.9283 40.2087 35.9099 40.152 35.8915C40.1232 35.8821 40.0945 35.8728 40.0653 35.8643C40.0436 35.8579 40.0218 35.8517 40.0001 35.8455C39.9265 35.8247 39.8533 35.8039 39.7833 35.7756C39.7139 35.7471 39.6488 35.7125 39.5835 35.6777L39.5834 35.6777L39.5367 35.6529L39.5067 35.6375C39.4326 35.5995 39.3587 35.5615 39.2893 35.5163C39.2167 35.4704 39.1495 35.4184 39.082 35.3661L39.0819 35.3661L39.0727 35.3589C38.9967 35.2989 38.922 35.2383 38.8513 35.1723C38.7893 35.1136 38.7307 35.0509 38.6733 34.9876C38.606 34.9116 38.5427 34.8329 38.4833 34.7516C38.4347 34.6849 38.388 34.6183 38.3453 34.5483C38.2893 34.4556 38.24 34.3583 38.194 34.2589L38.1737 34.2168C38.1474 34.1624 38.1212 34.1082 38.1 34.0509C38.0567 33.9356 38.0253 33.8149 37.996 33.6936C37.9901 33.6693 37.9835 33.6455 37.9768 33.6216C37.9661 33.5832 37.9554 33.5448 37.948 33.5049C37.9153 33.3163 37.8973 33.1223 37.902 32.9229V32.9223L38.5013 20.9329ZM57.7533 40.3336C57.0873 40.7623 56.4373 41.1003 55.7907 41.3703C55.5553 41.3749 55.3193 41.4183 55.094 41.5076C54.5707 41.7156 54.0887 41.8869 53.6007 42.0276C52.4527 42.2563 51.274 42.3336 49.9953 42.3336C48.7413 42.3336 47.6887 42.2656 46.6687 42.0723C45.9607 41.8836 45.3233 41.6356 44.6767 41.3349C44.408 41.2103 44.1207 41.1483 43.8333 41.1483C43.3287 40.9183 42.8073 40.6576 42.2373 40.3336C41.8053 39.9356 41.2233 38.1689 41.078 37.3336H58.7547C58.7726 37.3336 58.7903 37.3324 58.8079 37.3312C58.8224 37.3302 58.8369 37.3292 58.8513 37.3289C58.7533 38.1876 58.316 39.8909 57.7533 40.3336ZM39.1167 37.6669C39.326 38.8529 39.974 40.9689 40.8833 41.8056L41.0507 41.9589L41.2487 42.0716C41.4376 42.1791 41.6171 42.2727 41.7965 42.3662L41.7966 42.3662L41.7976 42.3667C41.8558 42.3971 41.9141 42.4274 41.9727 42.4583C41.8907 42.6776 41.8333 42.9069 41.8333 43.1483V51.0176C42.4467 50.8776 43.1013 50.7416 43.8333 50.6203V43.3223C44.6593 43.6469 45.4447 43.8716 46.228 44.0243C47.326 44.3109 48.5393 44.4683 50.082 44.4683C51.7087 44.4683 52.9067 44.2963 54.0453 43.9789C54.6433 43.8583 55.2387 43.6983 55.8333 43.4903V50.5689C56.566 50.6823 57.2233 50.8103 57.8333 50.9443V43.3656C57.8333 43.1276 57.7773 42.8996 57.6973 42.6816C58.0733 42.4823 58.4527 42.2616 58.8367 42.0143L58.916 41.9636L58.9893 41.9049C60.2353 40.9256 60.7 38.6076 60.83 37.5356C62.6493 36.7129 63.8093 34.9043 63.7647 32.8776L63.764 32.8503L63.7627 32.8223L62.564 8.86827C62.4873 6.18094 60.2473 4.00027 57.5553 4.00027H42.1113C39.4193 4.00027 37.1793 6.18094 37.1027 8.86761L35.904 32.8223L35.9033 32.8496L35.9027 32.8776C35.8547 35.0269 37.1673 36.9229 39.1167 37.6669ZM43.4372 12.0853L43.4385 12.0519V12.0179C43.4385 11.7553 43.6239 11.5419 43.8512 11.5419H55.8152C56.0432 11.5419 56.2279 11.7553 56.2279 12.0179V12.0519L56.2292 12.0853L57.1659 30.6906C57.1559 30.9426 56.9752 31.1433 56.7545 31.1433H42.9119C42.6919 31.1433 42.5112 30.9426 42.5005 30.6906L43.4372 12.0853ZM42.9119 32.4766H56.7545C57.7185 32.4766 58.4999 31.6666 58.4999 30.6666L57.5612 12.0179C57.5612 11.0186 56.7799 10.2086 55.8152 10.2086H43.8512C42.8872 10.2086 42.1052 11.0186 42.1052 12.0179L41.1665 30.6666C41.1665 31.6666 41.9485 32.4766 42.9119 32.4766ZM88 48V89.3333C88 92.184 86.4847 94 83.8113 94H15.6887C12.876 94 10.6667 91.71 10.6667 89V48.6667C10.6667 44.6233 7.43466 41 3.66666 41C3.11399 41 2.66666 41.4473 2.66666 42C2.66666 42.5527 3.11399 43 3.66666 43C6.28666 43 8.66666 45.776 8.66666 48.6667V89.3333C8.66666 93.1273 11.792 96 15.6887 96H83.8113C87.6247 96 90 92.9907 90 89V48.3333C90 45.592 93.092 43 95.8333 43C96.386 43 96.8333 42.5527 96.8333 42C96.8333 41.4473 96.386 41 95.8333 41C91.9953 41 88 44.162 88 48ZM55.8331 75.4454C54.8305 76.5541 52.7311 77.4287 49.6471 77.4287C46.9031 77.4287 44.9398 76.7347 43.8331 75.8014V79.6894C43.8331 80.7947 44.0418 81.7581 45.8331 82.2741V80.5834C45.8331 80.2441 46.1091 79.9894 46.4231 79.9894C46.4925 79.9894 46.5631 80.0021 46.6331 80.0287C47.3978 80.3174 48.6091 80.6854 49.8138 80.6854C50.9451 80.6854 52.0965 80.3601 52.8751 80.0814C52.9551 80.0527 53.0358 80.0401 53.1145 80.0401C53.4958 80.0401 53.8331 80.3527 53.8331 80.7621V82.2741C55.6251 81.7581 55.8331 80.7947 55.8331 79.6894V75.4454ZM42.7307 60.0534L42.7858 60.0401C43.1438 59.9534 43.4885 59.8707 43.8331 59.7921V73.8814H44.1171C44.1171 74.6294 45.7865 76.0954 49.6471 76.0954C53.5071 76.0954 55.1771 74.6294 55.1771 73.8814H55.8331V59.7194C56.2617 59.812 56.6832 59.9134 57.1229 60.0192L57.2098 60.0401C57.4138 60.0901 57.6225 60.1401 57.8331 60.1901V79.6894C57.8331 80.5587 57.8331 83.2027 54.3871 84.1961C54.2051 84.2487 54.0191 84.2741 53.8331 84.2741C53.4045 84.2741 52.9811 84.1361 52.6311 83.8727C52.1771 83.5307 51.8925 83.0147 51.8418 82.4547C51.2078 82.5907 50.5145 82.6854 49.8138 82.6854C49.1258 82.6854 48.4485 82.5947 47.8245 82.4614C47.7718 83.0194 47.4878 83.5327 47.0358 83.8727C46.6851 84.1361 46.2625 84.2741 45.8331 84.2741C45.6478 84.2741 45.4618 84.2487 45.2791 84.1961C41.8331 83.2027 41.8331 80.5587 41.8331 79.6894V60.2667C42.14 60.196 42.438 60.124 42.7294 60.0537L42.7303 60.0535L42.7307 60.0534ZM25.0231 56.6529C22.9395 56.3173 20.9694 56 16.9189 56C16.3663 56 15.9189 55.5527 15.9189 55C15.9189 54.4473 16.3663 54 16.9189 54C21.1336 54 23.2763 54.3453 25.3476 54.6793L25.3522 54.6801C27.4358 55.0158 29.4066 55.3333 33.4576 55.3333C37.5093 55.3333 39.4801 55.0158 41.5637 54.6801L41.5683 54.6793L41.5687 54.6793C43.6406 54.3453 45.7826 54 49.9976 54C54.2129 54 56.3556 54.3453 58.4276 54.6793L58.4342 54.6804C60.5177 55.016 62.4879 55.3333 66.5389 55.3333C70.5907 55.3333 72.5608 55.016 74.6444 54.6804L74.6509 54.6793C76.7229 54.3453 78.8656 54 83.0809 54C83.6336 54 84.0809 54.4473 84.0809 55C84.0809 55.5527 83.6336 56 83.0809 56C79.0299 56 77.0597 56.3173 74.9762 56.6529L74.9696 56.654C72.8976 56.988 70.7549 57.3333 66.5389 57.3333C62.3236 57.3333 60.1809 56.988 58.1089 56.654L58.1024 56.6529C56.0188 56.3173 54.0487 56 49.9976 56C45.9459 56 43.9752 56.3175 41.8915 56.6533L41.8869 56.654L41.8865 56.6541C39.8146 56.988 37.6726 57.3333 33.4576 57.3333C29.2429 57.3333 27.1009 56.988 25.0296 56.654L25.0231 56.6529Z"/>
	</svg>
);

///////// Web ///////
// From Experience Hub style sheet
//All are WIP
//Sizes are an issue

export const Email2WebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>emailWeb</title>
		<path fill={primary}
			d="M20.2,2.2c1,0,1.9,0.4,2.7,1.1C23.6,4,24,5,24,6V18c0,1-0.4,2-1.1,2.7c-0.7,0.7-1.7,1.1-2.7,1.1H3.8
c-1,0-1.9-0.4-2.7-1.1C0.4,20,0,19,0,18V6c0-1,0.4-2,1.1-2.7c0.7-0.7,1.7-1.1,2.7-1.1H20.2z M22.5,8.2l-10.1,6
c-0.1,0.1-0.2,0.1-0.3,0.1c-0.1,0-0.2,0-0.3,0l-0.1-0.1l-10.1-6V18c0,0.6,0.2,1.2,0.7,1.6c0.4,0.4,1,0.7,1.6,0.7h16.5
c0.6,0,1.2-0.2,1.6-0.7c0.4-0.4,0.7-1,0.7-1.6V8.2z M20.2,3.7H3.8c-0.6,0-1.2,0.2-1.6,0.7C1.7,4.8,1.5,5.4,1.5,6v0.5L12,12.6
l10.5-6.2V6c0-0.6-0.2-1.2-0.7-1.6C21.4,3.9,20.8,3.7,20.2,3.7z"
		/>
	</svg>
);

export const ChatWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>chatWeb</title>
		<path fill={primary}
			d="M21.7,0H6.9C6.3,0,5.7,0.2,5.3,0.7c-0.4,0.4-0.7,1-0.7,1.6v4.3H2.3c-0.6,0-1.2,0.2-1.6,0.7C0.2,7.7,0,8.3,0,8.9
v14.5c0,0.1,0,0.2,0.1,0.3c0.1,0.1,0.1,0.2,0.2,0.2S0.5,24,0.6,24c0.1,0,0.2-0.1,0.3-0.1l4.9-3.9h11.3c0.6,0,1.2-0.2,1.6-0.7
c0.4-0.4,0.7-1,0.7-1.6v-3.3l3.6,2.9c0.1,0.1,0.2,0.1,0.4,0.1c0.1,0,0.2,0,0.2-0.1c0.1,0,0.2-0.1,0.2-0.2C24,17,24,16.9,24,16.8V2.3
c0-0.6-0.2-1.2-0.7-1.6C22.9,0.2,22.3,0,21.7,0z M18.3,17.6c0,0.3-0.1,0.6-0.3,0.8c-0.2,0.2-0.5,0.3-0.8,0.3H5.6
c-0.1,0-0.3,0-0.4,0.1l-4.1,3.3V8.9c0-0.3,0.1-0.6,0.3-0.8C1.7,7.9,2,7.7,2.3,7.7h14.8c0.3,0,0.6,0.1,0.8,0.3
c0.2,0.2,0.3,0.5,0.3,0.8V17.6z M22.9,15.6l-3.4-2.7v-4c0-0.6-0.2-1.2-0.7-1.6c-0.4-0.4-1-0.7-1.6-0.7H5.7V2.3
c0-0.3,0.1-0.6,0.3-0.8c0.2-0.2,0.5-0.3,0.8-0.3h14.8c0.3,0,0.6,0.1,0.8,0.3c0.2,0.2,0.3,0.5,0.3,0.8V15.6z M14.3,10.3H5.1
c-0.2,0-0.3,0.1-0.4,0.2c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.2,0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.4,0.2h9.1c0.2,0,0.3-0.1,0.4-0.2
c0.1-0.1,0.2-0.3,0.2-0.4c0-0.2-0.1-0.3-0.2-0.4C14.6,10.3,14.4,10.3,14.3,10.3z M5.1,13.7h4.6c0.2,0,0.3,0.1,0.4,0.2
c0.1,0.1,0.2,0.3,0.2,0.4c0,0.2-0.1,0.3-0.2,0.4c-0.1,0.1-0.3,0.2-0.4,0.2H5.1c-0.2,0-0.3-0.1-0.4-0.2c-0.1-0.1-0.2-0.3-0.2-0.4
c0-0.2,0.1-0.3,0.2-0.4C4.8,13.8,5,13.7,5.1,13.7z"
		/>
	</svg>
);

export const ArrowRightWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>arrowRightW</title>
		<path fill={primary}
			d="M8.6,23.3c-1,0.9-2.5,0.9-3.4,0c-1-0.9-1-2.5,0-3.4l8.5-7.9L5.1,4.1c-1-0.9-1-2.5,0-3.4C5.6,0.2,6.2,0,6.8,0
C7.4,0,8,0.2,8.5,0.7l10.2,9.5l0,0c0,0,0.1,0.1,0.2,0.1c1,0.9,1,2.5,0,3.4L8.6,23.3z"
		/>
	</svg>
);

export const CheckListWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Check list</title>
		<path
			fill={primary}
			d="M9.3,21.7c-0.6,0-1.6-0.3-1.9-1L1,14.3c-1.3-1.3-1.3-3.2,0-4.5s3.2-1.3,4.5,0l3.9,3.9l9-10.3c1.3-1.3,3.2-1.6,4.5-0.3
s1.6,3.2,0.3,4.5L11.9,20.5C11.3,21.4,10.6,21.7,9.3,21.7C9.7,21.7,9.7,21.7,9.3,21.7z"
		/>
	</svg>
);

export const CheckboxWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Checkbox</title>
		<path
			fill={primary}
			d="M9.3,21.7c-0.6,0-1.6-0.3-1.9-1L1,14.3c-1.3-1.3-1.3-3.2,0-4.5s3.2-1.3,4.5,0l3.9,3.9l9-10.3c1.3-1.3,3.2-1.6,4.5-0.3
s1.6,3.2,0.3,4.5L11.9,20.5C11.3,21.4,10.6,21.7,9.3,21.7C9.7,21.7,9.7,21.7,9.3,21.7z"
		/>
	</svg>
);

export const ThreeSecHeatupWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>3-sec-heatup</title>
		<path
			fill={primary}
			d="M24,13.1c-0.2-2.2-1.2-4.2-2.7-5.8c-1.3-1.3-2.9-2.2-4.6-2.5V3.6c0.7-0.2,1.2-0.8,1.2-1.5
c0-0.9-0.7-1.6-1.6-1.6h-3.4c-0.9,0-1.6,0.7-1.6,1.6c0,0.7,0.5,1.4,1.2,1.5v1.2C10.8,5.2,9.2,6,7.8,7.4C7.5,7.6,7.3,7.9,7,8.3l0,0
H3.7c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5h2.7C6.1,9.8,5.8,10.4,5.6,11H0.5C0.2,11,0,11.2,0,11.5c0,0.3,0.2,0.5,0.5,0.5
h4.8c-0.1,0.6-0.2,1.2-0.2,1.8H3c-0.3,0-0.5,0.2-0.5,0.5s0.2,0.5,0.5,0.5h2.1c0,0.6,0.1,1.2,0.3,1.8h-3c-0.3,0-0.5,0.2-0.5,0.5
c0,0.3,0.2,0.5,0.5,0.5h3.3c0.5,1.2,1.2,2.4,2.2,3.3c1.5,1.5,3.6,2.5,5.8,2.7c0.3,0,0.6,0,0.9,0c1.8,0,3.6-0.5,5.2-1.5
c0.2-0.1,0.3-0.4,0.1-0.6c-0.1-0.2-0.4-0.3-0.6-0.1c-3.4,2.2-7.8,1.7-10.7-1.1c-3.3-3.3-3.3-8.7,0-12.1s8.7-3.3,12.1,0
c2.8,2.8,3.3,7.3,1.1,10.7c-0.1,0.2-0.1,0.5,0.1,0.6c0.2,0.1,0.5,0.1,0.6-0.1C23.6,17.4,24.2,15.2,24,13.1z M12.2,2.1
c0-0.4,0.3-0.7,0.7-0.7h3.4c0.4,0,0.7,0.3,0.7,0.7c0,0.4-0.3,0.7-0.7,0.7h-3.4C12.5,2.7,12.2,2.4,12.2,2.1z M13.4,4.7v-1h2.3v1
C14.9,4.6,14.1,4.6,13.4,4.7L13.4,4.7z M20.9,19.9c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.1,0,0.2,0.1,0.3
c0.1,0.1,0.2,0.1,0.3,0.1c0.1,0,0.2,0,0.3-0.1c0.1-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2-0.1-0.3C21.1,20,21,19.9,20.9,19.9z M7.3,14.1
c0-4,3.3-7.3,7.3-7.3c4,0,7.3,3.3,7.3,7.3c0,4-3.3,7.3-7.3,7.3C10.5,21.3,7.3,18.1,7.3,14.1z M8.2,14.1c0,3.5,2.8,6.3,6.3,6.3
c3.5,0,6.3-2.8,6.3-6.3c0-3.5-2.8-6.3-6.3-6.3C11,7.7,8.2,10.6,8.2,14.1z M17.5,10.4l-2.3,2.3c-0.2-0.1-0.4-0.2-0.6-0.2
c-0.2,0-0.4,0.1-0.6,0.2l-1-1c-0.2-0.2-0.5-0.2-0.7,0c-0.2,0.2-0.2,0.5,0,0.7l1,1c-0.1,0.2-0.2,0.4-0.2,0.6c0,0.8,0.7,1.5,1.5,1.5
c0.8,0,1.5-0.7,1.5-1.5c0-0.2-0.1-0.4-0.2-0.6l2.3-2.3l0.1-0.1c0.2-0.2,0.2-0.5,0-0.7C18,10.2,17.7,10.2,17.5,10.4L17.5,10.4z
M14.5,14.6c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5c0.3,0,0.5,0.2,0.5,0.5C15.1,14.3,14.8,14.6,14.5,14.6z M19.1,13.6h0.4
c0.3,0,0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5h-0.4c-0.3,0-0.5-0.2-0.5-0.5C18.6,13.8,18.8,13.6,19.1,13.6z M10,13.6H9.5
c-0.3,0-0.5,0.2-0.5,0.5c0,0.3,0.2,0.5,0.5,0.5H10c0.3,0,0.5-0.2,0.5-0.5C10.4,13.8,10.2,13.6,10,13.6z M15,9.5
c0,0.3-0.2,0.5-0.5,0.5c-0.3,0-0.5-0.2-0.5-0.5V9c0-0.3,0.2-0.5,0.5-0.5C14.8,8.6,15,8.8,15,9V9.5z M14.5,18.2
c-0.3,0-0.5,0.2-0.5,0.5v0.4c0,0.3,0.2,0.5,0.5,0.5c0.3,0,0.5-0.2,0.5-0.5v-0.4C15,18.4,14.8,18.2,14.5,18.2L14.5,18.2z M0.1,16.6
c0.1-0.1,0.2-0.1,0.3-0.1c0.1,0,0.2,0,0.3,0.1c0.1,0.1,0.1,0.2,0.1,0.3s0,0.2-0.1,0.3c-0.1,0.1-0.2,0.1-0.3,0.1
c-0.1,0-0.2-0.1-0.3-0.1C0,17.2,0,17.1,0,16.9C0,16.8,0,16.7,0.1,16.6z"
		/>
	</svg>
);

export const AccessoriesWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Accessories</title>
		<path
			fill={primary}
			d="M23.6,4.2h-2.1V3.3C21.7,3.2,22,3,22.1,2.7c0.2-0.3,0.2-0.6,0.2-0.9c-0.1-0.3-0.2-0.6-0.4-0.8c-0.2-0.2-0.5-0.3-0.8-0.3H3
c-0.3,0-0.6,0.1-0.8,0.3C1.9,1.3,1.8,1.6,1.7,1.9s0,0.6,0.2,0.9C2,3,2.3,3.2,2.6,3.3v0.9H0.4c-0.1,0-0.2,0-0.3,0.1
C0,4.5,0,4.6,0,4.7v1.7C0,7,0.2,7.5,0.6,7.9s0.9,0.6,1.5,0.6h0.4v1.3c0,0.5,0.2,1,0.5,1.4c0.3,0.4,0.7,0.6,1.2,0.7v0.5
c0,0.6,0.2,1.1,0.6,1.5c0.4,0.4,0.9,0.6,1.5,0.6h3v1.7H8.6c-1,0-2,0.4-2.7,1.1c-0.7,0.7-1.1,1.7-1.1,2.7v2.6c0,0.1,0,0.2,0.1,0.3
c0.1,0.1,0.2,0.1,0.3,0.1h0.4c0.7,0,1.3-0.3,1.8-0.8c0.5-0.5,0.8-1.1,0.8-1.8c0-0.2,0.1-0.4,0.3-0.6c0.2-0.2,0.4-0.3,0.6-0.3h0.5
c0.1,0.6,0.4,1.1,0.9,1.5c0.5,0.4,1.1,0.6,1.7,0.6c0.6,0,1.2-0.2,1.7-0.6c0.5-0.4,0.8-0.9,0.9-1.5H15c0.2,0,0.4,0.1,0.6,0.3
c0.2,0.2,0.3,0.4,0.3,0.6c0,0.7,0.3,1.3,0.8,1.8c0.5,0.5,1.1,0.8,1.8,0.8h0.4c0.1,0,0.2,0,0.3-0.1c0.1-0.1,0.1-0.2,0.1-0.3v-2.6
c0-1-0.4-2-1.1-2.7c-0.7-0.7-1.7-1.1-2.7-1.1h-0.9v-1.7h3c0.6,0,1.1-0.2,1.5-0.6c0.4-0.4,0.6-1,0.6-1.5V12c0.5-0.1,0.9-0.4,1.2-0.7
c0.3-0.4,0.5-0.9,0.5-1.4V8.6h0.4c0.6,0,1.1-0.2,1.5-0.6C23.8,7.5,24,7,24,6.4V4.7c0-0.1,0-0.2-0.1-0.3C23.8,4.3,23.7,4.2,23.6,4.2
L23.6,4.2z M3,1.7h18c0.1,0,0.2,0,0.3,0.1c0.1,0.1,0.1,0.2,0.1,0.3s0,0.2-0.1,0.3c-0.1,0.1-0.2,0.1-0.3,0.1H3c-0.1,0-0.2,0-0.3-0.1
C2.6,2.3,2.6,2.2,2.6,2.1c0-0.1,0-0.2,0.1-0.3C2.8,1.7,2.9,1.7,3,1.7z M2.1,7.7c-0.3,0-0.7-0.1-0.9-0.4C1,7.1,0.9,6.7,0.9,6.4V5.1
h1.7v2.6H2.1z M7.3,20.6c0,0.5-0.2,0.9-0.5,1.2c-0.3,0.3-0.8,0.5-1.2,0.5v-2.2c0-0.8,0.3-1.6,0.9-2.1c0.6-0.6,1.3-0.9,2.1-0.9h0.9
v1.7H9c-0.5,0-0.9,0.2-1.2,0.5C7.5,19.7,7.3,20.2,7.3,20.6L7.3,20.6z M15.4,17.2c0.8,0,1.6,0.3,2.1,0.9c0.6,0.6,0.9,1.3,0.9,2.1v2.2
c-0.5,0-0.9-0.2-1.2-0.5c-0.3-0.3-0.5-0.8-0.5-1.2c0-0.5-0.2-0.9-0.5-1.2c-0.3-0.3-0.8-0.5-1.2-0.5h-0.4v-1.7
C14.6,17.2,15.4,17.2,15.4,17.2z M13.7,19.3c0,0.5-0.2,0.9-0.5,1.2C12.9,20.9,12.5,21,12,21c-0.5,0-0.9-0.2-1.2-0.5
c-0.3-0.3-0.5-0.8-0.5-1.2v-4.7h3.4C13.7,14.6,13.7,19.3,13.7,19.3z M17.6,13.7H6.4c-0.3,0-0.7-0.1-0.9-0.4
c-0.2-0.2-0.4-0.6-0.4-0.9V12h13.7v0.4c0,0.3-0.1,0.7-0.4,0.9S17.9,13.7,17.6,13.7L17.6,13.7z M19.3,11.1H4.7
c-0.3,0-0.7-0.1-0.9-0.4c-0.2-0.2-0.4-0.6-0.4-0.9V3.4h17.1v6.5c0,0.3-0.1,0.7-0.4,0.9C20,11,19.6,11.1,19.3,11.1L19.3,11.1z
M23.1,6.4c0,0.3-0.1,0.7-0.4,0.9c-0.2,0.2-0.6,0.4-0.9,0.4h-0.4V5.1h1.7C23.1,5.1,23.1,6.4,23.1,6.4z"
		/>
	</svg>
);



export const AutoMilkWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Auto Milk</title>
		<path
			fill={primary}
			d="M23.2,18.8L23.2,18.8l0.2,0L24,6.9l0,0c0.1-1.5-1-2.9-2.6-3l0,0h0l0,0c-0.1,0-0.1,0-0.2,0V4h0V3.9h-2.1l-0.2-2.5l-0.2,0
c0.1,0,0.2,0,0.2,0c0-0.3-0.3-0.5-0.6-0.5v0H0.6C0.3,0.8,0,1.1,0,1.4c0,0,0,0.1,0,0.1L1.6,14v0c0.1,0.5,0.3,1,0.8,1.3L2,20.2
c-0.1,1.5,1,2.9,2.6,3v0c0,0,0,0,0,0c0.1,0,0.2,0,0.2,0h12.9c1.5,0,2.7-1.2,2.8-2.7h1h0l0-0.2c0,0,0,0,0,0l0,0.2
c1,0,1.8-0.8,1.9-1.8h0v0L23.2,18.8L23.2,18.8z M2.5,15.2L2.5,15.2C2.5,15.2,2.5,15.2,2.5,15.2L2.5,15.2C2.5,15.2,2.5,15.2,2.5,15.2
z M23,6.7h-0.2v0c0,0,0,0.1,0,0.1l0.2,0c0,0,0,0,0,0c0,0,0,0,0,0l-0.1,0v0l0,0l-0.6,11.9h0c0,0.4-0.3,0.7-0.7,0.7h-1.1L19.2,5.1h2
c0.9,0,1.6,0.7,1.6,1.6L23,6.7C23,6.7,23,6.7,23,6.7z M22.4,18.7L22.4,18.7L22.4,18.7L22.4,18.7z M3.5,2L2.6,12.4L1.3,2H3.5z
M3.1,20.4c0,0,0-0.1,0-0.1l0,0l0,0l0,0L4.7,2h13.1l1.5,18.3v0l0.1,0l0,0l0,0c0,0,0,0,0,0l-0.2,0c0,0.4-0.1,0.9-0.4,1.2l0,0l0,0l0,0
l0.1,0.1l-0.1-0.1c-0.3,0.3-0.7,0.5-1.2,0.5v0h0v0h0H4.8v0.2c0,0,0,0,0,0V22C3.9,22,3.1,21.3,3.1,20.4z M23.4,18.8L23.4,18.8
L23.4,18.8L23.4,18.8z M15.4,3.7L15.4,3.7l1.4,16.6h0l0,0.2l0.2,0v0l0.9-0.1l0,0l0.2,0l0-0.2L16.6,3.6l0-0.2l-0.2,0l-0.9,0.1l-0.2,0
L15.4,3.7L15.4,3.7z M15.6,3.6L15.6,3.6L15.6,3.6L15.6,3.6z"
		/>
	</svg>
);

export const AutomaticEaseWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Automatic Ease</title>
		<path
		 fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M4.3,1.1h15.4c1.8,0,3.2,1.4,3.2,3.2v15.4c0,1.8-1.4,3.2-3.2,3.2H4.3
c-1.8,0-3.2-1.4-3.2-3.2V4.3C1.1,2.5,2.5,1.1,4.3,1.1z M19.7,0H4.3C1.9,0,0,1.9,0,4.3v15.4C0,22.1,1.9,24,4.3,24h15.4
c2.4,0,4.3-1.9,4.3-4.3V4.3C24,1.9,22.1,0,19.7,0z M12.1,7.7l-1.7,4.6h3.4L12.1,7.7C12.1,7.7,12.1,7.7,12.1,7.7z M7.5,16.4l3.9-9.9
h1.5l3.9,9.9h-1.5l-1.1-3H10l-1.1,3H7.5z"
		/>
	</svg>
);

export const BaristaKitWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Barista Kit</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M19.9,7.1c-0.2,0-0.3,0.1-0.3,0.3v1.4h-1.4c-0.2,0-0.3,0.1-0.3,0.3v12.3
c0,0.2,0.1,0.3,0.3,0.3h0.4v1.1c0,0.2,0.1,0.3,0.3,0.3H23c0.2,0,0.3-0.1,0.3-0.3v-1.1h0.4c0.2,0,0.3-0.1,0.3-0.3V9.2
c0-0.2-0.1-0.3-0.3-0.3h-1.4V7.4c0-0.2-0.1-0.3-0.3-0.3C22,7.1,19.9,7.1,19.9,7.1z M20.2,8.8V7.8h1.4v1.1H20.2z M18.5,10.2V9.5h4.8
v0.8H18.5L18.5,10.2z M18.5,11.6v-0.8h4.8V17h-1.7c-0.2,0-0.3,0.1-0.3,0.3c0,0.2,0.1,0.3,0.3,0.3h1.7v0.8h-4.8v-6.2h1.7
c0.2,0,0.3-0.1,0.3-0.3c0-0.2-0.1-0.3-0.3-0.3H18.5L18.5,11.6z M21,12.3c-0.1,0-0.2,0-0.2,0.1c-0.1,0.2-0.5,0.6-0.8,1.2
c-0.2,0.3-0.3,0.5-0.4,0.8c-0.1,0.2-0.2,0.5-0.2,0.6c0,0.1,0,0.2,0,0.3l0,0.1v0l0,0l0,0c0.2,0.7,0.9,1.3,1.6,1.3
c0.8,0,1.4-0.5,1.6-1.3l0,0l0-0.1l0,0c0-0.1,0-0.2,0-0.3c0-0.2-0.1-0.4-0.2-0.6c-0.1-0.2-0.3-0.5-0.4-0.8c-0.3-0.5-0.7-1-0.8-1.2
C21.1,12.3,21.1,12.3,21,12.3z M19.3,15.4L19.3,15.4l0.1,0L19.3,15.4L19.3,15.4z M19.9,15c0,0,0-0.1,0-0.1c0-0.1,0-0.1,0.1-0.2
c0.1-0.2,0.2-0.3,0.3-0.5c0.2-0.3,0.4-0.7,0.7-1c0.2,0.3,0.5,0.7,0.7,1c0.1,0.2,0.2,0.4,0.3,0.5c0,0.1,0.1,0.1,0.1,0.2
c0,0.1,0,0.1,0,0.1c0,0.1,0,0.1,0,0.2l0,0.1c-0.1,0.5-0.5,0.8-1,0.8s-0.9-0.3-1-0.8l0-0.1C19.9,15.1,19.9,15,19.9,15z M18.5,21.1V19
h4.8v2.1H18.5L18.5,21.1z M19.2,22.5v-0.8h3.5v0.8H19.2L19.2,22.5z M11.5,10.5c0.5,0,0.9,0.3,1,0.8c0.9,0.1,1.6,0.8,1.7,1.7l0.3,3.8
H15c0.6,0,1.1,0.4,1.1,1l0.1,1c0,0.5-0.3,1-0.8,1.2l-0.7,0.2l0,0.4c0.5,0.1,0.8,0.6,0.8,1V22c0,0.6-0.5,1.1-1.1,1.1H1.8
c-0.6,0-1.1-0.5-1.1-1.1v-0.4c0-0.5,0.3-0.9,0.8-1l0.3-3.8H1.5c-0.8,0-1.5-0.7-1.5-1.5v-1.5c0-0.8,0.7-1.5,1.5-1.5h0.8
c0.3-0.6,0.8-1,1.5-1.1c0.2-0.5,0.6-0.8,1-0.8h0.1L3,7.2c-0.3-0.5-0.4-1-0.4-1.5V3C2.1,2.8,1.8,2.3,1.9,1.8C2,1.3,2.4,0.9,3,0.9
h10.4c0.5,0,1,0.4,1.1,0.9c0.1,0.5-0.2,1.1-0.7,1.2v2.7c0,0.5-0.1,1-0.4,1.5L11.5,10.5L11.5,10.5L11.5,10.5z M2.3,20.5H14L13.4,13
c0-0.5-0.4-0.9-0.8-1v5.5c0,0.2-0.2,0.4-0.4,0.4s-0.4-0.2-0.4-0.4v-4.8h-1.1v2.6c0,0.3-0.1,0.5-0.4,0.6v0.8c0,0.4-0.3,0.7-0.7,0.7
h-3c-0.4,0-0.7-0.3-0.7-0.7v-0.8c-0.2-0.1-0.4-0.4-0.4-0.6v-2.6H4.4v4.8c0,0.2-0.2,0.4-0.4,0.4c-0.2,0-0.4-0.2-0.4-0.4V12
c-0.4,0.1-0.7,0.4-0.8,0.8c0,0.1,0,0.1,0,0.2L2.3,20.5L2.3,20.5z M9.6,16h-3v0.7h3L9.6,16L9.6,16z M9.2,13.5H10v-0.7H6.3v0.7H7
c0.2,0,0.4,0.2,0.4,0.4S7.2,14.2,7,14.2H6.3v1.1H10v-1.1H9.2c-0.2,0-0.4-0.2-0.4-0.4S9,13.5,9.2,13.5z M13.3,1.6H3
C2.8,1.6,2.6,1.8,2.6,2S2.8,2.4,3,2.4h10.4c0.2,0,0.4-0.2,0.4-0.4S13.5,1.6,13.3,1.6z M3.3,5.7c0,0.4,0.1,0.8,0.3,1.1l2.1,3.7h4.8
l2.1-3.7c0.2-0.3,0.3-0.7,0.3-1.1V3.1H3.3V5.7z M11.5,11.2H4.8c-0.2,0-0.4,0.2-0.4,0.4V12h7.4v-0.4C11.8,11.4,11.7,11.2,11.5,11.2
L11.5,11.2z M0.7,13.8v1.5c0,0.4,0.3,0.7,0.7,0.7h0.4l0.2-3H1.5C1.1,13.1,0.7,13.4,0.7,13.8L0.7,13.8z M14.4,22.3
c0.2,0,0.4-0.2,0.4-0.4v-0.4c0-0.2-0.2-0.4-0.4-0.4H1.8c-0.2,0-0.4,0.2-0.4,0.4V22c0,0.2,0.2,0.4,0.4,0.4L14.4,22.3L14.4,22.3z
M15.4,17.9c0-0.2-0.2-0.3-0.4-0.3h-0.5l0.2,1.9l0.6-0.2c0.2-0.1,0.3-0.2,0.3-0.4L15.4,17.9L15.4,17.9z M4.8,5.7
c0,0.1,0,0.3,0.1,0.4l1.8,3.1c0.1,0.1,0.1,0.3,0,0.4S6.5,9.8,6.4,9.8c-0.1,0-0.3-0.1-0.3-0.2L4.3,6.5C4.1,6.2,4.1,6,4.1,5.7V4.2
c0-0.2,0.2-0.4,0.4-0.4c0.2,0,0.4,0.2,0.4,0.4C4.8,4.2,4.8,5.7,4.8,5.7z"
		/>
	</svg>
);

export const BaristaTrainingWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Barista training</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M23.2,16c0.5,0.5,0.8,1.2,0.8,1.9v3.7H10.9c0,0.2-0.1,0.5-0.1,0.8
c0,1-1.8,1.5-3.5,1.5s-3.5-0.5-3.5-1.5c0-0.6-0.1-1.1-0.2-1.5H2.7c-1.5,0-2.7-1.2-2.7-2.7c0-1.4,1-2.5,2.3-2.7c0,0,0-0.1,0-0.1
c0-0.1,0-0.2,0-0.3v-0.4h6.2c0.1-0.7,0.4-1.3,0.9-1.7c0.4-0.3,0.6-0.8,0.6-1.2v-6c0-3.2,2.6-5.8,5.8-5.8c3.2,0,5.8,2.6,5.8,5.8v6
c0,0.5,0.2,0.9,0.6,1.2c0.6,0.4,0.9,1.1,0.9,1.9L23.2,16L23.2,16z M22.5,14.9c0-0.5-0.2-0.9-0.6-1.2c-0.6-0.4-0.9-1.1-0.9-1.9v-1.8
c-0.2,0.2-0.5,0.3-0.8,0.4c-0.2,1.2-0.9,2.2-1.9,2.8V14l4,1.3c0.1,0,0.2,0.1,0.3,0.1L22.5,14.9L22.5,14.9z M18.6,15v2.8h0.8v-2.6
L18.6,15L18.6,15z M13.2,15l-0.8,0.3c0,1-0.1,1.8-0.3,2.6h1.1V15z M16,6.5c0.1,0.2,0.1,0.4,0.2,0.6l0.4,0.8l-0.8-0.2
c-1.2-0.2-2.3-0.7-3.4-1.3v3.2c0,1.9,1.6,3.5,3.5,3.5c1.9,0,3.5-1.6,3.5-3.5V7.3C18.2,7.3,17,7,16,6.5L16,6.5z M17.4,13.6
c-0.5,0.2-1,0.3-1.5,0.3c-0.5,0-1.1-0.1-1.5-0.3v0.5l1.5,1.9l1.5-1.9C17.4,14.2,17.4,13.6,17.4,13.6z M15.9,17.2l-1.9-2.3v2.9h3.9
v-2.9L15.9,17.2L15.9,17.2z M20.9,8.5c0-0.5-0.3-0.9-0.8-1.1v2.2C20.6,9.4,20.9,9,20.9,8.5z M20.9,5.8c0-2.8-2.3-5-5-5
c-2.8,0-5,2.3-5,5V7c0.2-0.2,0.5-0.3,0.8-0.4V5.1l0.8,0.5c0.9,0.5,1.8,0.9,2.8,1.2c-0.1-0.3-0.1-0.7-0.1-1V5.1l0.6,0.4
c1.1,0.7,2.4,1,3.7,1.1V5.8h0.8v0.8c0.3,0.1,0.5,0.2,0.8,0.4L20.9,5.8L20.9,5.8z M11.6,7.4c-0.4,0.2-0.8,0.6-0.8,1.1
c0,0.5,0.3,0.9,0.8,1.1V7.4z M10.8,11.8c0,0.7-0.3,1.4-0.9,1.9c-0.3,0.3-0.5,0.6-0.6,1h2.2l2-0.7v-0.8c-1-0.6-1.7-1.6-1.9-2.8
c-0.3-0.1-0.6-0.2-0.8-0.4L10.8,11.8L10.8,11.8z M0.8,18.2c0,1.1,0.9,1.9,1.9,1.9h0.7c-0.1-0.2-0.1-0.3-0.2-0.5
c-0.3-0.9-0.7-1.9-0.8-3.3C1.5,16.4,0.8,17.2,0.8,18.2z M7.4,23.2c1.8,0,2.7-0.5,2.7-0.8c0-1.2,0.3-2.1,0.7-3.1v0
c0.4-1.1,0.8-2.2,0.9-3.9H3.1c0.1,1.7,0.5,2.8,0.9,3.9l0,0l0,0c0.4,1,0.7,1.9,0.7,3.1C4.6,22.7,5.6,23.2,7.4,23.2L7.4,23.2z
M11.8,18.6c-0.1,0.4-0.2,0.7-0.4,1c-0.2,0.4-0.3,0.9-0.4,1.3h1.3v-1.5h7v1.5h1.5l-0.2-2.3H11.8z M13.2,20.1v0.8h5.4v-0.8L13.2,20.1
L13.2,20.1z M21.6,20.9h1.6v-3c0-0.8-0.5-1.6-1.3-1.8l-1.8-0.6v2.3h0.5l-0.1-0.7l0.8-0.1L21.6,20.9L21.6,20.9z M13.9,8.5
c0.2,0,0.4-0.2,0.4-0.4c0-0.2-0.2-0.4-0.4-0.4c-0.2,0-0.4,0.2-0.4,0.4C13.5,8.3,13.7,8.5,13.9,8.5z M17.8,8.5c0.2,0,0.4-0.2,0.4-0.4
c0-0.2-0.2-0.4-0.4-0.4c-0.2,0-0.4,0.2-0.4,0.4C17.4,8.3,17.6,8.5,17.8,8.5z M15.1,10.5h-0.8c0,0.9,0.7,1.5,1.5,1.5
c0.9,0,1.5-0.7,1.5-1.5h-0.8c0,0.4-0.3,0.8-0.8,0.8C15.4,11.2,15.1,10.9,15.1,10.5z"
		/>
	</svg>
);

export const CapacityWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Capacity</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M16.1,19.6v0.5H7.9V19h8.3C16.1,19,16.1,19.6,16.1,19.6z M16.2,17.9l0.1-0.2
l5.1-7.4V6.5l0.7-0.3c0.2-0.1,0.5-0.3,0.6-0.5c0.1-0.2,0.2-0.5,0.1-0.8c0-0.3-0.2-0.5-0.4-0.7C22.3,4,22,3.9,21.8,3.9H2.2
C2,3.9,1.7,4,1.5,4.2C1.3,4.4,1.2,4.6,1.1,4.9c0,0.3,0,0.6,0.1,0.8c0.1,0.2,0.4,0.4,0.6,0.5l0.7,0.3v3.9l5.1,7.4l0.1,0.2H16.2z
M18.8,21.2c0,0,0-0.7-0.1-1.1c0-0.1-0.1-0.2-0.1-0.2c-0.1-0.1-0.3-0.2-0.5-0.2h-0.8v-1.3l5.1-7.5c0.1-0.1,0.1-0.3,0.1-0.4V7.3
c0.5-0.2,0.9-0.5,1.2-1c0.3-0.5,0.4-1,0.3-1.5c-0.1-0.5-0.4-1-0.8-1.4c-0.4-0.3-0.9-0.5-1.5-0.5H2.2c-0.5,0-1,0.2-1.5,0.5
C0.4,3.7,0.1,4.2,0,4.7c-0.1,0.5,0,1.1,0.3,1.5c0.3,0.5,0.7,0.8,1.2,1v3.2c0,0.2,0,0.3,0.1,0.4l5.1,7.5v1.3H6
c-0.2,0-0.4,0.1-0.5,0.2c0,0-0.1,0.1-0.1,0.2c-0.1,0.4-0.1,1.1-0.1,1.1L18.8,21.2L18.8,21.2z M5,6.1c0-0.3,0.3-0.6,0.6-0.6H19
c0.3,0,0.6,0.3,0.6,0.6c0,0.3-0.3,0.6-0.6,0.6H5.6C5.3,6.7,5,6.4,5,6.1z M12,10.6c0.7-0.8,1.5-1.3,2.4-1.6c0.8-0.2,1.6,0,2.2,0.6
c0.5,0.7,0.8,1.7,0.5,2.6c-0.2,1.1-0.7,2.1-1.4,2.8c-0.7,0.8-1.5,1.3-2.4,1.6c-0.2,0.1-0.4,0.1-0.6,0.1c-0.6,0-1.2-0.2-1.6-0.7
c-0.5-0.7-0.8-1.7-0.5-2.6C10.9,12.4,11.4,11.4,12,10.6L12,10.6z M12.6,11.3c-0.6,0.6-1,1.5-1.1,2.4c-0.1,0.4-0.1,0.8,0,1.2
c0.2-0.2,0.4-0.3,0.7-0.5c0.3-0.2,0.6-0.4,0.9-0.7c0.3-0.3,0.5-0.6,0.6-1c0.4-0.9,1-1.7,1.8-2.1c0.2-0.1,0.4-0.2,0.5-0.4
c-0.3-0.2-0.6-0.3-0.9-0.3c-0.2,0-0.3,0-0.5,0.1C13.8,10.1,13.1,10.6,12.6,11.3L12.6,11.3z M15.3,14.4c0.6-0.6,1-1.5,1.1-2.4
c0.1-0.4,0.1-0.8,0-1.2c-0.2,0.2-0.4,0.3-0.7,0.5c-0.6,0.4-1.2,1-1.5,1.7c-0.2,0.5-0.4,0.9-0.8,1.2c-0.3,0.4-0.7,0.7-1.1,0.9
c-0.2,0.1-0.4,0.2-0.6,0.4c0.4,0.3,0.9,0.4,1.4,0.2C14.1,15.6,14.8,15.1,15.3,14.4z"
		/>
	</svg>
);

export const CloseWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Close</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M0.5,23.5c0.6,0.6,1.7,0.6,2.3,0l9.2-9.2l9.2,9.2c0.6,0.6,1.7,0.6,2.3,0
c0.6-0.6,0.6-1.7,0-2.3L14.3,12l9.2-9.2c0.6-0.6,0.6-1.7,0-2.3c-0.6-0.6-1.7-0.6-2.3,0L12,9.7L2.8,0.5c-0.6-0.6-1.7-0.6-2.3,0
s-0.6,1.7,0,2.3L9.7,12l-9.2,9.2C-0.2,21.9-0.2,22.9,0.5,23.5z"
		/>
	</svg>
);

export const CoffeeBagWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Coffee Bag</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M18.7,2.7L20,5.3c0.2,0.4,0.3,0.8,0.3,1.3v17c0,0.2-0.2,0.4-0.4,0.4H4.1
c-0.2,0-0.4-0.2-0.4-0.4v-17c0-0.4,0.1-0.9,0.3-1.3l1.3-2.6V0.4C5.3,0.2,5.4,0,5.6,0h12.7c0.2,0,0.4,0.2,0.4,0.4
C18.7,0.4,18.7,2.7,18.7,2.7z M18,0.8h-4.4v1.6H18L18,0.8L18,0.8z M11.2,3C11.2,3,11.2,3,11.2,3C11.2,3,11.2,3,11.2,3
C11.2,3,11.2,3,11.2,3L9.8,5.7C9.7,6,9.6,6.3,9.6,6.6v3h1.6v-3c0-0.4,0.1-0.9,0.3-1.3l1.3-2.6l0-1.9h-1.6v2c0,0,0,0,0,0c0,0,0,0,0,0
C11.2,2.9,11.2,2.9,11.2,3L11.2,3L11.2,3z M10.4,0.8H6v1.6h4.4V0.8L10.4,0.8z M16.4,23.2V6.6c0-0.4,0.1-0.9,0.3-1.3l1-2.1h-4.3
l-1.2,2.5C12.1,6,12,6.3,12,6.6V10c0,0.2-0.2,0.4-0.4,0.4H9.2c-0.2,0-0.4-0.2-0.4-0.4V6.6c0-0.4,0.1-0.9,0.3-1.3l1-2.1H5.9L4.7,5.7
C4.5,6,4.5,6.3,4.5,6.6v16.6H16.4z M17.2,23.2h2.4V6.6c0-0.3-0.1-0.6-0.2-0.9l-1-2l-1,2c-0.1,0.3-0.2,0.6-0.2,0.9L17.2,23.2
L17.2,23.2z M8.4,14.4c0.7-0.7,1.5-1.2,2.5-1.4c0.8-0.2,1.7,0,2.3,0.6c0.6,0.6,0.8,1.5,0.6,2.3c-0.2,1-0.7,1.8-1.4,2.5
c-0.7,0.7-1.5,1.2-2.5,1.4c-0.2,0-0.4,0.1-0.7,0.1c-0.6,0-1.2-0.2-1.6-0.6C7,18.6,6.8,17.7,7,16.9C7.3,16,7.7,15.1,8.4,14.4
L8.4,14.4z M9,15c-0.6,0.6-1,1.3-1.2,2.1c-0.1,0.4-0.1,0.7,0,1.1c0.2-0.2,0.4-0.3,0.7-0.4c0.3-0.2,0.6-0.4,0.9-0.6
c0.3-0.3,0.5-0.6,0.6-0.9c0.4-0.8,1.1-1.5,1.9-1.9c0.2-0.1,0.4-0.2,0.6-0.3c-0.3-0.2-0.6-0.3-0.9-0.3c-0.2,0-0.3,0-0.5,0.1
C10.3,14,9.6,14.4,9,15z M11.8,17.8c0.6-0.6,1-1.3,1.2-2.1c0.1-0.4,0.1-0.7,0-1.1c-0.2,0.2-0.4,0.3-0.7,0.4
c-0.7,0.3-1.2,0.9-1.5,1.5c-0.2,0.4-0.5,0.8-0.8,1.1c-0.3,0.3-0.7,0.6-1.1,0.8c-0.2,0.1-0.4,0.2-0.6,0.3c0.4,0.3,0.9,0.3,1.4,0.2
C10.5,18.8,11.3,18.4,11.8,17.8z"
		/>
	</svg>
);

export const CoffeeMachineWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Coffee Machine</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M22.8,7.3c0.7,0,1.2-0.5,1.2-1.2V1.2C24,0.5,23.5,0,22.8,0H1.2C0.5,0,0,0.5,0,1.2
v4.9c0,0.7,0.5,1.2,1.2,1.2H2v7l-0.9,1.5c-0.1,0.2,0,0.4,0.1,0.6L2,16.9v4.3H1.2c-0.7,0-1.2,0.5-1.2,1.2v0.8c0,0.7,0.5,1.2,1.2,1.2
h21.6c0.7,0,1.2-0.5,1.2-1.2v-0.8c0-0.7-0.5-1.2-1.2-1.2H22v-9.6l0.6,0.1c0.1,0,0.1,0,0.2,0c0.7,0,1.2-0.5,1.2-1.2v-1
c0-0.4-0.1-0.7-0.4-0.9c-0.3-0.2-0.6-0.3-0.9-0.3L22,8.4v-1C22,7.3,22.8,7.3,22.8,7.3z M0.8,6.1V1.2C0.8,1,1,0.8,1.2,0.8h21.6
c0.2,0,0.4,0.2,0.4,0.4v4.9c0,0.2-0.2,0.4-0.4,0.4H1.2C1,6.5,0.8,6.3,0.8,6.1z M17.2,10.2h-1.6V9.8h1.6
C17.2,9.8,17.2,10.2,17.2,10.2z M14.8,11c0,0.7-0.5,1.2-1.2,1.2h-3.2c-0.7,0-1.2-0.5-1.2-1.2V9h5.6V11z M11.6,13h0.8v0.8h-0.8V13z
M14,8.1h-4V7.3h4C14,7.3,14,8.1,14,8.1z M2,15.9l1.4-2.5l1,0.6l-1.4,2.5L2,15.9L2,15.9z M4.5,13.2l-0.1-0.1L4.3,13l1.3-2.2
c0-0.1,0.1-0.1,0.1-0.2V7.3H6v3.2C6,10.5,4.5,13.2,4.5,13.2z M2.8,7.3h2v3.2l-1.2,2.2l-0.1-0.1c-0.1-0.1-0.2-0.1-0.3,0
c-0.1,0-0.2,0.1-0.2,0.2l-0.1,0.2C2.8,12.9,2.8,7.3,2.8,7.3z M23.2,22.4v0.8c0,0.2-0.2,0.4-0.4,0.4H1.2c-0.2,0-0.4-0.2-0.4-0.4v-0.8
C0.8,22.2,1,22,1.2,22h21.6C23,22,23.2,22.2,23.2,22.4z M16,17.9v-0.4c0.4,0,0.8,0.4,0.8,0.8s-0.4,0.8-0.8,0.8h-0.2
C15.9,18.8,16,18.3,16,17.9z M15.2,17.9c0,1.6-1.3,2.9-2.8,2.9h-0.8c-1.5,0-2.8-1.3-2.8-2.9v-1.2h6.4V17.9z M21.2,21.2H14
c0.5-0.3,1-0.7,1.3-1.2H16c0.9,0,1.6-0.7,1.6-1.6s-0.7-1.6-1.6-1.6v-0.4c0-0.2-0.2-0.4-0.4-0.4H8.4c-0.2,0-0.4,0.2-0.4,0.4v1.6
c0,1.4,0.8,2.6,2,3.3H2.8v-3.8L3,17.5c0,0,0.1,0,0.1,0c0,0,0,0,0,0l0,0l0,0c0,0,0,0,0,0c0,0,0.1,0,0.1,0l0,0c0,0,0.1,0,0.1-0.1
c0,0,0,0,0,0c0,0,0.1-0.1,0.1-0.1l1.8-3.2c0.1-0.2,0.1-0.4-0.1-0.5l1.5-2.8c0-0.1,0-0.1,0-0.2V7.3h2.4v0.8H8.8
c-0.2,0-0.4,0.2-0.4,0.4V11c0,1.1,0.9,2,2,2h0.4v1.2c0,0.2,0.2,0.4,0.4,0.4h1.6c0.2,0,0.4-0.2,0.4-0.4V13h0.4c1.1,0,2-0.9,2-2h2
l3.6,0.5V21.2L21.2,21.2z M22.7,9.1c0.1,0,0.2,0,0.3,0.1c0.1,0.1,0.1,0.2,0.1,0.3v1c0,0.1,0,0.2-0.1,0.3c-0.1,0.1-0.2,0.1-0.3,0.1
l-1.1-0.1h0L18,10.2V9.7l3.7-0.5h0L22.7,9.1L22.7,9.1z M21.2,8.5L17.6,9h-2V8.6c0-0.2-0.2-0.4-0.4-0.4h-0.4V7.3h6.4L21.2,8.5
L21.2,8.5z M12,6.1c1.3,0,2.4-1.1,2.4-2.4S13.3,1.2,12,1.2c-1.3,0-2.4,1.1-2.4,2.4C9.6,5,10.7,6.1,12,6.1z M12,5.3
c-0.6,0-1.1-0.3-1.4-0.8h2.8C13.1,5,12.6,5.3,12,5.3z M12,2c0.9,0,1.6,0.7,1.6,1.6h-1.3L12,2.7c-0.1-0.2-0.3-0.3-0.5-0.3
c-0.2,0.1-0.3,0.3-0.3,0.5l0.2,0.7h-1C10.4,2.8,11.1,2,12,2z M5.2,3.7c0,0.9-0.7,1.6-1.6,1.6C2.7,5.3,2,4.6,2,3.7S2.7,2,3.6,2
C4.5,2,5.2,2.8,5.2,3.7z M4.4,3.7c0-0.5-0.4-0.8-0.8-0.8S2.8,3.2,2.8,3.7c0,0.4,0.4,0.8,0.8,0.8S4.4,4.1,4.4,3.7z M6.4,3.3h0.8
c0.2,0,0.4-0.2,0.4-0.4S7.4,2.4,7.2,2.4H6.4C6.2,2.4,6,2.6,6,2.9S6.2,3.3,6.4,3.3z M7.2,4.9H6.4C6.2,4.9,6,4.7,6,4.5
c0-0.2,0.2-0.4,0.4-0.4h0.8c0.2,0,0.4,0.2,0.4,0.4C7.6,4.7,7.4,4.9,7.2,4.9z M16.8,5.3h4.8c0.2,0,0.4-0.2,0.4-0.4V2.4
C22,2.2,21.8,2,21.6,2h-4.8c-0.2,0-0.4,0.2-0.4,0.4v2.4C16.4,5.1,16.6,5.3,16.8,5.3z M17.2,2.9h4v1.6h-4C17.2,4.5,17.2,2.9,17.2,2.9
z M4.4,22.4h15.2c0.2,0,0.4,0.2,0.4,0.4s-0.2,0.4-0.4,0.4H4.4C4.2,23.2,4,23,4,22.8S4.2,22.4,4.4,22.4L4.4,22.4z M20.8,22.8
c0,0.2,0.2,0.4,0.4,0.4H22c0.2,0,0.4-0.2,0.4-0.4s-0.2-0.4-0.4-0.4h-0.8C21,22.4,20.8,22.6,20.8,22.8z M2,22.4h0.8
c0.2,0,0.4,0.2,0.4,0.4S3,23.2,2.8,23.2H2c-0.2,0-0.4-0.2-0.4-0.4S1.8,22.4,2,22.4z"
		/>
	</svg>
);

export const CompareWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Compare</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M11.2,24V0h2.3v2.3h6.6c0.7,0,1.4,0.3,1.9,0.8c0.5,0.5,0.8,1.2,0.8,1.9V19
c0,0.7-0.3,1.4-0.8,1.9c-0.5,0.5-1.2,0.8-1.9,0.8h-6.6V24H11.2z M20.4,4.8c-0.1-0.1-0.2-0.1-0.3-0.1h-6.6v14.7h6.6
c0.1,0,0.2,0,0.3-0.1c0.1-0.1,0.1-0.2,0.1-0.3V5C20.5,4.9,20.5,4.8,20.4,4.8L20.4,4.8z M7.4,2.3H3.9C3.2,2.3,2.5,2.6,2,3.1
C1.4,3.6,1.2,4.3,1.2,5v3.5h2.3V5c0-0.1,0-0.2,0.1-0.3c0.1-0.1,0.2-0.1,0.3-0.1h3.5V2.3z M1.2,15.5V19c0,0.7,0.3,1.4,0.8,1.9
c0.5,0.5,1.2,0.8,1.9,0.8h3.5v-2.3H3.9c-0.1,0-0.2,0-0.3-0.1c-0.1-0.1-0.1-0.2-0.1-0.3v-3.5L1.2,15.5L1.2,15.5z"
		/>
	</svg>
);

export const ComparisonDeleteCircledWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Comparison delete circled</title>
		<path fill={primary}
			d="M12,0C5.4,0,0,5.4,0,12s5.4,12,12,12s12-5.4,12-12S18.6,0,12,0z M16.5,15.5c0.3,0.3,0.3,0.7,0,1c-0.3,0.3-0.7,0.3-1,0L12,13
l-3.5,3.5c-0.3,0.3-0.7,0.3-1,0c-0.3-0.3-0.3-0.7,0-1L11,12L7.5,8.5c-0.3-0.3-0.3-0.7,0-1c0.3-0.3,0.7-0.3,1,0L12,11l3.5-3.5
c0.3-0.3,0.7-0.3,1,0c0.3,0.3,0.3,0.7,0,1L13,12L16.5,15.5z"
		/>
	</svg>
);

export const ComparisonDeleteWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Comparison delete</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M3.1,0.5c-0.7-0.7-1.8-0.7-2.6,0s-0.7,1.8,0,2.5L9.5,12l-8.9,8.9
c-0.7,0.7-0.7,1.8,0,2.5s1.8,0.7,2.5,0l8.9-8.9l8.9,8.9c0.7,0.7,1.8,0.7,2.5,0c0.7-0.7,0.7-1.8,0-2.5L14.5,12l8.9-8.9
c0.7-0.7,0.7-1.8,0-2.5c-0.7-0.7-1.8-0.7-2.5,0L12,9.5L3.1,0.5L3.1,0.5z"
		/>
	</svg>
);

export const DeliveryWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Delivery</title>
		<path fill={primary}
			d="M22.3,10.8l-0.6-2.5C21.9,8.3,22,8.2,22,8V7.6C22,6.7,21.3,6,20.4,6h-2.8V5.2c0-0.4-0.3-0.8-0.8-0.8H2.4
C2,4.4,1.6,4.8,1.6,5.2V12c0,0.2,0.2,0.4,0.4,0.4c0.2,0,0.4-0.2,0.4-0.4V5.2c0,0,0,0,0,0h14.4c0,0,0,0,0,0V12c0,0.2,0.2,0.4,0.4,0.4
c0.2,0,0.4-0.2,0.4-0.4v-0.4H22c0.5,0,1,0.4,1.2,0.9H22c-0.2,0-0.4,0.2-0.4,0.4v0.8c0,0.6,0.5,1.2,1.2,1.2h0.4v1.7h-1
c-0.3-0.9-1.2-1.6-2.2-1.6s-1.9,0.7-2.2,1.6h-0.2v-2.8c0-0.2-0.2-0.4-0.4-0.4c-0.2,0-0.4,0.2-0.4,0.4v2.8H9
c-0.3-0.9-1.2-1.6-2.2-1.6c-1,0-1.9,0.7-2.2,1.6H2.4c0,0,0,0,0,0v-0.8H4c0.2,0,0.4-0.2,0.4-0.4c0-0.2-0.2-0.4-0.4-0.4H0.4
C0.2,14.8,0,15,0,15.2c0,0.2,0.2,0.4,0.4,0.4h1.2v0.8c0,0.4,0.3,0.8,0.8,0.8h2l0,0c0,1.3,1.1,2.4,2.4,2.4c1.3,0,2.4-1.1,2.4-2.4v0
h8.5l0,0c0,1.3,1.1,2.4,2.4,2.4c1.3,0,2.4-1.1,2.4-2.4v0h1.2c0.2,0,0.4-0.2,0.4-0.4v-4C24,11.8,23.3,11,22.3,10.8L22.3,10.8z
M17.6,6.8h2.8c0.5,0,0.8,0.4,0.8,0.8v0h-3.7L17.6,6.8L17.6,6.8z M17.6,10.8V8.4h3.3l0.6,2.5C21.5,10.8,17.6,10.8,17.6,10.8z
M6.8,18.8c-0.9,0-1.6-0.7-1.6-1.6c0-0.9,0.7-1.6,1.6-1.6c0.9,0,1.6,0.7,1.6,1.6C8.4,18.1,7.7,18.8,6.8,18.8L6.8,18.8z M20,18.8
c-0.9,0-1.6-0.7-1.6-1.6c0-0.9,0.7-1.6,1.6-1.6s1.6,0.7,1.6,1.6C21.6,18.1,20.9,18.8,20,18.8z M23.2,14h-0.4c-0.2,0-0.4-0.2-0.4-0.4
v-0.4h0.9V14L23.2,14z"
		/>
	</svg>
);

export const DimensionsRulerWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Dimensions Ruler</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M17.7,0.2l6.1,6.1c0.2,0.2,0.2,0.5,0,0.7L7,23.8c-0.2,0.2-0.5,0.2-0.7,0l-6.1-6.1
c-0.2-0.2-0.2-0.5,0-0.7L17,0.2C17.2-0.1,17.5-0.1,17.7,0.2z M1.2,17.3l5.4,5.4L22.8,6.7l-5.4-5.4l-1,1l2.9,2.9
c0.2,0.2,0.2,0.5,0,0.7c-0.2,0.2-0.5,0.2-0.7,0l-2.9-2.9l-1.3,1.3l1.8,1.9l0,0c0.2,0.2,0.2,0.5,0,0.7c-0.2,0.2-0.5,0.2-0.7,0L13.6,5
l-1.3,1.3l2.9,2.9c0.2,0.2,0.2,0.5,0,0.7c-0.2,0.2-0.5,0.2-0.7,0l-2.9-2.9l-1.3,1.3l1.8,1.8c0.2,0.2,0.2,0.5,0,0.7
c-0.2,0.2-0.5,0.2-0.7,0L9.4,9.1l-1.3,1.3l2.9,2.9c0.2,0.2,0.2,0.5,0,0.7c-0.2,0.2-0.5,0.2-0.7,0l-2.9-2.9L6,12.6l1.8,1.8l0,0
c0.2,0.2,0.2,0.5,0,0.7c-0.2,0.2-0.5,0.2-0.7,0l-1.8-1.8L1.2,17.3L1.2,17.3z M4.7,17c0.6-0.6,1.7-0.6,2.3,0c0.6,0.6,0.6,1.7,0,2.3
c-0.6,0.6-1.7,0.6-2.3,0C4.1,18.7,4.1,17.7,4.7,17z M6.3,18.6C6.4,18.7,6.4,18.6,6.3,18.6c0.3-0.3,0.3-0.7,0-0.9
c-0.2-0.2-0.6-0.2-0.9,0c-0.2,0.2-0.2,0.6,0,0.9C5.7,18.9,6.1,18.9,6.3,18.6L6.3,18.6z"
		/>
	</svg>
);

export const DimensionsWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Dimensions</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M2.6,17.8L3,18c0.3,0.2,0.4,0.5,0.2,0.8C3.1,19,3,19.1,2.8,19.1
c-0.1,0-0.2,0-0.2-0.1l-1.3-0.7c0,0,0,0,0,0c0,0,0,0,0,0l0,0l-0.1-0.1l0,0c0,0,0,0,0,0c0,0,0,0,0,0v0c0,0,0,0,0,0c0,0,0,0,0,0v0
c0,0,0-0.1,0-0.1V6.2c0,0,0-0.1,0-0.1c0,0,0,0,0,0c0,0,0,0,0,0C1,6,1,6,1,5.9l0.1-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l10.4-5.7
C11.8,0,12,0,12.2,0.1c0.1,0,0.1,0.1,0.2,0.2L13.6,1c0.3,0.2,0.4,0.5,0.2,0.8C13.7,1.9,13.5,2,13.3,2c-0.1,0-0.2,0-0.3-0.1l-0.6-0.3
v8c0,0.3-0.2,0.6-0.5,0.6c-0.3,0-0.5-0.3-0.5-0.6V1.5L2.6,6.3L3,6.5c0.3,0.1,0.4,0.5,0.2,0.7C3.1,7.4,2.9,7.5,2.7,7.5
c-0.1,0-0.2,0-0.2-0.1L2,7.2v9.6l7.4-4.5c0.3-0.2,0.6-0.1,0.8,0.2c0.2,0.3,0.1,0.6-0.2,0.8C10,13.3,2.6,17.8,2.6,17.8z M14.5,2.1
c-0.1,0.3-0.1,0.6,0.2,0.8l1.4,0.8c0.1,0.1,0.2,0.1,0.3,0.1c0.2,0,0.4-0.1,0.5-0.3c0.1-0.3,0.1-0.6-0.2-0.8l-1.4-0.8
C14.9,1.7,14.6,1.8,14.5,2.1z M17.7,4.6c-0.3-0.2-0.4-0.5-0.2-0.8c0.2-0.2,0.5-0.4,0.8-0.2l1.4,0.8C20,4.6,20.1,5,19.9,5.2
c-0.1,0.2-0.3,0.3-0.5,0.3c-0.1,0-0.2,0-0.3-0.1L17.7,4.6L17.7,4.6z M22.5,6c0.1,0,0.2,0,0.3,0.1c0.2,0.1,0.3,0.3,0.2,0.5V8
c0,0.3-0.2,0.6-0.5,0.6C22.2,8.6,22,8.3,22,8V7.5l-0.5,0.3c-0.3,0.1-0.6,0-0.7-0.3c-0.1-0.3,0-0.6,0.2-0.7l0.2-0.1l-0.5-0.2
c-0.3-0.2-0.4-0.5-0.2-0.8c0.2-0.3,0.5-0.4,0.8-0.2L22.5,6L22.5,6z M18.7,7.9l-3.3,1.7c-0.3,0.1-0.4,0.5-0.2,0.7
c0.1,0.2,0.3,0.3,0.5,0.3c0.1,0,0.2,0,0.2-0.1l3.3-1.7c0.3-0.1,0.4-0.5,0.2-0.7C19.3,7.9,19,7.8,18.7,7.9L18.7,7.9z M13.9,11
c0.1,0.3,0,0.6-0.2,0.7l-1,0.5v1.1c0,0.3-0.2,0.6-0.5,0.6c-0.3,0-0.5-0.3-0.5-0.6v-1.1l-1.2-0.6c-0.3-0.1-0.4-0.5-0.2-0.7
c0.1-0.3,0.5-0.4,0.7-0.2l1.2,0.6l1-0.5C13.4,10.6,13.7,10.7,13.9,11z M12.1,15.3c-0.3,0-0.5,0.3-0.5,0.6v3.6c0,0.3,0.2,0.6,0.5,0.6
s0.5-0.3,0.5-0.6v-3.6C12.7,15.6,12.4,15.3,12.1,15.3z M22.5,16.1c-0.3,0-0.5,0.3-0.5,0.6V17l-0.8-0.5c-0.3-0.2-0.6-0.1-0.8,0.2
c-0.2,0.3-0.1,0.6,0.2,0.8l0.9,0.5L21,18.2c-0.3,0.1-0.4,0.5-0.2,0.8c0.1,0.2,0.3,0.3,0.5,0.3c0.1,0,0.2,0,0.2-0.1l1.3-0.7
c0.2-0.1,0.3-0.3,0.3-0.5v-1.4C23.1,16.3,22.8,16.1,22.5,16.1z M18.7,19.4l-3.3,1.7c-0.3,0.1-0.4,0.5-0.2,0.7
c0.1,0.2,0.3,0.3,0.5,0.3c0.1,0,0.2,0,0.2-0.1l3.3-1.7c0.3-0.1,0.4-0.5,0.2-0.7C19.3,19.4,19,19.3,18.7,19.4L18.7,19.4z M12.7,22.6
l0.5-0.2c0.3-0.1,0.6,0,0.9,0.2c0.1,0.3,0,0.6-0.2,0.7L12.5,24h-0.1c0,0,0,0,0,0c0,0,0,0,0,0H12c0,0,0,0,0,0c0,0,0,0,0,0h-0.1
l-1.5-0.8c-0.3-0.1-0.4-0.5-0.2-0.7c0.1-0.3,0.5-0.4,0.7-0.3l0.7,0.4V22c0-0.3,0.2-0.6,0.5-0.6s0.5,0.2,0.5,0.6
C12.7,22,12.7,22.6,12.7,22.6z M22.5,10c-0.3,0-0.5,0.3-0.5,0.6v3.6c0,0.3,0.2,0.6,0.5,0.6c0.3,0,0.5-0.3,0.5-0.6v-3.6
C23.1,10.3,22.8,10,22.5,10L22.5,10z M8.7,10.8L7.2,10C7,9.8,6.9,9.5,7,9.2C7.1,9,7.5,8.9,7.7,9l1.5,0.8c0.3,0.1,0.4,0.5,0.2,0.7
c-0.1,0.2-0.3,0.3-0.5,0.3C8.9,10.8,8.8,10.8,8.7,10.8z M4.1,8.4l1.5,0.8c0.1,0.1,0.2,0.1,0.2,0.1c0.2,0,0.4-0.1,0.5-0.3
c0.1-0.3,0-0.6-0.2-0.7L4.6,7.4C4.3,7.2,4,7.3,3.9,7.6C3.7,7.9,3.9,8.2,4.1,8.4z M9.2,21.3l-1.5-0.8c-0.3-0.1-0.6,0-0.7,0.3
c-0.1,0.3,0,0.6,0.2,0.7l1.5,0.8c0.1,0.1,0.2,0.1,0.2,0.1c0.2,0,0.4-0.1,0.5-0.3C9.6,21.8,9.5,21.5,9.2,21.3z M6.1,19.7
c0.3,0.1,0.4,0.5,0.2,0.7c-0.1,0.2-0.3,0.3-0.5,0.3c-0.1,0-0.2,0-0.2-0.1l-1.5-0.8c-0.3-0.1-0.4-0.5-0.2-0.7
c0.1-0.3,0.5-0.4,0.7-0.3L6.1,19.7z M16,14.8c0.2,0,0.4-0.1,0.5-0.3c0.2-0.3,0.1-0.6-0.2-0.8l-1.7-1c-0.3-0.2-0.6-0.1-0.8,0.2
c-0.2,0.3-0.1,0.6,0.2,0.8l1.7,1C15.8,14.7,15.9,14.8,16,14.8z M19.5,15.6c0.2,0.2,0.4,0.5,0.2,0.8c-0.1,0.2-0.3,0.3-0.5,0.3
c-0.1,0-0.2,0-0.3-0.1l-1.7-1c-0.2-0.2-0.4-0.5-0.2-0.8c0.2-0.2,0.5-0.4,0.8-0.2C17.8,14.6,19.5,15.6,19.5,15.6z"
		/>
	</svg>
);

export const Frame21WebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Frame 21</title>
		<circle opacity="0.3" cx="12" cy="12" r="11.1" />
		<path
			fill="#FFFFFF"
			d="M12,24C5.4,24,0,18.6,0,12C0,5.4,5.4,0,12,0c6.6,0,12,5.4,12,12C24,18.6,18.6,24,12,24z M12,1.8
C6.4,1.8,1.8,6.4,1.8,12S6.4,22.2,12,22.2S22.2,17.6,22.2,12S17.6,1.8,12,1.8z"
		/>
		<path  fill={primary} d="M8.6,8.1l6.8,3.9l-6.8,3.9V8.1z" />
		<path fill="#FFFFFF" d="M7.7,17.5V6.5l9.6,5.5L7.7,17.5z M9.5,9.7v4.7l4-2.3L9.5,9.7z" />
	</svg>
);

export const GrinderWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Grinder</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M15.6,10.4c0.5,0,1,0.3,1.1,0.8c1,0.1,1.7,0.9,1.8,1.8l0.3,4.2h0.6
c0.6,0,1.1,0.5,1.2,1.1l0.1,1c0.1,0.6-0.3,1.1-0.8,1.2l-0.8,0.3l0,0.4c0.5,0.2,0.8,0.6,0.8,1.1v0.4c0,0.7-0.5,1.2-1.2,1.2H5.2
C4.6,24,4,23.5,4,22.8v-0.4c0-0.5,0.3-1,0.8-1.1l0.3-4.1H4.8c-0.9,0-1.6-0.7-1.6-1.6V14c0-0.9,0.7-1.6,1.6-1.6h0.8
c0.3-0.7,0.9-1.1,1.6-1.2c0.2-0.5,0.6-0.8,1.1-0.8h0.1L6.5,6.9C6.2,6.4,6,5.8,6,5.2V2.3C5.5,2.1,5.1,1.6,5.2,1c0.1-0.6,0.6-1,1.2-1
h11.2c0.6,0,1.1,0.4,1.2,1c0.1,0.6-0.2,1.1-0.8,1.3v2.9c0,0.6-0.2,1.1-0.4,1.6l-2.1,3.5C15.5,10.4,15.6,10.4,15.6,10.4z M5.7,21.2
h12.7l-0.7-8.1c0-0.5-0.4-0.9-0.9-1.1v6c0,0.2-0.2,0.4-0.4,0.4c-0.2,0-0.4-0.2-0.4-0.4v-5.2h-1.2v2.8c0,0.3-0.2,0.5-0.4,0.7v0.9
c0,0.4-0.4,0.8-0.8,0.8h-3.2c-0.4,0-0.8-0.4-0.8-0.8v-0.9c-0.2-0.1-0.4-0.4-0.4-0.7v-2.8H8V18c0,0.2-0.2,0.4-0.4,0.4
c-0.2,0-0.4-0.2-0.4-0.4v-6c-0.4,0.1-0.8,0.4-0.9,0.9c0,0.1,0,0.1,0,0.2L5.7,21.2L5.7,21.2z M13.6,16.4h-3.2v0.8h3.2V16.4z
M13.2,13.6H14v-0.8h-4v0.8h0.8c0.2,0,0.4,0.2,0.4,0.4c0,0.2-0.2,0.4-0.4,0.4H10v1.2h4v-1.2h-0.8c-0.2,0-0.4-0.2-0.4-0.4
C12.8,13.8,13,13.6,13.2,13.6z M17.6,0.8H6.4C6.2,0.8,6,1,6,1.2c0,0.2,0.2,0.4,0.4,0.4h11.2c0.2,0,0.4-0.2,0.4-0.4
C18,1,17.8,0.8,17.6,0.8z M6.8,5.2c0,0.4,0.1,0.8,0.3,1.2l2.3,3.9h5.1l2.3-3.9c0.2-0.4,0.3-0.8,0.3-1.2V2.4H6.8
C6.8,2.4,6.8,5.2,6.8,5.2z M15.6,11.2H8.4c-0.2,0-0.4,0.2-0.4,0.4V12h8v-0.4C16,11.4,15.8,11.2,15.6,11.2z M4,14v1.6
c0,0.4,0.4,0.8,0.8,0.8h0.4l0.3-3.2H4.8C4.4,13.2,4,13.6,4,14z M18.8,23.2c0.2,0,0.4-0.2,0.4-0.4v-0.4c0-0.2-0.2-0.4-0.4-0.4H5.2
c-0.2,0-0.4,0.2-0.4,0.4v0.4c0,0.2,0.2,0.4,0.4,0.4C5.2,23.2,18.8,23.2,18.8,23.2z M19.9,18.4c0-0.2-0.2-0.4-0.4-0.4h-0.5l0.2,2
l0.6-0.2c0.2-0.1,0.3-0.2,0.3-0.4C20,19.4,19.9,18.4,19.9,18.4z M8.4,5.2c0,0.1,0,0.3,0.1,0.4l2,3.4c0.1,0.1,0.1,0.3,0,0.4
c-0.1,0.1-0.2,0.2-0.4,0.2c-0.1,0-0.3-0.1-0.4-0.2L7.8,6C7.7,5.8,7.6,5.5,7.6,5.2V3.6c0-0.2,0.2-0.4,0.4-0.4c0.2,0,0.4,0.2,0.4,0.4
L8.4,5.2L8.4,5.2z"
		/>
	</svg>
);

export const InfoWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Grinder</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M22.9,12c0,6-4.9,10.9-10.9,10.9S1.1,18,1.1,12S6,1.1,12,1.1S22.9,6,22.9,12z
M24,12c0,6.6-5.4,12-12,12S0,18.6,0,12S5.4,0,12,0S24,5.4,24,12z M14.3,17.3l0.2-0.7c-0.1,0-0.2,0.1-0.4,0.1
c-0.2,0.1-0.4,0.1-0.5,0.1c-0.3,0-0.5-0.1-0.7-0.2c-0.1-0.1-0.2-0.3-0.2-0.6c0-0.1,0-0.3,0.1-0.5c0-0.2,0.1-0.4,0.1-0.6l0.6-2.2
c0.1-0.2,0.1-0.4,0.1-0.7c0-0.2,0-0.4,0-0.5c0-0.5-0.2-0.8-0.5-1.1c-0.3-0.3-0.8-0.4-1.4-0.4c-0.3,0-0.7,0.1-1.1,0.2
c-0.4,0.1-0.8,0.3-1.2,0.4l-0.2,0.7c0.1,0,0.3-0.1,0.4-0.1c0.2-0.1,0.3-0.1,0.5-0.1c0.3,0,0.5,0.1,0.7,0.2c0.1,0.1,0.2,0.3,0.2,0.6
c0,0.2,0,0.3-0.1,0.5c0,0.2-0.1,0.4-0.1,0.6l-0.6,2.2c-0.1,0.2-0.1,0.4-0.1,0.6c0,0.2,0,0.4,0,0.5c0,0.5,0.2,0.8,0.5,1.1
c0.3,0.3,0.8,0.4,1.4,0.4c0.4,0,0.7-0.1,1-0.2C13.5,17.6,13.9,17.5,14.3,17.3z M14.2,8.4c0.3-0.3,0.4-0.6,0.4-1c0-0.4-0.1-0.7-0.4-1
c-0.3-0.3-0.6-0.4-1-0.4c-0.4,0-0.8,0.1-1,0.4s-0.4,0.6-0.4,1c0,0.4,0.1,0.7,0.4,1c0.3,0.3,0.6,0.4,1,0.4
C13.6,8.8,13.9,8.6,14.2,8.4z"
		/>
	</svg>
);

export const LcdInterfaceWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Grinder</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M1.6,4.3h20.8c0.4,0,0.6,0.3,0.6,0.6v2.7H1V4.9C1,4.6,1.2,4.3,1.6,4.3z M1,8.6v7.3
H23V8.6H1z M1,19.1v-2.2H23v2.2c0,0.4-0.3,0.6-0.6,0.6H1.6C1.2,19.7,1,19.4,1,19.1z M0,4.9C0,4,0.7,3.3,1.6,3.3h20.8
C23.3,3.3,24,4,24,4.9v14.1c0,0.9-0.7,1.6-1.6,1.6H1.6C0.7,20.7,0,20,0,19.1V4.9z M9.9,5.5C9.6,5.5,9.4,5.7,9.4,6s0.2,0.5,0.5,0.5h4
c0.3,0,0.5-0.2,0.5-0.5s-0.2-0.5-0.5-0.5H9.9z M11,18.3c0,0.3,0.2,0.5,0.5,0.5h1.3c0.3,0,0.5-0.2,0.5-0.5s-0.2-0.5-0.5-0.5h-1.3
C11.2,17.8,11,18,11,18.3z M20.8,5.5c-0.3,0-0.5,0.2-0.5,0.5s0.2,0.5,0.5,0.5c0.3,0,0.5-0.2,0.5-0.5S21.1,5.5,20.8,5.5z"
		/>
	</svg>
);

export const ManualControlWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Manual Control</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M4.3,1.1h15.4c1.8,0,3.2,1.4,3.2,3.2v15.4c0,1.8-1.4,3.2-3.2,3.2H4.3
c-1.8,0-3.2-1.4-3.2-3.2V4.3C1.1,2.5,2.5,1.1,4.3,1.1z M19.7,0H4.3C1.9,0,0,1.9,0,4.3v15.4C0,22.1,1.9,24,4.3,24h15.4
c2.4,0,4.3-1.9,4.3-4.3V4.3C24,1.9,22.1,0,19.7,0z M18.9,11.8h1.2c0.3,0,0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5h-1.2
c-0.2,0.7-0.8,1.1-1.5,1.1c-0.7,0-1.3-0.5-1.5-1.1H4c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5h11.9c0.2-0.7,0.8-1.1,1.5-1.1
S18.7,11.1,18.9,11.8L18.9,11.8z M16.8,12.3c0,0.3,0.3,0.6,0.6,0.6c0.3,0,0.6-0.3,0.6-0.6c0-0.3-0.3-0.6-0.6-0.6S16.8,12,16.8,12.3
L16.8,12.3z M4,18.2h1.2c0.2,0.7,0.8,1.1,1.5,1.1s1.3-0.5,1.5-1.1h11.9c0.3,0,0.5-0.2,0.5-0.5s-0.2-0.5-0.5-0.5H8.3
C8.1,16.5,7.5,16,6.7,16c-0.7,0-1.3,0.5-1.5,1.1H4c-0.3,0-0.5,0.2-0.5,0.5C3.4,18,3.7,18.2,4,18.2z M6.7,17.1c0.3,0,0.6,0.3,0.6,0.6
c0,0.3-0.3,0.6-0.6,0.6c-0.3,0-0.6-0.3-0.6-0.6S6.4,17.1,6.7,17.1z M5.2,6.9H4c-0.3,0-0.5-0.2-0.5-0.5C3.4,6,3.7,5.8,4,5.8h1.2
C5.4,5.1,6,4.7,6.7,4.7s1.3,0.5,1.5,1.1h11.9c0.3,0,0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5H8.3C8.1,7.5,7.5,8,6.7,8
C6,8,5.4,7.5,5.2,6.9L5.2,6.9z M7.3,6.3c0-0.3-0.3-0.6-0.6-0.6C6.4,5.7,6.1,6,6.1,6.3c0,0.3,0.3,0.6,0.6,0.6
C7.1,6.9,7.3,6.7,7.3,6.3L7.3,6.3z"
		/>
	</svg>
);

export const ManualWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Manual</title>
		<path fill={primary}
			d="M20.1,5.1H5.2c-0.5,0-0.9-0.1-1.2-0.3v16.8c0,0.7,0.6,1.3,1.2,1.3h14.9V5.1L20.1,5.1z M21.1,4c0.1,0.1,0.2,0.3,0.2,0.4v18.9
c0,0.3-0.3,0.6-0.6,0.6H5.2c-1.4,0-2.5-1.1-2.5-2.5V2.5C2.7,1.1,3.8,0,5.2,0h14.3c0.3,0,0.6,0.3,0.6,0.6v3.2h0.6
C20.9,3.8,21,3.9,21.1,4z M11.4,8.2c0-0.3,0.3-0.6,0.6-0.6c0.3,0,0.6,0.3,0.6,0.6v5.1c0,0.3-0.3,0.6-0.6,0.6c-0.3,0-0.6-0.3-0.6-0.6
V8.2L11.4,8.2z M15.1,10.1c-0.2-0.2-0.2-0.6,0-0.9c0.2-0.2,0.6-0.2,0.9,0c2.2,2.2,2.2,5.8,0,8c-2.2,2.2-5.7,2.2-7.9,0
c-2.2-2.2-2.2-5.8,0-8C8.3,9,8.7,9,8.9,9.2c0.2,0.2,0.2,0.6,0,0.9c-1.7,1.7-1.7,4.5,0,6.3c1.7,1.7,4.4,1.7,6.1,0
C16.8,14.7,16.8,11.9,15.1,10.1L15.1,10.1z M18.8,3.8V1.3H5.2c-0.7,0-1.2,0.6-1.2,1.3c0,0.7,0.6,1.3,1.2,1.3
C5.2,3.8,18.8,3.8,18.8,3.8z"
		/>
	</svg>
);

export const MaterialsWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Materials</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M7.1,2.6C7.1,1.2,8.3,0,9.7,0h11.8c1.4,0,2.6,1.2,2.6,2.6v11.8
c0,1.4-1.2,2.6-2.6,2.6h-4.8v5c0,1.1-0.9,2-2,2H1.8c-1.1,0-2-0.9-2-2V9.2c0-1.1,0.9-2,2-2h5.2V2.6z M8.4,7.2h6.3c1.1,0,2,0.9,2,2
v6.6h4.8c0.7,0,1.3-0.6,1.3-1.3V2.6c0-0.7-0.6-1.3-1.3-1.3H9.7C9,1.3,8.4,1.9,8.4,2.6V7.2z M18.2,4.9c0-0.6,0.5-1,1-1
c0.6,0,1,0.5,1,1c0,0.6-0.5,1-1,1C18.7,6,18.2,5.5,18.2,4.9L18.2,4.9z M12,3.9c-0.6,0-1,0.5-1,1c0,0.6,0.5,1,1,1c0.6,0,1-0.5,1-1
C13,4.4,12.6,3.9,12,3.9L12,3.9z M18.2,12.2c0-0.6,0.5-1,1-1c0.6,0,1,0.5,1,1c0,0.6-0.5,1-1,1C18.7,13.2,18.2,12.7,18.2,12.2
L18.2,12.2z M14.7,8.5H10l-8.8,8.8V22c0,0,0,0.1,0,0.1L14.8,8.5C14.8,8.5,14.8,8.5,14.7,8.5L14.7,8.5z M1.8,8.5h6.3l-7,7V9.2
C1.2,8.8,1.5,8.5,1.8,8.5z M7.4,22.7H2.5L15.4,9.9v4.9L7.4,22.7z M9.3,22.7l6.1-6.1V22c0,0.4-0.3,0.7-0.7,0.7
C14.7,22.7,9.3,22.7,9.3,22.7z"
		/>
	</svg>
);

export const MilkTextureWandWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>milk texture wand</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M10,21c3,0,5.4-0.9,7.1-2.6c0.4-0.4,0.7-0.8,1-1.2c0.3-0.2,0.7-0.3,1.1-0.4
c0.2,0,0.3-0.1,0.5-0.1c1.6-0.3,4-0.8,4.3-3.2c0-0.2,0-0.3,0-0.5l0,0l0,0l0,0v-1.8c0-0.7-0.3-1.5-0.9-1.9C22.2,8.6,21,8.6,20,8.8
C19.5,5.6,15.2,3,10,3C4.5,3,0,5.9,0,9.4c0,0,0,0,0,0.1l0,0c0,2.2,0.3,6.2,2.9,8.9C4.6,20.1,7,21,10,21L10,21z M16.6,17.8
c-1.5,1.6-3.7,2.4-6.6,2.4c-2.8,0-5-0.8-6.6-2.4c-1.6-1.6-2.2-3.8-2.5-5.7c1.6,2.2,5.1,3.7,9,3.7c4,0,7.5-1.5,9.1-3.7
C18.8,14,18.1,16.2,16.6,17.8L16.6,17.8z M23.2,13.3c-0.2,1.8-2.1,2.2-3.7,2.5l-0.1,0c-0.1,0-0.3,0.1-0.4,0.1c-0.1,0-0.2,0-0.3,0.1
c0.7-1.4,1.1-3,1.2-4.5c1.1-0.3,2.2-0.2,2.8,0.2C23.1,12.1,23.3,12.6,23.2,13.3C23.2,13.3,23.2,13.3,23.2,13.3z M22.6,9.8
c0.4,0.3,0.6,0.8,0.6,1.3v0c-0.9-0.7-2.1-0.7-3.2-0.4c0-0.4,0-0.7,0-1C21,9.4,22.1,9.4,22.6,9.8C22.6,9.8,22.6,9.8,22.6,9.8z
M10,3.8c5,0,9.1,2.5,9.2,5.5v0c0,0,0,0,0,0v0c0,3.1-4.1,5.6-9.2,5.6c-5,0-9.1-2.5-9.2-5.5V9.4c0,0,0,0,0,0c0,0,0,0,0,0v0
C0.8,6.3,4.9,3.8,10,3.8z M10,18.6c-2.4,0-4.2-0.6-5.4-1.9c-0.2-0.2-0.4-0.2-0.6,0c-0.2,0.2-0.2,0.4,0,0.6c1.4,1.4,3.4,2.1,6,2.1
c0.2,0,0.4-0.2,0.4-0.4C10.4,18.8,10.2,18.6,10,18.6L10,18.6z M2.6,11.3c-0.1,0-0.3,0-0.4-0.2C1.8,10.7,1.6,10,1.6,9.4
C1.6,7.1,5,4.6,10,4.6c1.8,0,3.6,0.4,5.3,1.1c0.2,0.1,0.3,0.3,0.2,0.5c-0.1,0.2-0.3,0.3-0.5,0.2c-1.6-0.7-3.2-1.1-5-1
c-4.3,0-7.6,2.1-7.6,4c0,0.5,0.2,0.9,0.5,1.3c0.1,0.1,0.1,0.3,0,0.4C2.9,11.2,2.7,11.3,2.6,11.3z M17.6,9.4c0,1.9-3.2,4-7.6,4
c-1.9,0-3.8-0.4-5.5-1.3C4.3,12,4.2,12,4.1,12c-0.1,0.1-0.2,0.2-0.2,0.3c0,0.1,0.1,0.3,0.2,0.3c1.8,1,3.9,1.5,5.9,1.5
c5,0,8.4-2.5,8.4-4.8c0-0.8-0.4-1.6-1-2.2C17.3,7.1,17.1,7,17,7.1c-0.1,0-0.3,0.1-0.3,0.3c0,0.1,0,0.3,0.1,0.4
C17.3,8.2,17.6,8.8,17.6,9.4z M14.8,9.8c0,1.6-2.1,2.8-4.8,2.8c-2.7,0-4.8-1.2-4.8-2.8c0-0.2,0.2-0.4,0.4-0.4C5.8,9.4,6,9.6,6,9.8
c0,1,1.6,1.9,3.6,2V11c-1.3-0.1-2.1-0.6-2.3-1c-0.1-0.1-0.1-0.3,0-0.4c0.1-0.1,0.2-0.2,0.4-0.2c0.1,0,0.3,0.1,0.3,0.2
c0.5,0.4,1,0.6,1.6,0.6V9.3C8,8.8,7.2,8.1,7.2,7.4c0-0.7,0.7-1.2,1.6-1.2c0.4,0,0.8,0.1,1.2,0.3c0.4-0.2,0.8-0.3,1.2-0.3
c0.9,0,1.6,0.5,1.6,1.2c0,0.7-0.8,1.4-2.4,1.9v0.9c0.6,0,1.2-0.2,1.6-0.6c0.1-0.2,0.4-0.2,0.6-0.1c0.2,0.1,0.2,0.3,0.1,0.5
c-0.2,0.4-1.1,0.9-2.3,1v0.8c2-0.1,3.6-1,3.6-2c0-0.2,0.2-0.4,0.4-0.4S14.8,9.6,14.8,9.8z M8.8,7C8.3,7,8,7.2,8,7.4
c0,0.2,0.4,0.7,2,1.2c1.6-0.5,2-1,2-1.2C12,7.2,11.7,7,11.2,7c-0.3,0-0.7,0.1-0.9,0.3c-0.2,0.1-0.4,0.1-0.5,0C9.5,7.1,9.1,7,8.8,7z"
		/>
	</svg>
);

export const PhoneWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>phone</title>
		{/* replacing with one from EH Delete Accout screen */}
		{/* <path fillRule="evenodd" stroke={primary} strokeLinecap="round" strokeLinejoin="round" strokeWidth="1.5" d="M9.04 11.959c1.881 1.88 4.06 3.677 4.92 2.817 1.232-1.232 1.991-2.306 4.709-.121 2.715 2.182.628 3.638-.566 4.831-1.377 1.379-6.512.072-11.586-5.002C1.44 9.408.136 4.274 1.514 2.897c1.195-1.196 2.649-3.28 4.831-.565C8.53 5.048 7.457 5.808 6.224 7.04c-.86.861.936 3.038 2.816 4.919z" clipRule="evenodd" /> */}
		<path fill={primary}
			d="M19,24c-1,0-2.2-0.3-3.6-0.8c-2.9-1.1-5.9-3.2-8.7-6c-2.8-2.8-4.9-5.9-6-8.7c-1-2.8-1-5,0.1-6.1
C1,2.3,1.1,2.2,1.3,2.1c0.9-0.9,2-2.1,3.5-2c1,0,2,0.7,3,1.9c2.7,3.4,1.7,4.8,0,6.4L7.6,8.6c0,0-0.3,0.9,3.3,4.5
c3.6,3.6,4.5,3.3,4.5,3.3l0.2-0.2c1.6-1.6,3.1-2.7,6.4,0c1.2,1,1.8,2,1.9,3c0.1,1.5-1.1,2.6-2,3.5c-0.1,0.1-0.3,0.3-0.4,0.4
C20.9,23.7,20.1,24,19,24z M4.7,1.8C4,1.8,3.2,2.6,2.5,3.3C2.4,3.4,2.2,3.6,2.1,3.7c-0.5,0.5-0.6,2,0.3,4.2c1,2.6,3,5.5,5.6,8.1
c2.6,2.6,5.5,4.6,8.1,5.6c2.3,0.9,3.8,0.8,4.2,0.3c0.1-0.1,0.3-0.3,0.5-0.4c0.7-0.7,1.5-1.5,1.5-2.2c0-0.5-0.4-1.1-1.2-1.7
c-2.4-1.9-2.8-1.4-4.1-0.2l-0.3,0.3c-0.8,0.8-2,0.7-3.5-0.3c-1-0.6-2.1-1.6-3.5-3l0,0c-2.4-2.4-4.9-5.4-3.3-7l0.2-0.2
C7.9,5.8,8.4,5.4,6.5,3C5.8,2.2,5.2,1.8,4.7,1.8C4.7,1.8,4.7,1.8,4.7,1.8z"
		/>
	</svg>
);

export const PlayVideoWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Play Video</title>
		<path  fill={primary} d="M12 23.7C18.4617 23.7 23.7 18.4617 23.7 12C23.7 5.53827 18.4617 0.3 12 0.3C5.53827 0.3 0.3 5.53827 0.3 12C0.3 18.4617 5.53827 23.7 12 23.7Z" fillOpacity="0.3" />
		<path  fill={primary} d="M9.6 7.8432L16.8 12L9.6 16.1568V7.8432Z" />
		<path fill={tertiary} fillRule="evenodd" clipRule="evenodd" d="M0.6 12C0.6 5.70395 5.70395 0.6 12 0.6C18.296 0.6 23.4 5.70395 23.4 12C23.4 18.296 18.296 23.4 12 23.4C5.70395 23.4 0.6 18.296 0.6 12ZM12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0ZM17.4 12L9.3 7.32359V16.6764L17.4 12ZM16.2 12L9.9 15.6372V8.36281L16.2 12Z" />
	</svg>
);

PlayVideoWebSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const PlusWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Plus</title>
		<path  fill={primary} fillRule="evenodd" clipRule="evenodd" d="M15.4,0h-6v8.6H0v6.9h9.4V24h6v-8.6H24V8.6h-8.6V0z" />
	</svg>
);

// WIP - What should stroke be?
export const Polygon4WebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Polygon 4</title>
		<path d="M2.76208 1.84253L21.2378 12.5092L2.76208 23.1758V1.84253Z"
		fill={primary}/>
		<path fillRule="evenodd" clipRule="evenodd" d="M1.60742 0.509187L22.3926 12.5092L1.60742 24.5092V0.509187ZM3.14706 3.17588V21.8425L19.3133 12.5092L3.14706 3.17588Z"
		fill={colors.white}/>
	</svg>
);

export const PowerWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Power</title>
		<path fill={primary} fillRule="evenodd" clipRule="evenodd" d="M20.4852 3.5148C18.219 1.248 15.2052 0 12 0C8.7948 0 5.781 1.248 3.5148 3.5148C1.248 5.781 0 8.7948 0 12C0 15.2052 1.248 18.219 3.5148 20.4852C5.781 22.752 8.7948 24 12 24C15.2052 24 18.219 22.752 20.4852 20.4852C22.752 18.219 24 15.2052 24 12C24 8.7948 22.752 5.781 20.4852 3.5148ZM12 22.5936C6.1584 22.5936 1.4064 17.8416 1.4064 12C1.4064 6.1584 6.1584 1.4064 12 1.4064C17.8416 1.4064 22.5936 6.1584 22.5936 12C22.5936 17.8416 17.8416 22.5936 12 22.5936ZM15.12 4.86C14.7618 4.7034 14.3436 4.8672 14.187 5.226C14.0298 5.5842 14.193 6.0024 14.5524 6.159C16.8786 7.176 18.3822 9.4698 18.3822 12.0036C18.3822 15.5214 15.519 18.3828 12 18.3828C8.481 18.3828 5.6184 15.5202 5.6184 12.003C5.6184 9.4692 7.1214 7.1754 9.4476 6.1584C9.8064 6.0018 9.9702 5.5836 9.8136 5.2248C9.6564 4.8666 9.2382 4.7028 8.8794 4.8594C6.0372 6.102 4.2 8.9058 4.2 12.003C4.2 16.3026 7.6992 19.8 12 19.8C16.3008 19.8 19.8 16.302 19.8 12.003C19.8 8.9058 17.9634 6.102 15.12 4.8594V4.86ZM12.6 9.54C12.6 9.9048 12.3312 10.2 12 10.2C11.6688 10.2 11.4 9.9048 11.4 9.54V4.26C11.4 3.8958 11.6688 3.6 12 3.6C12.3312 3.6 12.6 3.8958 12.6 4.26V9.54Z" />
	</svg>
);

export const ProductRegistrationWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Product Registration</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M1.5,20.8h14.2c-1.4-1-2.3-2.6-2.3-4.3c0-2.9,2.3-5.3,5.3-5.3c0.3,0,0.7,0,1,0.1
V6.6h-6.2v3.8c0,0.3-0.2,0.5-0.4,0.6c-0.3,0.1-0.5,0.1-0.7,0l-2-1.1l-2,1.1c-0.1,0.1-0.3,0.1-0.4,0.1c-0.1,0-0.3-0.1-0.4-0.1
c-0.2-0.2-0.4-0.4-0.4-0.6V6.6H1v13.7C1,20.5,1.2,20.8,1.5,20.8z M18.9,21.7H1.5C0.7,21.7,0,21,0,20.2V5.9c0-0.1,0.1-0.1,0.1-0.1
L2.1,3c0.3-0.5,0.8-0.7,1.3-0.7h13.7c0.5,0,1,0.2,1.4,0.7l2.1,2.9c0.1,0,0.1,0.1,0.1,0.1v5.6c1.9,0.8,3.3,2.7,3.3,4.9
C24,19.3,21.7,21.6,18.9,21.7C18.9,21.7,18.9,21.7,18.9,21.7z M17.7,3.5c-0.1-0.2-0.3-0.3-0.5-0.3h-4.5l0.7,2.4h5.9
C19.2,5.6,17.7,3.5,17.7,3.5z M9,3.2L8.2,6.2h0.1v3.9l2.1-1.2l2.2,1.2V6.2l-0.9-2.9C11.7,3.2,9,3.2,9,3.2z M3.5,3.2
C3.3,3.2,3,3.3,2.9,3.5L1.4,5.6h5.9L8,3.2C8,3.2,3.5,3.2,3.5,3.2z M11.5,5.6c0.3,0,0.5,0.2,0.5,0.5c0,0.3-0.2,0.5-0.5,0.5H9.1
c-0.3,0-0.5-0.2-0.5-0.5c0-0.3,0.2-0.5,0.5-0.5H11.5z M18.7,20.8c2.4,0,4.3-1.9,4.3-4.3s-1.9-4.3-4.3-4.3s-4.3,1.9-4.3,4.3
C14.4,18.8,16.3,20.8,18.7,20.8z M21,15.2L18.3,18c-0.1,0.1-0.2,0.1-0.3,0.1c-0.1,0-0.2-0.1-0.3-0.1l-1.4-1.5
c-0.1-0.1-0.1-0.2-0.1-0.3c0-0.1,0.1-0.2,0.1-0.3c0,0,0.1,0,0.1,0c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.2,0.1,0.3,0.1l1,1
c0.1,0.1,0.3,0.1,0.4,0l2.2-2.3c0,0,0.1,0,0.1,0c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.2,0.1,0.3,0.1c0.1,0.1,0.1,0.2,0.1,0.3
C21.1,15,21.1,15.1,21,15.2z"
		/>
	</svg>
);

export const QuestionWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Question</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M22.9,12c0,6-4.9,10.9-10.9,10.9S1.1,18,1.1,12S6,1.1,12,1.1S22.9,6,22.9,12z
M24,12c0,6.6-5.4,12-12,12S0,18.6,0,12S5.4,0,12,0S24,5.4,24,12z M12.3,6.6C10.5,6.3,8.9,7.4,8.4,9c-0.2,0.5,0.2,1,0.8,1h0.2
c0.4,0,0.7-0.3,0.8-0.6c0.3-0.8,1.1-1.3,2-1.1c0.8,0.2,1.5,1,1.4,1.9c-0.1,0.7-0.5,1.1-1.1,1.5c-0.4,0.3-0.8,0.6-1.1,1.1
c0,0,0,0,0,0c0,0,0,0,0,0l0,0l0,0c-0.1,0.1-0.2,0.3-0.2,0.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c-0.1,0.3-0.2,0.7-0.2,1.1h1.8
c0-0.4,0.1-0.7,0.2-0.9c0,0,0,0,0,0c0,0,0,0,0,0c0.1-0.1,0.2-0.2,0.2-0.3l0,0l0,0c0.1-0.1,0.2-0.2,0.3-0.3c0.1-0.1,0.3-0.2,0.4-0.4
c0.8-0.7,1.5-1.3,1.3-2.8C15,8.1,13.8,6.8,12.3,6.6z M10.9,15.4h1.8v1.8h-1.8V15.4z"
		/>
	</svg>
);

export const ReturnPoliciesWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>return policies</title>
		<path fill={primary}
			d="M16.2,12c0,0.2-0.2,0.4-0.4,0.4H8.2v-0.1c0,0,0,0,0,0v0.1c-0.2,0-0.4-0.2-0.4-0.4c0-0.2,0.2-0.4,0.4-0.4v0.1c0,0,0,0,0,0
v-0.1h7.6h0v0C16,11.5,16.2,11.7,16.2,12z M13.4,6c0.1-0.2,0.1-0.5-0.1-0.6l0,0l0,0l0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0l0,0
c-0.2-0.1-0.5-0.1-0.6,0.1l0,0l0,0c0,0,0,0,0,0l0,0l0,0L11.6,7l-0.3-0.5c-0.1-0.2-0.4-0.2-0.6-0.1c-0.2,0.1-0.2,0.4-0.1,0.6l0,0
c0,0,0,0,0,0l0,0l0.7,1l0,0c0.1,0.1,0.2,0.2,0.3,0.2c0.1,0,0.3-0.1,0.4-0.2l0,0c0,0,0,0,0,0l0,0l0,0l0,0l0,0l0,0l0,0L13.4,6z
M23.3,2.5L23.3,2.5v2.1c0,0.2-0.2,0.4-0.4,0.4h-3.8v16.3c0,0.7-0.3,1.3-0.7,1.8c-0.5,0.5-1.2,0.8-1.9,0.8H3.2v0
c-1.3,0-2.4-1-2.5-2.3c0,0,0,0,0.1,0v0h0l-0.1,0v0v0v-0.2v-2.1c0-0.2,0.2-0.4,0.4-0.4h3.8V2.5h0c0-1.3,1-2.4,2.3-2.5h0H21h0
C22.3,0.1,23.3,1.2,23.3,2.5C23.3,2.5,23.3,2.5,23.3,2.5L23.3,2.5L23.3,2.5z M19,0.8L19,0.8L19,0.8L19,0.8z M14.4,22.8L14.4,22.8
L14.4,22.8C14.4,22.8,14.4,22.8,14.4,22.8z M14.4,22.7L14.4,22.7L14.4,22.7L14.4,22.7L14.4,22.7z M14.4,22.7
C14.4,22.7,14.4,22.7,14.4,22.7C14.4,22.7,14.4,22.7,14.4,22.7L14.4,22.7z M14.4,22.7L14.4,22.7L14.4,22.7L14.4,22.7L14.4,22.7
L14.4,22.7z M14.5,22.9C14.5,22.9,14.5,22.9,14.5,22.9C14.5,22.9,14.5,22.9,14.5,22.9L14.5,22.9L14.5,22.9z M1.5,21.6
c0.1,0.8,0.8,1.5,1.6,1.5v0h0.1v0c0,0,0,0,0,0v0h11.4c0,0,0,0,0,0l0,0l0,0v0l0,0l0,0l0,0l0,0l0,0l0,0c0,0,0-0.1-0.1-0.1l0,0l0,0
c0,0-0.1-0.1-0.1-0.2l0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0l-0.2-0.3h0l0,0l0,0l0,0v0l0,0c-0.1-0.3-0.2-0.6-0.2-0.9v-1.7H1.5v1.5v0v0.1
V21.6z M14.6,23L14.6,23L14.6,23C14.6,23,14.6,23,14.6,23z M16,23.1L16,23.1L16,23.1L16,23.1z M18.3,2.3L18.3,2.3L18.3,2.3L18.3,2.3
L18.3,2.3L18.3,2.3c0-0.5,0.1-0.9,0.4-1.2v0l0,0c0,0,0-0.1,0.1-0.1c0,0,0,0,0-0.1l0,0l0,0l0,0h0c0,0,0,0,0.1-0.1H7.6H7.3
C6.4,1,5.8,1.7,5.8,2.5v16.4h8.7c0.2,0,0.4,0.2,0.4,0.4v2.1c0,0.2,0,0.4,0.1,0.6c0.2,0.5,0.6,0.8,1,1v0l0,0.1l0,0l0,0h0l0,0
c0.1,0,0.3,0.1,0.5,0.1h0c0.5,0,0.9-0.2,1.2-0.5c0.3-0.3,0.4-0.7,0.4-1.1c0,0,0,0,0,0v0h0v-0.1h0V2.7V2.6v0V2.5V2.4L18.3,2.3
L18.3,2.3L18.3,2.3z M18.8,1L18.8,1L18.8,1C18.8,0.9,18.8,1,18.8,1z M18.8,1L18.8,1L18.8,1L18.8,1L18.8,1L18.8,1L18.8,1L18.8,1z
M18.7,1.2L18.7,1.2L18.7,1.2L18.7,1.2z M18.8,1C18.8,1,18.8,1,18.8,1L18.8,1L18.8,1L18.8,1z M18.9,0.9L18.9,0.9L18.9,0.9L18.9,0.9
L18.9,0.9z M19.1,4.2h3.3V2.5c0-0.9-0.7-1.6-1.5-1.7h-0.3c-0.1,0-0.2,0-0.3,0l0,0c-0.5,0.1-0.9,0.4-1.1,0.9l0,0
c-0.1,0.2-0.1,0.4-0.1,0.6v0.3V4.2z M19.1,2.4L19.1,2.4L19.1,2.4L19.1,2.4z M15.8,15.6L15.8,15.6L15.8,15.6l-7.6,0v0.1c0,0,0,0,0,0
v-0.1c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4v-0.1c0,0,0,0,0,0v0.1h7.6c0.2,0,0.4-0.2,0.4-0.4C16.2,15.8,16,15.6,15.8,15.6z
M10.2,9c-1-0.9-1.4-2.4-1.4-4.5c0,0,0,0,0,0c0,0,0,0,0,0h0c0-0.2,0.1-0.4,0.3-0.4l0,0c1-0.4,1.7-0.7,2.7-1.5l0,0l0,0l0,0
c0,0,0,0,0,0l0,0c0.2-0.1,0.4-0.1,0.6,0v0c1,0.8,1.7,1.1,2.7,1.5l0,0c0.2,0.1,0.3,0.2,0.3,0.4c0,2.1-0.5,3.6-1.4,4.5l0,0l0,0l0,0
c0,0,0,0,0,0l0,0l0,0C13,9.8,12.2,9.8,12,9.8H12V9.7c0,0,0,0,0,0v0.1C11.7,9.8,10.9,9.7,10.2,9L10.2,9z M13.3,8.4L13.3,8.4L13.3,8.4
C13.3,8.4,13.3,8.4,13.3,8.4z M13.3,8.4L13.3,8.4L13.3,8.4C13.3,8.4,13.3,8.4,13.3,8.4z M14.4,4.8C14.4,4.8,14.4,4.8,14.4,4.8
L14.4,4.8C14.4,4.8,14.4,4.8,14.4,4.8z M12,9L12,9L12,9L12,9C12,8.9,12,8.9,12,9L12,9L12,9L12,9L12,9L12,9L12,9L12,9L12,9L12,9
c0.1-0.1,0.7,0,1.3-0.6c0.7-0.6,1.1-1.8,1.1-3.5c-0.9-0.3-1.5-0.7-2.4-1.3c-0.9,0.7-1.5,1-2.4,1.3c0,1.7,0.4,2.9,1.1,3.5
C11.3,8.9,11.9,8.9,12,9C12,8.9,12,8.9,12,9z M14.4,4.8C14.4,4.8,14.4,4.8,14.4,4.8C14.4,4.8,14.4,4.8,14.4,4.8L14.4,4.8z M9.1,4.2
L9.1,4.2C9.1,4.2,9.1,4.2,9.1,4.2L9.1,4.2z M15.8,13.5H8.2v0.1c0,0,0,0,0,0v-0.1c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4v-0.1
c0,0,0,0,0,0v0.1h7.6v0c0.2,0,0.4-0.2,0.4-0.4C16.2,13.7,16,13.5,15.8,13.5z"
		/>
	</svg>
);

export const SelectedCircledWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>selected circled</title>
		<path fill={primary}
			d="M12,0C5.4,0,0,5.4,0,12s5.4,12,12,12s12-5.4,12-12S18.6,0,12,0z M16.2,9.7l-4.2,5.8c-0.2,0.2-0.5,0.4-0.8,0.4c0,0,0,0,0,0
c-0.3,0-0.6-0.1-0.8-0.3l-2.5-2.9c-0.4-0.4-0.3-1,0.1-1.4c0.4-0.4,1-0.3,1.4,0.1l1.7,1.9l3.5-4.7c0.3-0.4,1-0.5,1.4-0.2
C16.4,8.6,16.5,9.3,16.2,9.7z"
		/>
	</svg>
);

// Renamed to SupportHub
export const SupportHubWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>Support Hub</title>
		<path  fill={primary} fillRule="evenodd" clipRule="evenodd" d="M18.7416 12.5242C17.7917 13.159 16.6749 13.4978 15.5324 13.4978C14.0011 13.4955 12.533 12.8862 11.4503 11.8033C10.3675 10.7206 9.75814 9.25262 9.75588 7.7213C9.75588 6.57883 10.0947 5.462 10.7294 4.51207C11.3642 3.56212 12.2663 2.82174 13.3218 2.38453C14.3773 1.94732 15.5387 1.83293 16.6592 2.05582C17.7798 2.2787 18.8091 2.82886 19.6169 3.63671C20.4248 4.44457 20.975 5.47384 21.1978 6.59437C21.4208 7.7149 21.3063 8.87635 20.8691 9.93182C20.4319 10.9874 19.6915 11.8895 18.7416 12.5242ZM18.0285 3.98566C17.2895 3.49198 16.421 3.22848 15.5324 3.22848C14.3415 3.23076 13.2001 3.70482 12.358 4.5469C11.5159 5.38897 11.0418 6.53042 11.0395 7.7213C11.0395 8.6099 11.303 9.47853 11.7967 10.2174C12.2903 10.9562 12.9921 11.5321 13.813 11.8721C14.634 12.2122 15.5374 12.3012 16.4089 12.1277C17.2804 11.9544 18.081 11.5265 18.7092 10.8982C19.3376 10.2699 19.7655 9.46932 19.9388 8.5978C20.1122 7.72628 20.0232 6.82293 19.6832 6.00198C19.3432 5.18102 18.7673 4.47934 18.0285 3.98566ZM23.551 13.3095C22.6952 12.2569 21.3772 12.7704 20.7782 13.2068L17.2439 16.0651C17.1433 15.9332 17.0139 15.826 16.8655 15.7519C16.7172 15.6777 16.5539 15.6385 16.3881 15.6372C15.3162 15.6237 14.2619 15.363 13.3073 14.8756L13.2809 14.8592C12.0316 14.0889 10.1688 12.9405 8.06139 14.0198C7.51361 14.2287 6.91255 14.2527 6.34984 14.0883C6.23494 13.5651 5.94506 13.0968 5.52807 12.7608C5.11108 12.4247 4.59193 12.2409 4.05637 12.2398H2.34482C1.72658 12.242 1.13403 12.4874 0.695254 12.9229C0.256493 13.3584 0.0067435 13.9493 0 14.5675V19.7021C0 20.3263 0.247946 20.9249 0.689286 21.3662C1.13064 21.8076 1.72922 22.0555 2.35338 22.0555H4.06493C4.57486 22.0525 5.07006 21.884 5.47597 21.5754C5.88188 21.2667 6.17654 20.8345 6.31561 20.344H8.23255C9.08832 20.6264 13.5213 22.0555 15.1815 22.0555C16.9763 22.0555 19.4372 20.4853 19.8003 20.2536L19.8198 20.2413L23.5082 15.7399C23.8165 15.4128 23.9917 14.9824 23.9997 14.5329C24.0076 14.0835 23.8476 13.6472 23.551 13.3095ZM5.13464 19.7021C5.13465 19.9843 5.02313 20.2551 4.82436 20.4554C4.6256 20.6558 4.35571 20.7696 4.07348 20.7718H2.36194C2.22074 20.7729 2.08072 20.7461 1.94994 20.6929C1.81917 20.6396 1.70024 20.561 1.6 20.4615C1.49975 20.3621 1.4202 20.2438 1.3659 20.1135C1.3116 19.9831 1.28366 19.8433 1.28366 19.7021V14.5675C1.28366 14.2837 1.39636 14.0117 1.59698 13.811C1.79758 13.6104 2.06967 13.4978 2.35338 13.4978H4.06493C4.34863 13.4978 4.62072 13.6104 4.82133 13.811C5.02194 14.0117 5.13464 14.2837 5.13464 14.5675V19.7021ZM22.5581 14.8927L19.0325 19.2229C18.2879 19.6936 16.371 20.7718 15.19 20.7718C14.0091 20.7718 9.95271 19.5566 8.54918 19.0603H6.4183V15.4233C7.16405 15.5762 7.93893 15.4924 8.63476 15.1836C10.0041 14.499 11.2535 15.1836 12.6227 16.0394C13.7368 16.6378 14.978 16.9606 16.2426 16.9808C16.2498 17.0547 16.2498 17.1292 16.2426 17.2033C16.2509 17.3486 16.2509 17.4943 16.2426 17.6397H13.4785C13.3083 17.6397 13.145 17.7073 13.0246 17.8276C12.9042 17.948 12.8367 18.1113 12.8367 18.2815C12.8367 18.4517 12.9042 18.615 13.0246 18.7353C13.145 18.8557 13.3083 18.9234 13.4785 18.9234H15.9945C17.2439 18.9234 17.4921 18.2388 17.5349 17.5113L21.5228 14.2337C21.5228 14.2337 22.2159 13.7374 22.524 14.1139C22.5778 14.1625 22.6214 14.2213 22.6523 14.2869C22.6831 14.3525 22.7007 14.4236 22.7039 14.4961C22.707 14.5685 22.6958 14.6409 22.6708 14.709C22.6458 14.7771 22.6076 14.8395 22.5581 14.8927ZM3.81684 18.6836H2.51607C2.34585 18.6836 2.18259 18.7513 2.06222 18.8717C1.94185 18.9921 1.87424 19.1552 1.87424 19.3255C1.87424 19.4957 1.94185 19.659 2.06222 19.7794C2.18259 19.8998 2.34585 19.9673 2.51607 19.9673H3.81684C3.98706 19.9673 4.15032 19.8998 4.27069 19.7794C4.39106 19.659 4.45867 19.4957 4.45867 19.3255C4.45867 19.1552 4.39106 18.9921 4.27069 18.8717C4.15032 18.7513 3.98706 18.6836 3.81684 18.6836ZM14.2487 6.86539C14.4188 6.86539 14.5821 6.79776 14.7025 6.67739C14.8229 6.55703 14.8905 6.39377 14.8905 6.22356C14.8905 6.09662 14.9281 5.97252 14.9987 5.86697C15.0692 5.76143 15.1694 5.67916 15.2867 5.63058C15.404 5.582 15.533 5.56929 15.6575 5.59406C15.782 5.61882 15.8963 5.67995 15.9861 5.76971C16.0759 5.85948 16.137 5.97383 16.1617 6.09834C16.1866 6.22284 16.1738 6.35189 16.1252 6.46917C16.0767 6.58645 15.9945 6.68669 15.8889 6.75722C15.7834 6.82774 15.6592 6.86539 15.5324 6.86539C15.3628 6.8676 15.2008 6.93594 15.081 7.05582C14.961 7.17571 14.8927 7.33768 14.8905 7.50722V8.57693C14.8905 8.74715 14.9581 8.91041 15.0785 9.03077C15.1988 9.15114 15.3621 9.21876 15.5324 9.21876C15.7025 9.21876 15.8658 9.15114 15.9861 9.03077C16.1065 8.91041 16.1742 8.74715 16.1742 8.57693V8.02068C16.507 7.90299 16.8011 7.69606 17.0243 7.42249C17.2474 7.14893 17.3911 6.81928 17.4396 6.46956C17.4881 6.11985 17.4394 5.76355 17.299 5.43961C17.1586 5.11567 16.9318 4.83657 16.6435 4.63282C16.3552 4.42908 16.0164 4.30852 15.6641 4.28434C15.3119 4.26016 14.9599 4.33328 14.6465 4.49571C14.3329 4.65815 14.0701 4.90364 13.8869 5.20536C13.7035 5.50707 13.6066 5.85338 13.6069 6.20644C13.6046 6.29215 13.6194 6.37746 13.6507 6.45732C13.6819 6.53717 13.7288 6.60997 13.7886 6.67139C13.8484 6.73281 13.92 6.78162 13.999 6.81494C14.078 6.84826 14.1629 6.86541 14.2487 6.86539ZM15.1283 9.68357C15.2479 9.60365 15.3886 9.56098 15.5324 9.56098C15.7253 9.56098 15.9104 9.63763 16.0467 9.77401C16.1832 9.91048 16.2598 10.0954 16.2598 10.2884C16.2598 10.4323 16.2172 10.5728 16.1372 10.6925C16.0573 10.8121 15.9437 10.9053 15.8108 10.9605C15.6778 11.0155 15.5316 11.0299 15.3905 11.0019C15.2494 10.9738 15.1198 10.9044 15.0181 10.8028C14.9163 10.701 14.847 10.5714 14.819 10.4303C14.7909 10.2892 14.8053 10.1429 14.8604 10.01C14.9154 9.87716 15.0086 9.76352 15.1283 9.68357Z" />
	</svg>
);

export const ShareWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>share</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M23.9,12.3c0-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2-0.1-0.3c0-0.1-0.1-0.2-0.1-0.2l0,0
L19.3,7c-0.3-0.3-0.8-0.3-1.1,0c-0.3,0.3-0.3,0.8,0,1.1l3.1,3.1H8.6c-0.4,0-0.8,0.4-0.8,0.8s0.4,0.8,0.8,0.8h12.6l-3.1,3.1
c-0.3,0.3-0.3,0.8,0,1.1c0.3,0.3,0.8,0.3,1.1,0l4.5-4.5C23.8,12.5,23.9,12.4,23.9,12.3z M12.8,3c0,0.4-0.4,0.8-0.8,0.8H3
C2.7,3.8,2.3,4,2,4.3c-0.3,0.3-0.4,0.6-0.4,1v13.4c0,0.4,0.2,0.7,0.4,1c0.3,0.3,0.6,0.4,1,0.4h9c0.4,0,0.8,0.4,0.8,0.8
s-0.4,0.8-0.8,0.8H3c-0.8,0-1.6-0.3-2.2-0.9C0.3,20.3,0,19.5,0,18.7V5.3c0-0.8,0.3-1.6,0.9-2.2C1.5,2.6,2.2,2.2,3,2.2h9
C12.4,2.2,12.8,2.6,12.8,3z"
		/>
	</svg>
);

export const TouchScreenWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>touch Screen</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M8,6c0,0-0.1,0.1-0.1,0.1C7.8,6.2,7.7,6.2,7.5,6.2C7.4,6.2,7.3,6.1,7.3,6
C6.9,5.4,6.7,4.7,6.7,4c0-0.7,0.2-1.4,0.5-2C7.5,1.4,8,0.9,8.6,0.5C9.2,0.2,9.9,0,10.6,0c0.7,0,1.4,0.2,2,0.5
c0.6,0.4,1.1,0.9,1.4,1.5c0.3,0.6,0.5,1.3,0.5,2S14.3,5.4,14,6c0,0.1-0.1,0.1-0.2,0.2c-0.1,0-0.1,0.1-0.2,0.1c-0.1,0-0.2,0-0.2-0.1
c0,0-0.1-0.1-0.1-0.1c0,0-0.1-0.1-0.1-0.2c0-0.1,0-0.1,0-0.2c0-0.1,0-0.1,0.1-0.2c0.3-0.5,0.5-1,0.4-1.6c0-0.5-0.1-0.9-0.3-1.4
c-0.2-0.4-0.5-0.8-0.9-1.1c-0.4-0.3-0.8-0.5-1.3-0.6C10.7,0.8,10.2,0.9,9.8,1S8.9,1.3,8.5,1.7C8.2,2,7.9,2.4,7.8,2.8
C7.6,3.3,7.5,3.7,7.6,4.2c0,0.5,0.2,0.9,0.4,1.3c0,0.1,0.1,0.1,0.1,0.2s0,0.1,0,0.2C8.1,5.9,8,6,8,6L8,6z M19.2,10.3
c0.2,0.1,0.4,0.2,0.6,0.4c0.2,0.2,0.3,0.4,0.4,0.6c0.1,0.2,0.1,0.4,0.1,0.7v4.9c0,1.9-0.7,3.7-2.1,5c-1.3,1.3-3.1,2.1-5,2.1
c-1.3,0-2.5-0.4-3.6-1.1c-1.1-0.7-2-1.6-2.6-2.7l-3.2-5.1c-0.2-0.3-0.2-0.6-0.2-0.9c0.1-0.3,0.2-0.6,0.5-0.8l0.3-0.2
c0.4-0.4,1-0.5,1.6-0.5c0.6,0.1,1.1,0.3,1.5,0.7l1.2,1.4V4.5c0-0.4,0.1-0.8,0.4-1.2c0.3-0.3,0.6-0.6,1-0.7c0.3-0.1,0.6,0,0.9,0
c0.3,0.1,0.5,0.2,0.7,0.4c0.2,0.2,0.3,0.4,0.4,0.6c0.1,0.2,0.1,0.4,0.1,0.7v4.2c0.3-0.2,0.7-0.3,1.1-0.2c0.4,0.1,0.7,0.2,1,0.5
c0.2,0.2,0.3,0.4,0.4,0.7c0.3-0.2,0.6-0.3,1-0.3c0.5,0,0.9,0.2,1.2,0.5c0.2,0.2,0.3,0.4,0.4,0.7c0.3-0.2,0.6-0.3,1-0.3
C18.8,10.2,19,10.3,19.2,10.3L19.2,10.3z M17.6,21.3c1.2-1.2,1.8-2.8,1.8-4.4V12c0-0.2-0.1-0.5-0.3-0.6c-0.2-0.2-0.4-0.3-0.6-0.3
c-0.2,0-0.5,0.1-0.6,0.3c-0.2,0.2-0.3,0.4-0.3,0.6v1.3c0,0.1,0,0.2-0.1,0.3c-0.1,0.1-0.2,0.1-0.3,0.1c-0.1,0-0.2,0-0.3-0.1
c-0.1-0.1-0.1-0.2-0.1-0.3v-2.2c0-0.2-0.1-0.5-0.3-0.6c-0.2-0.2-0.4-0.3-0.6-0.3c-0.2,0-0.5,0.1-0.6,0.3c-0.2,0.2-0.3,0.4-0.3,0.6
v2.2c0,0.1,0,0.2-0.1,0.3s-0.2,0.1-0.3,0.1c-0.1,0-0.2,0-0.3-0.1s-0.1-0.2-0.1-0.3v-3.1c0-0.2-0.1-0.5-0.3-0.6
c-0.2-0.2-0.4-0.3-0.6-0.3c-0.2,0-0.5,0.1-0.6,0.3c-0.2,0.2-0.3,0.4-0.3,0.6v3.1c0,0.1,0,0.2-0.1,0.3s-0.2,0.1-0.3,0.1
c-0.1,0-0.2,0-0.3-0.1c-0.1-0.1-0.1-0.2-0.1-0.3V4.4c0-0.1,0-0.2-0.1-0.3c0-0.1-0.1-0.2-0.2-0.3c-0.1-0.1-0.2-0.2-0.4-0.2
c-0.1,0-0.3,0-0.4,0C10.1,3.6,10,3.7,9.8,3.9C9.7,4.1,9.6,4.3,9.7,4.5v12.7L6.9,14c-0.1-0.1-0.3-0.3-0.5-0.3
c-0.2-0.1-0.4-0.1-0.5-0.1c-0.3,0-0.6,0.1-0.8,0.3l-0.3,0.2c-0.1,0.1-0.1,0.1-0.2,0.2c0,0.1,0,0.2,0,0.3l3.2,5.1
c1.2,2,3.3,3.3,5.4,3.3C14.9,23.1,16.4,22.5,17.6,21.3L17.6,21.3z"
		/>
	</svg>
);

export const TrainingAndSupportWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>training And Support</title>
		<path fill={primary}
			d="M12.3,1.6c0,0.2-0.2,0.4-0.4,0.4c-0.2,0-0.4-0.2-0.4-0.4c0-0.2,0.2-0.4,0.4-0.4C12.1,1.2,12.3,1.4,12.3,1.6z M9.5,9.6
c0.5,0,0.6,0,0.6-0.1c0-0.1-0.1-0.1-0.6-0.1c-0.3,0-0.5,0-0.5,0c-0.1,0-0.1,0.2,0,0.2C9,9.6,9.3,9.6,9.5,9.6z M11.9,9.6
c-0.1-0.1-0.1-0.1,0-0.2c0,0,0.1,0,0.5,0c0.4,0,0.5,0,0.5,0.1c0,0,0.1,0.1,0.1,0.1c0,0,0,0.1-0.1,0.1c-0.1,0.1-0.1,0.1-0.5,0.1
C12,9.6,11.9,9.6,11.9,9.6z M9.9,10.9c0.1-0.1,0.1-0.6,0-0.8C9.8,9.9,9.5,9.8,9.3,10c-0.2,0.1-0.2,0.2-0.2,0.5l0,0v0.3l0.1,0.1
C9.3,11,9.3,11,9.5,11C9.7,11,9.8,11,9.9,10.9z M9.6,10.7l-0.1,0c-0.1,0-0.2,0-0.2-0.3c0-0.2,0-0.3,0.2-0.3l0.1,0V10.7z M12.4,9.9
c-0.1,0-0.2,0-0.3,0v0C12,10,12,10.1,12,10.5c0,0.3,0,0.4,0.2,0.5c0.1,0,0.1,0.1,0.3,0.1c0.1,0,0.2,0,0.3-0.1
c0.1-0.1,0.2-0.2,0.2-0.5C12.8,10.1,12.7,9.9,12.4,9.9z M12.4,10.7L12.4,10.7l-0.1-0.2c0-0.1,0-0.2,0-0.3c0,0,0.2,0,0.2,0l0,0
c0,0,0,0.1,0,0.2C12.5,10.7,12.5,10.8,12.4,10.7z M10.1,13.1c0.2,0.1,0.4,0.2,0.8,0.2c0.4,0,0.7,0,0.9-0.1c0.2-0.1,0.3-0.1,0.3,0
c0,0.1,0,0.1-0.2,0.2c-0.5,0.3-1.4,0.3-1.9,0c-0.2-0.1-0.2-0.2-0.2-0.3C9.9,13.1,10,13.1,10.1,13.1z M20.4,3v18h0v0.7h0v0.7
c0,0.9-0.7,1.6-1.6,1.6H5.1c-0.9,0-1.6-0.7-1.6-1.6V1.6C3.5,0.7,4.3,0,5.1,0h13.7c0.9,0,1.6,0.7,1.6,1.6v0.7h0L20.4,3L20.4,3z
M4.2,21h1.7v-0.8c0-0.9,0-0.9,0.1-0.9s0.1,0,0.1,0.9c0,0.5,0,0.8,0,0.8c0,0,0.2-0.2,0.3-0.4c0-0.1,0.1-0.3,0.2-0.4
c0.1-0.2,0.1-0.3,0.1-1.6c0-0.7,0-1.3,0-1.3c-0.3,0-1,0.3-1.4,0.6c-0.2,0.2-0.6,0.5-0.7,0.7c-0.2,0.4-0.4,0.8-0.4,1.3V21z M7.1,19.6
c0,0,0,0.1,0,0.2c0,0.3-0.2,0.8-0.3,1.1c0,0.1-0.1,0.1-0.1,0.1h8.5c0,0-0.1-0.1-0.2-0.2c0-0.1-0.1-0.1-0.1-0.2
c-0.1-0.2-0.3-0.4-0.4-0.8h0c0,0,0-0.1,0-0.1v-0.1h-3.7C8.7,19.6,7.1,19.6,7.1,19.6z M7.9,19.3l0-1.1l0-1.1l-0.1,0
c-0.1,0-0.2,0-0.4,0.1l-0.3,0v2.2H7.9z M8.1,8L8.1,8C7.9,7.9,7.7,7.7,7.5,7.6c0,0-0.1-0.1-0.1-0.1c0,0-0.1,0.4,0,1.1l0,0.7l0.4-0.5
c0.2-0.3,0.5-0.6,0.5-0.6C8.3,8.2,8.3,8.1,8.1,8z M9.8,8.1c0.7,0,1.5-0.1,2.8-0.3c0.3-0.1,0.6-0.1,0.6-0.1c0.1,0,0.1,0.1,0.9,1
l0.5,0.6l0-0.7c0-0.5,0-0.8,0-1c-0.2-1.3-1.1-2.1-2.5-2.3c-0.3,0-0.9-0.1-1.2-0.1c0,0-0.2,0-0.3,0h0l0,0l-0.4,0
c-0.3,0-0.7,0.1-0.9,0.1c-0.5,0-1.9,0-2.2,0c-0.1,0-0.2,0-0.2,0c0,0,0.1,0.5,0.2,0.8c0.2,0.5,0.5,1,0.8,1.3C8.3,7.9,8.8,8.1,9.8,8.1
z M7.4,9.8l0,1.2c0,1.1,0,1.2,0.1,1.4c0.1,0.4,0.2,0.7,0.4,1c0.5,0.9,1.3,1.5,2.3,1.7c0.3,0.1,0.4,0.1,0.8,0.1c0.4,0,0.5,0,0.8-0.1
c0.4-0.1,0.7-0.2,1.1-0.4c0.5-0.3,0.9-0.6,1.2-1.1c0.2-0.3,0.2-0.4,0.4-0.7c0.2-0.5,0.2-0.7,0.2-1.9V9.8v0l-0.7-0.9
C13.4,8.4,13,8,13,8c0,0-0.2,0-0.3,0.1c-0.1,0-0.4,0.1-0.7,0.1c-1.5,0.2-2.5,0.3-3.2,0.1L8.6,8.3L7.4,9.8z M9.4,17
c-0.1,0-0.1,0,0,0.2c0.2,0.2,0.6,0.4,0.9,0.5c0.2,0.1,0.3,0.1,0.7,0.1c0.5,0,0.7,0,1-0.2c0.3-0.1,0.7-0.4,0.7-0.5c0,0,0,0-0.1,0
c-0.1,0-0.1,0-0.1-0.1c0,0,0-0.4-0.1-0.9c0-0.5,0-0.9,0-0.9c0,0-0.1,0-0.3,0.1c-0.7,0.2-1.5,0.2-2.1,0c0,0-0.1,0-0.1,0
c-0.1,0-0.2-0.1-0.2,0c0,0,0,0.1-0.1,0.3c0,0,0,0.1,0,0.2l0,0.1C9.5,16.7,9.5,16.9,9.4,17C9.4,17,9.4,17,9.4,17L9.4,17z M9,17.2
C8.9,17,8.8,17,8.8,17c0,0-0.2,0-0.3,0l-0.2,0v2.3h5.5V17l-0.6,0l-0.2,0.2c-0.3,0.4-0.8,0.6-1.3,0.8c-0.4,0.1-1,0.1-1.3,0
C9.8,17.8,9.4,17.5,9,17.2z M14.8,17.1L14.8,17.1c0,0-0.2,0-0.4-0.1c-0.2,0-0.3,0-0.4,0c0,0,0,0.1,0,1.1l0,1.1h0.4l0-0.5
c0-0.3,0-0.6,0-0.6c0,0,0.1,0,0.2,0h0.2v-0.5C14.8,17.2,14.8,17.2,14.8,17.1z M16.3,17.7L16.3,17.7c0-0.1-0.5-0.3-0.8-0.4l-0.1,0
c-0.1,0-0.2-0.1-0.2-0.1l-0.1,0l0,0.9l0.9,0c0.5,0,0.9,0,0.9,0c0,0-0.1-0.1-0.1-0.1c-0.1-0.1-0.1-0.1-0.2-0.1
c-0.1,0.1-0.2,0-0.2-0.1C16.4,17.8,16.3,17.7,16.3,17.7z M14.7,18.4l0,0.7l0,0.7l0.1,0.2c0.1,0.3,0.2,0.3,0.5,0.7l0.2,0.2h2.3
l0.3-0.3c0.2-0.2,0.3-0.4,0.4-0.5h0c0.2-0.4,0.2-0.5,0.2-1.2v-0.6H14.7z M19.7,21.7H4.2v0.7c0,0.5,0.4,0.9,0.9,0.9h13.7
c0.5,0,0.9-0.4,0.9-0.9V21.7z M19.7,19.9c-0.2,0.2-0.5,0.3-0.8,0.4c-0.2,0-0.2,0.1-0.3,0.2c-0.1,0.1-0.2,0.3-0.4,0.4h1.5V19.9z
M19.7,18.9C19.7,18.8,19.7,18.8,19.7,18.9c-0.2-0.2-0.4-0.2-0.6-0.1l-0.1,0l0,0.5c0,0.4,0,0.5-0.1,0.6c0,0.1,0,0.1,0,0.1
c0.1,0,0.4-0.2,0.6-0.3c0.1-0.1,0.1-0.2,0.2-0.2V18.9z M19.7,3H4.2v15.8c0,0,0,0,0,0c0.2-0.5,0.6-0.9,1.1-1.2c0.4-0.3,1-0.5,1.7-0.6
c0.3-0.1,1.1-0.1,2-0.2l0.2,0l0-0.5l0-0.5l0,0l0-0.4l0-0.4l-0.2-0.1C8.7,14.7,8.3,14.4,8,14c-0.3-0.4-0.6-1-0.8-1.5l-0.1-0.3l-0.1,0
c-0.3,0-0.7-0.3-0.9-0.7c-0.3-0.5-0.3-1.1,0-1.5c0.1-0.2,0.2-0.3,0.4-0.3c0.1-0.1,0.2-0.1,0.4,0l0.2,0v-1c0-0.8,0-1,0.1-1.2l0.1-0.3
L7.1,7.1C7,6.9,6.8,6.4,6.7,6.1v0C6.5,5.6,6.5,5.1,6.5,5c0,0,0,0,0.5,0c0.5,0.1,2,0.1,3,0c1.2-0.1,2-0.1,2.6,0.1
c1.2,0.4,1.9,1.2,2.1,2.4c0,0.2,0.1,0.4,0.1,1.2v0.9l0.2,0c0.3,0,0.4,0,0.6,0.1c0.3,0.2,0.4,0.6,0.4,1c0,0.5-0.3,0.9-0.6,1.2
c-0.1,0.1-0.4,0.2-0.5,0.2c-0.1,0-0.1,0-0.1,0.2c-0.1,0.5-0.4,1-0.7,1.4c-0.3,0.4-0.9,0.9-1.3,1.1c-0.2,0.1-0.2,0.1-0.2,0.2
c0,0,0,0.3,0,0.5l0,0.3l0,0.7l0.2,0c0.8,0,1.7,0.1,2,0.2c0.5,0.1,0.9,0.2,1.3,0.4c0.1,0.1,0.2,0.1,0.2,0.1c0,0,0-0.1,0.1-0.2
c0-0.1,0.1-0.3,0.2-0.4c0.1-0.2,0.1-0.3,0.1-0.5c0-0.3-0.1-0.4-0.3-0.6c-0.2-0.2-0.1-0.3,0-0.3c0.1,0,0.3,0.3,0.4,0.5
C17,15.9,17,16,17,16.2c0,0.3,0,0.3-0.1,0.6c-0.2,0.3-0.2,0.5-0.2,0.6c0,0.1,0,0.1,0.1,0.2l0.1,0.1l0-0.1c0-0.2,0.1-0.4,0.3-0.6
c0.3-0.3,0.4-0.5,0.4-0.8c0-0.2,0-0.2-0.1-0.4c-0.1-0.1-0.2-0.3-0.3-0.4c-0.1-0.1-0.2-0.2-0.2-0.2c0-0.1,0.1-0.1,0.2-0.1
c0.1,0,0.3,0.2,0.4,0.4c0.2,0.3,0.2,0.4,0.2,0.8c0,0.2,0,0.3-0.1,0.5c-0.1,0.1-0.2,0.3-0.3,0.4c-0.2,0.2-0.3,0.3-0.3,0.5
c0,0.1-0.1,0.2-0.2,0.1c0,0,0.1,0.1,0.2,0.2l0.2,0.2h0.8c0.5,0,0.8,0,0.8,0c0,0,0.1,0.1,0.1,0.2c0,0.1,0,0.1,0.1,0.1
c0.1,0,0.5-0.1,0.6,0c0,0,0,0,0.1,0V3z M14.9,11.9c0.6-0.1,1-1.1,0.7-1.6c-0.1-0.2-0.2-0.3-0.3-0.3c-0.1-0.1-0.3,0-0.4,0L14.8,10
L14.9,11.9L14.9,11.9z M7.1,10L7.1,10C6.9,9.9,6.7,9.9,6.5,10c-0.2,0.1-0.3,0.4-0.3,0.7c0,0.3,0.1,0.7,0.3,0.9
c0.1,0.1,0.3,0.3,0.5,0.3l0.1,0L7.1,10L7.1,10z M19.7,1.6c0-0.5-0.4-0.9-0.9-0.9H5.1c-0.5,0-0.9,0.4-0.9,0.9v0.7h15.5V1.6z
M11.9,22.8c0.2,0,0.4-0.2,0.4-0.4c0-0.2-0.2-0.4-0.4-0.4c-0.2,0-0.4,0.2-0.4,0.4C11.5,22.6,11.7,22.8,11.9,22.8z"
		/>
	</svg>
);

export const VoltageWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>voltage</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M13.6,2.9l10.1,16.1c0.4,0.6,0.4,1.3,0.1,1.9c-0.3,0.6-1,1-1.6,1H1.9
c-0.7,0-1.3-0.4-1.6-1c-0.3-0.6-0.3-1.3,0.1-1.9L10.4,2.9c0.3-0.6,0.9-0.9,1.6-0.9C12.7,2.1,13.2,2.4,13.6,2.9z M22.1,21
c0.3,0,0.7-0.2,0.8-0.5c0.2-0.3,0.2-0.7,0-1L12.8,3.4C12.6,3.2,12.3,3,12,3c-0.3,0-0.6,0.2-0.8,0.4L1.1,19.6c-0.2,0.3-0.2,0.7,0,1
C1.2,20.8,1.5,21,1.9,21L22.1,21L22.1,21z M12,4.7c0.2,0,0.3,0.1,0.4,0.2l8.9,14.2c0.1,0.1,0.1,0.3,0,0.5c-0.1,0.1-0.2,0.2-0.4,0.2
h-6.8c-0.3,0-0.5-0.2-0.5-0.5s0.2-0.5,0.5-0.5h6L12,6L3.9,18.9h6c0.3,0,0.5,0.2,0.5,0.5s-0.2,0.5-0.5,0.5H3.1
c-0.2,0-0.3-0.1-0.4-0.2c-0.1-0.1-0.1-0.3,0-0.5l8.9-14.2C11.7,4.8,11.8,4.7,12,4.7z M12.4,19.2c-0.1-0.2-0.3-0.3-0.5-0.3
c-0.2,0-0.4,0.2-0.4,0.3c-0.1,0.2,0,0.4,0.2,0.5c0.2,0.1,0.4,0.1,0.5,0C12.5,19.7,12.5,19.4,12.4,19.2z M14.9,13L12,17.9
c-0.1,0.1-0.2,0.2-0.4,0.2c0,0-0.1,0-0.1,0c-0.2-0.1-0.3-0.2-0.3-0.5v-3H9.5c-0.2,0-0.3-0.1-0.4-0.2C9,14.3,9,14.2,9.1,14L12,9.1
c0.1-0.2,0.3-0.3,0.5-0.2c0.2,0.1,0.3,0.2,0.3,0.5v3h1.7c0.2,0,0.3,0.1,0.4,0.2C15,12.7,15,12.9,14.9,13z M11.9,11.1l-1.6,2.7h1.3
c0.3,0,0.5,0.2,0.5,0.5V16l1.6-2.7h-1.3c-0.3,0-0.5-0.2-0.5-0.5C11.9,12.8,11.9,11.1,11.9,11.1z"
		/>
	</svg>
);

export const Warranty1WebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>warranty 1</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M12.3,0.1c3.2,1.7,6.5,3.1,10,4.1c0.2,0.1,0.4,0.3,0.4,0.5v4.6
c0,2.1-0.4,4.2-1.2,6.1c-0.8,1.9-2,3.6-3.5,5c-1.1,1.1-2.4,2-3.7,2.7c-0.2,0.1-1.4,0.8-2.1,0.9c0,0-0.1,0-0.1,0c0,0-0.1,0-0.1,0
c-0.7-0.1-1.9-0.8-2.1-0.9c-1.3-0.7-2.6-1.6-3.7-2.7c-1.5-1.5-2.7-3.2-3.5-5c-0.8-1.9-1.2-3.9-1.2-6.1V4.7c0-0.2,0.2-0.5,0.4-0.5
c3.4-1,6.8-2.4,10-4.1C11.9,0,12.1,0,12.3,0.1L12.3,0.1z M11,15.1l6.3-6.3c0.2-0.2,0.2-0.4,0.2-0.6c0-0.2-0.1-0.4-0.2-0.6
c-0.2-0.2-0.4-0.2-0.6-0.2c-0.2,0-0.4,0.1-0.6,0.2l-5.2,5.2c-0.3,0.3-0.8,0.3-1,0l-2.2-2.2c-0.2-0.2-0.4-0.2-0.6-0.2
c-0.2,0-0.4,0.1-0.6,0.2c-0.2,0.2-0.2,0.4-0.2,0.6c0,0.2,0.1,0.4,0.2,0.6l3.3,3.3c0.2,0.2,0.4,0.2,0.6,0.2
C10.7,15.3,10.9,15.2,11,15.1L11,15.1z"
		/>
	</svg>
);

export const Warranty2WebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>warranty 2</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M22.4,4.2c-3.5-0.9-7-2.3-10.2-4.1c-0.1-0.1-0.3-0.1-0.5,0
C8.5,1.9,5.2,3.2,1.6,4.2C1.4,4.2,1.2,4.4,1.2,4.6v5.2c0,5.3,2.6,8.8,4.8,10.9c2.3,2.2,5,3.4,6,3.4c1,0,3.7-1.2,6-3.4
c2.2-2,4.8-5.5,4.8-10.9V4.6C22.8,4.4,22.6,4.2,22.4,4.2z M17.4,20c-1.3,1.2-2.5,2-3.4,2.4c-1,0.5-1.8,0.7-2,0.7c-0.2,0-1-0.2-2-0.7
c-0.8-0.4-2.1-1.2-3.4-2.4c-2-1.9-4.5-5.2-4.5-10.2V5C5.6,4,8.9,2.7,12,1c3.1,1.7,6.5,3.1,9.8,4v4.8C21.8,14.8,19.4,18.1,17.4,20
L17.4,20z M20.8,5.4c-3-0.9-5.9-2-8.6-3.5c-0.1-0.1-0.3-0.1-0.4,0C9.1,3.4,6.2,4.6,3.2,5.4C3,5.5,2.9,5.7,2.9,5.9v3.9
c0,1.8,0.3,3.6,1,5.2c0.7,1.6,1.7,3,3,4.3c0.9,0.9,2,1.7,3.2,2.3c0.2,0.1,1.2,0.7,1.8,0.8c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1,0
c0.6-0.1,1.6-0.7,1.8-0.8c1.1-0.6,2.2-1.4,3.2-2.3c1.3-1.3,2.3-2.7,3-4.3c0.7-1.6,1-3.3,1-5.2V5.9C21.1,5.6,21,5.5,20.8,5.4
L20.8,5.4z M20.2,9.8c0,3.4-1.3,6.4-3.8,8.8c-0.9,0.9-1.9,1.6-3,2.2C13,21,12.4,21.3,12,21.4c-0.4-0.1-1-0.4-1.5-0.7
c-1-0.6-2.1-1.3-3-2.2c-2.5-2.4-3.8-5.4-3.8-8.8V6.2c2.8-0.8,5.6-2,8.2-3.3c2.6,1.4,5.4,2.5,8.2,3.3C20.2,6.2,20.2,9.8,20.2,9.8z
M15.1,7.8c0.4,0,0.8,0.2,1,0.4c0.3,0.3,0.4,0.6,0.4,1c0,0.4-0.2,0.8-0.4,1l-4,4c-0.3,0.3-0.6,0.4-1,0.4c-0.4,0-0.8-0.2-1-0.4
l-2.1-2.1c-0.6-0.6-0.6-1.5,0-2.1c0.3-0.3,0.6-0.4,1-0.4c0.4,0,0.8,0.2,1,0.4l1.1,1.1l3-3C14.3,7.9,14.7,7.8,15.1,7.8L15.1,7.8z
M11.4,13.6l4-4c0.1-0.1,0.2-0.2,0.2-0.4c0-0.1-0.1-0.3-0.2-0.4s-0.2-0.2-0.4-0.2s-0.3,0.1-0.4,0.2l-3.3,3.3c-0.2,0.2-0.5,0.2-0.7,0
l-1.4-1.4c-0.1-0.1-0.2-0.2-0.4-0.2c-0.1,0-0.3,0.1-0.4,0.2c-0.1,0.1-0.2,0.2-0.2,0.4c0,0.1,0.1,0.3,0.2,0.4l2.1,2.1
c0.1,0.1,0.2,0.2,0.4,0.2C11.2,13.8,11.3,13.7,11.4,13.6z"
		/>
	</svg>
);

export const WarrantyWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>warranty</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M22.4,4.2c-3.5-0.9-7-2.3-10.2-4.1c-0.1-0.1-0.3-0.1-0.5,0
C8.5,1.9,5.2,3.2,1.6,4.2C1.4,4.2,1.2,4.4,1.2,4.6v5.2c0,5.3,2.6,8.8,4.8,10.9c2.3,2.2,5,3.4,6,3.4c1,0,3.7-1.2,6-3.4
c2.2-2,4.8-5.5,4.8-10.9V4.6C22.8,4.4,22.6,4.2,22.4,4.2z M17.4,20c-1.3,1.2-2.5,2-3.4,2.4c-1,0.5-1.8,0.7-2,0.7c-0.2,0-1-0.2-2-0.7
c-0.8-0.4-2.1-1.2-3.4-2.4c-2-1.9-4.5-5.2-4.5-10.2V5C5.6,4,8.9,2.7,12,1c3.1,1.7,6.5,3.1,9.8,4v4.8C21.8,14.8,19.4,18.1,17.4,20
L17.4,20z M20.8,5.4c-3-0.9-5.9-2-8.6-3.5c-0.1-0.1-0.3-0.1-0.4,0C9.1,3.4,6.2,4.6,3.2,5.4C3,5.5,2.9,5.7,2.9,5.9v3.9
c0,1.8,0.3,3.6,1,5.2c0.7,1.6,1.7,3,3,4.3c0.9,0.9,2,1.7,3.2,2.3c0.2,0.1,1.2,0.7,1.8,0.8c0,0,0.1,0,0.1,0c0,0,0.1,0,0.1,0
c0.6-0.1,1.6-0.7,1.8-0.8c1.1-0.6,2.2-1.4,3.2-2.3c1.3-1.3,2.3-2.7,3-4.3c0.7-1.6,1-3.3,1-5.2V5.9C21.1,5.6,21,5.5,20.8,5.4
L20.8,5.4z M20.2,9.8c0,3.4-1.3,6.4-3.8,8.8c-0.9,0.9-1.9,1.6-3,2.2C13,21,12.4,21.3,12,21.4c-0.4-0.1-1-0.4-1.5-0.7
c-1-0.6-2.1-1.3-3-2.2c-2.5-2.4-3.8-5.4-3.8-8.8V6.2c2.8-0.8,5.6-2,8.2-3.3c2.6,1.4,5.4,2.5,8.2,3.3C20.2,6.2,20.2,9.8,20.2,9.8z
M15.1,7.8c0.4,0,0.8,0.2,1,0.4c0.3,0.3,0.4,0.6,0.4,1c0,0.4-0.2,0.8-0.4,1l-4,4c-0.3,0.3-0.6,0.4-1,0.4c-0.4,0-0.8-0.2-1-0.4
l-2.1-2.1c-0.6-0.6-0.6-1.5,0-2.1c0.3-0.3,0.6-0.4,1-0.4c0.4,0,0.8,0.2,1,0.4l1.1,1.1l3-3C14.3,7.9,14.7,7.8,15.1,7.8L15.1,7.8z
M11.4,13.6l4-4c0.1-0.1,0.2-0.2,0.2-0.4c0-0.1-0.1-0.3-0.2-0.4s-0.2-0.2-0.4-0.2s-0.3,0.1-0.4,0.2l-3.3,3.3c-0.2,0.2-0.5,0.2-0.7,0
l-1.4-1.4c-0.1-0.1-0.2-0.2-0.4-0.2c-0.1,0-0.3,0.1-0.4,0.2c-0.1,0.1-0.2,0.2-0.2,0.4c0,0.1,0.1,0.3,0.2,0.4l2.1,2.1
c0.1,0.1,0.2,0.2,0.4,0.2C11.2,13.8,11.3,13.7,11.4,13.6z"
		/>
	</svg>
);

export const WatchvideoWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<title>watch video</title>
		<path fill={primary}
			fillRule="evenodd"
			clipRule="evenodd"
			d="M12,24c6.6,0,12-5.4,12-12S18.6,0,12,0S0,5.4,0,12S5.4,24,12,24z M9.3,16.7
l8.1-4.7L9.3,7.3V16.7z"
		/>
	</svg>
);

export const OrderPartsWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 30 30"} width={size} height={size}>
		<title>Order Parts</title>
		<path  fill={primary} fillRule="evenodd" clipRule="evenodd" d="M14.2935 2.49837C13.9639 2.49837 13.6489 2.8134 13.6489 3.14301V7.37831C13.6495 7.39123 13.6499 7.40424 13.6499 7.41732C13.6499 7.43041 13.6495 7.44342 13.6489 7.45634V7.69619C13.6489 8.25344 13.537 8.70051 13.4456 9.0663L13.4407 9.08588C13.424 9.15241 13.3983 9.21634 13.3643 9.27588L13.2614 9.4559H15.3679L15.2121 9.16381C15.1833 9.10976 15.1612 9.05239 15.1464 8.99296L15.138 8.95956C15.0505 8.61001 14.9382 8.16126 14.9382 7.69619V7.43599L14.9379 7.41732L14.9382 7.39866V3.14301C14.9382 2.8134 14.6231 2.49837 14.2935 2.49837ZM12.1505 3.14301V6.66814H10.4847C6.26115 6.66814 2.8593 10.07 2.8593 14.2935V19.6968C1.82923 19.8415 1 20.7587 1 21.8199C1 22.8488 1.77943 23.7423 2.76565 23.9277V24.8867C2.76565 26.4155 4.03029 27.6801 5.55912 27.6801H5.82566C6.08755 28.204 6.61979 28.6093 7.32373 28.6093H8.25295C8.72891 28.6093 9.14217 28.3789 9.42154 28.0995C9.53937 27.9817 9.64849 27.84 9.73529 27.6799H18.2117C18.8042 28.2542 19.6163 28.6094 20.5192 28.6094H25.6299C27.6525 28.6094 29.2169 26.8287 28.9753 24.8883C28.7904 23.3623 28.582 21.4171 28.4197 19.7944C28.3385 18.9831 28.2692 18.2543 28.2201 17.7005C28.1956 17.4234 28.1763 17.192 28.1632 17.0168C28.1567 16.9291 28.1519 16.8576 28.1487 16.8025L28.1467 16.7659L28.1454 16.7361L28.1448 16.7164L28.1447 16.7118C28.1446 16.7101 28.1446 16.7095 28.1446 16.7096C28.1446 16.1896 28.3962 15.7766 28.6799 15.3109L28.7464 15.2014C29.0286 14.8513 29.0049 14.4202 28.8554 14.1212C28.7189 13.8482 28.4037 13.5444 27.953 13.5444H25.6925C25.3183 9.67489 22.0738 6.66814 18.1031 6.66814H16.4365V3.14301C16.4365 1.98587 15.4507 1 14.2935 1C13.1364 1 12.1505 1.98587 12.1505 3.14301ZM7.1446 26.9309L7.14447 26.9451C7.1482 26.9804 7.1654 27.0239 7.19774 27.0585C7.22759 27.0905 7.26536 27.1109 7.32373 27.1109H8.25037C8.25279 27.1105 8.26186 27.1086 8.27733 27.1014C8.30186 27.0899 8.33245 27.0696 8.36204 27.04C8.39163 27.0104 8.41197 26.9798 8.42342 26.9553C8.43064 26.9398 8.43255 26.9307 8.43299 26.9283V25.0725H7.14369V26.8937C7.1443 26.9061 7.1446 26.9185 7.1446 26.9309ZM5.28603 23.9629L5.7648 24.4417C5.68847 24.6314 5.64533 24.8428 5.64533 25.0725V26.1818H5.55912C4.85782 26.1818 4.26402 25.588 4.26402 24.8867V23.9629H5.28603ZM9.81251 24.4433L10.2928 23.9629H12.4352C13.5924 23.9629 14.5782 22.9771 14.5782 21.8199C14.5782 20.6628 13.5924 19.6769 12.4352 19.6769H9.92036C9.92756 19.6181 9.93135 19.558 9.93135 19.497C9.93135 18.4778 9.0818 17.8186 8.25295 17.8186H7.32373C6.49488 17.8186 5.64533 18.4778 5.64533 19.497C5.64533 19.557 5.64878 19.617 5.65557 19.6769H4.35767V14.2935C4.35767 10.8975 7.08867 8.16651 10.4847 8.16651H12.1104C12.0859 8.31389 12.0517 8.46126 12.0119 8.62261L11.32 9.83339C11.1875 10.0653 11.1884 10.3501 11.3225 10.5811C11.4565 10.8121 11.7034 10.9543 11.9705 10.9543H16.6166C16.8797 10.9543 17.1236 10.8162 17.259 10.5905C17.3944 10.3649 17.4014 10.0847 17.2776 9.85253L16.5778 8.54043C16.544 8.40364 16.5147 8.2805 16.4917 8.16651H18.1031C21.2454 8.16651 23.8184 10.5047 24.1853 13.5444H18.8466C18.4557 13.5444 18.1305 13.845 18.0998 14.2347L17.9782 15.7744H16.9893C15.8321 15.7744 14.8462 16.7603 14.8462 17.9174V22.0989C14.8462 23.2561 15.8321 24.2419 16.9893 24.2419H17.3097L17.2661 24.7941C17.1862 25.2777 17.214 25.747 17.3298 26.1815H9.93135V25.0725C9.93135 24.8434 9.88844 24.6325 9.81251 24.4433ZM8.25097 19.6769H7.31631C7.26212 19.6752 7.22631 19.6553 7.19774 19.6246C7.16089 19.5852 7.14369 19.5341 7.14369 19.497C7.14369 19.4386 7.16409 19.4008 7.19607 19.371C7.23556 19.3341 7.28657 19.3169 7.32373 19.3169H8.25295C8.29011 19.3169 8.34112 19.3341 8.38061 19.371C8.41213 19.4004 8.4324 19.4375 8.43297 19.4945C8.43252 19.497 8.43057 19.506 8.42342 19.5214C8.41197 19.5459 8.39163 19.5765 8.36204 19.6061C8.33245 19.6357 8.30186 19.656 8.27733 19.6675C8.26293 19.6742 8.25408 19.6763 8.25097 19.6769ZM7.30344 21.1753H3.63008C3.6229 21.1755 3.61571 21.1756 3.60849 21.1756C3.60126 21.1756 3.59407 21.1755 3.58689 21.1753H3.14301C2.8134 21.1753 2.49837 21.4903 2.49837 21.8199C2.49837 22.1495 2.8134 22.4646 3.14301 22.4646H12.4352C12.7648 22.4646 13.0799 22.1495 13.0799 21.8199C13.0799 21.4903 12.7648 21.1753 12.4352 21.1753H8.27117L8.25295 21.1754H7.32373L7.30344 21.1753ZM17.428 22.7436L17.8599 17.2728H16.9893C16.6596 17.2728 16.3446 17.5878 16.3446 17.9174V22.0989C16.3446 22.4285 16.6596 22.7436 16.9893 22.7436H17.428ZM18.7572 24.9457L19.539 15.0428H27.0995C26.8702 15.479 26.6463 16.051 26.6463 16.7096C26.6463 16.9904 26.769 18.3456 26.9288 19.9435C27.0917 21.5726 27.3013 23.5296 27.488 25.0698L27.4884 25.0726C27.6174 26.105 26.7662 27.111 25.6299 27.111H20.5192C19.3944 27.111 18.5522 26.1271 18.7481 25.0169C18.7523 24.9933 18.7553 24.9696 18.7572 24.9457ZM22.6502 20.2403C22.6502 19.8265 22.9857 19.4911 23.3994 19.4911H25.4437C25.8575 19.4911 26.1929 19.8265 26.1929 20.2403C26.1929 20.654 25.8575 20.9894 25.4437 20.9894H23.3994C22.9857 20.9894 22.6502 20.654 22.6502 20.2403ZM23.3994 22.4649C22.9857 22.4649 22.6502 22.8003 22.6502 23.2141C22.6502 23.6278 22.9857 23.9633 23.3994 23.9633H25.4437C25.8575 23.9633 26.1929 23.6278 26.1929 23.2141C26.1929 22.8003 25.8575 22.4649 25.4437 22.4649H23.3994Z"/>
	</svg>
);

export const BiltAppWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<path  fill={primary} d="M6,1.7h12L24,12l-6,10.3H6L0,12L6,1.7z M6.8,21h10.5l5.2-9l-5.2-9H6.8l-5.2,9L6.8,21z M7.3,11.7c0-2.6,2.1-4.7,4.7-4.7c2.6,0,4.7,2.1,4.7,4.7v0c0,2.6-2.1,4.7-4.7,4.7C9.4,16.4,7.3,14.3,7.3,11.7z M8.6,11.7c0,1.9,1.5,3.4,3.4,3.4c1.9,0,3.4-1.5,3.4-3.4c0-1.9-1.5-3.4-3.4-3.4C10.1,8.4,8.6,9.9,8.6,11.7z" />
	</svg>
);

export const LiveEventWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<rect fill={tertiary} x="2.9" y="5.9" width="17.7" height="10.8" />
		<path fill={secondary} d="M20.3,6.3l-8.6,10.2h8.9l0-10.2H20.3z" />
		<path fill={primary} d="M21.6,5.7c0.1,0.1,0.1,0.3,0.1,0.4v11.5H1.9V6.2C1.9,6,2,5.9,2,5.7c0.1-0.1,0.2-0.3,0.3-0.4c0.1-0.1,0.2-0.2,0.4-0.2C2.8,5,3,5,3.1,5h17.4c0.2,0,0.3,0,0.4,0.1c0.1,0.1,0.3,0.1,0.4,0.2C21.5,5.4,21.6,5.6,21.6,5.7z M3.7,16.1H20V6.7H3.7V16.1z M14.2,19c0,0,0-0.1,0-0.1v-0.6h9.7v0.9c0,0.2-0.1,0.4-0.2,0.5c-0.1,0.1-0.3,0.2-0.5,0.2H0.7c-0.2,0-0.4-0.1-0.5-0.2c-0.1-0.1-0.2-0.3-0.2-0.5v-0.9h9.4v0.6c0,0.1,0,0.1,0.1,0.2c0,0,0.1,0,0.1,0.1c0,0,0.1,0,0.1,0h4.2H14l0.1-0.1C14.1,19.1,14.2,19,14.2,19z" />
	</svg>
);

LiveEventWebSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const LocationWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<path fill={tertiary} d="M12,3.2c0.9,0,1.9,0.2,2.7,0.6c0.9,0.4,1.6,0.9,2.3,1.6c0.7,0.7,1.2,1.5,1.5,2.3c0.3,0.9,0.5,1.8,0.5,2.7c0,3-2.1,6.4-6.2,10c-0.2,0.2-0.5,0.3-0.8,0.3c-0.3,0-0.6-0.1-0.8-0.3l-0.3-0.2C7,16.7,5,13.4,5,10.5c0-0.9,0.2-1.9,0.5-2.7S6.4,6.1,7,5.4s1.4-1.2,2.3-1.6C10.1,3.5,11.1,3.3,12,3.2z" />
		<path fill={secondary} d="M13,3.1v5l1.6,2.1L13,12.3v8.4c0,0,0.1,0,0.1,0c4.4-4,6.6-7.5,6.6-10.5c0-0.9-0.2-1.8-0.5-2.7c-0.3-0.8-0.8-1.6-1.5-2.3c-0.6-0.6-1.4-1.2-2.2-1.5c-0.6-0.3-1.3-0.4-1.9-0.5L13,3.1z" />
		<path fill={primary} d="M12,2c1.1,0,2.1,0.2,3.1,0.7c1,0.4,1.9,1,2.6,1.8c0.7,0.8,1.3,1.7,1.7,2.7s0.6,2,0.6,3.1c0,3.5-2.4,7.3-7.1,11.4C12.7,21.9,12.3,22,12,22c-0.3,0-0.7-0.1-0.9-0.4l-0.3-0.3C6.3,17.4,4,13.6,4,10.2c0-1.1,0.2-2.1,0.6-3.1s1-1.9,1.7-2.7s1.6-1.4,2.6-1.8C9.9,2.2,10.9,2,12,2z M12,3.8c-0.8,0-1.7,0.2-2.5,0.5C8.8,4.6,8.1,5.1,7.5,5.7c-1.2,1.2-1.9,2.9-1.9,4.6c0,2.8,2,6,6,9.6l0.3,0.3c0,0,0.1,0.1,0.1,0.1s0.1,0,0.1-0.1c4.2-3.7,6.3-7,6.3-9.9c0-1.7-0.7-3.4-1.9-4.6c-0.6-0.6-1.3-1.1-2.1-1.4c-0.6-0.2-1.2-0.4-1.8-0.5L12,3.8z M12,7.1c0.4,0,0.8,0.1,1.2,0.2c0.4,0.2,0.7,0.4,1,0.7c0.6,0.6,0.9,1.4,0.9,2.2c0,0.8-0.3,1.6-0.9,2.2c-0.3,0.3-0.6,0.5-1,0.7c-0.4,0.2-0.8,0.2-1.2,0.2c-0.4,0-0.8-0.1-1.2-0.2c-0.4-0.2-0.7-0.4-1-0.7c-0.6-0.6-0.9-1.4-0.9-2.2c0-0.8,0.3-1.6,0.9-2.2c0.3-0.3,0.6-0.5,1-0.7C11.2,7.1,11.6,7.1,12,7.1z M12,8.5c-0.2,0-0.4,0-0.6,0.1C11.2,8.8,11,8.9,10.8,9c-0.3,0.3-0.5,0.7-0.5,1.2c0,0.4,0.2,0.9,0.5,1.2c0.1,0.2,0.3,0.3,0.5,0.4c0.2,0.1,0.4,0.1,0.6,0.1c0.2,0,0.4,0,0.6-0.1c0.2-0.1,0.4-0.2,0.5-0.4c0.3-0.3,0.5-0.7,0.5-1.2c0-0.4-0.2-0.9-0.5-1.2c-0.1-0.2-0.3-0.3-0.5-0.4C12.4,8.6,12.2,8.6,12,8.5z" />
	</svg>
);

LocationWebSvg.colorVariation = [
	"threeFillsColorThemeOne",
	"threeFillsColorThemeTwo",
];

export const ImageMissingWebSvg = ({ primary, secondary, tertiary, size }) => (
	<svg xmlns={XMLNS} viewBox={"0 0 24 24"} width={size} height={size}>
		<path fill={primary} d="M19 0C19.5128 0 19.9355 0.38604 19.9933 0.883379L20 1V3.8648L23.2747 4.31425C23.7438 4.37851 23.9571 4.54821 24 5L24.0042 5.11431L23.9949 5.23077L21.6847 23.2738C21.6151 23.7819 21.0006 24.0103 20.5 24L20.3836 23.9908L2.72522 21.6847C2.21696 21.6151 1.85057 21.175 1.8611 20.6742L1.87033 20.5577L1.947 19.9998L1 20C0.526613 20 0.130053 19.6711 0.0264107 19.2293L0.00672773 19.1166L0 19V1C0 0.487164 0.38604 0.0644928 0.883379 0.00672773L1 0H19ZM4.092 18.999L4.09383 19.0245L4.08467 19.1363L3.987 19.8388L19.839 22.0108L22.012 6.1588L19 5.747V17.9995C19 18.5518 18.5523 18.9995 18 18.9995C18 18.9995 18 18.9995 18 18.9995L4.092 18.999ZM18 1.9998H2V17.9998L18 17.999V1.9998ZM17 2.9995V12.9995H3V2.9995H17Z" />
	</svg>
);

