import React from "react";

/**
 * @typedef {Object} SvgIconTheme
 * @property {string} primary
 * @property {string} secondary
 * @property {string} tertiary
 * @property {string} size
 * @property {string} [title]
 */

/**
 * @typedef {Object} ThemeOverrideProps
 * @property {string} [size]
 * @property {string} [colorOneOverride]
 * @property {string} [colorTwoOverride]
 * @property {string} [colorThreeOverride]
 */

// These should be all 24x24 icons from Sketch. With padding.
// Would be helpful to set default overides per icon
// Test transparency
// Some icons are rendered bad in Framer - Why. Maybe illustrator snappin gis off.
// Add rotate prop
// Revist some icons and make white fills an option to align with others?
// Need opacity fill color eqivalent on some products, as can't do opacity. Maybe thi is manual
// Other themes (based on extra colors examples from Audrey)
// Check all white fills
// Add extra icons from differnt sizes
// Icons with animation frames needs work. Lottie?
// Show/hide padding. Instead of doing new designs for 30px...?
// PLACE IN FRAMER CODE componets (merge SvgIcon and SVGIcons)?
// Foundation/Product/Web/Mobile
// Resonsive option prop

// export type Theme = {
// 	primary: string;
// 	secondary: string;
// 	tertiary: string;
// 	svgIconSize: string;
//   };

/** @type {Object.<string, string>} */
export const sizes = {
	// // Review all sizes
	12: "12px",
	// tiny: "14px",
	16: "16px",
	24: "24px",
	30: "30px",
	// 32: "32px",
	40: "40px",
	48: "48px",
	60: "60px",
	// 64: "64px",
	80: "80px",
	100: "100px",
};

/** @type {Object.<string, string>} */
export const colors = {
	disabled: "#D0D0D0",
	transparent: "rgba(0, 0, 0, 0)",
	white: "#FFF",
	black: "#000",
	primary: "#3235FF",
	secondary: "#FF5F42",
	gray: "#F0F0F0",
	darkGray: "#3D3D3D",
	error: "#E31D29",
	success: "#86AB2C",
	//Product
	functionCommercial: "#634368",
	functionOrange: "#F29400",
	//Original colors
	productBlue: "#007AFF", 
	productDarkGrey: "#5F5F5F",
	productLightGrey: "#909FAB",
	productLightGrey2: "#9C9C9C",
	productPurple2: "#BE5999",
	productBlue2: "#596BC4",
	
	
	
	
	// Process colors in Props on Framer is changing
	// This could be gradients auto generated from 2 colors
	process1: "#FCDF01",
	process2: "#FCC401",
	process3: "#FCC402", // Should be #FCC401 but will not let me select if same value - wierd. Changed last value
	process4: "#FCAA01",
	process5: "#FCAA02", // Should be #FCAA01?  but will not let me select if same value - wierd. Changed last value
	process6: "#EB661C",
	process7: "#EB661D", // Should be #EB661C but will not let me select if same value - wierd. Changed last value
	process8: "#E42520",
	process9: "#E42521", // Should be #E42520 but will not let me select if same value - wierd. Changed last value
	process10: "#981737",
	process11: "#981738", // Should be #981737 but will not let me select if same value - wierd. Changed last value

	//Animations & Illustrations
	illustration1: "#F5B4B6",
	illustration2: "#E94B65",
	illustration3: "#C48B3F",
	illustration4: "#854B27",

	// Icon Colour Palette from DU's Food icons_V27.ai file
	icp1: "#DEDEE0",
	icp2: "#E9ECEE",
	icp3: "#B1BCD2",
	icp4: "#5F6275",
	icp5: "#231F20",
	icp6: "#40B1E5",
	icp7: "#C0DA73",
	icp8: "#7FC342",
	icp9: "#2B8B43",
	icp10: "#174A24",
	icp11: "#4B220C",
	icp12: "#854B27",
	icp13: "#EE3325",
	icp14: "#9A1B1F",
	icp15: "#650D0D",
	icp16: "#F5DBB2",
	icp17: "#F5D18E",
	icp18: "#BE8849",
	icp19: "#FCCD16",
	icp20: "#F47C20",
	icp21: "#F8AEAE",
	icp22: "#F0566C",
	icp23: "#931A4D",
	icp24: "#7B2983",
	icp25: "#301432",
	// How about these colors?
	icp26: "#EE3225",
	icp27: "#670A09",
	icp28: "#4D1F03",
	icp29: "#C14321",
	icp30: "#134A1E",
	icp31: "#FFC202",
	icp32: "#80C41C",
	icp33: "#FBAFAE",
	icp34: "#F5D18F",
	icp35: "#BE894A",
	icp36: "#4D1F08",
	icp37: "#804800",
	icp38: "#670000",
	icp39: "#311233",
	icp40: "#DFE4E2",
	icp41: "#905627",
	icp42: "#93014A",
	icp43: "#80C342",
	icp44: "#B0B2BB",
	icp45: "#F77D12",
	icp46: "#DEDDE0",
	icp47: "#FFCC10",
	icp48: "#C0DA72",
	icp49: "#278A2A",
	icp50: "#F57C20",
	icp51: "#FCCC13",
	icp52: "#FD2817",
	icp53: "#0D0D0C",
	icp54: "#FBCF0B",
	icp55: "#E2E6DD",
	icp56: "#F6DCB2",
	icp57: "#FCC10F",
	icp58: "#C14427",
	icp59: "#F9546B",
	icp60: "#5FB423",
	icp61: "#0C3014",
	icp62: "#BAC78C",
	icp63: "#B0BBD1",
	icp64: "#95489F",
	icp65: "#FFCB1F",
	icp66: "#F99D20",
	icp67: "#F2DAB2",
	icp68: "#DA8E5C",
	icp69: "#771213",
	icp70: "#A12B2A",
	icp71: "#7F848E",
	icp72: "#E8EBED",
	icp73: "#006838",
	icp74: "#DB8E5C",
	icp75: "#8DC63F",
	icp76: "#EBE1D1",
	icp77: "#965F36",
	icp78: "#5F6276",
	icp79: "#221F1F",
	icp80: "#9A0000",
	icp81: "#4B435B",
	icp82: "#991B1F",
	icp83: "#3FB0E4",
	icp84: "#979797",
	
		


	//// Web
	// Main colors
	primaryWeb: "#313638",
	backgroundWeb: "#ffffff",
	commercialWeb: "#421540",
	secondaryWeb: "#4C8314",
	noticeWeb: "#d34300",
	errorWeb: "#E61F02",
	// Gray shades
	shade1Web: "#717578",
	shade2Web: "#C0C0C0",
	shade3Web: "#D0D0D0",
	shade4Web: "#E2E2E2",
	shade5Web: "#F2F2F2",
	shade6Web: "#F8F8F8",
	borderBgAAWeb: "#949494",

};

/** @type {Object.<string, SvgIconTheme>} */
export const themes = {
	original: {
		primary: null,
		secondary: null,
		tertiary: null,
		size: sizes[24],
		title: "Original",
	},
	oneFillWhite: {
		primary: colors.white,
		secondary: colors.transparent,
		tertiary: colors.transparent,
		size: sizes[24],
		title: "White",
	},
	// twoFillsWhite: {
	// 	primary: colors.white,
	// 	secondary: colors.white,
	// 	tertiary: colors.transparent,
	// 	size: sizes[24],
	// 	title: "2.twoFillsWhite",
	// },
	oneFillGrey: {
		primary: colors.shade3Web,
		secondary: colors.transparent,
		tertiary: colors.transparent,
		size: sizes[24],
		title: "Grey",
	},
	// twoFillsGrey: {
	// 	primary: colors.shade3Web,
	// 	secondary: colors.shade3Web,
	// 	tertiary: colors.transparent,
	// 	size: sizes[24],
	// 	title: "4.twoFillsGrey",
	// },
	oneFillBlack: {
		primary: colors.black,
		secondary: colors.transparent,
		tertiary: colors.transparent,
		size: sizes[24],
		title: "Black",
	},
	// COULD BRING TWO FILL THEMES BACK AS SOME ICONS USED IT
	// twoFillsBlack: {
	// 	primary: colors.black,
	// 	secondary: colors.black,
	// 	tertiary: colors.transparent,
	// 	size: sizes[24],
	// 	title: "6.twoFillsBlack",
	// },
	oneFillPrimary: {
		primary: colors.primaryWeb,
		secondary: colors.transparent,
		tertiary: colors.transparent,
		size: sizes[24],
		title: "7.oneFillPrimary",
	},
	// twoFillsPrimary: {
	// 	primary: colors.primaryWeb,
	// 	secondary: colors.primaryWeb,
	// 	tertiary: colors.transparent,
	// 	size: sizes[24],
	// 	title: "8.twoFillsPrimary",
	// },
	oneFillPurple: {
		primary: colors.functionCommercial,
		secondary: colors.transparent,
		tertiary: colors.transparent,
		size: sizes[24],
		title: "Purple",
	},
	// twoFillsPurple: {
	// 	primary: colors.functionCommercial,
	// 	secondary: colors.functionCommercial,
	// 	tertiary: colors.transparent,
	// 	size: sizes[24],
	// 	title: "8.twoFillsPurple",
	// },
	threeFillsColorThemeOne: {
		primary: colors.functionCommercial,
		secondary: colors.functionOrange,
		tertiary: "#fff",
		size: sizes[24],
		title: "Purple & orange",
	},
	threeFillsColorThemeTwo: {
		primary: colors.primaryWeb,
		secondary: colors.shade3Web,
		// Made EH icons align with Product icons more. Added White, else many icons will be just 1 color.
		// tertiary: colors.transparent,
		tertiary: "#fff",
		size: sizes[24],
		title: "Dark & light Grey",
	},
	illustrationColorPallete: {
		primary: colors.white,
		secondary: colors.transparent,
		tertiary: colors.transparent,
		size: sizes[24],
		title: "9. Illustration Color Pallete",
	},
};

/**
 * Clone an existing theme.
 * @param {Object|SvgIconTheme} theme 
 * @returns {SvgIconTheme} svgIconTheme
 */
export function copyTheme(theme={}) {
	const copy = {
		primary: theme.primary,
		secondary: theme.secondary,
		tertiary: theme.tertiary,
		size: theme.size,
	};
	return copy;
}

/**
 * Generate props to be used for Icon's svgs
 * @param {string|SvgIconTheme} theme 
 * @param {ThemeOverrideProps} props 
 * @returns {SvgIconTheme|Object}
 */
export function generateThemeProps (theme, props) {
	/** @type {SvgIconTheme} */
	let clonedTheme;

	if (typeof theme === 'string') {
		clonedTheme = copyTheme(themes[theme || 'primary']);
	}
	else {
		clonedTheme = copyTheme(themes['primary']);
	}
	
	if (props.size !== "") {
		clonedTheme.size = props.size;
	}

	if (props.colorOneOverride !== "") {
		clonedTheme.primary = props.colorOneOverride;
	}

	if (props.colorTwoOverride !== "") {
		clonedTheme.secondary = props.colorTwoOverride;
	}

	if (props.colorThreeOverride !== "") {
		clonedTheme.tertiary = props.colorThreeOverride;
	}

	return clonedTheme;
}

export const generateOneFillTheme = ( name = '', color = colors.transparent ) => {
  return {
	name: name ? name : `theme-${color}`,
	theme: {
		primary: color,
		secondary: colors.transparent,
		tertiary: colors.transparent,
		size: sizes[24],
	}
  }
}

export const generateTwoFillsTheme = ( name = '', color = colors.transparent )  => {
  return {
		name: name ? name : `theme-${color}`,
		theme: {
			primary: color,
			secondary: color,
			tertiary: colors.transparent,
			size: sizes[24],
		}
  }
}

export const generateCustomTheme = ( name = 'Custom', ...customColors  ) => {

	const customTheme = {
			name,
			theme: {
				primary: colors.transparent,
				secondary: colors.transparent,
				tertiary: colors.transparent,
				colorList: {},
				size: 24,
			}
	}

	const { theme } = customTheme;

	if ( !customColors ) {
		return customTheme;
	}

	if ( customColors.length > 0) {
		theme.primary = customColors[0] || theme.primary;
		theme.secondary = customColors[1] || theme.secondary;
		theme.tertiary = customColors[2] || theme.tertiary;
		if ( customColors.length > 3) {
		customColors.forEach((color, index) => {
			theme.colorList[`color${index + 1}`] = color || colors.transparent;
		});
		}
	}
	return customTheme;
}

export const DEFAULT_THEME_PACK = [
	"oneFillBlack",
	"oneFillWhite",
	"oneFillPurple",
	"oneFillGrey",
];

export const addedDefaultThemes = themes => {
	if (!themes) {
		return DEFAULT_THEME_PACK;
	}
	return [...themes, ...DEFAULT_THEME_PACK];
}

export default {
	sizes,
	colors,
	themes,
	DEFAULT_THEME_PACK,
	generateThemeProps,
	generateOneFillTheme,
	generateTwoFillsTheme,
	generateCustomTheme,
	addedDefaultThemes,
};
