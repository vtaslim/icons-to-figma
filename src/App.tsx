import * as React from "react";
import { Page, ComponentSet, Component, Svg, Frame } from "react-figma";
import { renderToString } from "react-dom/server";
import { themes, sizes, addedDefaultThemes } from "./svgs/utils";
import { Svgs } from "./svgs";

export const App = () => {
	return (
		<Page isCurrent name="Code generated Icons" style={{ flexDirection: "column" }}>
			{
				Object.entries(Svgs).map(([key, value], index) => {
					const SvgIcons = value;
					const iconNames = Object.keys(SvgIcons),
						// pageSize = 234,
						// page = 2;
						pageSize = 100,
						page = 2;

					const start = (page - 1) * pageSize,
						pageIconNames = iconNames.slice(start, start + pageSize);
					return(
						<Frame 
							key={key} 
							name={key} 
							style={{ 
								flexDirection: "row",
								marginTop: "60rem",
							}}
						>

						{pageIconNames.map((iconName) => {
							const iconFn = SvgIcons[iconName];
							const dirtCompStr: string = renderToString(iconFn(themes.oneFillBlack));
							// Could do this better as this may sizesnot render connectly, like:
							// StartLogSvg is missing rect?
							// 2 color fills are missing
							// Icebath i 	s missign icecubes
							
							const pathMatches = dirtCompStr.match(/(path|rect)\sfill/g);
							const totalPaths = (pathMatches && pathMatches.length) || 0;
							const transformName = iconName.replace(/Svg$/, '');
							// const svgTitle = dirtCompStr.match(/title/);	

							return (
								<ComponentSet key={iconName.toString()} name={iconFn.figmaName||transformName} style={{ marginLeft: "10px" }}>
									{
										iconFn.colorCount=== 0 && (
											<>
												<Component name="Theme=original">
													<Svg source={renderToString(iconFn(themes.original))} />
												</Component>
											</>
										)
									}
									{
										iconFn.colorCount !== 0 && 
										(addedDefaultThemes(iconFn.colorVariation) || []).map((variation, index) => {
											if ( typeof variation === 'string') {
												return (
													<Component key={ `${variation}-${index}`.toString() } name={ `Theme=${themes[variation].title}` }>
														<Svg source={ renderToString( iconFn( themes[variation] ) ) } />
													</Component>	
												)
											}
											return (
												<Component key={ `${variation}-${index}`.toString() } name={ `Theme=${variation.name}` }>
													<Svg source={ renderToString( iconFn( variation.theme  ) ) } />
												</Component> 
											)
										})
									}
								</ComponentSet>
							);
						})}
					</Frame>
					)
				})
			}
		</Page>
	);
};
